% Examine the frequency response of simulated TRF-style filters
eFs = 128;
tmin = 0;
tmax = 250;
dur = 20; % duration of the full, untruncated TRF, in s

allt = (0:dur*eFs-1)/eFs;

x = sinc(2*allt);
x(allt>tmax/1000) = 0; % truncate to the TRF duration by setting everything else to zero

% First plot the impulse response
figure
plot(allt,x,'k','LineWidth',2);
set(gca,'FontSize',16);
xlabel('Delay (ms)');
ylabel('Weight');

[Px,frq] = pwelch(x,dur*eFs,[],[],eFs);
figure
plot(frq,Px,'k','LineWidth',2);
set(gca,'XScale','log','YScale','log');
xlabel('Frequency (Hz)');
ylabel('Power spectral density');