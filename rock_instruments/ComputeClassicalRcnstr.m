function ComputeClassicalRcnstr(sbj,wnd)
% Compute a PCA/spline reconstructions of the classical music envelopes
% (similar to the analysis for ComputeDrumsRcnstr)

stimcodes = 20:29; % specify the particular trial using the stimulus code
tmin = 0;
tmax = wnd-tmin;
map = -1;
desFs = 512;
mdlFs = 1000/(tmax-tmin)*16; % 3x highest frequency (32 Hz splines was optimal for 
    % 500 ms window to reconstruct dB envelope for Natural Speech)
tlims = 16;
npcs = 64; % 64 principal components was optimized on the Natural Speech dataset
    % using a 500 ms window to reconstruct dB envelope
do_mov_avg = true;
% do_mov_avg = false;

% determine the stimulus type
stimtype = 'classical';

% load the subject results for this stimulus type
res_pth = 'rcnstr_tbt/';
res_fl = sprintf('%s_%s_%dms',sbj,stimtype,tmax);
res = load([res_pth res_fl]);
% get cf, then remove res to reduce RAM
cf = res.cf;
clear res

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);
if isempty(eeg)
    error('Stimulus code does not correspond to one of the trials for this subject');
end

% Load the stimuli
disp('Loading stimuli...');
stimpth = 'stims/';
stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtype);
    %%% I will need to change this to be 512 Hz envelopes, no downsampling
s = load([stimpth stimfl]);
stims = s.stimset;
% stims = stims(stim_idx);
% identify the indexes within the set of all 10 stimuli that correspond to
% EEG trials
stim_used = find(cellfun(@(x) ~isempty(x),eeg));
% remove empty trials
eeg = eeg(stim_used);
stims = stims(stim_used);
% flip each of the stimulus arrays into columns
for ii = 1:length(stims), stims{ii} = stims{ii}'; end

% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
    if eFs~=desFs
        eeg{ii} = resample(eeg{ii},desFs,eFs);
    end
end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims)
    %%% NZ (19-1-2020), don't skip silences, could have had strange effects
    %%% on null distribution, overenflated tracking at low frequencies
    tidx{ii} = ones(length(stims{ii}),1);
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(st));
    eeg{ii} = eeg{ii}-avg;
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        % NZ (15-1-2020), don't remove silences when doing moving average
        s_on = stims{ii};
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii} = s_on;
    end
end
clear avg s_on

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,eeg);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(eeg{n},1)); end
    end
end

% Compute the downsampling rate, for the spline transform
ds = desFs/mdlFs;

pred = cell(length(eeg),1);
r = NaN(length(eeg),1);
for n = 1:length(eeg)
    fprintf('** Trial %d/%d **\n',n,length(eeg));
    drumstm = tic;

    disp('Model training...');
    traintrs = setxor(1:length(stims),n);
    pc = cell(length(traintrs),1);
    for ii = 1:length(traintrs)
    %     eeg{ii} = detrend(eeg{ii},0); % center each channel
        pc{ii} = eeg{traintrs(ii)}*cf{n}(:,1:npcs); % transform to pcs
        pc{ii} = zscore(pc{ii}); % normalize to variance of 1
    end

    disp('(Using spline interpolation...)');
    model = mTRFtrain_spline(stims(traintrs),pc,desFs,ds,map,tmin,tmax,0,tidx(traintrs));

    disp('Computing the envelope reconstruction...');
        % transform the testing trial into pc
    testpc = zscore(eeg{n}*cf{n}(:,1:npcs));
    [pred{n},r(n)] = mTRFpredict_spline(stims{n},testpc,model,desFs,ds,map,tmin,tmax,tidx(n));
    
    % display time elapsed
    fprintf('** Completed trial %d @ %.3f s\n',n,toc(drumstm));
end

svpth = 'classical_rcnstrs/';
svfl = sprintf('ClassicalRcnstr_%s_%dms',sbj,round(tmax-tmin));
save([svpth svfl],'pred','r','stim_used');