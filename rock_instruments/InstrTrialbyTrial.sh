#!/bin/bash
#SBATCH --time=3-00:00:00
#SBATCH -c 8
#SBATCH -n 1
#SBATCH --mem=64GB

sbj=$1
instr=$2
wnd=(31.25 62.5 125 250 500 1000 2000 4000 8000 16000)

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(8); sbj='${sbj}'; instrtype='${instr}'; wnd=${wnd[$SLURM_ARRAY_TASK_ID]}; InstrRcnstr_trialbytrial(sbj,instrtype,wnd);"
rm -rf /scratch/$USER/$SLURM_JOB_ID
