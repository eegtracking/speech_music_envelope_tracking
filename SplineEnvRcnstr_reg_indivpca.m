function SplineEnvRcnstr_reg_indivpca(sbj,stimtype,window_size,varargin)
% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes
% This cannot be used for window_size = 125 ms (because it cant be spline
% transformed) or 16000 ms (because the moving average shouldn't be
% removed)
% NZ (3-1-2020) -- do PCA on individual data for window size / stimulus
% type, instead of using the general PCs

tmin = 0;
tmax = window_size+tmin;
map = -1;
% npcs = 16;
desFs = 128;
mdlFs = 1000/(tmax-tmin)*16; % 2x highest frequency of interest in the model
tlims = 2;
nfolds = 10;
niter = 10;
voltlimdb = -55; % NZ (22-3-2019) changed to -55 dB, specified where silences occur
    % see also HistLevel_NeuroMusic.m
sil_tol = 0.001; % the sound is identified as "on" when the silence vector equals 1,
    % this is the tolerance around 1 at which to define the sound as "on"
do_mov_avg = true;
lambdas = [0 10.^(0:8)];

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
stimpth = '~/NeuroMusic/stims/';
stimfl = sprintf('NeuroMusicdBEnvelopes_128Hz_%s',stimtype);
    %%% NZ -- change to 512 Hz envelopes
s = load([stimpth stimfl]);
stims = s.stimset;
% Transpose all stimuli to column vectors
for ii = 1:length(stims), stims{ii} = stims{ii}'; end
% clear s

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

disp('Convert stimuli to dB');
silence = cell(length(stims),1); % to store silent periods
for ii = 1:length(stims),
    % store 0 anytime there's silence
    silence{ii} = ones(length(stims{ii}),1);
    silence{ii}(stims{ii}<voltlimdb) = 0;
    stims{ii}(stims{ii}<voltlimdb) = voltlimdb;
end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
disp('...and removing silences...');
tidx = cell(length(silence),1);
for ii = 1:length(silence),
    tidx{ii} = ones(length(silence{ii}),1);
    l = usetinds(tlims,desFs,length(silence{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
et = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(et));
    eeg{ii} = eeg{ii}-avg;
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        s_on = stims{ii};
        s_on(~tidx{ii}) = mean(s_on(tidx{ii})); % set silent periods to the average of
            % the non-silent envelope, to avoid edge effects as speech
            % turns on and off
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii}(tidx{ii}) = s_on(tidx{ii});
%         savg = movmean(stims{ii},length(t));
%         stims{ii} = stims{ii}-savg;
    end
end
clear avg s_on

% Downsample the EEG data
disp('Downsampling the EEG data...');
for ii = 1:length(eeg)
    eeg{ii} = resample(eeg{ii},desFs,eFs);
end

% Transform EEG into PCs
disp('Transform to PCs...');
allEEG = cell2mat(eeg);
cf = pca(allEEG); % get the principal components of the EEG
pc = cell(length(eeg),1);
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii},0); % center each channel
    pc{ii} = eeg{ii}*cf; % transform to pcs
    pc{ii} = zscore(pc{ii}); % normalize to variance of 1
end
clear allEEG eeg

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,pc);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(pc{n},1)); end
    end
end

% Compute the downsampling rate, for the spline transform
ds = desFs/mdlFs;

disp('Computing the envelope reconstruction...');
if mdlFs<desFs,
    disp('(Using spline interpolation...)');
    [r,~,mse,model] = mTRFcrossval_spline(stims,pc,desFs,ds,map,tmin,tmax,lambdas,tidx,...
        'nfolds',nfolds);
elseif mdlFs==desFs,
    [r,~,mse,model] = mTRFcrossval(stims,pc,desFs,map,tmin,tmax,lambdas,tidx,...
        'nfolds',nfolds);
else
    error('Model sampling frequency must be equal to or less than initial sampling frequency');
end

disp('Computing null distribution...');
% optlmb = lambdas(mean(r)==min(mean(r)));
[nullr,nullmse] = mTRFnull(stims,pc,desFs,ds,map,tmin,tmax,0,tidx,...
      'niter',niter,'randomize_idx',true,'size_for_testing',nfolds);
% no regularization tends to perform best

if do_mov_avg, 
    svpth = sprintf('~/NeuroMusic/reg_effects_indivpca/%dms/',round(tmax-tmin));
else
    svpth = sprintf('~/NeuroMusic/reg_effects_indivpca/%dms_noavg/',round(tmax-tmin));
end
svfl = sprintf('%s_%s_reg',sbj,stimtype);
save([svpth svfl],'r','mse','model','mdlFs','desFs','ds','tmin','tmax','tlims',...
     'nfolds','voltlimdb','tidx','lambdas','nullr','nullmse','cf');
