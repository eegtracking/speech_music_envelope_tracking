#!/bin/bash
#SBATCH --time=02:00:00
#SBATCH -c 4
#SBATCH -n 1
#SBATCH --mem=16GB

sbj=$1
stim=$2
wnd=(500 1000 2000)

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(4); sbj='${sbj}'; stimtype='${stim}'; wnd=${wnd[$SLURM_ARRAY_TASK_ID]}; SpatialEnvRcnstr_delays(sbj,stimtype,wnd);"
rm -rf /scratch/$USER/$SLURM_JOB_ID
