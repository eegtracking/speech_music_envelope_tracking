% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes

addpath('~/Projects/EEGanly/PCA_spline_modeling/');
addpath('~/Documents/MATLAB/ellipsoid_fit/');
addpath('~/Projects/mTRF_Zukedits/');

sbj = 'HC';
stimtype = 'speech';
voltlimdb = -80;

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
if strcmp(stimtype,'rock'), stimcolumn = 1;
elseif strcmp(stimtype,'classical'), stimcolumn = 2;
elseif strcmp(stimtype,'vocals'), stimcolumn = 3;
elseif strcmp(stimtype,'speech'), stimcolumn = 4;
else
    error('Unknown stimulus tag');
end
stimpth = '/Volumes/Untitled/NeuroMusic/';
stimfl = 'NeuroMusicEnvelopes_raw';
load([stimpth stimfl]);
stims = stimset(:,stimcolumn);

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

disp('Convert stimuli to dB');
voltlim = 10^(voltlimdb/20);
for ii = 1:length(stims),
    stims{ii}(stims{ii}<voltlim) = voltlim;
    stims{ii} = 20*log10(stims{ii});
end

% % Transform EEG into PCs
% disp('Transform to PCs...');
% pc = cell(length(eeg),1);
% for ii = 1:length(eeg),
%     pc{ii} = eeg{ii}*cf(:,1:npcs+1);
%     pc{ii} = zscore(pc{ii}); % zscore the pcs
% end
% clear eeg

% Downsample to EEG and stimulus by 4x, to make it more managable for the
% computer
desFs = 128;
for ii = 1:length(eeg),
    eeg{ii} = resample(eeg{ii},desFs,eFs);
    stims{ii} = resample(stims{ii},desFs,eFs);
end

% Compute the coherence between the stimulus and EEG
disp('Computing coherence...');
nfft = desFs*5; % number of sample points in the fft
window = 10*desFs; % size of the window within which to compute the spectra
nchan = size(eeg{1},2);
cxy = NaN(nfft/2+1,nchan,length(eeg));
for ii = 1:length(eeg),
    minlen = min([length(stims{ii}) size(eeg{ii},1)]);
    s = stims{ii}(1:minlen);
    p = eeg{ii}(1:minlen,:);
    [cxy(:,:,ii),frq] = mscohere(s,p,hann(window),window/2,nfft,desFs);
end

% Plot the coherence
CXY = mean(cxy,3);
frq_range = [0.5 16]; % frequency range to plot
frq_idx = frq>=frq_range(1)&frq<=frq_range(2);
figure
plot(frq(frq_idx),mean(CXY(frq_idx,:),2),'k'); % average across electrodes
xlabel('Frequency (Hz)');
ylabel('Coherence');