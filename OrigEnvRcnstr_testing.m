function OrigEnvRcnstr_testing(sbj,stimtype,window_size,varargin)
%%% NZ (9-1-2020) -- detrend EEG before doing movmean, in order to avoid
%%% edge artifacts (especially an issue for the "w/ silences" version where
%%% a large movmean window is used.
%%% NZ (15-1-2020) -- There may be an issue with skipping silences for getting
%%% the null distribution, since it introduces discontinuities that change 
%%% the relative phases across frequencies, which could explain the high d-primes
%%% at very low frequencies. I've removed that here

tmin = 0;
tmax = window_size+tmin;
map = -1;
desFs = 128;
tlims = 16;
nfolds = 10;
voltlimdb = -55; % NZ (22-3-2019) changed to -55 dB, specified where silences occur
    % see also HistLevel_NeuroMusic.m
do_mov_avg = true;
lambdas = 10.^(0:8);

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
stimpth = '~/NeuroMusic/stims/';
stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtype);
s = load([stimpth stimfl]);
stims = s.stimset;
% Transpose all stimuli to column vectors
for ii = 1:length(stims), stims{ii} = stims{ii}'; end
% clear s

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
    eeg{ii} = resample(eeg{ii},desFs,eFs);
end

% disp('Convert stimuli to dB');
% silence = cell(length(stims),1); % to store silent periods
% for ii = 1:length(stims),
%     % store 0 anytime there's silence
%     silence{ii} = ones(length(stims{ii}),1);
%     silence{ii}(stims{ii}<voltlimdb) = 0;
%     stims{ii}(stims{ii}<voltlimdb) = voltlimdb;
% end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
%if do_mov_avg, disp('...and removing silences...'); end
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims),
%%% NZ (15-1-2020) -- don't remove silences
%     if do_mov_avg, 
%        tidx{ii} = silence{ii}; %%% NZ (3-1-2020), silences weren't being removed here before
%     else
%        tidx{ii} = ones(length(silence{ii}),1);
%     end
    tidx{ii} = ones(length(stims{ii}),1);
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
% et = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    if do_mov_avg,
        avg = movmean(eeg{ii},length(st));
        eeg{ii} = eeg{ii}-avg;
    else % use moving average of 10 s window (corresponding to highpass filtering at 0.1 Hz)
        avg = movmean(eeg{ii},desFs*10);
        eeg{ii} = eeg{ii}-avg;
    end
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        s_on = stims{ii};
%         s_on(~tidx{ii}) = mean(s_on(tidx{ii})); % set silent periods to the average of
            % the non-silent envelope, to avoid edge effects as speech
            % turns on and off
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
%         stims{ii}(tidx{ii}) = s_on(tidx{ii});
        stims{ii} = s_on;
%         savg = movmean(stims{ii},length(t));
%         stims{ii} = stims{ii}-savg;
    end
end
clear avg s_on

% Z-score each EEG channel
for ii = 1:length(eeg),
%     eeg{ii} = resample(eeg{ii},desFs,eFs);
%     eeg{ii} = detrend(eeg{ii}); % remove linear trends in the EEG
    eeg{ii} = zscore(eeg{ii}); % zscore after resampling, because this can change the variance
end

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,eeg);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(eeg{n},1)); end
    end
end

r_cv = cell(length(stims),1);
mse_cv = cell(length(stims),1);
model = cell(length(stims),1);
r_test = NaN(length(stims),1);
mse_test = NaN(length(stims),1);
optidx = NaN(length(stims),1);
for n = 1:length(stims)
    fprintf('** Testing with trial %d **\n',n);
    tst_tm = tic; % keep track of total training and testing time for this trial
    traintrs = setxor(1:length(stims),n);
    disp('Computing the envelope reconstruction...');
    [r_cv{n},~,mse_cv{n},model{n}] = mTRFcrossval(stims(traintrs),eeg(traintrs),desFs,...
        map,tmin,tmax,lambdas,tidx(traintrs),'nfolds',nfolds);
    disp('-- Testing --');
    % Use the model with the best CV accuracy
    optidx(n) = find(mean(r_cv{n})==max(mean(r_cv{n})),1,'first');  
    use_model = model{n}(:,optidx(n));
    % test the model
    [~,r_test(n),~,mse_test(n)] = mTRFpredict(stims{n},eeg{n},use_model,desFs,map,tmin,tmax,tidx(n));
    fprintf('-- Completed training and testing for trial %d @ %.3f s\n',n,toc(tst_tm));
end

if do_mov_avg, 
    svpth = sprintf('~/NeuroMusic/orig_rcnstr_res/%dms/',round(tmax-tmin));
else
    svpth = sprintf('~/NeuroMusic/orig_rcnstr_res/%dms_noavg/',round(tmax-tmin));
end
svfl = sprintf('%s_%s_db',sbj,stimtype);
save([svpth svfl],'model','desFs','tmin','tmax','tlims','voltlimdb','tidx',...
    'nfolds','lambdas','r_cv','mse_cv','r_test','mse_test','optidx');
