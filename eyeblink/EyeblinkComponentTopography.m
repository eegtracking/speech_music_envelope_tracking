% Plot the average topography for the eyeblink component for all subjects

addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));
addpath('../analysis/');

% 'MB' was not included in eyeblink reconstruction
sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MT','SL','SOS'};

eyeblink_path = '/Volumes/Untitled/NeuroMusic/eyeblink/';

% Matrix to store the activation weights of the eyeblink components for
% each subject
A = NaN(128,length(sbjs));

% Save the activation weights for each subject
for s = 1:length(sbjs)
    eyeblink_fl = sprintf('%s_eyeblink',sbjs{s});
    d = load([eyeblink_path eyeblink_fl]);
    A(:,s) = d.A(:,d.eyeblink_cmp);
    % if the topography is skewed negative because of the eyeblink weights, make it positive
    if skewness(A(:,s))<0, A(:,s) = -A(:,s); end
    disp(eyeblink_fl);
end

% Plot the average
figure
topoplot(mean(A,2),'chanlocs.xyz');
set(gca,'FontSize',14);
colorbar;

% Plot all topographies (to make sure their aligned)
figure
for s = 1:length(sbjs)
    subplot(ceil(length(sbjs)/4),4,s);
    topoplot(A(:,s),'chanlocs.xyz');
    colorbar;
    title(sbjs{s});
end