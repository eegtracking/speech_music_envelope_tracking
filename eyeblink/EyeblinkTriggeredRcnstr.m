function EyeblinkTriggeredRcnstr(sbj,stimtype,window_size,varargin)
% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes
% This cannot be used for window_size = 125 ms (because it cant be spline
% transformed) or 16000 ms (because the moving average shouldn't be
% removed)
%%% NZ - PCA-transform each dataset individually, aka. for each particular
%%% window size and stimulus type.  This would be optimal.
%%% NZ (28-1-2020) -- Andy mentioned that pca should be applied to only the
%%% training data, and then the testing data can be transformed with the
%%% eigenvectors from the training set.  The testing data should be
%%% COMPLETELY separate from all transformation applied to get the model on
%%% the trainin data.
%%% NZ (28-1-2020) -- I expect all of the results I have seen so far will
%%% persist doing trial-by-trial testing, rather than a random sampling of
%%% the data across trials. While the performance for music may still be
%%% smaller, the methods will be easier to explain.
%%% NZ (9-2-2020) -- I ran the PCA/spline optimization on the Natural
%%% Speech dataset, and found that the optimum r values for a 500 ms window
%%% for dB envelope reconstruction were at 32 Hz splines (a 3-octave
%%% window) and 64 principal components. I have chaged the parameters used
%%% here accordingly
%%% NZ (4-4-2021) -- This new function runs the envelope reconstruction
%%% using the eyeblink component extracted for each subject with ICA run on
%%% all trials (except stim code 85, see ExtractEyeblinkComponent.m and
%%% SaveEyeblinkComponent.m). The rest of the code is copied from
%%% SplineEnvRcnstr_trialbytrial.m. Because I'm using a single component
%%% with eyeblinks, there is no need for an initial PCA step.
%%% NZ (12-6-2021) -- Instead of using the eyeblink independent component,
%%% the eyeblinks will be identified and only the time points of the peaks
%%% will be retained. The resulting "EEG" input is an onset vector
%%% containing the peak times of eyeblinks.

% addpath('~/mTRF_Zukedits/');
addpath('..');

tmin = 0;
tmax = window_size+tmin;
map = -1;
desFs = 512;
mdlFs = 1000/(tmax-tmin)*16; % 3x highest frequency (32 Hz splines was optimal for 
    % 500 ms window to reconstruct dB envelope for Natural Speech)
tlims = 16;
niter = 50;
% npcs = 64; % 64 principal components was optimized on the Natural Speech dataset
%     % using a 500 ms window to reconstruct dB envelope
do_mov_avg = true;
eyeblink_qntl = 0.99; % eyeblinks are identified if their voltages are above this quantile

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/Volumes/Untitled/NeuroMusic/eyeblink/';
eyeblinks = load([eegpth sbj '_eyeblink']);
% identify trials corresponding to the stimulus codes for this stimtype
[~,eyeblink_tr_idx,useidx] = intersect(eyeblinks.used_stim,stimcodes);
    % note: because I'm not using loadneuromusiceeg, I need to get 'useidx'
    % this way instead of looking for empty trials
eeg = eyeblinks.eyeblink(eyeblink_tr_idx);
eFs = eyeblinks.eFs; % sampling rate of the eeg
clear eyeblinks

% Load the stimuli
disp('Loading stimuli...');
stimpth = '/Volumes/ZStore/NeuroMusic/';
if strcmp(stimtype,'ALL') % if ALL stimtypes are specified, load all stimulus types
    use_types = {'rock','classical','vocals','speech'};
    stims = cell(length(stimcodes),1);
    for ii = 1:length(use_types)
        fprintf('-- %s --\n',use_types{ii});
        type_idx = (ii-1)*10+(1:10); % indexes in the stimulus cell array for this stimulus type
        stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,use_types{ii});
        %%% I will need to change this to be 512 Hz envelopes, no downsampling
        s = load([stimpth stimfl]);
        stims_one_type = s.stimset;
        % Transpose all stimuli to column vectors
        for jj = 1:length(stims_one_type), stims{type_idx(jj)} = stims_one_type{jj}'; end
    end
else
    stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtype);
        %%% I will need to change this to be 512 Hz envelopes, no downsampling
    s = load([stimpth stimfl]);
    stims = s.stimset;
    % Transpose all stimuli to column vectors
    for ii = 1:length(stims), stims{ii} = stims{ii}'; end
    % clear s
end

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
% useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
% eeg = eeg(useidx);

% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
    if eFs~=desFs
        eeg{ii} = resample(eeg{ii},desFs,eFs);
    end
end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims)
    %%% NZ (19-1-2020), don't skip silences, could have had strange effects
    %%% on null distribution, overenflated tracking at low frequencies
    tidx{ii} = ones(length(stims{ii}),1);
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(st));
%     eeg{ii} = eeg{ii}-avg;
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        % NZ (15-1-2020), don't remove silences when doing moving average
        s_on = stims{ii};
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii} = s_on;
    end
end
clear avg s_on

% Filter the EEG at 1 Hz (makes the eyeblinks easier to detect)
for ii = 1:length(eeg)
    avg = movmean(eeg{ii},desFs);
    eeg{ii} = eeg{ii}-avg;
end

% Get the eyeblink onset times
disp('Getting eyeblink times...');
EEG = cell2mat(eeg);
% flip the EEG signal if the eyeblinks are negative (negative skewness)
if skewness(EEG)<0
    flip_eeg = 1;
    EEG = -EEG;
else
    flip_eeg = 0;
end
% identify threshold based on >0.99 percentile of voltage values
eyeblink_thres = quantile(EEG,eyeblink_qntl);
%%% check here if it's the appropriate threshold:
%%% figure; hold on; plot(EEG,'k'); plot([0 length(EEG)],[eyeblink_thres eyeblink_thres],'r--');
clear EEG
% get the eyeblink times
eyeblink_times = cell(length(eeg),1);
for ii = 1:length(eeg)
    eyeblink_times{ii} = zeros(size(eeg{ii},1),1);
    % flip the eeg if the eyeblinks are negative
    if flip_eeg, eeg{ii} = -eeg{ii}; end
    [~,idx] = get_triggered_eyeblinks(eeg{ii},eyeblink_thres,desFs);
    % the center of the index array is the peak eyeblink time
    cnt = ceil(size(idx,1)/2);
    pkidx = NaN(size(idx,2),1);
    for n = 1:size(idx,2) % for each eyeblink
        pkidx(n) = idx(cnt,n);
        % if the center value is NaN, use the last non-NaN index
        if isnan(pkidx(n))
            pkidx(n) = idx(find(~isnan(idx(:,n)),1,'last'),n); 
        end
    end
%     eyeblink_times{ii}(idx(cnt,:)) = 1;
    try
        eyeblink_times{ii}(pkidx) = 1;
    catch err
        keyboard;
    end
end
% save the eyeblink times as the new eeg
eeg = eyeblink_times;
clear eyeblink_times

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,eeg);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check)
        if len_check(n), stims{n} = stims{n}(1:size(eeg{n},1)); end
    end
end

% Compute the downsampling rate, for the spline transform
ds = desFs/mdlFs;

disp('Computing the envelope reconstruction from random samplings of data across trials...');
r = NaN(length(stims),1);
mse = NaN(length(stims),1);
for n = 1:length(stims)
    fprintf('** Trial %d/%d **\n',n,length(stims));
    trtm = tic;
    traintrs = setxor(1:length(stims),n); % get the testing trials
    
    disp('(Using spline interpolation...)');
    model = mTRFtrain_spline(stims(traintrs),eeg(traintrs),desFs,ds,map,tmin,tmax,0,tidx(traintrs));
    
    [~,r(n),~,mse(n)] = mTRFpredict_spline(stims{n},eeg{n},model,desFs,ds,map,tmin,tmax,tidx(n));
    
    fprintf('** Completed trial %d/%d @ %.3f s\n',n,length(stims),toc(trtm));
end

[nullr,nullmse] = mTRFnull(stims,eeg,desFs,ds,map,tmin,tmax,0,tidx,'niter',niter,...
     'randomize_idx',false);

svpth = 'eyeblink_triggered_res/';
svfl = sprintf('%s_%s_%dms_eye',sbj,stimtype,round(tmax-tmin));
save([svpth svfl],'r','mse','mdlFs','desFs','ds','tmin','tmax','tlims',...
    'nullr','nullmse','eyeblink_qntl','eyeblink_thres');
