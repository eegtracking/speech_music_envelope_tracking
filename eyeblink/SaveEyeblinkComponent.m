%%% Save the eyeblink component after ICA analysis
%%% You must run ExtractEyeblinkComponent.m first to run ICA and identify
%%% which component corresponds to eyeblinks. Then change the variable
%%% 'eyeblink_cmp' accordingly.

eyeblink_cmp = 1; % index for the ICA component that corresponds to eyeblinks

eyeblink = cell(length(eeg),1);
nidx = [0; cellfun(@(x) size(x,1),eeg)]; % number of indexes for each trial
for n = 1:length(eyeblink)
    % get the indexes of the eyeblink signal that correspond to each trial
    idx_tr = (1:nidx(n+1))+sum(nidx(1:n));
    % save the eyeblink component for this trial
    eyeblink{n} = icasig(eyeblink_cmp,idx_tr)';
end

% Save
sv_path = '/scratch/nzuk/NeuroMusic/eyeblink/';
sv_fl = sprintf('%s_eyeblink',sbj);
save([sv_path sv_fl],'eyeblink','used_stim','eFs','eyeblink_cmp','A');