%%% Load the blocks of trials for a subject, and run fastICA to isolate the
%%% eyeblink component. Save the eyeblink component.
% Nate Zuk (2021)

clear all; % make sure no variables are retained from the last time this was run

addpath('..');
addpath('~/FastICA_25/');
addpath('~/fieldtrip-20200607/');

sbj = 'SOS';
eeg_path = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';

%%% Load the eeg data from all blocks
stimcodes = 10:49; % set of all stimulus codes
[eeg,eFs] = loadneuromusiceeg(eeg_path,stimcodes,sbj);

%%% Leave out empty trials, but identify which stim were presented
skipped_trs = cellfun(@(x) isempty(x),eeg); % flag skipped trials
used_stim = stimcodes(~skipped_trs);
eeg = eeg(~skipped_trs); % remove skipped trials

%%% Highpass filter the EEG
avg_wnd = 16; % window size for the moving average, in s.
    % the moving average is removed to highpass-filter the EEG (as in
    % SplineEnvRcntr_trialbytrial.m and related functions)
fprintf('Removing the moving average with a %.1f s window...\n',avg_wnd);
for n = 1:length(eeg)
    % zero-center the eeg
    eeg{n} = detrend(eeg{n},0);
    avg = movmean(eeg{n},eFs*avg_wnd);
    eeg{n} = eeg{n}-avg;
end

% Run ICA
[icasig,A,eeg_center] = icaexamineeeg(eeg,eFs);