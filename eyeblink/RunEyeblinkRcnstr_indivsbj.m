% Run all EyeblinkTriggeredRcnstr.m for a single subject
sbj = 'EB';
wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000];
stimtype = {'rock','classical','vocals','speech'};
eyeblink_qntl = 0.99;

for s = 1:length(stimtype)
    for w = 1:length(wnd)
        fprintf('***** EyeblinkTriggeredRcnstr, %s, %s, %.2f ms *****\n',...
            sbj,stimtype{s},wnd(w));
        EyeblinkTriggeredRcnstr(sbj,stimtype{s},wnd(w),...
            'eyeblink_qntl',eyeblink_qntl);
    end
end