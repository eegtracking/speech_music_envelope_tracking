% Test dot_median_plot
nlbls = 6; % number of labels (defines position on x-axis)
nreps = 10; % number of repeats for each labelxcolumn
nconds = 3; % number of conditions (defines position relative to label on x-axis)

% The data: y-values have a mean of 1:4 equal to the value of the label,
% and a value of 0:-1:-2 for each condition, with a variance of 0.2 across 
% repetitions
yvals = randn(nreps,nlbls,nconds)*0.2;
for ii = 1:nlbls,
    for jj = 1:nconds,
        yvals(:,ii,jj) = yvals(:,ii,jj)+ii-(jj-1);
    end
end
lbls = ones(nreps,1)*[1:nlbls];

% Reshape the data so that reps x labels are in one column
YVALS = reshape(yvals,[nreps*nlbls nconds]);
LBLS = reshape(lbls,[nreps*nlbls 1]);

median_handles = dot_median_plot(LBLS,YVALS);
xlabel('Label');
ylabel('Y-value');
% Make the legend
leg = cell(nconds,1);
for n = 1:nconds,
    leg{n} = sprintf('Cond %d',n);
end
legend(median_handles,leg);