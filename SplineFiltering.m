% Demonstrate how the spectrum of some signal (random noise, for example)
% changes after the moving average is removed and spline interpolation
addpath('~/Projects/mTRF-toolbox/');
% dur = 180; % duration of the signal (s)
dur = 32; % duration of the signal (s);
eFs = 512;
desFs = 512;
s = randn(dur*eFs,1); % take a speech example
% s = [1; zeros(dur*eFs-1,1)];
nscale = 16; % # of splines in the TRF window
% nscale = 8; % NZ (11-1-2020), optimal is 8x 1/window_size

% Compute the power spectrum of each step
[P,frq_e] = pwelch(s,5*desFs,[],[],desFs);

% win_sizes = [31.25 62.5 125 250 500 1000 2000 4000 8000 16000];
% win_sizes = [31.25 62.5 125 250 500];
% win_sizes = 32000*2.^(0:-1:-2);
win_sizes = 500;
Pflt = NaN(length(P),1);
for ii = 1:length(win_sizes),
    fprintf('Window = %.2f ms\n',win_sizes(ii));
    % size of mTRF window
    tmin = 0;
    tmax = win_sizes(ii);
    % remove moving average
    t = -ceil(tmax/1000*desFs):-floor(tmin/1000*desFs);
    if desFs<eFs,
        s = resample(s,desFs,eFs);
    end
    smv = s - movmean(s,length(t));
    
    % get the lagGen matrix of noise
%     delays = -ceil(win_sizes(ii)/1000*desFs):0;
    SMV = lagGen(smv,t);

    % spline interpolation
    mdlFs = 1000/(tmax-tmin)*nscale;
    ds = desFs/mdlFs;
    if ds>1
        [spline_matrix,SSPL] = spline_transform(SMV,ds);
    else
        spline_matrix = eye(length(t));
%         SSPL = SMV;
    end
%     sflt = sspl*spline_matrix';
%     sflt = SSPL(:,end);

    % transform a delta function (zero lag) using the spline matrix
    % then compute dot product with the spline-transformed design matrix
%     dlt = [zeros(1,length(t)-1) 1];
    dlt = zeros(1,length(t)); dlt(ceil(length(t)/2)) = 1;
    spdlt = dlt*spline_matrix*(spline_matrix'*spline_matrix)^(-1);
%     sflt = SSPL*spdlt';
    sflt = SMV*(spdlt*spline_matrix')';
    
    clear SSPL

    % [Pmv,frq_mv] = pwelch(smv,10*desFs,[],[],desFs);
    [Pflt(:,ii),~] = pwelch(sflt,5*desFs,[],[],desFs);
end

figure
hold on
plot(frq_e,10*log10(P),'k');
% plot(frq_mv,Pmv,'b');
cmap = colormap('jet');
for ii = 1:length(win_sizes),
    clr_idx = round((ii-1)/length(win_sizes)*63)+1;
    plot(frq_e,10*log10(Pflt(:,ii)),'Color',cmap(clr_idx,:),'LineWidth',2);
end
% set(gca,'XScale','log','YScale','log','FontSize',16);
set(gca,'XScale','log','FontSize',16);
xlabel('Frequency (Hz)');
ylabel('Power spectral density (dB)');
% legend('Original','Removed movmean','Spline filtered');

figure
hold on
plot([frq_e(2) frq_e(end)],[0 0],'k--');
for ii = 1:length(win_sizes),
    clr_idx = round((ii-1)/length(win_sizes)*63)+1;
%     plot(frq_e,20*log10(Pflt(:,ii))-20*log10(P),'Color',cmap(clr_idx,:),'LineWidth',2);
    plot(frq_e,10*log10(Pflt(:,ii))-10*log10(P),'Color',cmap(clr_idx,:),'LineWidth',2);
end
set(gca,'XScale','log','FontSize',16);
xlabel('Frequency (Hz)');
ylabel('Power spectral density of filter');

% fprintf('Geometric mean: %.3f\n',geomean([mdlFs/2 1000/(tmax-tmin)]));