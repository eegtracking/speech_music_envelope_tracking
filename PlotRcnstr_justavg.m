% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbj = 'EJ';
stimtype = {'rock','classical','vocals','speech'};
clr = {'r','g','c','b'};
wnd = [62.5 125 250 500 1000 2000 4000];
nfolds = 10;
niter = 50;
respth = '~/Projects/NeuroMusic/rcnstr_res/';

% Load the no-averaging results
r = NaN(nfolds,length(wnd),length(stimtype));
nr = NaN(niter,length(wnd),length(stimtype));
zr = NaN(nfolds,length(wnd),length(stimtype));
dp = NaN(length(wnd),length(stimtype));
for ii = 1:length(wnd),
    for jj = 1:length(stimtype),
        fl = sprintf('%dms/%s_%s_db',round(wnd(ii)),sbj,stimtype{jj});
        d = load([respth fl]);
        r(:,ii,jj) = d.r;
        nr(:,ii,jj) = d.nullr;
        zr(:,ii,jj) = (d.r-mean(d.nullr))/std(d.nullr);
        dp(ii,jj) = (mean(d.r)-mean(d.nullr))/sqrt(0.5*(var(d.r)+var(d.nullr)));
        disp(fl)
    end
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Plot the results
% reconstruction accuracy
figure
hold on
for jj = 1:length(stimtype),
    mrnull = squeeze(mean(nr(:,:,jj)));
    strnull = squeeze(std(nr(:,:,jj)));
    shadedErrorBar(wnd,mrnull,strnull,'lineProps',{clr{jj},'LineWidth',1});
end
% moving averaged
r_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype),
    mr = squeeze(mean(r(:,:,jj)));
    str = squeeze(std(r(:,:,jj)));
    r_plts(jj) = errorbar(wnd,mr,str,clr{jj},'LineWidth',2);
end
set(gca,'XScale','log','FontSize',16,'XTick',wnd,'XLim',[min(wnd) max(wnd)]);
xlabel('Window Size (ms)');
ylabel('Reconstruction accuracy');
title(sbj);
legend(r_plts,stimtype);

% d-prime
figure
hold on
% moving averaged
for jj = 1:length(stimtype),
    plot(wnd,dp(:,jj),clr{jj},'LineWidth',2);
end
set(gca,'XScale','log','FontSize',16,'XTick',wnd,'XLim',[min(wnd) max(wnd)]);
xlabel('Window size (ms)');
ylabel('d-prime');
title(sbj);
legend('rock','classical','vocals','speech');