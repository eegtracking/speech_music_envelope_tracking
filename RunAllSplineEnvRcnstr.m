% For a subject, run each SplineEnvRcnstr condition: stimtype, window_size,
% removing moving average vs no moving average removal

sbj = 'JH';
stimtypes = {'speech','vocals','rock','classical'};
wnd = 125*2.^(0:5);

disp('** With moving average **')
for ii = 1:length(stimtypes),
    for jj = 1:length(wnd)
        fprintf('-- %s, %d ms --\n',stimtypes{ii},wnd(jj));
        SplineEnvRcnstr_general(sbj,stimtypes{ii},wnd(jj));
    end
end

% wnd_noavg = 125*2.^(1:5);
% disp('** Without moving average **');
% for ii = 1:length(stimtypes),
%     for jj = 1:length(wnd_noavg)
%         fprintf('-- %s, %d ms, no moving average --\n',stimtypes{ii},wnd_noavg(jj));
%         SplineEnvRcnstr_general(sbj,stimtypes{ii},wnd_noavg(jj),'do_mov_avg',false);        
%     end
% end
% 
% % Run 16000 ms
% for ii = 1:length(stimtypes),
%     fprintf('** %s, 16000 ms, no moving average **\n',stimtypes{ii});
%     SplineEnvRcnstr_general(sbj,stimtypes{ii},16000,'do_mov_avg',false,'desFs',32);  
% end

% Run 62.5 ms
for ii = 1:length(stimtypes),
    fprintf('** %s, 62.5 ms **\n',stimtypes{ii});
    SplineEnvRcnstr_general(sbj,stimtypes{ii},62.5,'desFs',256);  
end

% Run 250 and 2000 ms for the different rock instruments
instrtypes = {'vocals','guitar','bass','drums'};
instrwnd = [250 2000];
for ii = 1:length(instrtypes),
    for jj = 1:length(instrwnd),
        fprintf('-- Instrument: %s, %d ms --\n',instrtypes{ii},instrwnd(jj));
        SplineEnvRcnstr_instrument(sbj,instrtypes{ii},instrwnd(jj));
    end
end