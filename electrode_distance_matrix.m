function D = electrode_distance_matrix(loc_txt,loc_path)
% Load a text file containing the locations of the electrodes, and use
% those locations to create a matrix of the angular distance between
% electrodes.
% Inputs:
% - loc_txt = filename for the text file containing the electrode locations
% - loc_path = path containing the text file
% Outputs:
% - D = angular distance matrix
% Nate Zuk (2019)

% Load the file
chfl = fopen([loc_path loc_txt]);
xyz = []; % assume we don't know the number of channels initially
pos = fgetl(chfl);
chind = 1;
while ischar(pos),
    s = strsplit(pos,'\t');
    p = NaN(1,3); % for x y z coordinates
    p(1) = str2double(s(2));
    p(2) = str2double(s(3));
    p(3) = str2double(s(4));
    xyz = [xyz; p];
    pos = fgetl(chfl);
    chind = chind+1;
end
fclose(chfl);

% Find the center of the best fit ellipsoid to the 
center = ellipsoid_fit(xyz); % get the best fit ellipsoid
% For each pair of points...
D = zeros(size(xyz,1));
for ii = 1:size(xyz,1)
    for jj = 1:ii-1
        % ...compute the plane that passes through those two
        % points and the center...
        % plane: ax + by + cz = 1
        Xpl = [xyz([ii jj],:)' center];
%         bpl = Xpl \ ones(3,1);
%         normal = bpl/norm(bpl); % normal vector for the plane
%         % ...project the vectors to those points onto the plane...
%         n_xyz = xyz([ii jj],:)-(xyz([ii jj],:)*normal)*normal';
%         % ...then compute the circle that fits those points
%         % projected along the plane...
%         A = [normal n_xyz(1,:)'/norm(n_xyz(1,:)) n_xyz(2,:)'/norm(n_xyz(2,:))];
            % rotation matrix to the plane
        cntXpl = Xpl-mean(Xpl,2)*ones(1,3); % center the points about the origin
        basis = orth(cntXpl); % get the orthonormal basis the two points and the center
            % (two vectors will be in the plane, one will be perpendicular)
%         n_xyz = basis'*xyz([ii jj],:)';
        n_xyz = basis'*cntXpl(:,1:2);
%         if rank(basis)==3,
%             keyboard;
%             rot_xyz = A\n_xyz;
%             % ...and compute the angular distance based on the
%             % circle
%             red_xyz = rot_xyz(2:3,:)';
%         else
            red_xyz = n_xyz';
%         end
        Xc = [sum(red_xyz.^2,2) -2*red_xyz];
        bc = Xc \ ones(2,1);
        r = sqrt(1/bc(1)+bc(2)^2/(4*bc(1)^2)+bc(3)^2/(4*bc(1)^2)); % radius
        uo = -bc(2)/(2*bc(1)); % center coordinates uo and vo
        vo = -bc(3)/(2*bc(1));
        c = red_xyz-ones(2,1)*[uo vo];
        ang = acos(c(1,:)*c(2,:)'/(norm(c(1,:))*norm(c(2,:)))); % angle (radians)
        D(ii,jj) = ang*r; % angular distance
    end
end

% Add upper triangular to make D symmetrical
D = D+D';