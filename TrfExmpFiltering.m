% Demonstrate how the spectrum of some signal (random noise, for example)
% changes after the applying a TRF
% addpath('~/Projects/EEGanly/PCA_spline_modeling');
addpath('~/Projects/mTRF-Toolbox/');
dur = 100; % duration of the signal (s)
eFs = 128;
s = randn(dur*eFs,1); % take a speech example

% Load TRF example
% d = load('~/Documents/Matlab/NeuralOscSim/TRF_example');
% trf = d.TRF';
% tmin = -150;
% tmax = 350;
% t_idx = ceil(tmin/1000*eFs):floor(tmax/1000*eFs);
% t = t_idx/eFs*1000;

% Load NeuroMusic TRFs
d = load('RegularizedTRFModels'); % load the models for each of the stimulus types
trf = squeeze(mean(d.model(:,:,:,4),3)); % get the average model across subjects
t_idx = d.dly;
t = t_idx/d.desFs*1000;
use_chan = 85; % for Fz

% Simulate a simple shift TRF
% d = load('RegularizedTRFModels');
% t_idx = d.dly;
% t = t_idx/d.desFs*1000;
% trf = zeros(length(t_idx),1);
% shift = 400; % amount of shift in the signal, in ms
% t_shift = find(abs(t-shift)==min(abs(t-shift)));
% trf(t_shift) = 1;
% use_chan = 1;

% set a minimum delay to examine in the trf
min_dly = 0;
max_dly = 500;
use_idx = t>=min_dly & t<=max_dly;
trf = trf(use_idx,use_chan);
t_idx = t_idx(use_idx);
t = t(use_idx);

% normalize to have a sum of 1
trf = trf/abs(sum(trf));

% Use the TRF to filter the input s
X = lagGen(s,t_idx);
y = X*trf;
clear X;

% Plot the TRF
figure
subplot(2,1,1);
plot(t,trf,'k','LineWidth',2);
xlabel('Time (ms)');
ylabel('TRF');

subplot(2,1,2);
TRF = fft(trf);
% frq_trf = (0:length(trf)-1)/length(trf)*d.desFs;
frq_trf = (0:length(trf)-1)/length(trf)*eFs;
plot(frq_trf,abs(TRF).^2,'k','LineWidth',2);
set(gca,'FontSize',16,'XLim',[1 40]);
xlabel('Frequency (Hz)');
ylabel('TRF power');

% Compute the power spectrum of each step
S = fft(s)/length(s);
Y = fft(y)/length(y);
frq = (0:length(S)-1)/length(S)*eFs;

figure
set(gcf,'Position',[360 5 550 700]);
subplot(2,1,1);
hold on
plot(frq,abs(S).^2,'k');
plot(frq,abs(Y).^2,'b','LineWidth',2);
set(gca,'FontSize',16,'XScale','log','YScale','log','XLim',[0.01 100]);
xlabel('Frequency (Hz)');
ylabel('Power (squared magnitude)');
legend('Original','TRF filtered');

subplot(2,1,2);
hold on
% plot(frq,angle(S),'k');
ph_diff = angle(Y)-angle(S);
% detects when a jump is >pi
% ph_change = diff(ph_diff);
% jumps = abs(ph_change)>pi;
% ph_diff([false; jumps]) = ph_diff([false; jumps])+2*pi*sign([0; ph_change(jumps)]); % adjust so that all 2*pi jumps don't occur
ph_diff(ph_diff<-pi) = ph_diff(ph_diff<-pi)+2*pi;
ph_diff(ph_diff>pi) = ph_diff(ph_diff>pi)-2*pi;
plot(frq,unwrap(ph_diff),'b','LineWidth',2);
set(gca,'FontSize',16,'XScale','log','XLim',[0.1 25]);
xlabel('Frequency (Hz)');
ylabel('Phase difference');

% Compute group delay for low frequencies
frq_range = [0.1 1];
frq_idx = frq>frq_range(1)&frq<frq_range(2);
pd = unwrap(ph_diff);
X = [ones(sum(frq_idx),1) frq(frq_idx)'];
b = X \ pd(frq_idx);
fprintf('Group delay = %.3f s\n',-b(2)/(2*pi));

% Plot the pwelch power spectrum
[P,frq_e] = pwelch(s,20*eFs,10*eFs,[],eFs);
[Py,frq_y] = pwelch(y,20*eFs,10*eFs,[],eFs);
figure
hold on
plot(frq_e,10*log10(P),'k','LineWidth',1);
plot(frq_y,10*log10(Py),'b','LineWidth',1.5);
wnd = 1000/(max_dly-min_dly);
plot([wnd wnd],[-100 0],'k--');
set(gca,'FontSize',14,'XScale','log');
xlabel('Frequency (Hz)');
ylabel('Power (dB)');
legend('Original','TRF filtered','Width of TRF');