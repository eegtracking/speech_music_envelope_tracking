function stimcodes = getstimcodes(stimtag)
% Load the stimulus codes for the given stimtag (rock, classical, vocals, 
% speech) as applied in the NeuroMusic dataset
% Nate Zuk (2019)

if strcmp(stimtag,'rock'),
    stimcodes = 10:19;
elseif strcmp(stimtag,'classical'),
    stimcodes = 20:29;
elseif strcmp(stimtag,'vocals'),
    stimcodes = 30:39;
elseif strcmp(stimtag,'speech'),
    stimcodes = 40:49;
else
    error('Unknown stimulus tag');
end