#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH -c 4
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --mem=16GB

sbj=$1
wnd=$2

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(4); sbj='${sbj}'; wnd=${wnd}; OrigEnvRcnstr_testing(sbj,'vocals',wnd,'voltlimdb',-30);"
rm -rf /scratch/$USER/$SLURM_JOB_ID
