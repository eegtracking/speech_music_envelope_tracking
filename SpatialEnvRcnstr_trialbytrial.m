function SpatialEnvRcnstr_trialbytrial(sbj,stimtype,window_size,delay,varargin)
% Load neuromusic data and compute the reconstruction accuracy using a
% single-lag model
%%% NZ - PCA-transform each dataset individually, aka. for each particular
%%% window size and stimulus type.  This would be optimal.
%%% NZ (28-1-2020) -- Andy mentioned that pca should be applied to only the
%%% training data, and then the testing data can be transformed with the
%%% eigenvectors from the training set.  The testing data should be
%%% COMPLETELY separate from all transformation applied to get the model on
%%% the trainin data.
%%% NZ (28-1-2020) -- I expect all of the results I have seen so far will
%%% persist doing trial-by-trial testing, rather than a random sampling of
%%% the data across trials. While the performance for music may still be
%%% smaller, the methods will be easier to explain.

% addpath('~/mTRF_Zukedits/');

tmin = 0;
tmax = window_size+tmin;
map = -1;
desFs = 512;
tlims = 16;
niter = 50;
do_mov_avg = true;

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
stimpth = '~/NeuroMusic/stims/';
stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtype);
    %%% I will need to change this to be 512 Hz envelopes, no downsampling
s = load([stimpth stimfl]);
stims = s.stimset;
% Transpose all stimuli to column vectors
for ii = 1:length(stims), stims{ii} = stims{ii}'; end
% clear s

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
    if eFs~=desFs
        eeg{ii} = resample(eeg{ii},desFs,eFs);
    end
end

% disp('Convert stimuli to dB');
% % voltlim = 10^(voltlimdb/20);
% silence = cell(length(stims),1); % to store silent periods
% for ii = 1:length(stims),
%     % store 0 anytime there's silence
%     silence{ii} = ones(length(stims{ii}),1);
%     silence{ii}(stims{ii}<voltlimdb) = 0;
%     stims{ii}(stims{ii}<voltlimdb) = voltlimdb;
% end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims),
    %%% NZ (19-1-2020), don't skip silences, could have had strange effects
    %%% on null distribution, overenflated tracking at low frequencies
    tidx{ii} = ones(length(stims{ii}),1);
%     if do_mov_avg,
%         tidx{ii} = silence{ii}; %%% NZ (3-1-2020), silences weren't being removed here before
%     else
%         tidx{ii} = ones(length(silence{ii}),1); % only skip silences if removing the envelope moving average
%     end
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(st));
    eeg{ii} = eeg{ii}-avg;
%     if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
%         s_on = stims{ii};
%         s_on(~tidx{ii}) = mean(s_on(tidx{ii})); % set silent periods to the average of
%             % the non-silent envelope, to avoid edge effects as speech
%             % turns on and off
%         s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
%             % avoid edge effects of moving average)
%         s_on = s_on-movmean(s_on,length(st));
%         stims{ii}(tidx{ii}) = s_on(tidx{ii});
% %         savg = movmean(stims{ii},length(t));
% %         stims{ii} = stims{ii}-savg;
%     end
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        % NZ (15-1-2020), don't remove silences when doing moving average
        s_on = stims{ii};
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii} = s_on;
    end
end
clear avg s_on

% Transform EEG into PCs
% disp('Transform to PCs...');
% allEEG = cell2mat(eeg);
% cf = pca(allEEG); % get the principal components of the EEG
% pc = cell(length(eeg),1);
% var_pc = NaN(length(eeg),size(cf,2)); % to store the original PC variances,
%     % for appopriately reweighting the PCs in the model
% for ii = 1:length(eeg)
% %     eeg{ii} = detrend(eeg{ii},0); % center each channel
%     pc{ii} = eeg{ii}*cf; % transform to pcs
%     var_pc(ii,:) = var(pc{ii});
%     pc{ii} = zscore(pc{ii}); % normalize to variance of 1
% end
% clear allEEG eeg

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,eeg);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(eeg{n},1)); end
    end
end

% round the delay to the nearest integer index
delay = round(delay/1000*desFs)/desFs*1000;
fprintf('-- Using delay = %.3f ms\n',delay);

disp('Computing the envelope reconstruction from random samplings of data across trials...');
r = NaN(length(stims),1);
mse = NaN(length(stims),1);
model = cell(length(stims),1);
cf = cell(length(stims),1);
var_pc = NaN(128,length(stims));
for n = 1:length(stims)
    fprintf('** Trial %d/%d **\n',n,length(stims));
    trtm = tic;
    traintrs = setxor(1:length(stims),n); % get the testing trials
    
    disp('Transform to PCs...');
    allEEG = cell2mat(eeg(traintrs));
    cf{n} = pca(allEEG); % get the principal components of the EEG
    pc = cell(length(traintrs),1);
    npc = size(cf{n},2);
    v = NaN(npc,length(traintrs)); % to store the original PC variances,
        % for appopriately reweighting the PCs in the model
    for ii = 1:length(traintrs)
    %     eeg{ii} = detrend(eeg{ii},0); % center each channel
        pc{ii} = eeg{traintrs(ii)}*cf{n}; % transform to pcs
        v(:,ii) = var(pc{ii});
        pc{ii} = zscore(pc{ii}); % normalize to variance of 1
    end
    lenpcs = cellfun(@(x) size(x,1),pc);
    % weighted average of variances across pcs
    var_pc(1:npc,n) = sum(v.*(ones(npc,1)*lenpcs'),2)/sum(lenpcs);
    clear allEEG
    
    model{n} = mTRFtrain(stims(traintrs),pc,desFs,map,delay,delay,0,tidx(traintrs));
    
    % transform the testing trial into pc
    testpc = zscore(eeg{n}*cf{n});
    [~,r(n),~,mse(n)] = mTRFpredict(stims{n},testpc,model{n},desFs,map,delay,delay,tidx(n));
    
    fprintf('** Completed trial %d/%d @ %.3f s\n',n,length(stims),toc(trtm));
end

[nullr,nullmse] = mTRFnull_standard_pca(stims,eeg,cf,desFs,map,delay,delay,0,tidx,'niter',niter,...
     'randomize_idx',false);

svpth = sprintf('~/NeuroMusic/rcnstr_spatial_tbt/%dms/',round(tmax-tmin));
svfl = sprintf('%s_%s_%dms',sbj,stimtype,round(delay));
save([svpth svfl],'r','mse','desFs','tmin','tmax','tlims',...
    'nullr','nullmse','tidx','var_pc','model','cf');
