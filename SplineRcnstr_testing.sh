#!/bin/bash
#SBATCH --time=08:00:00
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --mem=32GB

sbj=$1
stim=$2
wnd=$3
mdlfs=$4
npcs=$5

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(8); sbj='${sbj}'; stimtype='${stim}'; wnd=${wnd}; SplineEnvRcnstr_testing(sbj,stimtype,wnd,'mdlFs',${mdlfs},'npcs',${npcs});"
rm -rf /scratch/$USER/$SLURM_JOB_ID
