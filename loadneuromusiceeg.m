function [eegset,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj)
% Load the stimuli and the associated EEG data
% Inputs:
%   - eegpth = path containing eeg
%   - stimcodes = array of numbers containing stimulus codes to examine
%   - sbj = subject code (a string)
% Outputs:
%   - eegset = structure containing EEG responses
%   - eFs = sampling rate of both the output stimuli and the EEG signals
% Nate Zuk (2019)

% Load the set of EEG data
rfls = what(eegpth);
res = rfls.mat;

eegset = cell(length(stimcodes),1);
for ii = 1:length(res) % For each results file...
    fnd = strfind(res{ii},sbj); % ...check if it's a results file for the desired subject...
    if ~isempty(fnd)
        loadtm = tic;
        disp(['Loading EEG data in ' res{ii} '...']);
        d = load([eegpth res{ii}]); % ...load the results...
        eFs = d.eFs; % get the sampling rate of the EEG
        % ...and compute the model separately for each trial
        for jj = 1:length(d.stim)
            % Load the sound file for that trial
            lstind = find(d.stim(jj)==stimcodes);
            if ~isempty(lstind)
                eeg = d.eeg{jj}; % load the EEG signal
                eegset{lstind} = eeg;
            end
        end
        disp(['Completed @ ' num2str(toc(loadtm)) ' s!']);
    end
end
