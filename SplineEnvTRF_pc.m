% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes

addpath('~/Projects/EEGanly/PCA_spline_modeling/');
addpath('~/Documents/MATLAB/ellipsoid_fit/');
addpath('~/Projects/mTRF_Zukedits/');
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbj = 'JS';
stimtype = 'speech';
tmin = 0;
tmax = 2000;
map = 1;
tlims = 2;
npcs = 64;
voltlimdb = -80;
sil_tol = 0.001;
mdlFs = 1000/(tmax-tmin)*16; % 2x highest frequency of interest in the model

% Load the general PCA transformation
D = electrode_distance_matrix('chanlocs.xyz','~/Projects/EEGanly/');
cf = pca(D);
cf = [ones(size(D,1),1)/sqrt(size(D,1)) cf];

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
if strcmp(stimtype,'rock'), stimcolumn = 1;
elseif strcmp(stimtype,'classical'), stimcolumn = 2;
elseif strcmp(stimtype,'vocals'), stimcolumn = 3;
elseif strcmp(stimtype,'speech'), stimcolumn = 4;
else
    error('Unknown stimulus tag');
end
stimpth = '/Volumes/Untitled/NeuroMusic/';
stimfl = 'NeuroMusicEnvelopes_raw';
load([stimpth stimfl]);
stims = stimset(:,stimcolumn);

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

disp('Convert stimuli to dB');
voltlim = 10^(voltlimdb/20);
silence = cell(length(stims),1); % to store silent periods
for ii = 1:length(stims),
    % store 0 anytime there's silence
    silence{ii} = ones(length(stims{ii}),1);
    silence{ii}(stims{ii}<voltlim) = 0;
    stims{ii}(stims{ii}<voltlim) = voltlim;
    stims{ii} = 20*log10(stims{ii});
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
t = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(t));
    eeg{ii} = eeg{ii}-avg;
    savg = movmean(stims{ii},length(t));
    stims{ii} = stims{ii}-savg;
end
clear avg

% Transform EEG into PCs
disp('Transform to PCs...');
pc = cell(length(eeg),1);
for ii = 1:length(eeg),
    pc{ii} = eeg{ii}*cf;
end
allPC = cell2mat(pc);
allEEG = cell2mat(eeg);
% sort the principal components by variance, and retain only the largest
% ones
var_pc = var(allPC);
[sortv,sortidx] = sort(var_pc,'descend');
% compute the % variance explained by the retained PCs
% usepcs = sortidx(1:npcs+1);
usepcs = 1:npcs+1;
transPC = allPC(:,usepcs)*cf(:,usepcs)';
SSE_red = sum(sum((transPC-allEEG).^2));
SST = sum(sum((allEEG).^2));
vexp_red = 1-SSE_red/SST;
fprintf('Variance explained by retained PCs: %.2f%%\n',vexp_red*100);
for ii = 1:length(pc),
    pc{ii} = pc{ii}(:,usepcs);
end
clear allPC allEEG eeg transPC

% Downsample to EEG and stimulus by 4x, to make it more managable for the
% computer
desFs = 128;
for ii = 1:length(pc),
    pc{ii} = resample(pc{ii},desFs,eFs);
    pc{ii} = zscore(pc{ii});
    stims{ii} = resample(stims{ii},desFs,eFs);
    silence{ii} = resample(silence{ii},desFs,eFs); % get affects of ringing due to downsampling here
end
dest = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
tidx = cell(length(silence),1);
for ii = 1:length(silence),
    tidx{ii} = zeros(length(silence{ii}),1);
    tidx{ii}(abs(silence{ii}-1)<sil_tol) = 1; % set to 1 when stimulus is on, and no ringing
    l = usetinds(tlims,desFs,length(silence{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

ds = desFs/mdlFs;

disp('Computing the envelope reconstruction...');
% model = mTRFtrain_spline(stims,pc,desFs,ds,map,tmin,tmax,0,tlims);
% [~,r] = mTRFpredict_spline(stims,pc,model,desFs,ds,map,tmin,tmax,tlims);
[r,~,mse,model] = mTRFcrossval_spline(stims,pc,desFs,ds,map,tmin,tmax,0,tidx);
model = squeeze(model);
% scale the model weights based on reconstruction accuracy
R = squeeze(mean(r,1));
vM = (model.*(ones(length(dest),1)*sqrt(var_pc(usepcs))));
scaleR = (R-min(R))/(max(R)-min(R)); % scale from 0 to 1
weighted_model = vM.*(ones(size(model,1),1)*scaleR');
orig_model = weighted_model*cf(:,usepcs)'; % transform back to EEG space

figure
plot(R);
xlabel('PC');
ylabel('Prediction accuracy, r');
colorbar

figure
plot(dest/desFs*1000,model*scaleR,'k','LineWidth',2);
set(gca,'FontSize',16);
xlabel('Delay (ms)');
ylabel('Weighted average TRF');

% Plot the model
figure
imagesc(dest/desFs*1000,1:128,orig_model');
set(gca,'FontSize',16);
xlabel('Delay (ms)');
ylabel('Channel');
colorbar

% Plot the topographies of the weights between 50 to 100 ms and 100 to 200
% ms
topo_ranges = (tmax-tmin)*[1/10 1/5 1/2 3/4];
figure
subplot(1,3,1);
tidx = dest/desFs*1000>topo_ranges(1) & dest/desFs*1000<topo_ranges(2);
topoplot(mean(orig_model(tidx,:),1),'~/Projects/EEGanly/chanlocs.xyz');
title(sprintf('%d - %d ms',round(topo_ranges(1)),round(topo_ranges(2))));
subplot(1,3,2);
tidx = dest/desFs*1000>topo_ranges(2) & dest/desFs*1000<topo_ranges(3);
topoplot(mean(orig_model(tidx,:),1),'~/Projects/EEGanly/chanlocs.xyz');
title(sprintf('%d - %d ms',round(topo_ranges(2)),round(topo_ranges(3))));
subplot(1,3,3);
tidx = dest/desFs*1000>topo_ranges(3) & dest/desFs*1000<topo_ranges(4);
topoplot(mean(orig_model(tidx,:),1),'~/Projects/EEGanly/chanlocs.xyz');
title(sprintf('%d - %d ms',round(topo_ranges(3)),round(topo_ranges(4))));