function [r,mse] = mTRFnull_pca(stim,resp,cf,fs,ds,npcs,map,tmin,tmax,lambda,tlims,varargin)
%mTRFtrain mTRF Toolbox training function.
%   MODEL = MTRFNULL(STIM,RESP,FS,DS,MAP,TMIN,TMAX,LAMBDA,TLIMS) determines
%   a null distribution of r and mse values by randomly circularly shifting
%   and permuting stimulus and response trials, and then either testing on
%   randomly sampled data or data per trial
%
%   [...,T] = MTRFTRAIN(...) also returns the vector of time lags T for
%   plotting MODEL and the regression constant C for absorbing any bias
%   when testing MODEL.
%
%   NZ edits (25-1-2019): 
%   -- The design matrix is z-scored before training the
%      model
%   -- Only ridge regression is used, quadratic (or Tikhonov) regression
%      has been removed
%   NZ (28-1-2020):
%   -- PCA transform the training data on each iteration before computing
%   the model
%
%   Inputs:
%   stim   - stimulus property (time by features)
%   resp   - neural response data (time by channels)
%   cf     - cell array of coefficient matrices to convert to principal
%       components
%   fs     - sampling frequency (Hz)
%   ds     - downsampling ratio, for the spline transformation
%   npcs   - # of principal components to retain in the model
%   map    - mapping direction (forward==1, backward==-1)
%   tmin   - minimum time lag (ms)
%   tmax   - maximum time lag (ms)
%   lambda - possible ridge parameters (must be a 1D vector)
%
%   Outputs:
%   model  - linear mapping function (MAP==1: feats by lags by chans,
%            MAP==-1: chans by lags by feats)
%   t      - vector of time lags used (ms)
%
%   See README for examples of use.
%
%   See also LAGGEN MTRFTRANSFORM MTRFPREDICT MTRFCROSSVAL
%   MTRFMULTICROSSVAL.

%   References:
%      [1] Lalor EC, Pearlmutter BA, Reilly RB, McDarby G, Foxe JJ (2006)
%          The VESPA: a method for the rapid estimation of a visual evoked
%          potential. NeuroImage 32:1549-1561.
%      [1] Crosse MC, Di Liberto GM, Bednar A, Lalor EC (2015) The
%          multivariate temporal response function (mTRF) toolbox: a MATLAB
%          toolbox for relating neural signals to continuous stimuli. Front
%          Hum Neurosci 10:604.

%   Author: Edmund Lalor, Michael Crosse, Giovanni Di Liberto
%   Lalor Lab, Trinity College Dublin, IRELAND
%   Email: edmundlalor@gmail.com
%   Website: www.lalorlab.net
%   April 2014; Last revision: Jan 8, 2016

randomize_idx = false; % flag if testing data should be randomly sampled across trials
niter = 100; % number of times to repeat model testing
size_for_testing = length(stim); % specify an integer representing the number
    % of equal groups in which to split the data, where one of the groups
    % is used for testing (so a value of 10 will use 1/10th of the data for
    % testing) (only applicable if randomize_idx==true)

if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Define x and y
if tmin > tmax
    error('Value of TMIN must be < TMAX')
end
if map == 1
    x = stim;
    y = resp;
elseif map == -1
    x = resp;
    y = stim;
    [tmin,tmax] = deal(tmax,tmin);
else
    error('Value of MAP must be 1 (forward) or -1 (backward)')
end
clear stim resp

% Convert time lags to samples
tmin = floor(tmin/1e3*fs*map);
tmax = ceil(tmax/1e3*fs*map);

% Generate lag matrix
fprintf('Creating the lag matrices...');
mattm = tic;
Xc = cell(length(x),1);
yc = cell(length(y),1);
dimx = size(x{1},2); % get the original dimension of x
% spline_matrix = spline_transform(tmin:tmax,ds);
% w_spl = (spline_matrix'*spline_matrix)^(-1)*spline_matrix';
if ds>1 % if downsampling is necessary (using splines) (NZ, 11-2-2020)
    spline_matrix = spline_transform(tmin:tmax,ds);
else
    spline_matrix = eye(length(tmin:tmax)); % use the identity matrix (no transformation)
end
w_spl = (spline_matrix'*spline_matrix)^(-1)*spline_matrix'; % whitened spline transform
for ii = 1:length(x),
    yc{ii} = y{ii};
    catx = [];
    for jj = 1:dimx,
        tmpx = lagGen(x{ii}(:,jj),tmin:tmax); %%% skip constant term (NZ)
%         idx = jj:dimx:size(tmpx,2); % get the indexes of the correct rows
        % compute the spline transform
        %[splx,~] = spline_transform(tmpx,ds);
        splx = w_spl*tmpx';
        catx = [catx splx'];
    end
    Xc{ii} = catx; % replace with spline-transformed lagGen matrix
    % Set X and y to the same length
    minlen = min([size(Xc{ii},1) size(yc{ii},1)]);
    Xc{ii} = Xc{ii}(1:minlen,:);
    yc{ii} = yc{ii}(1:minlen,:);
    % Remove time indexes, if specified
    if iscell(tlims), % if tlims is a cell array, it means that specific indexes were supplied
        tinds = tlims{ii};
    else
        tinds = usetinds(tlims,fs,minlen);
    end
%     Xc{ii} = zscore(Xc{ii}(tinds,:)); % zscore the columns of X
%     yc{ii} = zscore(yc{ii}(tinds,:)); % zscore the output matrix Y
    %%% zscore after transforming to PCs
    Xc{ii} = Xc{ii}(tinds,:);
    yc{ii} = yc{ii}(tinds,:);
end
fprintf('Completed @ %.3f s\n',toc(mattm));
clear x y tmpx

% Set up regularisation
% dim = size(Xc{1},2);
nsplines = size(spline_matrix,2); % get the number of splines
M = eye(npcs*nsplines); % regularization matrix, for ridge
% M = eye(dim,dim);

dimy = size(yc{1},2); % get the number of output variables

% Compute the largest possible shift, equal to the length of the shortest
% trial
max_shift = min(cellfun(@(m) size(m,1),yc));

fprintf('Computing the null distribution (%d iterations)',niter);
nulltm = tic;
ntr = length(Xc);
r = NaN(niter,dimy);
mse = NaN(niter,dimy);
for n = 1:niter,
    fprintf('.');
    
    test_tr = randi(ntr);
    train_tr = setxor(1:ntr,test_tr);
    
    % Randomly circularly shift the data
    rndshft = randi(max_shift);
    tmpx = cell(ntr,1);
    tmpy = cell(ntr,1);
    for ii = 1:ntr
%         rndshftx = randi(size(Xc{ii},1));
%         tmpx{ii} = circshift(Xc{ii},rndshftx);
%         rndshfty = randi(size(yc{ii},1));
%         tmpy{ii} = circshift(yc{ii},rndshfty);
        % transform into PCs
%         tmpx{ii} = NaN(size(Xc{ii}));
        tmpx{ii} = NaN(size(Xc{ii},1),npcs*nsplines);
        for jj = 1:nsplines
            % for this particular spline, get the indexes for all EEG channels
            spl_idx = (0:dimx-1)*nsplines+jj;
            % get the indexes after reduces to npcs
            pcs_idx = (0:npcs-1)*nsplines+jj;
            % transform the EEG channels into principal components
            tmpx{ii}(:,pcs_idx) = Xc{ii}(:,spl_idx)*cf{test_tr}(:,1:npcs);
        end
        tmpy{ii} = circshift(yc{ii},rndshft);
        % zscore here
        tmpx{ii} = zscore(tmpx{ii});
        tmpy{ii} = zscore(tmpy{ii});
    end
    
    % Randomly permute the data
    %%% don't randomly permute, in case there is variation in EEG spectrum
    %%% across trials
%     rndorder = randperm(ntr);
%     tmpy = tmpy(rndorder);
    
    % Truncate the data so that they have the same lengths by trial
    %%% don't need to do this if we're not permuting the trials
%     for ii = 1:ntr,
%         minlen = min([size(tmpx{ii},1) size(tmpy{ii},1)]);
%         tmpx{ii} = tmpx{ii}(1:minlen,:);
%         tmpy{ii} = tmpy{ii}(1:minlen,:);
%     end
    
    % Compute the correlation matrices X'X and X'y and get testing X and y
    if randomize_idx,
        totidx = sum(cellfun(@(x) size(x,1),tmpx));
        test_idx = randperm(totidx,round(totidx/size_for_testing));
        train_idx = setxor(1:totidx,test_idx);
        [xtx,xty] = compute_linreg_matrices(tmpx,tmpy,train_idx,[],'verbose',0);
        xtest = cell_to_time_samples(tmpx,test_idx);
        ytest = cell_to_time_samples(tmpy,test_idx);
    else
        [xtx,xty] = compute_linreg_matrices(tmpx(train_tr),tmpy(train_tr),[],[],'verbose',0);
        xtest = tmpx{test_tr};
        ytest = tmpy{test_tr};
    end

    % Randomly pick a lambda from the lambdas provided
    lmbuse = lambda(randi(length(lambda)));
    
    % Calculate model
    spl_model = (xtx+lmbuse*M)^(-1)*xty;
    pred = xtest*spl_model;
    for i = 1:dimy
        r(n,i) = corr(ytest(:,i),pred(:,i));
        mse(n,i) = mean((ytest(:,i)-pred(:,i)).^2);
    end
    
    clear tmpx tmpy xtst ytst xtx xty
end
fprintf('\n');
fprintf('Completed @ %.3f s\n',toc(nulltm));

end
