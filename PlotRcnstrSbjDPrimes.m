% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JS'};
stimtype = {'rock','classical','vocals','speech'};
clr = {[1 0 0],[0 0.7 0],[0 0.7 0.7],[0 0 1]};
wnd = [62.5 125 250 500 1000 2000 4000];
nfolds = 10; % number of folds for CV
niter = 50; % number of iterations to get the null distribution
respth = '~/Projects/NeuroMusic/rcnstr_res/';

% Load the moving-average-removed results
dp = NaN(length(wnd),length(stimtype),length(sbjs));
for s = 1:length(sbjs),
    for ii = 1:length(wnd),
        for jj = 1:length(stimtype),
            fl = sprintf('%dms/%s_%s_db',round(wnd(ii)),sbjs{s},stimtype{jj});
            d = load([respth fl]);
            dp(ii,jj,s) = (mean(d.r)-mean(d.nullr))/sqrt(0.5*(var(d.r)+var(d.nullr)));
            disp(fl);
        end
    end
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% d-prime
figure
hold on
% moving averaged
dp_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype),
    dp_plts(jj) = errorbar(wnd,mean(dp(:,jj,:),3),std(dp(:,jj,:),[],3),'Color',clr{jj},'LineWidth',2);
end
set(gca,'XScale','log','FontSize',16,'XTick',unique(wnd),'XLim',[min(wnd) max(wnd)]);
xlabel('Window size (ms)');
ylabel('d-prime');
legend(dp_plts,'rock','classical','vocals','speech');

% do anova for each window
panova = NaN(length(wnd),1);
stanova = cell(length(wnd),1);
for ii = 1:length(wnd),
    [panova(ii),~,stanova{ii}] = anova1(squeeze(dp(ii,:,:))',[],'off');
end