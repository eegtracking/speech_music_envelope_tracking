% Plot the topography for a reconstruction model
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));
addpath('~/Documents/MATLAB/shadedErrorBar/');
addpath('~/Projects/EEGanly/PCA_spline_modeling/');

sbjs = {'AB','AF','CB','CQ','DH','EB','HC','JS'};
instrtypes = {'vocals','guitar','bass','drums'};
% clr = {'r','g','c','b'};
clr = {[0.7 0 0.7],[1 0 0],[0 0.7 0],[0 0 1]};
wnd = 250;
desFs = 128;
nchan = 128;
pth_suffix = '';

% Load the general PCA transformation
D = electrode_linear_distance_matrix('chanlocs.xyz','~/Projects/EEGanly/');
cf = pca(D);
cf = [ones(size(D,1),1)/sqrt(size(D,1)) cf];

dly = 0:ceil(wnd/1000*desFs);
model = NaN(length(dly),nchan,length(sbjs),length(instrtypes));
for ii = 1:length(instrtypes),
    for s = 1:length(sbjs),
        pth = sprintf('instr_rcnstr_res/%dms%s/',round(wnd),pth_suffix);
        fl = sprintf('%s_%s_db',sbjs{s},instrtypes{ii});
        d = load([pth fl]);
        pc_model = reshape(d.model_t,[length(dly) length(d.usepcs)]);
        w_model = flipud(pc_model).*(ones(length(dly),1)*sqrt(d.var_pc(d.usepcs)));
        model(:,:,s,ii) = w_model*cf(:,d.usepcs)';
        disp(fl);
    end
end

% Plot the images of all TRFs, delay x channel
figure
allM = reshape(model,[numel(model),1]);
crange = [quantile(allM,0.01) quantile(allM,0.99)];
% crange = [-1 1];
for ii = 1:length(instrtypes),
    subplot(2,2,ii);
    imagesc(dly/desFs*1000,1:128,mean(model(:,:,:,ii),3)');
    caxis(crange);
    xlabel('Delay (ms)');
    ylabel('Channel');
    title(instrtypes{ii});
    colorbar;
end

figure
stimplts = NaN(length(instrtypes),1);
for ii = 1:length(instrtypes),
    M = mean(model(:,:,:,ii),3);
    plt = shadedErrorBar(dly/desFs*1000,mean(M,2),std(M,[],2),'lineProps',{'Color',clr{ii},'LineWidth',2});
    stimplts(ii) = plt.mainLine;
end
set(gca,'FontSize',16);
xlabel('Delay (ms)');
ylabel('Average TRF weight');
legend(stimplts,instrtypes);

% topo_ranges = wnd*[0 1/4 1/2 3/4];
topo_ranges = [75 125 175]; % 250 ms window
% topo_ranges = [100 800 1500]; % 2000 ms window
figure
for ii = 1:length(instrtypes),
    for jj = 1:length(topo_ranges)-1;
        subplot(length(instrtypes),length(topo_ranges)-1,...
            (ii-1)*(length(topo_ranges)-1)+jj);
        tidx = dly/desFs*1000>topo_ranges(jj) & dly/desFs*1000<topo_ranges(jj+1);
        topoplot(mean(mean(model(tidx,:,:,ii),3),1),'~/Projects/EEGanly/chanlocs.xyz');
        caxis(crange);
        title(sprintf('%d - %d ms, %s',round(topo_ranges(jj)),round(topo_ranges(jj+1)),instrtypes{ii}));
    end
end