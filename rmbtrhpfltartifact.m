function neweeg = rmbtrhpfltartifact(eeg,Fs,varargin)
% Remove artifacts at the start and end of the eeg signal caused by the
% butterworth filtering process.
% Inputs:
% - eeg = eeg signal (time x channel)
% - Fs = sampling frequency (Hz)
% Outputs:
% - neweeg = eeg with artifacts removed
% Nate Zuk (2019)

% Filter parameters
F3dB = 0.1; % lower 3dB cutoff frequency
N = 4; % 4 order filter

if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Remove artifacts produced by the filter using linear regression
dltstart = [1; zeros(size(eeg,1)-1,1)]; %starting artifact
artfct = btrfiltereeg(dltstart,Fs,'F3dB',F3dB,'N',N);
b = artfct \ eeg;
neweeg = eeg-artfct*b;

dltend = [zeros(size(eeg,1)-1,1); 1]; %ending artifact
artfct = btrfiltereeg(dltend,Fs,'F3dB',F3dB,'N',N);
b = artfct \ neweeg;
neweeg = neweeg-artfct*b;