% Demonstrate how the spectrum of a stimulus changes as it undergoes the
% processing in SplineEnvRcnstr
stimpth = '/Volumes/Untitled/NeuroMusic/';
stimfl = 'NeuroMusicEnvelopes_raw';
load([stimpth stimfl]);
s = stimset{1,4}; % take a speech example
eFs = 512;
desFs = 128;
voltlimdb = -80;

% convert to dB
voltlim = 10^(voltlimdb/20);
sdb = s;
sdb(sdb<voltlim) = voltlim;
scutoff = sdb;
sdb = 20*log10(sdb);

% size of mTRF window
tmin = 0;
tmax = 125;
% remove moving average
t = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
sm = sdb - movmean(sdb,length(t));

% downsample
sds = resample(sm,desFs,eFs);
% sds = decimate(sm,eFs/desFs);

% Compute the power spectrum of each step
[P,frq_e] = pwelch(s,10*eFs,5*eFs,[],eFs);
[Pdb,frq_db] = pwelch(sdb,10*eFs,5*eFs,[],eFs);
[Pcut,frq_cut] = pwelch(scutoff,10*eFs,5*eFs,[],eFs);
[Pm,frq_sm] = pwelch(sm,10*eFs,5*eFs,[],eFs);
[Pds,frq_ds] = pwelch(sds,10*desFs,5*desFs,[],desFs);


figure
hold on
plot(frq_e,P,'k');
plot(frq_sm,Pm,'b');
plot(frq_ds,Pds,'c');
plot(frq_db,Pdb,'r');
plot(frq_cut,Pcut,'m');
set(gca,'XScale','log','YScale','log');
xlabel('Frequency (Hz)');
ylabel('Power spectral density');
legend('Original','Removed movmean','Downsampled','dB');