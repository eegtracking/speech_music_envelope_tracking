function OrigEnvPred(sbj,stimtype,window_size,varargin)
% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes
% This cannot be used for window_size = 125 ms (because it cant be spline
% transformed) or 16000 ms (because the moving average shouldn't be
% removed)

addpath('~/Projects/EEGanly/PCA_spline_modeling/');
% addpath('~/Documents/MATLAB/ellipsoid_fit/');
addpath('~/Projects/mTRF_Zukedits/');

tmin = 0;
tmax = window_size+tmin;
npcs = 16;
desFs = 128;
mdlFs = 1000/(tmax-tmin)*16; % 2x highest frequency of interest in the model
tlims = 2;
voltlimdb = -55; % NZ (22-3-2019) changed to -55 dB, specified where silences occur
    % see also HistLevel_NeuroMusic.m
do_mov_avg = true;

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Load the general PCA transformation
D = electrode_linear_distance_matrix('chanlocs.xyz','~/Projects/EEGanly/');
cf = pca(D);
cf = [ones(size(D,1),1)/sqrt(size(D,1)) cf];

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
stimpth = '/Volumes/ZStore/NeuroMusic/';
stimfl = sprintf('NeuroMusicdBEnvelopes_128Hz_%s',stimtype);
s = load([stimpth stimfl]);
stims = s.stimset;
% Transpose all stimuli to column vectors
for ii = 1:length(stims), stims{ii} = stims{ii}'; end

% Load the model
if do_mov_avg,
    mdlpth = sprintf('~/Projects/NeuroMusic/rcnstr_res/%dms/',window_size);
else
    mdlpth = sprintf('~/Projects/NeuroMusic/rcnstr_res/%sms_naovg/',window_size);
end
mdlfl = sprintf('%s_%s_db',sbj,stimtype);
m = load([mdlpth mdlfl]);
model = m.model;

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

for ii = 1:length(stims),
    % store voltlimdb
    stims{ii}(stims{ii}<voltlimdb) = voltlimdb;
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
et = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(et));
    eeg{ii} = eeg{ii}-avg;
end
clear avg

% Transform EEG into PCs
disp('Transform to PCs...');
pc = cell(length(eeg),1);
for ii = 1:length(eeg),
    pc{ii} = eeg{ii}*cf;
end
allPC = cell2mat(pc);
allEEG = cell2mat(eeg);
% compute the % variance explained by the retained PCs
usepcs = 1:npcs+1;
transPC = allPC(:,usepcs)*cf(:,usepcs)';
SSE_red = sum(sum((transPC-allEEG).^2));
SST = sum(sum((allEEG).^2));
vexp_red = 1-SSE_red/SST;
fprintf('Variance explained by retained PCs: %.2f%%\n',vexp_red*100);
for ii = 1:length(pc),
    pc{ii} = pc{ii}(:,usepcs);
end
clear allPC allEEG eeg transPC

% Downsample to EEG and stimulus by 4x, to make it more managable for the
% computer
for ii = 1:length(pc),
    pc{ii} = resample(pc{ii},desFs,eFs);
    pc{ii} = zscore(pc{ii}); % zscore after resampling, because this can change the variance
end

% Compute the downsampling rate, for the spline transform
ds = desFs/mdlFs;

disp('Computing the envelope reconstruction...');
pred = cell(length(stims),1);
r = NaN(length(stims),1);
mse = NaN(length(stims),1);
if mdlFs<desFs,
    disp('(Using spline interpolation...)');
    for ii = 1:length(stims),
        [pred{ii},r(ii),~,mse(ii)] = mTRFpredict_spline(stims{ii},pc{ii},model,desFs,ds,map,tmin,tmax,tlims);
    end
elseif mdlFs==desFs,
    for ii = 1:length(stims),
        [pred{ii},r(ii),~,mse(ii)] = mTRFpredict(stims{ii},pc{ii},model,desFs,map,tmin,tmax,tlims);
    end
else
    error('Model sampling frequency must be equal to or less than initial sampling frequency');
end

if do_mov_avg, 
    svpth = sprintf('~/Projects/NeuroMusic/pred_res/%dms/',round(tmax-tmin));
else
    svpth = sprintf('~/Projects/NeuroMusic/pred_res/%dms_noavg/',round(tmax-tmin));
end
svfl = sprintf('%s_%s',sbj,stimtype);
save([svpth svfl],'r','mse','pred','useidx');