% Plot the reconstruction accuracy before and average removing the moving
% average of the stimulus

sbj = 'AB';
stimtypes = {'rock','classical','vocals','speech'};
% stimtypes = {'vocals','speech'};
wnd = 250;
nfolds = 10;
niter = 10;
% npcs = 127;
lambdas = 10.^(0:8);

% Load the result with the moving average removed
mse_spl = NaN(nfolds,length(stimtypes));
mse_orig = NaN(nfolds,length(stimtypes));
dp_spl = NaN(length(stimtypes),1);
dp_orig = NaN(length(stimtypes),1);
allmse_spl = NaN(nfolds,length(lambdas),length(stimtypes));
allnmse_spl = NaN(niter,length(stimtypes));
allmse_orig = NaN(nfolds,length(lambdas),length(stimtypes));
allnmse_orig = NaN(niter,length(stimtypes));
for ii = 1:length(stimtypes),
    d_spl = load(sprintf('~/Projects/NeuroMusic/reg_effects_indivpca/%dms/%s_%s_reg',wnd,sbj,stimtypes{ii}));
%     optidx = find(mean(d_spl.mse)==min(mean(d_spl.mse)));
%     r_spl(:,ii) = d_spl.r(:,optidx);
%     mse_spl(:,ii) = d_spl.mse(:,1)-d_spl.mse(:,end);
    mse_spl(:,ii) = d_spl.mse(:,1);
    allmse_spl(:,:,ii) = d_spl.mse(:,2:end)-d_spl.mse(:,end)*ones(1,length(lambdas));
    allnmse_spl(:,ii) = d_spl.nullmse;
    dp_spl(ii) = (mean(d_spl.mse(:,1))-mean(d_spl.nullmse))/sqrt(0.5*(std(d_spl.mse(:,1))^2+std(d_spl.nullmse)^2));
%     fprintf('Spline: lambda = %d\n',lambdas(optidx));
    d_orig = load(sprintf('~/Projects/NeuroMusic/orig_rcnstr_res/%dms/%s_%s_db',wnd,sbj,stimtypes{ii}));
    % Use the lambda that minimizes the mean mse
    optidx = find(mean(d_orig.mse)==min(mean(d_orig.mse)));
%     mse_orig(:,ii) = d_orig.mse(:,optidx)-d_orig.mse(:,end);
    mse_orig(:,ii) = d_orig.mse(:,optidx);
    allmse_orig(:,:,ii) = d_orig.mse-d_orig.mse(:,end)*ones(1,length(lambdas));
    allnmse_orig(:,ii) = d_orig.nullmse;
    dp_orig(ii) = (mean(d_orig.mse(:,optidx))-mean(d_orig.nullmse))/sqrt(0.5*(std(d_orig.mse(:,optidx))^2+std(d_orig.nullmse)^2));
    fprintf('Orig: lambda = %d\n',d_orig.lambdas(optidx));
end

MSE_SPL = reshape(mse_spl,[nfolds*length(stimtypes) 1]);
MSE_ORIG = reshape(mse_orig,[nfolds*length(stimtypes) 1]);
lbl = reshape(ones(nfolds,1)*[1:length(stimtypes)],[nfolds*length(stimtypes) 1]);
cmap = [1 0 0; 0 0 0];
[median_handles,cell_midline] = dot_median_plot(lbl,[MSE_SPL MSE_ORIG],cmap,'tot_span',0.6);
set(gcf,'Position',[50 200 650 500]);
hold on
for ii = 1:length(stimtypes)
    % spl
    errorbar(cell_midline(1)+ii,mean(allnmse_spl(:,ii)),std(allnmse_spl(:,ii)),'o',...
        'Color',cmap(1,:),'LineWidth',2,'MarkerSize',12);
    % orig
    errorbar(cell_midline(2)+ii,mean(allnmse_orig(:,ii)),std(allnmse_orig(:,ii)),'o',...
        'Color',cmap(2,:),'LineWidth',2,'MarkerSize',12);
end
set(gca,'XTickLabel',stimtypes);
ylabel('Reconstruction accuracy, MSE');
title(sprintf('%s, %d ms',sbj,wnd));
legend(median_handles,{'PCA/Spline','Original'})

% Plot the r values as a function of lambda
figure
set(gcf,'Position',[600 200 650 500]);
nrows = ceil(length(stimtypes)/2);
r_plts = NaN(2,1);
for ii = 1:length(stimtypes),
    subplot(2,ceil(length(stimtypes)/2),ii);
    hold on
    orig_pl = plot(lambdas,allmse_orig(:,:,ii),'k');
    r_plts(1) = orig_pl(1);
    spl_pl = plot(lambdas,allmse_spl(:,:,ii),'r');
    r_plts(2) = spl_pl(1);
    set(gca,'XScale','log','FontSize',16);
    xlabel('\lambda');
    ylabel('MSE');
    tle = sprintf('%s, %s, %d ms',sbj,stimtypes{ii},wnd);
    title(tle);
end
legend(r_plts,'Original','PCA/Spline');