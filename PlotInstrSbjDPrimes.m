% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JS'};
instrtype = {'vocals','guitar','bass','drums'};
clr = {[0.7 0 0.7],[1 0 0],[0 0.7 0],[0 0 1]};
wnd = [250 2000];
nfolds = 10; % number of folds for CV
niter = 50; % number of iterations to get the null distribution
respth = '~/Projects/NeuroMusic/rcnstr_res/';
instrpth = '~/Projects/NeuroMusic/instr_rcnstr_res/';

% Load the moving-average-removed results
dp = NaN(length(wnd),length(instrtype),length(sbjs));
for s = 1:length(sbjs),
    for ii = 1:length(wnd),
        for jj = 1:length(instrtype),
            fl = sprintf('%dms/%s_%s_db',round(wnd(ii)),sbjs{s},instrtype{jj});
            d = load([instrpth fl]);
            dp(ii,jj,s) = (mean(d.r)-mean(d.nullr))/sqrt(0.5*(var(d.r)+var(d.nullr)));
            disp(fl);
        end
    end
end

dpfull = NaN(length(wnd),length(sbjs));
for s = 1:length(sbjs),
    for ii = 1:length(wnd),
        fl = sprintf('%dms/%s_%s_db',round(wnd(ii)),sbjs{s},'rock');
        d = load([respth fl]);
        dpfull(ii,s) = (mean(d.r)-mean(d.nullr))/sqrt(0.5*(var(d.r)+var(d.nullr)));
        disp(fl);
    end
end

% d-prime
figure
hold on
% moving averaged
dp_plts = NaN(length(instrtype)+1,1);
for jj = 1:length(instrtype),
    dp_plts(jj) = errorbar(wnd,mean(dp(:,jj,:),3),std(dp(:,jj,:),[],3),'Color',clr{jj},'LineWidth',2);
end
dp_plts(length(instrtype)+1) = errorbar(wnd,mean(dpfull,2),std(dpfull,[],2),'k--','LineWidth',2);
set(gca,'XScale','log','FontSize',16,'XTick',unique(wnd),'XLim',[min(wnd)/2 max(wnd)*2]);
xlabel('Window size (ms)');
ylabel('d-prime');
legend(dp_plts,[instrtype {'full'}]);

% Examine if there are significant differences across instruments
panova = NaN(length(wnd),1);
stanova = cell(length(wnd),1);
cmp = cell(length(wnd),1);
for ii = 1:length(wnd),
    [panova(ii),~,stanova{ii}] = anova1(squeeze(dp(ii,:,:))',[],'off');
    cmp{ii} = multcompare(stanova{ii},'display','off','ctype','bonferroni');
end

% Examine if the change in dp is significant between window sizes
pt = NaN(length(instrtype),1);
stt = cell(length(instrtype),1);
diffdp = squeeze(diff(dp));
for jj = 1:length(instrtype),
    [~,pt(jj),~,stt{jj}] = ttest(diffdp(jj,:));
end