% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbj = 'EJ';
instrtype = {'vocals','guitar','bass','drums'};
clr = {'r','g','c','b'};
wnd = [250 2000];
nfolds = 10;
niter = 50;
respth = '~/Projects/NeuroMusic/rcnstr_res/';
instrpth = '~/Projects/NeuroMusic/instr_rcnstr_res/';

% Load the instrument
r = NaN(nfolds,length(wnd),length(instrtype));
nr = NaN(niter,length(wnd),length(instrtype));
dp = NaN(length(wnd),length(instrtype));
for ii = 1:length(wnd)
    for jj = 1:length(instrtype),
        fl = sprintf('%dms/%s_%s_db',round(wnd(ii)),sbj,instrtype{jj});
        d = load([instrpth fl]);
        r(:,ii,jj) = d.r;
        nr(:,ii,jj) = d.nullr;
        dp(ii,jj) = (mean(d.r)-mean(d.nullr))/sqrt(0.5*(var(d.r)+var(d.nullr)));
        disp(fl)
    end
end

% Load the full envelope result
rfull = NaN(nfolds,length(wnd));
nrfull = NaN(niter,length(wnd));
dpfull = NaN(length(wnd),1);
for ii = 1:length(wnd),
    fl = sprintf('%dms/%s_%s_db',round(wnd(ii)),sbj,'rock');
    d = load([respth fl]);
    rfull(:,ii) = d.r;
    nrfull(:,ii) = d.nullr;
    dpfull(ii) = (mean(d.r)-mean(d.nullr))/sqrt(0.5*(var(d.r)+var(d.nullr)));
    disp(fl)
end

figure
hold on
for jj = 1:length(instrtype),
    plot(wnd,dp(:,jj),[clr{jj} '-'],'LineWidth',2);
end
% plot(1:length(instrtype)+1,[dp dpfull],'k.','MarkerSize',16);
plot(wnd,dpfull,'k--','LineWidth',2);
set(gca,'FontSize',16,'XScale','log','XTick',wnd,'XLim',[min(wnd)/2 max(wnd)*2]);
ylabel('d-prime');
legend([instrtype {'full'}]);
title(sbj);