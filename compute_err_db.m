function [dberr,frq,dbstim] = compute_err_db(RSDL,STIMS,Fs,varargin)
% Compute the ratio of the power of the reconstruction residuals to the 
% stimulus power as a function of frequency. This can be used to identify
% specific frequency ranges that are reconstruction best. Default is to
% plot the result (set plotting=0 otherwise).
% Inputs:
% - rsdl = cell array of residuals
% - stims = cell array of stimulus waveforms
% - Fs = sampling frequency
% Outputs:
% - dberr = ratio of residuals to stimulus, in dB
% - frq = frequency array corresponding to the dberr array
% - dbstim = db of stimulus
% Nate Zuk (2018)

% Initial variables
nfrqbins = Fs/2; % number of frequency bins
septrials = false; % flag if spectra should be computed from separate trials,
    % instead of averaging residuals across trials first
plotting = true; % flag if things are being plotted
frqrange = [0.01 Fs/2]; % range of frequencies at which to compute the dB ratio (in Hz)

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Make sure all of the residual and stimulus arrays are the same length
rsdllen = cellfun(@(x) length(x),RSDL);
stimlen = cellfun(@(x) length(x),STIMS);
if sum(rsdllen~=stimlen)>0,
    error('All residual and stimulus arrays must be the same length');
end

% Check if there are the same number of trials
if length(RSDL)~=length(STIMS),
    error('Residual and stimulus arrays must contain the same number of trials');
end
ntr = length(RSDL);

% Crop the arrays to the shortest length
L = min(rsdllen);
rsdl = NaN(L,ntr);
stim = NaN(L,ntr);
for ii = 1:ntr,
    rsdl(:,ii) = RSDL{ii}(1:L);
    stim(:,ii) = STIMS{ii}(1:L);
end

% Average signals in time
if ~septrials,
    mr = mean(rsdl,2);
    ms = mean(stim,2);
else
    mr = rsdl;
    ms = stim;
end

% Center mr and ms
mr = mr - ones(size(mr,1),1)*mean(mr);
ms = ms - ones(size(ms,1),1)*mean(ms);

% Compute the difference in power, in dB
% fulldbstim = 10*log10(abs(fft(ms)).^2);
% fulldberr = 10*log10(abs(fft(mr)).^2)-10*log10(abs(fft(ms)).^2);
MR = fft(mr);
MS = fft(ms);
fullfrq = (0:L-1)/L*Fs; % frequency array with all sampling points

% Compute logarithmically spaced bins between 0 and Fs/2 and average the db
% error within +/-0.5 times the center of the bin
frq = logspace(log10(frqrange(1)),log10(frqrange(2)),nfrqbins);
% dfrq = log10(frq(2))-log10(frq(1)); % find the log distance between neighboring frequencies
dberr = NaN(length(frq),size(mr,2));
dbstim = NaN(length(frq),size(ms,2));
for f = 1:length(frq),
    % Identify the frequency indexes to include
%     fidx = fullfrq>=frq(f)/2 & fullfrq<=frq(f)*3/2;
%     fidx = fullfrq>=frq(f)*10^(-dfrq) & fullfrq<=frq(f)*10^(dfrq);
    fidx = fullfrq>=frq(f)*10^(-0.1) & fullfrq<=frq(f)*10^(0.1);
        % bin edges are spaced halfway between neighboring centers, in log
        % space (NZ, 6-3-2019)
    fftmr = sum(abs(MR(fidx,:)).^2);
    fftms = sum(abs(MS(fidx,:)).^2);
    dberr(f,:) = 10*log10(fftmr)-10*log10(fftms);
%     dberr(f,:) = 10*log10(fftmr);
    dbstim(f,:) = 10*log10(fftms);
%     dberr(f,:) = mean(fulldberr(fidx,:));
%     dbstim(f,:) = mean(fulldbstim(fidx,:));
end

% Plotting
if plotting,
    figure
    plot(frq,dberr,'k','LineWidth',2);
    set(gca,'XScale','log');
    xlabel('Frequency (Hz)');
    ylabel('Ratio of residual to stimulus power');
end