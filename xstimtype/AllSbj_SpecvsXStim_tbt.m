% Get the reconstruction accuracies for stimulus-specific and each of the
% cross-stimulus models (found in xstim_res). Test to see how the
% trial-by-trial performance fo the cross-stimulus models compares to the
% stimulus-specific models. Plot the median values on a 4x4 grid (diagonal
% will be zero).
%%% Plot the TRF models of each type of stimulus

addpath('~/Documents/MATLAB/shadedErrorBar/');
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
stimtype = {'rock','classical','vocals','speech'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
wnd = [250 2000];
maxtr = 7;
specpth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';
xstimpth = '/Volumes/ZStore/NeuroMusic/xstim_res/';

%% Load the reconstruction accuracies

r_sp = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
% (dimensions for r_xstim are: trs x wnds x test stim x train stim x sbjs)
r_xstim = NaN(maxtr,length(wnd),length(stimtype),length(stimtype),length(sbjs));
for s = 1:length(sbjs)
    for ii = 1:length(wnd)
        for jj = 1:length(stimtype)
            % stimulus-specific
            fl = sprintf('%s_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d = load([specpth fl]);
            ntr = size(d.r,1);
            r_sp(1:ntr,ii,jj,s) = reshape(d.r,[ntr 1]);
            disp(fl);
            % cross-stimulus
            for kk = 1:length(stimtype) % iterate through training stimulus types
                if jj~=kk % ...as long as the training stimulus types don't match testing
                    fl_xst = sprintf('%s_trn%s_tst%s_%dms',...
                        sbjs{s},stimtype{kk},stimtype{jj},round(wnd(ii)));
                    d_xst = load([xstimpth fl_xst]);
                    r_xstim(1:ntr,ii,jj,kk,s) = reshape(d_xst.r,[ntr 1]);
                    disp(fl_xst)
                end
            end
        end
    end
end

% Title labels for each grid plot
lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*8);
end

%% Plot difference between specific and xstim

% Compute the differences between the specific and xstim accuracies, and
% rearrange so trials x subjects are along columns
DIFF_R = NaN(maxtr,length(wnd),length(stimtype),length(stimtype),length(sbjs));
for jj = 1:length(stimtype) % testing stim
    for kk = 1:length(stimtype) % training stim
        if jj~=kk
            DIFF_R(:,:,jj,kk,:) = squeeze(r_sp(:,:,jj,:)) - squeeze(r_xstim(:,:,jj,kk,:));
        else % if the training and testing match, set the value to zero 
            % (training and testing on same stim type is the specific
            % model)
            DIFF_R(:,:,jj,kk,:) = 0;
        end
    end
end
% rearrange the differences so trials x subjects are along columns
DIFF_R = permute(DIFF_R,[1 5 3 4 2]); % trs x sbjs x tst x trn x wnd
DIFF_R = reshape(DIFF_R,[maxtr*length(sbjs) length(stimtype) length(stimtype) length(wnd)]);

% Go through each window size, and plot the median difference across all
% trials x subjects (note when train==test, the values are all NaN)
figure
set(gcf,'Position',[100 350 900 300]);
for n = 1:length(wnd)
    subplot(1,length(wnd),n);
    md = squeeze(median(DIFF_R(:,:,:,n),'omitnan'));
    imagesc(md);
    axis('square');
    colorbar;
    set(gca,'FontSize',14,'XTick',1:length(stimtype),'XTickLabel',stimtype,...
        'YTick',1:length(stimtype),'YTickLabel',stimtype);
    xlabel('Train');
    ylabel('Test');
    title(sprintf('%d ms',wnd(n)));
end

% Test if any of these time bins are significantly different than 0,
% cumulatively test across subjects
% Use wilcoxon signed-rank
p_diffr = NaN(length(stimtype),length(stimtype),length(wnd));
z_diffr = NaN(length(stimtype),length(stimtype),length(wnd));
for w = 1:length(wnd)
    for jj = 1:length(stimtype) % testing
        for kk = 1:length(stimtype) % training
            if jj~=kk
                [p_diffr(jj,kk,w),~,st] = signrank(DIFF_R(:,jj,kk,w),0,'tail','both');
                z_diffr(jj,kk,w) = st.zval;
            end
        end
    end
end