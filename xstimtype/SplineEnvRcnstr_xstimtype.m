function SplineEnvRcnstr_xstimtype(sbj,train_stimtype,test_stimtype,window_size,varargin)
% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes
% This cannot be used for window_size = 125 ms (because it cant be spline
% transformed) or 16000 ms (because the moving average shouldn't be
% removed)
%%% NZ - PCA-transform each dataset individually, aka. for each particular
%%% window size and stimulus type.  This would be optimal.
%%% NZ (28-1-2020) -- Andy mentioned that pca should be applied to only the
%%% training data, and then the testing data can be transformed with the
%%% eigenvectors from the training set.  The testing data should be
%%% COMPLETELY separate from all transformation applied to get the model on
%%% the trainin data.
%%% NZ (28-1-2020) -- I expect all of the results I have seen so far will
%%% persist doing trial-by-trial testing, rather than a random sampling of
%%% the data across trials. While the performance for music may still be
%%% smaller, the methods will be easier to explain.
%%% NZ (9-2-2020) -- I ran the PCA/spline optimization on the Natural
%%% Speech dataset, and found that the optimum r values for a 500 ms window
%%% for dB envelope reconstruction were at 32 Hz splines (a 3-octave
%%% window) and 64 principal components. I have chaged the parameters used
%%% here accordingly
%%% NZ (31-3-2021) -- This is a modification of
%%% SplineEnvRcnstr_trialbytrial, where instead I am training on one
%%% stimulus type and testing on another. The train_stimtype and
%%% test_stimtype should be specified separately.

% addpath('~/mTRF_Zukedits/');
addpath('..'); % add the upper directory

tmin = 0;
tmax = window_size+tmin;
map = -1;
desFs = 512;
mdlFs = 1000/(tmax-tmin)*16; % 3x highest frequency (32 Hz splines was optimal for 
    % 500 ms window to reconstruct dB envelope for Natural Speech)
tlims = 16;
npcs = 64; % 64 principal components was optimized on the Natural Speech dataset
    % using a 500 ms window to reconstruct dB envelope
do_mov_avg = true;

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Get the stimulus codes
%%% Concatenate stimcodes for both types together. The data will be
%%% separated into training and testing later
train_stimcodes = getstimcodes(train_stimtype);
test_stimcodes = getstimcodes(test_stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,[train_stimcodes test_stimcodes],sbj);

% Load the stimuli
disp('Loading stimuli...');
stimpth = '~/NeuroMusic/stims/';
% Training stimuli
stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,train_stimtype);
    %%% I will need to change this to be 512 Hz envelopes, no downsampling
s = load([stimpth stimfl]);
stims = s.stimset;
% Transpose all stimuli to column vectors
for ii = 1:length(stims), stims{ii} = stims{ii}'; end
% Testing stimuli
teststimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,test_stimtype);
s = load([stimpth teststimfl]);
teststims = s.stimset;
for ii = 1:length(teststims), teststims{ii} = teststims{ii}'; end
% concatenate training and testing stimuli
stims = [stims; teststims];

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg); 
    % this will be used later to identify training and testing stimcodes
stims = stims(useidx);
eeg = eeg(useidx);

% Preprocessing of the EEG and stimuli
% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
    if eFs~=desFs
        eeg{ii} = resample(eeg{ii},desFs,eFs);
    end
end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims)
    %%% NZ (19-1-2020), don't skip silences, could have had strange effects
    %%% on null distribution, overenflated tracking at low frequencies
    tidx{ii} = ones(length(stims{ii}),1);
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(st));
    eeg{ii} = eeg{ii}-avg;
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        % NZ (15-1-2020), don't remove silences when doing moving average
        s_on = stims{ii};
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii} = s_on;
    end
end
clear avg s_on

% Check if the eegs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,eeg);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(eeg{n},1)); end
    end
end

% Compute the downsampling rate, for the spline transform
ds = desFs/mdlFs;

% Now go back to the stimulus codes, and identify the trials for training
% and testing
allstimcodes = [train_stimcodes test_stimcodes];
usedstimcodes = allstimcodes(useidx); % retain only the ones that were run
% get the trials that are part of the training set
traintrs = find(ismember(usedstimcodes,train_stimcodes));
% get the trials that are part of the testing set
testtrs = find(ismember(usedstimcodes,test_stimcodes));

%% Training
fprintf('-- Training on %s, %d trials --\n',train_stimtype,length(traintrs));
traintm = tic;
% transform training trials to principal components
% the same transformation will be re-used on the testing data
disp('Transform to PCs...');
allEEG = cell2mat(eeg(traintrs));
cf = pca(allEEG); % get the principal components of the EEG
pc = cell(length(traintrs),1);
% npc = size(cf{n},2);
% v = NaN(npcs,length(traintrs)); % to store the original PC variances,
%     for appopriately reweighting the PCs in the model
for ii = 1:length(traintrs)
    tridx = traintrs(ii);
%     eeg{tridx} = detrend(eeg{tridx},0); % center each channel
    pc{ii} = eeg{traintrs(tridx)}*cf(:,1:npcs); % transform to pcs
%     v(:,ii) = var(pc{ii}); % get the variance for all components
    pc{ii} = zscore(pc{ii}); % normalize to variance of 1
end
% lenpcs = cellfun(@(x) size(x,1),pc);
% weighted average of variances across pcs
% var_pc(1:npcs,n) = sum(v.*(ones(npcs,1)*lenpcs'),2)/sum(lenpcs);
clear allEEG

disp('(Using spline interpolation...)');
model = mTRFtrain_spline(stims(traintrs),pc,desFs,ds,map,tmin,tmax,0,tidx(traintrs));

fprintf('Completed training @ %.3f s\n',toc(traintm));

%% Testing

disp('-- Now testing the model...');
r = NaN(length(testtrs),1);
mse = NaN(length(testtrs),1);
for n = 1:length(testtrs)
    fprintf('** Trial %d/%d **\n',n,length(testtrs));
    trtm = tic;

    % transform the testing trial into pc
    testpc = zscore(eeg{testtrs(n)}*cf(:,1:npcs));
    [~,r(n),~,mse(n)] = mTRFpredict_spline(stims{testtrs(n)},testpc,model,desFs,...
        ds,map,tmin,tmax,tidx(testtrs(n)));

    fprintf('** Completed trial %d/%d @ %.3f s\n',n,length(testtrs),toc(trtm));
end

%% Save results
svpth = '~/NeuroMusic/xstimtype/xstim_res/';
svfl = sprintf('%s_trn%s_tst%s_%dms',sbj,train_stimtype,test_stimtype,round(tmax-tmin));
save([svpth svfl],'r','mse','mdlFs','desFs','ds','tmin','tmax','tlims','tidx','npcs',...
    'usedstimcodes','traintrs','testtrs');
