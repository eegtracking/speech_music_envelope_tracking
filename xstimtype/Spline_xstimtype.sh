#!/bin/bash
#SBATCH --time=3-00:00:00
#SBATCH -c 8
#SBATCH -n 1
#SBATCH --mem=64GB

sbj=$1
trainstim=$2
teststim=$3
wnd=(250 2000)

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(8); sbj='${sbj}'; train_stimtype='${trainstim}'; test_stimtype='${teststim}'; wnd=${wnd[$SLURM_ARRAY_TASK_ID]}; SplineEnvRcnstr_xstimtype(sbj,train_stimtype,test_stimtype,wnd);"
rm -rf /scratch/$USER/$SLURM_JOB_ID
