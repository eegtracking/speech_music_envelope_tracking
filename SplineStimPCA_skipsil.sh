#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH -c 8
#SBATCH -n 1
#SBATCH --mem=32GB

sbj=$1
stim=$2
wnd=(62.5 125 250 500 1000 2000 4000 8000 16000 32000)
dbthres=$3

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(8); sbj='${sbj}'; stimtype='${stim}'; wnd=${wnd[$SLURM_ARRAY_TASK_ID]}; SplineEnvRcnstr_pcabystim(sbj,stimtype,wnd,'voltlimdb',${dbthres});"
rm -rf /scratch/$USER/$SLURM_JOB_ID
