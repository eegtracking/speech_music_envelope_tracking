% Plot the reconstruction accuracy before and average removing the moving
% average of the stimulus

sbj = 'AB';
stimtypes = {'rock','classical','vocals','speech'};
% stimtypes = {'vocals','speech'};
wnd = 500;
nfolds = 10;
niter = 10;
% npcs = 127;
lambdas = 10.^(0:8);

% Load the result with the moving average removed
r_spl = NaN(nfolds,length(stimtypes));
r_orig = NaN(nfolds,length(stimtypes));
dp_spl = NaN(length(stimtypes),1);
dp_orig = NaN(length(stimtypes),1);
allr_spl = NaN(nfolds,length(lambdas),length(stimtypes));
allnr_spl = NaN(niter,length(stimtypes));
allr_orig = NaN(nfolds,length(lambdas),length(stimtypes));
allnr_orig = NaN(niter,length(stimtypes));
for ii = 1:length(stimtypes),
    d_spl = load(sprintf('~/Projects/NeuroMusic/reg_effects_indivpca/%dms/%s_%s_reg',wnd,sbj,stimtypes{ii}));
%     optidx = find(mean(d_spl.mse)==min(mean(d_spl.mse)));
%     r_spl(:,ii) = d_spl.r(:,optidx);
    r_spl(:,ii) = d_spl.r(:,1);
    allr_spl(:,:,ii) = d_spl.r(:,2:end);
    allnr_spl(:,ii) = d_spl.nullr;
    dp_spl(ii) = (mean(d_spl.r(:,1))-mean(d_spl.nullr))/sqrt(0.5*(std(d_spl.r(:,1))^2+std(d_spl.nullr)^2));
%     fprintf('Spline: lambda = %d\n',lambdas(optidx));
    d_orig = load(sprintf('~/Projects/NeuroMusic/orig_rcnstr_res/%dms/%s_%s_db',wnd,sbj,stimtypes{ii}));
    % Use the lambda that minimizes the mean mse
    optidx = find(mean(d_orig.r)==max(mean(d_orig.r)));
    r_orig(:,ii) = d_orig.r(:,optidx);
    allr_orig(:,:,ii) = d_orig.r;
    allnr_orig(:,ii) = d_orig.nullr;
    dp_orig(ii) = (mean(d_orig.r(:,optidx))-mean(d_orig.nullr))/sqrt(0.5*(std(d_orig.r(:,optidx))^2+std(d_orig.nullr)^2));
    fprintf('Orig: lambda = %d\n',d_orig.lambdas(optidx));
end

R_SPL = reshape(r_spl,[nfolds*length(stimtypes) 1]);
R_ORIG = reshape(r_orig,[nfolds*length(stimtypes) 1]);
lbl = reshape(ones(nfolds,1)*[1:length(stimtypes)],[nfolds*length(stimtypes) 1]);
cmap = [1 0 0; 0 0 0];
[median_handles,cell_midline] = dot_median_plot(lbl,[R_SPL R_ORIG],cmap,'tot_span',0.6);
set(gcf,'Position',[50 200 650 500]);
hold on
for ii = 1:length(stimtypes)
    % spl
    errorbar(cell_midline(1)+ii,mean(allnr_spl(:,ii)),std(allnr_spl(:,ii)),'o',...
        'Color',cmap(1,:),'LineWidth',2,'MarkerSize',12);
    % orig
    errorbar(cell_midline(2)+ii,mean(allnr_orig(:,ii)),std(allnr_orig(:,ii)),'o',...
        'Color',cmap(2,:),'LineWidth',2,'MarkerSize',12);
end
set(gca,'XTickLabel',stimtypes);
ylabel('Reconstruction accuracy, r');
title(sprintf('%s, %d ms',sbj,wnd));
legend(median_handles,{'PCA/Spline','Original'})

% Plot the r values as a function of lambda
figure
set(gcf,'Position',[600 200 650 500]);
nrows = ceil(length(stimtypes)/2);
r_plts = NaN(2,1);
for ii = 1:length(stimtypes),
    subplot(2,ceil(length(stimtypes)/2),ii);
    hold on
    orig_pl = plot(lambdas,allr_orig(:,:,ii),'k');
    r_plts(1) = orig_pl(1);
    spl_pl = plot(lambdas,allr_spl(:,:,ii),'r');
    r_plts(2) = spl_pl(1);
    set(gca,'XScale','log','FontSize',16);
    xlabel('\lambda');
    ylabel('r');
    tle = sprintf('%s, %s, %d ms',sbj,stimtypes{ii},wnd);
    title(tle);
end
legend(r_plts,'Original','PCA/Spline');