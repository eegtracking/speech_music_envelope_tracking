#!/bin/bash
#SBATCH --time=2-00:00:00
#SBATCH -c 8
#SBATCH -n 1
#SBATCH --mem=32GB

sbj=$1
stim=$2
delay=$3
wnd=(31.25 62.5 125 250 500 1000 2000 4000 8000 16000)

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(8); sbj='${sbj}'; stimtype='${stim}'; delay=${delay}; wnd=${wnd[$SLURM_ARRAY_TASK_ID]}; SplineEnvRcnstr_spatialrmv(sbj,stimtype,wnd,delay);"
rm -rf /scratch/$USER/$SLURM_JOB_ID
