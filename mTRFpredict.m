function [pred,r,p,mse] = mTRFpredict(stim,resp,model,fs,map,tmin,tmax,tlims)
%mTRFpredict mTRF Toolbox prediction function.
%   PRED = MTRFPREDICT(STIM,RESP,MODEL,FS,MAP,TMIN,TMAX,C) performs a
%   convolution of the stimulus property STIM or the neural response data
%   RESP with their linear mapping function MODEL to solve for the
%   prediction PRED. Pass in MAP==1 to predict RESP or MAP==-1 to predict
%   STIM. The sampling frequency FS should be defined in Hertz and the time
%   lags should be set in milliseconds between TMIN and TMAX. The
%   regression constant C absorbs any bias in MODEL.
%   NZ (13-3-2019): Includes spline transform
%
%   [...,R,P,MSE] = MTRFPREDICT(...) also returns the correlation
%   coefficients R between the original and predicted values, the
%   corresponding p-values P and the mean squared errors MSE.
%   
%   NZ edits (25-1-2019): 
%   -- the design matrix X and output matrix Y are z-scored (you must use
%      a model that has been computed from mTRFtrain or mTRFcrossval after
%      z-scoring)
%
%   Inputs:
%   stim   - stimulus property (time by features)
%   resp   - neural response data (time by channels)
%   model  - linear mapping function (MAP==1: feats by lags by chans,
%            MAP==-1: chans by lags by feats)
%   fs     - sampling frequency (Hz)
%   map    - mapping direction (forward==1, backward==-1)
%   tmin   - minimum time lag (ms)
%   tmax   - maximum time lag (ms)
%   c      - regression constant
%
%   Outputs:
%   pred   - prediction (MAP==1: time by chans, MAP==-1: time by feats)
%   r      - correlation coefficients
%   p      - p-values of the correlations
%   mse    - mean squared errors
%
%   See README for examples of use.
%
%   See also LAGGEN MTRFTRAIN MTRFCROSSVAL MTRFMULTICROSSVAL.

%   Author: Michael Crosse, Giovanni Di Liberto
%   Lalor Lab, Trinity College Dublin, IRELAND
%   Email: edmundlalor@gmail.com
%   Website: www.lalorlab.net
%   April 2014; Last revision: Jan 8, 2016

if nargin<8, tlims = []; end

% Define x and y
if tmin > tmax
    error('Value of TMIN must be < TMAX')
end
if map == 1
    x = stim;
    y = resp;
elseif map == -1
    x = resp;
    y = stim;
    [tmin,tmax] = deal(tmax,tmin);
else
    error('Value of MAP must be 1 (forward) or -1 (backward)')
end

% Convert time lags to samples
tmin = floor(tmin/1e3*fs*map);
tmax = ceil(tmax/1e3*fs*map);

% Generate lag matrix
disp('Creating X and y matrices...');
mat_tm = tic;
% tmpx = lagGen(x,tmin:tmax);
X = lagGen(x,tmin:tmax);
% Set X and y to the same length
minlen = min([size(X,1) size(y,1)]);
X = X(1:minlen,:);
y = y(1:minlen,:);
% Remove time indexes, if specified
if iscell(tlims),
    tinds = tlims{1};
else
    tinds = usetinds(tlims,fs,minlen);
end
X = zscore(X(tinds,:)); % zscore the columns of X
y = zscore(y(tinds,:)); % zscore the output matrix Y
fprintf('Completed @ %.3f s\n',toc(mat_tm));

clear x splx

% Calculate prediction using the spline model
pred = X*model;

% Calculate accuracy
if ~isempty(y)
    r = zeros(1,size(y,2));
    p = zeros(1,size(y,2));
    mse = zeros(1,size(y,2));
    for i = 1:size(y,2)
        [r(i),p(i)] = corr(y(:,i),pred(:,i));
        mse(i) = mean((y(:,i)-pred(:,i)).^2);
    end
end

end
