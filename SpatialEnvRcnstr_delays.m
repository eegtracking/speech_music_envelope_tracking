function SpatialEnvRcnstr_delays(sbj,stimtype,window_size,varargin)
% Load neuromusic data, transform the EEG into principal components, and
% find the optimal spatial weighting of the EEG channels to best
% reconstruct the stimulus envelope
%%% NZ - PCA-transform each dataset individually, aka. for each particular
%%% window size and stimulus type.  This would be optimal.

% addpath('~/mTRF_Zukedits/');

tmin = 0;
tmax = window_size+tmin;
    % this is for getting the window for removing low frequencies
maxdly = tmax; % maximum delay to examine
map = -1;
desFs = 512;
tlims = 16;
nfolds = 10;
nreps = 5; % number of times to repeat mTRFcrossval (use a new resampling of the data)
do_mov_avg = true;

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
stimpth = '~/NeuroMusic/stims/';
stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtype);
    %%% I will need to change this to be 512 Hz envelopes, no downsampling
s = load([stimpth stimfl]);
stims = s.stimset;
% Transpose all stimuli to column vectors
for ii = 1:length(stims), stims{ii} = stims{ii}'; end
% clear s

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
    if eFs~=desFs
        eeg{ii} = resample(eeg{ii},desFs,eFs);
    end
end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims),
    %%% NZ (19-1-2020), don't skip silences, could have had strange effects
    %%% on null distribution, overenflated tracking at low frequencies
    tidx{ii} = ones(length(stims{ii}),1);
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(st));
    eeg{ii} = eeg{ii}-avg;
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        % NZ (15-1-2020), don't remove silences when doing moving average
        s_on = stims{ii};
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii} = s_on;
    end
end
clear avg s_on

% Transform EEG into PCs
disp('Transform to PCs...');
allEEG = cell2mat(eeg);
cf = pca(allEEG); % get the principal components of the EEG
pc = cell(length(eeg),1);
var_pc = NaN(length(eeg),size(cf,2)); % to store the original PC variances,
    % for appopriately reweighting the PCs in the model
for ii = 1:length(eeg)
%     eeg{ii} = detrend(eeg{ii},0); % center each channel
    pc{ii} = eeg{ii}*cf; % transform to pcs
    var_pc(ii,:) = var(pc{ii});
    pc{ii} = zscore(pc{ii}); % normalize to variance of 1
end
clear allEEG eeg

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,pc);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(pc{n},1)); end
    end
end

% Delay the stimulus forward up to dlymax
dly = floor(tmin/1000*desFs):ceil(maxdly/1000*desFs);
for n = 1:length(stims)
    stims{n} = lagGen(stims{n},dly);
end

disp('Computing the envelope reconstruction from random samplings of data across trials...');
r = NaN(nfolds,length(dly),nreps);
mse = NaN(nfolds,length(dly),nreps);
model = cell(nreps,1);
for n = 1:nreps
    fprintf('** Rep %d/%d **\n',n,nreps);
    reptm = tic;
    [r(:,:,n),~,mse(:,:,n),M] = mTRFcrossval(stims,pc,desFs,map,0,0,0,tidx,...
         'nfolds',nfolds);
    model{n} = squeeze(M);
    
    fprintf('** Completed rep %d/%d @ %.3f s\n',n,nreps,toc(reptm));
end

svpth = sprintf('~/NeuroMusic/rcnstr_spatial/%dms/',round(tmax-tmin));
svfl = sprintf('%s_%s_delay',sbj,stimtype);
save([svpth svfl],'r','mse','model','desFs','tmin','tmax','tlims','dly',...
    'tidx','var_pc','cf');
