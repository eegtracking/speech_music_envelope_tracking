% Plot the reconstruction accuracy before and average removing the moving
% average of the stimulus

sbj = 'AB';
stimtypes = {'rock','classical','vocals','speech'};
wnd = 1000;
nfolds = 10;

% Load the result with the moving average removed
r_avg = NaN(nfolds,length(stimtypes));
r_noavg = NaN(nfolds,length(stimtypes));
for ii = 1:length(stimtypes),
    d_avg = load(sprintf('~/Projects/NeuroMusic/rcnstr_res/%dms/%s_%s_db',wnd,sbj,stimtypes{ii}));
    r_avg(:,ii) = d_avg.r;
    d_noavg = load(sprintf('~/Projects/NeuroMusic/rcnstr_res/%dms_noavg/%s_%s_db',wnd,sbj,stimtypes{ii}));
    r_noavg(:,ii) = d_noavg.r;
end

R_AVG = reshape(r_avg,[nfolds*length(stimtypes) 1]);
R_NOAVG = reshape(r_noavg,[nfolds*length(stimtypes) 1]);
lbl = reshape(ones(nfolds,1)*[1:length(stimtypes)],[nfolds*length(stimtypes) 1]);
cmap = [1 0 0; 0 0 1];
median_handles = dot_median_plot(lbl,[R_AVG R_NOAVG],cmap);
set(gca,'XTickLabel',stimtypes);
ylabel('Reconstruction accuracy, r');
title(sprintf('%s, %d ms',sbj,wnd));
legend(median_handles,{'Removed moving average','With moving average'})