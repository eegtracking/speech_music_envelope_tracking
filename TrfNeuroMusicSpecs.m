% Demonstrate how the spectrum of some signal (random noise, for example)
% changes after the applying a TRF
% addpath('~/Projects/EEGanly/PCA_spline_modeling');
addpath('~/Projects/mTRF-Toolbox/');
eFs = 128;

stimtypes = {'rock','classical','vocals','speech'};
clr = {'r','g','m','b'};
use_chan = 85; % for Fz

d = load('RegularizedTRFModels'); % load the models for each of the stimulus types
trfs = [];
for ii = 1:length(stimtypes),
    trf_st = squeeze(mean(d.model(:,:,:,ii),3)); % get the average model across subjects
    trfs = [trfs trf_st(:,use_chan)];
end
t_idx = d.dly;
t = t_idx/d.desFs*1000;

% Plot the TRF
figure
subplot(2,1,1);
hold on
for ii = 1:length(stimtypes),
    plot(t,trfs(:,ii),'Color',clr{ii},'LineWidth',2);
end
set(gca,'FontSize',16);
xlabel('Time (ms)');
ylabel('TRF');
legend(stimtypes);

subplot(2,1,2);
hold on
for ii = 1:length(stimtypes),
    TRF = fft(trfs(:,ii));
    frq_trf = (0:length(TRF)-1)/length(TRF)*d.desFs;
    plot(frq_trf,abs(TRF).^2,'Color',clr{ii},'LineWidth',2);
end
set(gca,'FontSize',16,'XLim',[1 15],'YScale','log');
xlabel('Frequency (Hz)');
ylabel('TRF power');