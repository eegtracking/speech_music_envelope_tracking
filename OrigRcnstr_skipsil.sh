#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --mem=32GB

sbj=$1
stim=$2
wnd=$3

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(8); sbj='${sbj}'; stimtype='${stim}'; wnd=${wnd}; OrigEnvRcnstr_rmvsil(sbj,stimtype,wnd);"
rm -rf /scratch/$USER/$SLURM_JOB_ID
