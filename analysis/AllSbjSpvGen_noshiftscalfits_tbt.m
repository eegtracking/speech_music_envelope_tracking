% Get the reconstruction accuracies for stimulus-specific and
% stimulus-general models, and compute the difference between them as a
% function of window size (frequency)
%%% (21-6-2021): Similar to AllSbjSpvGen_fitbychan_tbt.m except that no shifting or
%%% scaling is applied when computing r-squared.
%%% Plot the TRF models of each type of stimulus

addpath('~/Documents/MATLAB/shadedErrorBar/');
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));
addpath('..');

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
% sbjs = {'AF','CB','CQ','EB','EJ','HC','JH','JS','KR','MB','SL','SOS'}; 
    % without subjects that have a strong eyeblink component 350-550 ms
stimtype = {'rock','classical','vocals','speech'};
clr = {'r','g','c','b'};
% wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000];
wnd = 2000;
maxtr = 7;
nchan = 128;
nshuff = 20; % number of times to get a null R^2 value for each stimulus type and subject
% use_chans = 1:128;
Fs = 512;
respth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';

% Load the reconstruction accuracies
r_sp = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
r_gen = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
all_mdl_sp = cell(length(wnd),length(sbjs));
all_mdl_gen = cell(length(wnd),length(sbjs));
DLY = cell(length(wnd),1);
for s = 1:length(sbjs)
    for ii = 1:length(wnd),
        % setup storing info for model
        DLY{ii} = -(-ceil(wnd(ii)/1000*Fs):0); % see lagGen, delays are ordered
        % from -max to -min in the backwards model
        M_sp = NaN(length(DLY{ii}),nchan,maxtr,length(stimtype));
        M_gen = NaN(length(DLY{ii}),nchan,maxtr,length(stimtype));
        for jj = 1:length(stimtype),
            % load the stimulus-general results
            fl_gen = sprintf('%s_ALL_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d_gen = load([respth fl_gen]);
            % stimulus-specific
            fl = sprintf('%s_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d = load([respth fl]);
            ntr = size(d.r,1);
            r_sp(1:ntr,ii,jj,s) = reshape(d.r,[ntr 1]);
            % stimulus-general
            r_gen(1:ntr,ii,jj,s) = reshape(d_gen.r,[ntr 1]);
            % tranform model to EEG
            for n = 1:ntr
                % stimulus specific model
                mdl = reshape(d.model_t{n},[length(DLY{ii}),d.npcs]);
                % normalize each model weight based on the ratio of PC variance
                % to total variance
                rM = mdl.*(ones(length(DLY{ii}),1)*d.var_pc(:,n)')/sum(d.var_pc(:,n));
                M_sp(:,:,n,jj) = rM*d.cf{n}(:,1:d.npcs)';
                % stimulus general model
                mdl = reshape(d_gen.model_t{n},[length(DLY{ii}),d_gen.npcs]);
                % normalize each model weight based on the ratio of PC variance
                % to total variance
                rM = mdl.*(ones(length(DLY{ii}),1)*d_gen.var_pc(:,n)')/sum(d_gen.var_pc(:,n));
                M_gen(:,:,n,jj) = rM*d_gen.cf{n}(:,1:d_gen.npcs)';
            end
            % save models without averaging across trials
            all_mdl_sp{ii,s} = M_sp;
            all_mdl_gen{ii,s} = M_gen;
            disp(fl)
            disp(fl_gen)
        end
    end
end
clear M_sp M_gen d_gen d

% Compute the best fit scaling and circular shift of the stimulus-general
% model to match the stimulus-specific model
disp('Compute R^2 without doing any fitting...');
nofit_rsq = NaN(maxtr,length(stimtype),length(sbjs));
for s = 1:length(sbjs)
    sbj_tm = tic;
    for ii = 1:length(stimtype) 
        % average across channels, then center
        m_mdl_sp = squeeze(all_mdl_sp{1,s}(:,:,:,ii));
        m_mdl_sp = detrend(m_mdl_sp,0);
        m_mdl_gen = squeeze(all_mdl_gen{1,s}(:,:,:,ii));
        m_mdl_gen = detrend(m_mdl_gen,0);
        % fit the general model by scaling and circular shifting
        ntr = sum(~isnan(r_sp(:,1,ii,s))); % get the number of trials for this subject / stimulus
        for t = 1:ntr
            nofit_rsq(t,ii,s) = 1-sum(sum((m_mdl_sp(:,:,t) - m_mdl_gen(:,:,t)).^2))/sum(sum(m_mdl_sp(:,:,t).^2));
        end
    end
    fprintf('Completed subject %s @ %.3f s\n',sbjs{s},toc(sbj_tm));
end
clear all_mdl_sp all_mdl_gen

% Plot the model fits
% labels for different stimulus types
% RSQ = permute(mdl_fit_rsq,[1 4 2 3]);
% RSQ = reshape(RSQ,[maxtr*length(sbjs) nchan length(stimtype)]);
% figure
% for ii = 1:length(stimtype)
%     subplot(1,length(stimtype),ii);
%     topoplot(median(RSQ(:,:,ii),'omitnan'),'~/Projects/EEGanly/chanlocs.xyz');
%     caxis([0 1]);
%     colorbar
%     title(['R^2, ' stimtype{ii}]);
% end
lbls = reshape(repmat(1:length(stimtype),maxtr*length(sbjs),1),[maxtr*length(sbjs)*length(stimtype) 1]);
RSQ = reshape(permute(nofit_rsq,[1 3 2]),[maxtr*length(sbjs)*length(stimtype) 1]);
% skip nan values
nanidx = isnan(RSQ);
dot_median_plot(lbls(~nanidx),RSQ(~nanidx));
set(gcf,'Position',[185 400 450 400]);
set(gca,'XTickLabel',stimtype,'YLim',[-1 1]);
ylabel('R^2 (without scaling and shifting)');

% svfl = sprintf('SpvGenCmp_%dms',wnd);
% save(svfl,'sbjs','mdl_fit_mag','mdl_fit_shift','mdl_fit_rsq',...
%     'mdl_null_rsq','p_cmp_null','st_cmp_null','p_shft','st_shft','stimtype',...
%     'nshuff','r_sp','r_gen','fz','pz','pscdiff','stscdiff');