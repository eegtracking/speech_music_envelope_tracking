% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbj = 'AB';
stimtype = 'speech';
% stimtype = {'classical','vocals','speech'};
% clr = {'r','g','c','b'};
clr = 'b';
wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000 16000];
% wnd = [500 1000 2000 4000 8000 16000];
nfolds = 50;
niter = 50;
respth = '~/Projects/NeuroMusic/rcnstr_spatial/';
nchan = 128;

% Load the no-averaging results
r = NaN(nfolds,length(wnd));
nr = NaN(niter,length(wnd));
zr = NaN(nfolds,length(wnd));
dp = NaN(length(wnd),1);
mdl = NaN(nchan,length(wnd));
for ii = 1:length(wnd),
    fl = sprintf('%dms/%s_%s_spdly',round(wnd(ii)),sbj,stimtype);
    d = load([respth fl]);
    r(:,ii) = reshape(d.r,[nfolds 1]);
    nr(:,ii) = d.nullr;
    zr(:,ii) = (r(:,ii)-mean(nr(:,ii)))/std(nr(:,ii));
    dp(ii) = (mean(r(:,ii))-mean(nr(:,ii)))/sqrt(0.5*(var(r(:,ii))+var(nr(:,ii))));
    M = cell2mat(d.model');
    M = M(1:nchan,:); % get only the first time index (if there are multiple) 
    disp(fl)
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g',1000/wnd(n));
end

% Plot the results
% reconstruction accuracy
figure
hold on
mrnull = squeeze(mean(nr));
strnull = squeeze(std(nr));
shadedErrorBar(1:length(wnd),mrnull,strnull,'lineProps',{clr,'LineWidth',1});
% moving averaged
mr = squeeze(mean(r));
str = squeeze(std(r));
r_plts = errorbar(1:length(wnd),mr,str,clr,'LineWidth',2);
% moving average null
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Highpass cutoff (Hz)');
ylabel('Reconstruction accuracy');
title(sbj);
legend(r_plts,stimtype);

% d-prime
figure
hold on
% moving averaged
plot(1:length(wnd),dp,clr,'LineWidth',2);
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Highpass cutoff (Hz)');
ylabel('d-prime');
title(sbj);
legend(stimtype);