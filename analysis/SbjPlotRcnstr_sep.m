% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbj = 'CB';
stimtype = {'rock','classical','vocals','speech'};
% clr = {'r','g','c','b'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
wnd = [31.25 62.5 125 250 500 1000 2000 4000];
% wnd = [250 500 1000 2000 4000];
nfolds = 50;
niter = 50;
maxtr = 7;
% respth = '~/Projects/NeuroMusic/rcnstr_res_genPCA/';
respth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_sep/';

% Load the no-averaging results
r = NaN(nfolds,maxtr,length(wnd),length(stimtype));
nr = NaN(niter,maxtr,length(wnd),length(stimtype));
dp = NaN(maxtr,length(wnd),length(stimtype));
for ii = 1:length(wnd),
    for jj = 1:length(stimtype),
        fl = sprintf('%s_%s_%dms',sbj,stimtype{jj},round(wnd(ii)));
        d = load([respth fl]);
        ntr = size(d.r,3);
        r(:,1:ntr,ii,jj) = reshape(d.r,[nfolds ntr]);
        nr(:,1:ntr,ii,jj) = d.nullr;
        dp(:,ii,jj) = (mean(r(:,:,ii,jj),'omitnan')-mean(nr(:,:,ii,jj),'omitnan'))./...
            sqrt(0.5*(var(r(:,:,ii,jj),[],'omitnan')+var(nr(:,:,ii,jj),[],'omitnan')));
        disp(fl)
    end
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*8);
end

% Plot the results
% reconstruction accuracy
figure
max_plot_ntr = 1; % number of trials to plot
set(gcf,'Position',[145 300 500 400]);
hold on
for jj = 1:length(stimtype),
    mrnull = squeeze(mean(nr(:,:,:,jj)));
    strnull = squeeze(std(nr(:,:,:,jj)));
    for n = 1:max_plot_ntr
        errorbar(1:length(wnd),mrnull(n,:),strnull(n,:),'--','Color',clr{jj},'LineWidth',1);
    end
%     shadedErrorBar(1:length(wnd),mrnull,strnull,'lineProps',{clr{jj},'LineWidth',1});
end
% moving averaged
r_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype),
    mr = squeeze(mean(r(:,:,:,jj)));
    str = squeeze(std(r(:,:,:,jj)));
    for n = 1:max_plot_ntr
        r_plts(jj) = errorbar(1:length(wnd),mr(n,:),str(n,:),'Color',clr{jj},'LineWidth',1.5);
    end
end
% moving average null
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('Reconstruction accuracy');
title(sbj);
legend(r_plts,stimtype);

% d-prime
figure
set(gcf,'Position',[445 300 500 400]);
hold on
% moving averaged
dp_pl = NaN(length(stimtype),1);
for jj = 1:length(stimtype),
    mdp = mean(dp(:,:,jj),'omitnan');
    plot(1:length(wnd),dp(:,:,jj),'Color',clr{jj},'LineWidth',1);
    dp_pl(jj) = plot(1:length(wnd),mdp,'Color',clr{jj},'LineWidth',3);
end
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('d-prime');
title(sbj);
legend(dp_pl,stimtype);

