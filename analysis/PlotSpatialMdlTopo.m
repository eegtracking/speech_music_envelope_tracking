% Plot the topography of the spatial model weights at 2000 ms
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbj = 'AB';
stimtype = 'speech';
wnd = 2000;

res_fld = '../rcnstr_spatial/';
wnd_fld = sprintf('%dms/',wnd);
fl = sprintf('%s_%s_db',sbj,stimtype);

d = load([res_fld wnd_fld fl]);

M = cell2mat(d.model');
% scale the model weights by the variance of the principal components
rM = M.*(mean(d.var_pc)'*ones(1,size(d.r,2)));
% convert to EEG channels
eM = (rM'*d.cf')';

figure
topoplot(mean(eM,2),'~/Projects/EEGanly/chanlocs.xyz','maplimits','maxmin');
colorbar;