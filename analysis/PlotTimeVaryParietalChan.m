% Plot the topography of the spatial model weights at 2000 ms
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbj = 'AB';
stimtype = 'speech';
wnd = [500 1000 2000 4000 8000 16000];
use_chn = 3;

res_fld = '../rcnstr_spatial/';

ch_val = NaN(length(wnd),1);
v = NaN(128,length(wnd));
for n = 1:length(wnd)
    wnd_fld = sprintf('%dms/',wnd(n));
    fl = sprintf('%s_%s_db',sbj,stimtype);
    d = load([res_fld wnd_fld fl]);
    M = cell2mat(d.model');
    % scale the model weights by the variance of the principal components
    norm_v = mean(d.var_pc)/sum(mean(d.var_pc)); % normalize by the total "power" in the components
%     norm_v = mean(d.var_pc);
    rM = M.*(norm_v'*ones(1,size(d.r,2)));
    % convert to EEG channels
    eM = (rM'*d.cf')';
%     ch_val(n) = mean(eM(use_chn,:),2);
    ch_val(n) = mean(eM(use_chn,:),2)-mean(mean(eM,2));
    % save the variance of each principal component
    v(:,n) = mean(d.var_pc);
    disp(fl);
end

figure
plot(1000./wnd,ch_val,'k','LineWidth',2);
set(gca,'FontSize',16,'XScale','log');
xlabel('Highpass cutoff (Hz)');
ylabel(sprintf('Relative model weight for channel %d',use_chn));