% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbj = 'AB';
stimtype = {'rock','classical','vocals','speech'};
% stimtype = {'classical','vocals','speech'};
% clr = {'r','g','c','b'};
clr = {'r','g','c','b'};
wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000 16000];
% wnd = [250 500 1000 2000 4000];
ntr = 7;
niter = 50;
delay = 0;
% respth = '~/Projects/NeuroMusic/rcnstr_res_genPCA/';
respth = '~/Projects/NeuroMusic/rcnstr_spatial_tbt/';

% Load the no-averaging results
r = NaN(ntr,length(wnd),length(stimtype));
nr = NaN(niter,length(wnd),length(stimtype));
zr = NaN(ntr,length(wnd),length(stimtype));
dp = NaN(length(wnd),length(stimtype));
for ii = 1:length(wnd),
    for jj = 1:length(stimtype),
        fl = sprintf('%dms/%s_%s_%dms',round(wnd(ii)),sbj,stimtype{jj},delay);
        d = load([respth fl]);
        r(:,ii,jj) = reshape(d.r,[ntr 1]);
        nr(:,ii,jj) = d.nullr;
        zr(:,ii,jj) = (r(:,ii,jj)-mean(nr(:,ii,jj)))/std(nr(:,ii,jj));
        disp(fl)
    end
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g',1000/wnd(n));
end

% Plot the results
% reconstruction accuracy
figure
hold on
for jj = 1:length(stimtype),
    mrnull = squeeze(mean(nr(:,:,jj)));
    strnull = squeeze(std(nr(:,:,jj)));
    shadedErrorBar(1:length(wnd),mrnull,strnull,'lineProps',{clr{jj},'LineWidth',1});
end
r_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype),
    pl = plot(1:length(wnd),r(:,:,jj),clr{jj},'LineWidth',1);
    r_plts(jj) = pl(1);
end
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('Reconstruction accuracy');
title(sbj);
legend(r_plts,stimtype);

% Plot zr
figure
hold on
z_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype)
    plot(1:length(wnd),zr(:,:,jj),clr{jj},'LineWidth',1);
    pl = plot(1:length(wnd),mean(zr(:,:,jj),1),clr{jj},'LineWidth',3);
    z_plts(jj) = pl(1);
end
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('Z-transformed reconstruction accuracy');
title(sbj);
legend(z_plts,stimtype);