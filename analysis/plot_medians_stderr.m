function [plt_lines,mdy,uqy,lqy] = plot_medians_stderr(y,nboot,clrs)
% Plot the median of the values of y within its first two dimensions and
% the 95-percentile range of the median.  The range of median values is computed by
% shuffling the values of y within the first dimension, so that each column
% of y contains the same number of bootstrapped values, and recalculating the median.
% The range is plotted as a shaded region around the median line. 
% If y is a 4-D array, the data along the fourth dimension will be plotted 
% as its own line.
% ** Uses shadedErrorBar to make the plots.
% Inputs:
% - y = data values, can be either a 3-D or 4-D array
% - nboot = number of times to do bootstrap (default = 1000);
% - clrs = the colors for each line (default: black if only one line, 'jet'
%       colormap if multiple lines)
% Outputs:
% - plt_lines = handles for the median lines in the plot
% - mdy = median values
% - uqy = 95-percentile of the bootstrapped medians
% - lqy = 5-percentile of the boostrapped medians
% Nate Zuk (2020)

addpath('~/Documents/MATLAB/shadedErrorBar/');

% Determine what the colors should be for the plot
if nargin<3
    if size(y,4)==1, 
        clrs = {'k'}; % just plot in black
    else % use an even spacing along the 'jet' colormap to get the colors
        cmap = colormap('jet');
        nlines = size(y,4); % get the number of lines
        clrs = cell(nlines,1);
        for n = 1:nlines
            clr_idx = round((n-1)/nlines*254)+1; % get an even spacing of colors along the map
            cmap{n} = cmap(clr_idx,:);
        end
    end
end

% If nboot isn't specified, using 1000
if nargin<2, nboot = 1000; end

%% Compute the median and bootstrapped distribution of the median
Y = reshape(y,[size(y,1)*size(y,2) size(y,3) size(y,4)]);
mdy = squeeze(median(Y,'omitnan'));

bootmd = NaN(nboot,size(y,3),size(y,4));
for n = 1:nboot
    % randomly sample values with replacement, so that there are the same
    % number of samples for column
    boot_y = NaN(size(y));
    for s = 1:size(y,2)
        for jj = 1:size(y,3)
            for kk = 1:size(y,4)
                tmpy = y(:,s,jj,kk);
                % only shuffle non-NaN values
                shfidx = randi(sum(~isnan(tmpy)),sum(~isnan(tmpy)),1);
                tmpy_idx = find(~isnan(tmpy)); % get the indexes of non-NaN values
                boot_y(1:sum(~isnan(tmpy)),s,jj,kk) = tmpy(tmpy_idx(shfidx));
            end
        end
    end
    BY = reshape(boot_y,[size(y,1)*size(y,2) size(y,3) size(y,4)]);
    bootmd(n,:,:) = squeeze(median(BY,'omitnan'));
end
% compute the interquartile range of the bootstrapped medians
uqy = squeeze(quantile(bootmd,0.95));
lqy = squeeze(quantile(bootmd,0.05));

%% Plotting
figure
hold on
plt_lines = NaN(nlines,1);
for jj = 1:nlines
    pl = shadedErrorBar(1:length(wnd),mdy(:,jj),[mdy(:,jj)-lqy(:,jj) uqy(:,jj)-mdy(:,jj)],...
        'lineProps',{'Color',clrs{jj}});
    plt_lines(jj) = pl.mainLine;
end