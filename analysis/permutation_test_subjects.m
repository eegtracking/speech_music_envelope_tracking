function [p,nulldiff] = permutation_test_subjects(a,b,varargin)
% Performs a permutation-type test to test for a significant difference
% between the data in a and b, where the permutation is performed within
% subjects.  Subject specific data is contained in the columns of a and b
% Nate Zuk (2020)

nperm = 1000; % number of permutations
tail = 'both';

if ~isempty(varargin)
    for n = 2:2:length(varargin)
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% make sure an appropriate tail variable is specified
if ~strcmp(tail,'both') && ~strcmp(tail,'right') && ~strcmp(tail,'left')
    error('Tail must be both, right, or left');
end

% make sure a and b have the same number of columns
if size(a,2)~=size(b,2)
    error('Number of columns in a and b must be the same');
end

ngroups = size(a,2);

nulldiff = NaN(nperm,1);
for n = 1:nperm
    perm_a = NaN(size(a));
    perm_b = NaN(size(b));
    for s = 1:ngroups
        % get the number of datapoints for each group
        ntr_a = sum(~isnan(a(:,s)));
        ntr_b = sum(~isnan(b(:,s)));
        allsbjz = [a(:,s); b(:,s)];
        allsbjz = allsbjz(randperm((size(a,1)+size(b,1)))); % randomly rearrange zrs
        allsbjz = allsbjz(~isnan(allsbjz)); % remove nans
        % then reallocate values to separate groups, keeping the same
        % number of trials in each
        perm_a(1:ntr_a,s) = allsbjz(1:ntr_a);
        perm_b(1:ntr_b,s) = allsbjz(ntr_a+1:end);
    end
    % compute the difference in medians
    PERMA = reshape(perm_a,[numel(perm_a) 1]); PERMB = reshape(perm_b,[numel(perm_b) 1]);
    nulldiff(n) = median(PERMB,'omitnan')-median(PERMA,'omitnan');
end
% rearrange true values into column arrays
TRUEA = reshape(a,[numel(a) 1]);
TRUEB = reshape(b,[numel(b) 1]);
mddiff = median(TRUEB,'omitnan')-median(TRUEA,'omitnan');
% compute the two-tailed p-value
if strcmp(tail,'both')
    p = (sum(nulldiff>abs(mddiff)) + sum(nulldiff<-abs(mddiff)))/nperm;
elseif strcmp(tail,'right')
    p = sum(nulldiff>mddiff)/nperm;
elseif strcmp(tail,'left')
    p = sum(nulldiff<mddiff)/nperm;
end