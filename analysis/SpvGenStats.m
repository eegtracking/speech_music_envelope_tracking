% Run more statistical analyses on the scaling factors for vocals and speech
% Nate Zuk (2021)

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};

stimtypes = {'rock','classical','vocals','speech'};

maxtr = 7;
nsbjs = length(sbjs);
nchan = 128;

% Load the general model fit results
wnd = 2000; % window size
res_fl = sprintf('SpvGenCmp_%dms',wnd);
d = load(res_fl);

%%% Rearrange scaling factors so all subjects&trials are along single
%%% columns (like in AllSbjSpvGen_fitbychan_tbt.m)
MAG = permute(d.mdl_fit_mag,[1 4 2 3]);
MAG = reshape(MAG,[maxtr*nsbjs nchan length(stimtypes)]);
npts = size(MAG,1); % get the number of datapoints overall

%%% Plot vocals and speech scaling factors for Fz and Pz. Compare the
%%% speech and vocals scaling factors for each plot.
jit_span = 0.2;
jit = (rand(npts,1)-0.5)*jit_span; % jitter points along x-axis
chan_idx = [19 85];
chan_lbl = {'Pz','Fz'};
pcmp = NaN(length(chan_idx),1);
zcmp = NaN(length(chan_idx),1);
for c = 1:length(chan_idx)
    figure
    set(gcf,'Position',[185 260 250 400]);
    hold on
    % plot vocals magnitudes and median
    plot(ones(npts,1)+jit,MAG(:,chan_idx(c),3),'.','Color',clr{3},'MarkerSize',12);
    plot([-jit_span jit_span]+1,median(MAG(:,chan_idx(c),3),'omitnan')*ones(1,2),...
        'Color',clr{3},'LineWidth',3);
    plot(ones(npts,1)*2+jit,MAG(:,chan_idx(c),4),'.','Color',clr{4},'MarkerSize',12);
    plot([-jit_span jit_span]+2,median(MAG(:,chan_idx(c),4),'omitnan')*ones(1,2),...
        'Color',clr{4},'LineWidth',3);
    % label plot
    set(gca,'FontSize',14,'YScale','log','XTick',[1 2],'XTickLabel',stimtypes(3:4));
    ylabel('Scaling factor');
    title(chan_lbl{c});
    % compare scaling factors
    [pcmp(c),~,stcmp] = ranksum(MAG(:,chan_idx(c),3),MAG(:,chan_idx(c),4));
    zcmp(c) = stcmp.zval;
    fprintf('Rank-sum for channel %s: z = %.3f, p = %.3f\n',chan_lbl{c},zcmp(c),pcmp(c));
end

%%% Plot the difference between Fz and Pz for both stimulus types
DIFFMAG = squeeze(diff(MAG(:,[85 19],:),[],2));
figure
set(gcf,'Position',[185 260 250 350]);
hold on
% plot vocals magnitudes and median
plot([0 3],[0 0],'k--');
plot(ones(npts,1)+jit,DIFFMAG(:,3),'.','Color',clr{3},'MarkerSize',12);
plot([-jit_span jit_span]+1,median(DIFFMAG(:,3),'omitnan')*ones(1,2),...
    'Color',clr{3},'LineWidth',3);
plot(ones(npts,1)*2+jit,DIFFMAG(:,4),'.','Color',clr{4},'MarkerSize',12);
plot([-jit_span jit_span]+2,median(DIFFMAG(:,4),'omitnan')*ones(1,2),...
    'Color',clr{4},'LineWidth',3);
% label plot
set(gca,'FontSize',14,'XTick',[1 2],'XTickLabel',stimtypes(3:4),'YLim',[-5 5]);
ylabel('Pz - Fz scaling factor difference');
title(sprintf('%s ms',wnd));
% compare scaling factors
[pdiffcmp,~,stdiffcmp] = ranksum(DIFFMAG(:,3),DIFFMAG(:,4));
zdiffcmp = stdiffcmp.zval;
fprintf('Rank-sum of Pz-Fz difference: z = %.3f, p = %.3f\n',zdiffcmp,pdiffcmp);