% Get the reconstruction accuracies for stimulus-specific and
% stimulus-general models, and compute the difference between them as a
% function of window size (frequency)
%%% Plot the TRF models of each type of stimulus

addpath('~/Documents/MATLAB/shadedErrorBar/');
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
% sbjs = {'AF','CB','CQ','EB','EJ','HC','JH','JS','KR','MB','SL','SOS'}; 
    % without subjects that have a strong eyeblink component 350-550 ms
stimtype = {'rock','classical','vocals','speech'};
clr = {'r','g','c','b'};
wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000];
% wnd = 250;
maxtr = 7;
nchan = 128;
use_chans = 1:128;
% use_chans = 65:109; % frontal channels
% plot_chans = setxor(1:128,[72:74 78:82 91:96]);
Fs = 512;
respth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';

% Load the reconstruction accuracies
r_sp = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
r_gen = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
all_mdl_sp = cell(length(wnd),length(sbjs));
all_mdl_gen = cell(length(wnd),length(sbjs));
DLY = cell(length(wnd),1);
for s = 1:length(sbjs)
    for ii = 1:length(wnd),
        % setup storing info for model
        DLY{ii} = -(-ceil(wnd(ii)/1000*Fs):0); % see lagGen, delays are ordered
        % from -max to -min in the backwards model
        M_sp = NaN(length(DLY{ii}),nchan,maxtr,length(stimtype));
        M_gen = NaN(length(DLY{ii}),nchan,maxtr,length(stimtype));
        for jj = 1:length(stimtype),
            % load the stimulus-general results
            fl_gen = sprintf('%s_ALL_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d_gen = load([respth fl_gen]);
            % stimulus-specific
            fl = sprintf('%s_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d = load([respth fl]);
            ntr = size(d.r,1);
            r_sp(1:ntr,ii,jj,s) = reshape(d.r,[ntr 1]);
            % stimulus-general
            r_gen(1:ntr,ii,jj,s) = reshape(d_gen.r,[ntr 1]);
            % tranform model to EEG
            for n = 1:ntr
                % stimulus specific model
                mdl = reshape(d.model_t{n},[length(DLY{ii}),d.npcs]);
                % normalize each model weight based on the ratio of PC variance
                % to total variance
                rM = mdl.*(ones(length(DLY{ii}),1)*d.var_pc(:,n)')/sum(d.var_pc(:,n));
                M_sp(:,:,n,jj) = rM*d.cf{n}(:,1:d.npcs)';
                % stimulus general model
                mdl = reshape(d_gen.model_t{n},[length(DLY{ii}),d_gen.npcs]);
                % normalize each model weight based on the ratio of PC variance
                % to total variance
                rM = mdl.*(ones(length(DLY{ii}),1)*d_gen.var_pc(:,n)')/sum(d_gen.var_pc(:,n));
                M_gen(:,:,n,jj) = rM*d_gen.cf{n}(:,1:d_gen.npcs)';
            end
            % save models without averaging across trials
            all_mdl_sp{ii,s} = M_sp;
            all_mdl_gen{ii,s} = M_gen;
            disp(fl)
            disp(fl_gen)
        end
    end
end
clear M_sp M_gen d_gen d

% Plot the median difference in reconstruction accuracies abd the standard
% error of the median
DIFFR = r_sp-r_gen;
DIFFR = permute(DIFFR,[1 4 2 3]);

% Compute the best fit scaling and circular shift of the stimulus-general
% model to match the stimulus-specific model
mdl_fit_mag = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
mdl_fit_shift = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
mdl_fit_rsq = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
for jj = 1:length(wnd)
    for s = 1:length(sbjs)
        for ii = 1:length(stimtype) 
            % average across channels, then center
            m_mdl_sp = squeeze(mean(all_mdl_sp{jj,s}(:,use_chans,:,ii),2));
            m_mdl_sp = detrend(m_mdl_sp,0);
            m_mdl_gen = squeeze(mean(all_mdl_gen{jj,s}(:,use_chans,:,ii),2));
            m_mdl_gen = detrend(m_mdl_gen,0);
            % fit the general model by scaling and circular shifting
            ntr = sum(~isnan(r_sp(:,jj,ii,s))); % get the number of trials for this subject / stimulus
            for t = 1:ntr
                mag = NaN(length(DLY{jj}),1);
                rsq = NaN(length(DLY{jj}),1);
                for c = 1:length(DLY{jj})
                    shftgen = circshift(m_mdl_gen(:,t),c-1); % circularly shift the general model
                    mag(c) = abs(shftgen \ m_mdl_sp(:,t)); % compute the best fit scaling
                        % make sure it's a positive value
                    rsq(c) = 1-sum((m_mdl_sp(:,t) - shftgen*mag(c)).^2)/sum(m_mdl_sp(:,t).^2); % compute R^2
                        % note that the mean of m_mdl_sp is zero
                end
                % identify the best fit model
                best_mdl_idx = find(rsq==max(rsq),1,'first');
                mdl_fit_mag(t,jj,ii,s) = mag(best_mdl_idx);
                mdl_fit_shift(t,jj,ii,s) = -(best_mdl_idx-1)/Fs*1000/wnd(jj); % save shift as delay relative to window size
                    % the shift is negative because the delays in the model are
                    % reversed
                mdl_fit_rsq(t,jj,ii,s) = rsq(best_mdl_idx);
            end
        end
    end
end
clear all_mdl_sp all_mdl_gen

% Plot the model fits
% labels for different stimulus types
lbls = reshape(repmat(1:length(stimtype),maxtr*length(sbjs),1),[maxtr*length(sbjs)*length(stimtype) 1]);
RSQ = reshape(permute(squeeze(mdl_fit_rsq),[1 3 2]),[maxtr*length(sbjs)*length(stimtype) 1]);
% skip nan values
nanidx = isnan(RSQ);
dot_median_plot(lbls(~nanidx),RSQ(~nanidx));
set(gca,'XTickLabel',stimtype);
ylabel('R^2');

% Plot the scaling
MAG = reshape(permute(squeeze(mdl_fit_mag),[1 3 2]),[maxtr*length(sbjs)*length(stimtype) 1]);
dot_median_plot(lbls(~nanidx),MAG(~nanidx));
set(gca,'XTickLabel',stimtype);
ylabel('Scaling');

% Plot the shift
SHIFT = reshape(permute(squeeze(mdl_fit_shift),[1 3 2]),[maxtr*length(sbjs)*length(stimtype) 1]);
% center around 0
SHIFT(SHIFT<-0.5) = SHIFT(SHIFT<-0.5)+1;
dot_median_plot(lbls(~nanidx),SHIFT(~nanidx));
hold on
plot([0 length(stimtype)+1],[0 0],'k--');
set(gca,'XTickLabel',stimtype);
ylabel('Shift, relative to window');