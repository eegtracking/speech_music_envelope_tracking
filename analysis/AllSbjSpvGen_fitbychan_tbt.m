% Get the reconstruction accuracies for stimulus-specific and
% stimulus-general models, and compute the difference between them as a
% function of window size (frequency)
%%% Plot the TRF models of each type of stimulus

addpath('~/Documents/MATLAB/shadedErrorBar/');
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));
addpath('..');

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
% sbjs = {'AF','CB','CQ','EB','EJ','HC','JH','JS','KR','MB','SL','SOS'}; 
    % without subjects that have a strong eyeblink component 350-550 ms
stimtype = {'rock','classical','vocals','speech'};
clr = {'r','g','c','b'};
% wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000];
wnd = 2000;
maxtr = 7;
nchan = 128;
nshuff = 20; % number of times to get a null R^2 value for each stimulus type and subject
% use_chans = 1:128;
Fs = 512;
respth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';

% Load the reconstruction accuracies
r_sp = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
r_gen = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
all_mdl_sp = cell(length(wnd),length(sbjs));
all_mdl_gen = cell(length(wnd),length(sbjs));
DLY = cell(length(wnd),1);
for s = 1:length(sbjs)
    for ii = 1:length(wnd),
        % setup storing info for model
        DLY{ii} = -(-ceil(wnd(ii)/1000*Fs):0); % see lagGen, delays are ordered
        % from -max to -min in the backwards model
        M_sp = NaN(length(DLY{ii}),nchan,maxtr,length(stimtype));
        M_gen = NaN(length(DLY{ii}),nchan,maxtr,length(stimtype));
        for jj = 1:length(stimtype),
            % load the stimulus-general results
            fl_gen = sprintf('%s_ALL_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d_gen = load([respth fl_gen]);
            % stimulus-specific
            fl = sprintf('%s_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d = load([respth fl]);
            ntr = size(d.r,1);
            r_sp(1:ntr,ii,jj,s) = reshape(d.r,[ntr 1]);
            % stimulus-general
            r_gen(1:ntr,ii,jj,s) = reshape(d_gen.r,[ntr 1]);
            % tranform model to EEG
            for n = 1:ntr
                % stimulus specific model
                mdl = reshape(d.model_t{n},[length(DLY{ii}),d.npcs]);
                % normalize each model weight based on the ratio of PC variance
                % to total variance
                rM = mdl.*(ones(length(DLY{ii}),1)*d.var_pc(:,n)')/sum(d.var_pc(:,n));
                M_sp(:,:,n,jj) = rM*d.cf{n}(:,1:d.npcs)';
                % stimulus general model
                mdl = reshape(d_gen.model_t{n},[length(DLY{ii}),d_gen.npcs]);
                % normalize each model weight based on the ratio of PC variance
                % to total variance
                rM = mdl.*(ones(length(DLY{ii}),1)*d_gen.var_pc(:,n)')/sum(d_gen.var_pc(:,n));
                M_gen(:,:,n,jj) = rM*d_gen.cf{n}(:,1:d_gen.npcs)';
            end
            % save models without averaging across trials
            all_mdl_sp{ii,s} = M_sp;
            all_mdl_gen{ii,s} = M_gen;
            disp(fl)
            disp(fl_gen)
        end
    end
end
clear M_sp M_gen d_gen d

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*8);
end

% Plot the median difference in reconstruction accuracies abd the standard
% error of the median
DIFFR = r_sp-r_gen;
DIFFR = permute(DIFFR,[1 4 2 3]);

% Compute the best fit scaling and circular shift of the stimulus-general
% model to match the stimulus-specific model
disp('Best fitting of general to specific model...');
mdl_fit_mag = NaN(maxtr,nchan,length(stimtype),length(sbjs));
mdl_fit_shift = NaN(maxtr,length(stimtype),length(sbjs));
mdl_fit_rsq = NaN(maxtr,length(stimtype),length(sbjs));
mdl_null_rsq = NaN(nshuff,length(stimtype),length(sbjs));
for s = 1:length(sbjs)
    sbj_tm = tic;
    for ii = 1:length(stimtype) 
        % average across channels, then center
        m_mdl_sp = squeeze(all_mdl_sp{1,s}(:,:,:,ii));
        m_mdl_sp = detrend(m_mdl_sp,0);
        m_mdl_gen = squeeze(all_mdl_gen{1,s}(:,:,:,ii));
        m_mdl_gen = detrend(m_mdl_gen,0);
        % fit the general model by scaling and circular shifting
        ntr = sum(~isnan(r_sp(:,1,ii,s))); % get the number of trials for this subject / stimulus
        for t = 1:ntr
            mag = NaN(length(DLY{1}),nchan);
            rsq = NaN(length(DLY{1}),1);
            for n = 1:length(DLY{1})
                shftgen = circshift(m_mdl_gen(:,:,t),n-1); % circularly shift the general model
                for c = 1:nchan
                    mag(n,c) = abs(shftgen(:,c) \ m_mdl_sp(:,c,t)); % compute the best fit scaling
                end
                    % make sure it's a positive value
                    % sum across all channels
                shft_fit = shftgen.*(ones(length(DLY{1}),1)*mag(n,:));
                rsq(n) = 1-sum(sum((m_mdl_sp(:,:,t) - shft_fit).^2))/sum(sum(m_mdl_sp(:,:,t).^2)); % compute R^2
                    % note that the mean of m_mdl_sp is zero
            end
            % identify the best fit model
            best_mdl_idx = find(rsq==max(rsq),1,'first');
            mdl_fit_mag(t,:,ii,s) = mag(best_mdl_idx,:);
            mdl_fit_shift(t,ii,s) = -(best_mdl_idx-1)/Fs*1000/wnd; % save shift as delay relative to window size
                % the shift is negative because the delays in the model are
                % reversed
            mdl_fit_rsq(t,ii,s) = rsq(best_mdl_idx);
        end
        % Compute a null distribution of R^2 values by randomizing the
        % phases of the stimulus-specific model
        for b = 1:nshuff
            null_mag = NaN(length(DLY{1}),nchan);
            null_rsq = NaN(length(DLY{1}),1);
            % randomly select two trials
            rnd_gen_trial = randi(ntr);
            rnd_sp_trial = randi(ntr);
            % randomize the phases of the stimulus-specific model
            rnd_sp = randomize_phases(m_mdl_sp(:,:,rnd_sp_trial));
            % find the best-fit model
            for n = 1:length(DLY{1})
                shftgen = circshift(m_mdl_gen(:,:,rnd_gen_trial),n-1); % circularly shift the general model
                for c = 1:nchan
                    null_mag(n,c) = abs(shftgen(:,c) \ rnd_sp(:,c)); % compute the best fit scaling
                end
                shft_fit = shftgen.*(ones(length(DLY{1}),1)*null_mag(n,:));
                null_rsq(n) = 1-sum(sum((rnd_sp - shft_fit).^2))/sum(sum(rnd_sp.^2)); % compute R^2
                    % note that the mean of m_mdl_sp is zero
            end
            % get the maximum rsq value
            mdl_null_rsq(b,ii,s) = max(null_rsq);
        end
    end
    fprintf('Completed subject %s @ %.3f s\n',sbjs{s},toc(sbj_tm));
end
clear all_mdl_sp all_mdl_gen

% Plot the model fits
% labels for different stimulus types
% RSQ = permute(mdl_fit_rsq,[1 4 2 3]);
% RSQ = reshape(RSQ,[maxtr*length(sbjs) nchan length(stimtype)]);
% figure
% for ii = 1:length(stimtype)
%     subplot(1,length(stimtype),ii);
%     topoplot(median(RSQ(:,:,ii),'omitnan'),'~/Projects/EEGanly/chanlocs.xyz');
%     caxis([0 1]);
%     colorbar
%     title(['R^2, ' stimtype{ii}]);
% end
lbls = reshape(repmat(1:length(stimtype),maxtr*length(sbjs),1),[maxtr*length(sbjs)*length(stimtype) 1]);
RSQ = reshape(permute(mdl_fit_rsq,[1 3 2]),[maxtr*length(sbjs)*length(stimtype) 1]);
NULLRSQ = reshape(permute(mdl_null_rsq,[1 3 2]),[nshuff*length(sbjs) length(stimtype)]);
% skip nan values
nanidx = isnan(RSQ);
dot_median_plot(lbls(~nanidx),RSQ(~nanidx));
set(gcf,'Position',[185 400 500 300]);
for ii = 1:length(stimtype)
    md_null = median(NULLRSQ(:,ii));
    uq_null = quantile(NULLRSQ(:,ii),0.95);
    lq_null = quantile(NULLRSQ(:,ii),0.05);
    errorbar(ii,md_null,md_null-lq_null,uq_null-md_null,'o',...
        'Color',[0.5 0.5 0.5],'LineWidth',2,'MarkerSize',14);
end
set(gca,'XTickLabel',stimtype);
ylabel('R^2');

% Test if the rsq and null distributions are significantly different
p_cmp_null = NaN(length(stimtype),1);
st_cmp_null = cell(length(stimtype),1);
for ii = 1:length(stimtype)
    [p_cmp_null(ii),~,st_cmp_null{ii}] = ranksum(RSQ(lbls==ii),NULLRSQ(:,ii));
end

SHIFT = reshape(permute(squeeze(mdl_fit_shift),[1 3 2]),[maxtr*length(sbjs)*length(stimtype) 1]);
% center around 0
SHIFT(SHIFT<-0.5) = SHIFT(SHIFT<-0.5)+1;
dot_median_plot(lbls(~nanidx),SHIFT(~nanidx));
set(gcf,'Position',[350 400 500 300]);
hold on
plot([0 length(stimtype)+1],[0 0],'k--');
set(gca,'XTickLabel',stimtype);
ylabel('Shift, relative to window');

% Test if the shift is significantly different than zero
p_shft = NaN(length(stimtype),1);
st_shft = cell(length(stimtype),1);
for ii = 1:length(stimtype)
    [p_shft(ii),~,st_shft{ii}] = signrank(SHIFT(lbls==ii));
end

% Plot the scaling
MAG = permute(mdl_fit_mag,[1 4 2 3]);
MAG = reshape(MAG,[maxtr*length(sbjs) nchan length(stimtype)]);
figure
set(gcf,'Position',[75 400 1125 250]);
for ii = 1:length(stimtype)
    subplot(1,length(stimtype),ii);
    topoplot(median(MAG(:,:,ii),'omitnan'),'~/Projects/EEGanly/chanlocs.xyz');
    caxis([0 3]);
    colorbar;
    title(['Scaling, ' stimtype{ii}]);
end

% Plot the correlation between scaling on each channel and diffr for speech
figure
for ii = 1:length(stimtype)
    SP_DIFFR = reshape(DIFFR(:,:,:,ii),[maxtr*length(sbjs) 1]);
    [cr,pr] = corr(SP_DIFFR,MAG(:,:,ii),'type','Spearman','rows','complete');
    subplot(length(stimtype),2,(ii-1)*2+1);
    topoplot(cr,'~/Projects/EEGanly/chanlocs.xyz');
    colorbar
    title('Spearmans corr');
    subplot(length(stimtype),2,(ii-1)*2+2);
    topoplot(pr,'~/Projects/EEGanly/chanlocs.xyz');
    colorbar
    caxis([0 1]);
    title('p-value');
end
