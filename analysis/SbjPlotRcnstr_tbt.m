% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbj = 'HC';
stimtype = {'rock','classical','vocals','speech'};
% clr = {'r','g','c','b'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000 16000];
maxtr = 7;
niter = 50;
% respth = '~/Projects/NeuroMusic/rcnstr_res_genPCA/';
respth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';

% Load the no-averaging results
r = NaN(maxtr,length(wnd),length(stimtype));
nr = NaN(niter,length(wnd),length(stimtype));
zr = NaN(maxtr,length(wnd),length(stimtype));
for ii = 1:length(wnd),
    for jj = 1:length(stimtype),
        fl = sprintf('%s_%s_%dms',sbj,stimtype{jj},round(wnd(ii)));
        d = load([respth fl]);
        ntr = size(d.r,1);
        r(1:ntr,ii,jj) = reshape(d.r,[ntr 1]);
        nr(:,ii,jj) = d.nullr;
        zr(1:ntr,ii,jj) = (d.r-mean(d.nullr))/std(d.nullr);
        disp(fl)
    end
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*8);
end

% Plot the results
% reconstruction accuracy
figure
set(gcf,'Position',[145 300 500 400]);
hold on
for jj = 1:length(stimtype),
    mrnull = squeeze(mean(nr(:,:,jj)));
    strnull = squeeze(std(nr(:,:,jj)));
    shadedErrorBar(1:length(wnd),mrnull,strnull,'lineProps',{'Color',clr{jj},'LineWidth',1});
end
% moving averaged
r_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype),
    pl = plot(1:length(wnd),r(:,:,jj),'Color',clr{jj},'LineWidth',1);
    r_plts(jj) = pl(1);
end
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Reconstruction accuracy');
title(sbj);
legend(r_plts,stimtype);

% Plot zr
figure
set(gcf,'Position',[445 300 500 400]);
hold on
z_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype)
    plot(1:length(wnd),zr(:,:,jj),'Color',clr{jj},'LineWidth',1);
    pl = plot(1:length(wnd),mean(zr(:,:,jj),1,'omitnan'),'Color',clr{jj},'LineWidth',3);
    z_plts(jj) = pl(1);
end
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Z-transformed reconstruction accuracy');
title(sbj);
legend(z_plts,stimtype);