% Plot an example of a spline transformation

addpath('~/Projects/EEGanly/');
addpath('..');
addpath('~/Projects/mTRF-Toolbox/');

sbj = 'HC';
block = 1; % number of the block to plot
tr = 4; % trial to plot
start_time = 10; % start time of the plot (in s)
plot_dur = 1; % duration of lagGen (in s)
tmin = 0; % minimum delay of model (in ms)
tmax = 500; % maximum delay of model (in ms)
chn_plot = 85; % particular EEG channel to use as an example
ds = 16; % downsampling factor for the splines

% load the EEG
eegpth = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/';
eegfl = sprintf('%s_block_%d',sbj,block);
load([eegpth eegfl]);

% get the appropriate indexes for the plot
t = (0:size(eeg{tr},1)-1)/eFs;
t_idx = t>=start_time & t<=(start_time+plot_dur);

% compute the design matrix for the model
dly = floor(-tmax/1000*eFs):ceil(-tmin/1000*eFs);
% remove the moving average of the EEG
mveeg = detrend(eeg{tr}(:,chn_plot));
mveeg = mveeg - movmean(mveeg,length(dly));
X = lagGen(mveeg,dly);
X = X(t_idx,:);

% compute the spline transformation matrix
[spl_matrix,splX] = spline_transform(X,ds);

%% Plotting
% Plot the original design matrix
EEGplot(fliplr(X(:,1:10:end)),eFs,1);
set(gcf,'Position',[100 415 700 300]);
title('Original design matrix');

% Plot the three steps for the spline transformation
figure
subplot(1,3,1)
plot(-dly/eFs*1000,X(1,:),'k');
xlabel('Delay (ms)');
ylabel('EEG');

subplot(1,3,2)
plot(-dly/eFs*1000,spl_matrix');
xlabel('Delay (ms)');
ylabel('Spline basis');

subplot(1,3,3)
hold on
plot(-dly/eFs*1000,X(1,:),'k');
plot(-dly/eFs*1000,splX(1,:)*spl_matrix','b','LineWidth',2);
xlabel('Delay (ms)');
ylabel('Spline-transformed EEG');

% Plot the spline-basis design matrix
EEGplot(fliplr(splX(:,1:5:end)),eFs,1);
set(gcf,'Position',[100 415 700 300]);
title('Spline-transformed design matrix');