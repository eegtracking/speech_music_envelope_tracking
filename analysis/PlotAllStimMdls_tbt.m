% Plot the topography of the model weights for a subject and particular
% window size
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbj = 'AB';
stimtypes = {'rock','classical','vocals','speech'};
clr = {'r','g','c','b'};
maxtr = 7;
wnd = 125;
nchan = 128;
npc = 64;
Fs = 512;
% plot_chans = 65:109;
plot_chans = 1:128;
topo_dly = [50 100];
dly = -(-ceil(wnd/1000*Fs):0);

res_fld = '../rcnstr_tbt/';
% wnd_fld = sprintf('%dms/',round(wnd));

M = NaN(length(dly),nchan,maxtr,length(stimtypes));
for s = 1:length(stimtypes)
    fl = sprintf('%s_%s_%dms',sbj,stimtypes{s},round(wnd));

%     d = load([res_fld wnd_fld fl]);  
    d = load([res_fld fl]);

    % Reshape the model so it's delay x time
    nreps = length(d.model_t);
    for n = 1:nreps
        mdl = reshape(d.model_t{n},[length(dly),npc]);
%         mdl = reshape(d.model{n},[length(dly),npc]);
        rM = mdl.*(ones(length(dly),1)*d.var_pc(:,n)')/sum(d.var_pc(:,n));
        M(:,:,n,s) = rM*d.cf{n}(:,1:npc)';
    end
    disp(fl);
end
% Average across reps
M = squeeze(mean(M,3));

figure
hold on
for s = 1:length(stimtypes)
    plot(dly/Fs*1000,mean(M(:,plot_chans,s),2),clr{s},'LineWidth',2)
end
set(gca,'FontSize',16);
title(sprintf('%s, %d ms',sbj,wnd));
xlabel('Time (ms)');
ylabel('Model weight');
legend(stimtypes);

% dly_idx = dly/Fs*1000>=topo_dly(1) & dly/Fs*1000<=topo_dly(2);
% 
% figure
% topoplot(mean(M(dly_idx,:)),'~/Projects/EEGanly/chanlocs.xyz','maplimits','maxmin');
% colorbar;
% title(sprintf('%d - %d ms',dly_range(1),dly_range(2)));