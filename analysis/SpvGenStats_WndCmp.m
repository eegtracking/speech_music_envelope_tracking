% Statistically compare 250 - 2000 ms window performance for vocals and speech
% Nate Zuk (2021)

maxtr = 7;
nsbjs = length(sbjs);
nchan = 128;
chan_idx = [19 85];
chan_lbl = {'Pz','Fz'};

% Load the general model fit results for 250 ms model
wnd = 250; % window size
res_fl = sprintf('SpvGenCmp_%dms',wnd);
d = load(res_fl);
MAG_250 = permute(d.mdl_fit_mag,[1 4 2 3]);
MAG_250 = reshape(MAG_250,[maxtr*nsbjs nchan length(stimtypes)]);

% Load the general model fit results for 2000 ms model
wnd = 2000; % window size
res_fl = sprintf('SpvGenCmp_%dms',wnd);
d = load(res_fl);
MAG_2000 = permute(d.mdl_fit_mag,[1 4 2 3]);
MAG_2000 = reshape(MAG_2000,[maxtr*nsbjs nchan length(stimtypes)]);

% Compute the difference in scaling factors between Pz and Fz
DIFFMAG_250 = squeeze(diff(MAG_250(:,[85 19],:),[],2));
DIFFMAG_2000 = squeeze(diff(MAG_2000(:,[85 19],:),[],2));

% Stats
disp('Pz-Fz difference between 250 ms and 2000 ms models (signed-rank test):')
disp('(positive z-val means 250 > 2000)');
[pdiff_vocals,~,stdiff_vocals] = signrank(DIFFMAG_250(:,3),DIFFMAG_2000(:,3));
fprintf('Vocals: z = %.3f, p = %.3f\n',stdiff_vocals.zval,pdiff_vocals);
[pdiff_speech,~,stdiff_speech] = signrank(DIFFMAG_250(:,4),DIFFMAG_2000(:,4));
fprintf('Speech: z = %.3f, p = %.3f\n',stdiff_speech.zval,pdiff_speech);