% Plot the topography of the model weights for a subject and particular
% window size
%%% Run SbjPlotRcnstr_tbt first
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbj = 'EB';
stimtypes = {'rock','classical','vocals','speech'};
clr = {'r','g','c','b'};
maxtr = 7;
wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000 16000];
nchan = 128;
npc = 64;
Fs = 512;
plot_chans = 65:109;
% plot_chans = 1:128;
% dly = -(-ceil(wnd/1000*Fs):0);

res_fld = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';

% M = NaN(length(dly),nchan,maxtr,length(stimtypes));
all_mdls = cell(length(wnd),1);
DLY = cell(length(wnd),1); % to store the delay vectors for the models
for w = 1:length(wnd) % for each model window size
    % compute the delay vector, in indexes
    DLY{w} = -(-ceil(wnd(w)/1000*Fs):0); % see lagGen, delays are ordered
        % from -max to -min in the backwards model
    M = NaN(length(DLY{w}),nchan,maxtr,length(stimtypes));
    
    for s = 1:length(stimtypes)
        fl = sprintf('%s_%s_%dms',sbj,stimtypes{s},round(wnd(w)));

        d = load([res_fld fl]);

        % Reshape the model so it's delay x time
        nreps = length(d.model_t);
        for n = 1:nreps
            mdl = reshape(d.model_t{n},[length(DLY{w}),npc]);
            % normalize each model weight based on the ratio of PC variance
            % to total variance
            rM = mdl.*(ones(length(DLY{w}),1)*d.var_pc(:,n)')/sum(d.var_pc(:,n));
            M(:,:,n,s) = rM*d.cf{n}(:,1:npc)';
        end
        disp(fl);
    end
    % Average across reps
    all_mdls{w} = squeeze(mean(M,3,'omitnan'));
end

% compute the average of the zrs (see SbjPlotRcnstr_tbt)
mean_zrs = squeeze(mean(zr,1,'omitnan')); 
max_zr_val = max(reshape(zr,[numel(zr),1]),[],'omitnan'); % compute the maximum possible zr value
min_zr_val = min(reshape(zr,[numel(zr),1]),[],'omitnan');

figure
max_ln_wdth = 5; % maximum possible line width
min_ln_wdth = 0.1; % minimum possible line width
for s = 1:length(stimtypes)
    subplot(length(stimtypes),1,s); % different plot for each stimulus
    hold on
    for w = 1:length(wnd)
        norm_zr = (mean_zrs(w,s)-min_zr_val)/(max_zr_val-min_zr_val); % normalize zr value to be 0 to 1
        ln_wdth = norm_zr*(max_ln_wdth-min_ln_wdth) + min_ln_wdth; 
        plot(DLY{w}/Fs*1000,mean(all_mdls{w}(:,plot_chans,s),2),clr{s},'LineWidth',ln_wdth)
    end
    set(gca,'FontSize',16,'XScale','log');
    ylabel('w');
    title(sprintf('%s, %s',sbj,stimtypes{s}));
end
xlabel('Time (ms)');

% Just plot the shorter timescales
figure
for s = 1:length(stimtypes)
    subplot(length(stimtypes),1,s); % different plot for each stimulus
    hold on
    for w = 1:length(wnd)/2
        norm_zr = (mean_zrs(w,s)-min_zr_val)/(max_zr_val-min_zr_val); % normalize zr value to be 0 to 1
        ln_wdth = norm_zr*(max_ln_wdth-min_ln_wdth) + min_ln_wdth; 
        plot(DLY{w}/Fs*1000,mean(all_mdls{w}(:,plot_chans,s),2),clr{s},'LineWidth',ln_wdth)
    end
    set(gca,'FontSize',16);
    ylabel('w');
    title(sprintf('%s, %s',sbj,stimtypes{s}));
end
xlabel('Time (ms)');