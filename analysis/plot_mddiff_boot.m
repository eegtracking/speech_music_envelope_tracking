function [plt_lines,mddiff,uqdiff,lqdiff] = plot_mddiff_boot(x,y,nboot,clrs)
% Plot the difference in median x and median y values within its their two dimensions and
% the 95-percentile range of difference of medians.  The range of differences is computed by
% shuffling the values of x and y within the first dimension, so that each column
% of y contains the same number of bootstrapped values, and recalculating the median.
% The range is plotted as a shaded region around the median line. 
% If either x or y is a 3-D array, each median is the difference relative
% to the singleton 4th dimension of the 3-D array.
% If either x and y is a 4-D array, the data along the fourth dimension will be plotted 
% as its own line.
% ** Uses shadedErrorBar to make the plots.
% Inputs:
% - x, y = data values, can be either a 3-D or 4-D array (but must be the
% same size)
% - nboot = number of times to do bootstrap (default = 1000);
% - clrs = the colors for each line (default: black if only one line, 'jet'
%       colormap if multiple lines)
% Outputs:
% - plt_lines = handles for the median lines in the plot
% - mdy = median values
% - uqy = 95-percentile of the bootstrapped medians
% - lqy = 5-percentile of the boostrapped medians
% Nate Zuk (2020)

addpath('~/Documents/MATLAB/shadedErrorBar/');

% Make sure x and y are the same size for the first three dimensions
sx = [size(x,1) size(x,2) size(x,3)]; sy = [size(y,1) size(y,2) size(y,3)];
if any(sx~=sy)
    error('The first three dimensions of x and y must be the same size');
end

% Make sure the 4th dimension of x and y are the same, otherwise either of
% them can be at most 1
if size(x,4)~=size(y,4)
    if size(x,4)>1 && size(y,4)>1
        error('4th dimension of x and y must be the same size or at most 1')
    end
end

% Determine what the colors should be for the plot
nlines = max([size(x,4) size(y,4)]); % get the number of lines
if nargin<3
    if size(y,4)==1 
        clrs = {'k'}; % just plot in black
    else % use an even spacing along the 'jet' colormap to get the colors
        cmap = colormap('jet');
        clrs = cell(nlines,1);
        for n = 1:nlines
            clr_idx = round((n-1)/nlines*254)+1; % get an even spacing of colors along the map
            cmap{n} = cmap(clr_idx,:);
        end
    end
end

% If nboot isn't specified, using 1000
if nargin<2, nboot = 1000; end

%% Compute the median and bootstrapped distribution of the median
X = reshape(x,[size(x,1)*size(x,2) size(x,3) size(x,4)]);
Y = reshape(y,[size(y,1)*size(y,2) size(y,3) size(y,4)]);
% Make the same size before computing difference
if size(x,4)==1, X = repmat(X,1,1,1,size(y,4)); end
if size(y,4)==1, Y = repmat(Y,1,1,1,size(x,4)); end
mddiff = squeeze(median(Y,'omitnan'))-squeeze(median(X,'omitnan'));

bootdiff = NaN(nboot,size(y,3),nlines);
for n = 1:nboot
    % randomly sample values with replacement, so that there are the same
    % number of samples for column
    boot_x = NaN(size(x));
    boot_y = NaN(size(y));
    for s = 1:size(y,2)
        for jj = 1:size(y,3)
            % x values
            for kk = 1:size(x,4)
                tmpx = x(:,s,jj,kk);
                % only shuffle non-NaN values
                shfidx = randi(sum(~isnan(tmpx)),sum(~isnan(tmpx)),1);
                tmpx_idx = find(~isnan(tmpx)); % get the indexes of non-NaN values
                boot_x(1:sum(~isnan(tmpx)),s,jj,kk) = tmpx(tmpx_idx(shfidx));
            end
            % y values
            for kk = 1:size(y,4)
                tmpy = y(:,s,jj,kk);
                % only shuffle non-NaN values
                shfidx = randi(sum(~isnan(tmpy)),sum(~isnan(tmpy)),1);
                tmpy_idx = find(~isnan(tmpy)); % get the indexes of non-NaN values
                boot_y(1:sum(~isnan(tmpy)),s,jj,kk) = tmpy(tmpy_idx(shfidx));
            end
        end
    end
    BX = reshape(boot_x,[size(x,1)*size(x,2) size(x,3) size(x,4)]);
    BY = reshape(boot_y,[size(y,1)*size(y,2) size(y,3) size(y,4)]);
    if size(x,4)==1, BX = repmat(BX,1,1,1,size(y,4)); end
    if size(y,4)==1, BY = repmat(BY,1,1,1,size(x,4)); end
    bootdiff(n,:,:) = squeeze(median(BY,'omitnan'))-squeeze(median(BX,'omitnan'));
end
% compute the interquartile range of the bootstrapped medians
uqdiff = squeeze(quantile(bootdiff,0.95));
lqdiff = squeeze(quantile(bootdiff,0.05));

%% Plotting
figure
hold on
plt_lines = NaN(nlines,1);
for jj = 1:nlines
    pl = shadedErrorBar(1:size(y,3),mddiff(:,jj),[mddiff(:,jj)-lqdiff(:,jj) uqdiff(:,jj)-mddiff(:,jj)],...
        'lineProps',{'Color',clrs{jj}});
    plt_lines(jj) = pl.mainLine;
end