% Load all of the drum reconstructions as well as the reconstruction
% accuracies for the drum instruments. Check to make sure that the
% reconstruction accuracies are equal. This is to validate that the
% reconstructions are identical to the ones computed earlier, before the
% reconstructions themselves are analyzed further.
% Nate Zuk (2020)

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KRK','MB',...
    'MT','SL','SOS'};
wnd = [125 250 500 1000];
maxtr = 7; % maximum number of trials per subject
classicalpth = '/Volumes/ZStore/NeuroMusic/new_analyses/classical_rcnstrs/';
stimspth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';

r_classical = NaN(maxtr,length(wnd),length(sbjs)); % for the drum reconstruction results
r_stims = NaN(maxtr,length(wnd),length(sbjs)); % for the general instrument reconstruction results
for s = 1:length(sbjs)
    for w = 1:length(wnd)
        % drum reconstruction
        classicalfl = sprintf('ClassicalRcnstr_%s_%dms',sbjs{s},wnd(w));
        S_classical = load([classicalpth classicalfl]);
        ntr = length(S_classical.r);
        r_classical(1:ntr,w,s) = S_classical.r;
        disp(classicalfl)
        % instrument reconstruction
        stimsfl = sprintf('%s_classical_%dms',sbjs{s},wnd(w));
        S_stims = load([stimspth stimsfl]);
        r_stims(1:ntr,w,s) = S_stims.r;
        disp(stimsfl)
    end
end

% Compute the difference between these values
diffr = r_classical-r_stims;
% Compute the error for each subject
maxdiffr = NaN(length(sbjs),1);
mindiffr = NaN(length(sbjs),1);
for s = 1:length(sbjs)
    DIFFR = reshape(diffr(:,:,s),[maxtr*length(wnd),1]);
    maxdiffr(s) = max(DIFFR);
    mindiffr(s) = min(DIFFR);
end