% Plot z-scored instrument-based reconstruction accuracies for all
% subjects.
% - Plot: z-scored reconstruction accuracies for each instrument, overlaid
% with the median reconstruction accuracies for rock reconstruction
% - Plot: difference between z-scored instrument accuracy and rock accuracy
% - Plot: power spectrum of reconstructions, averaged across subjects, for
% each trial (identify maximum synchronization)
% Nate Zuk (2020)

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
stimtype = {'vocals','guitar','bass','drums'};
% clr = {'r','g','c','b'};
clr = {[0.8 0 0.8],[0.9 0.5 0],[0.8 0.8 0],[0.8 0 0]};
rock_clr = [1 0.5 0];
wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000 16000];
maxtr = 7;
niter = 50;
respth = '/Volumes/ZStore/NeuroMusic/new_analyses/instr_tbt/';
rockpth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';
nperm = 1000*length(wnd)*(length(stimtype)-1); 
    % number of times to permute the data to get a null distribution of median differences

% Load the no-averaging results
r = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
nr = NaN(niter,length(wnd),length(stimtype),length(sbjs));
zr = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
r_rock = NaN(maxtr,length(wnd),length(sbjs));
nr_rock = NaN(niter,length(wnd),length(sbjs));
zr_rock = NaN(maxtr,length(wnd),length(sbjs));
for s = 1:length(sbjs)
    for ii = 1:length(wnd),
        % load the results for the individual instrument types
        for jj = 1:length(stimtype),
            fl = sprintf('%s_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d = load([respth fl]);
            ntr = size(d.r,1);
            r(1:ntr,ii,jj,s) = reshape(d.r,[ntr 1]);
            nr(:,ii,jj,s) = d.nullr;
            zr(1:ntr,ii,jj,s) = (d.r-mean(d.nullr))/std(d.nullr);
            disp(fl)
        end
        % load the results for rock music
        fl_rock = sprintf('%s_rock_%dms',sbjs{s},round(wnd(ii)));
        d_rock = load([rockpth fl_rock]);
        r_rock(1:ntr,ii,s) = reshape(d_rock.r,[ntr 1]);
        nr_rock(:,ii,s) = d_rock.nullr;
        zr_rock(1:ntr,ii,s) = (d_rock.r-mean(d_rock.nullr))/std(d_rock.nullr);
        disp(['* ' fl_rock]);
    end
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*8);
end

% Plot zr
figure
set(gcf,'Position',[300,300,550,400]);
hold on
z_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype)
    pl = plot(1:length(wnd),squeeze(mean(zr(:,:,jj,:),1,'omitnan')),'Color',clr{jj},'LineWidth',1.5);
    z_plts(jj) = pl(1);
end
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Z-transformed reconstruction accuracy');
legend(z_plts,stimtype);

% Reshape zr so that we can compute 
rshp_zr = permute(zr,[1 4 2 3]);
rshp_rockzr = permute(zr_rock,[1 3 2]);
ROCKZR = reshape(rshp_rockzr,[maxtr*length(sbjs) length(wnd)]);

mdzr_plts = plot_medians_boot(rshp_zr,1000,clr);
set(gcf,'Position',[300,300,550,400]);
mdzr_plts(5) = plot(1:length(wnd),median(ROCKZR,'omitnan'),'--','Color',rock_clr,'LineWidth',3);
plot([1 length(wnd)],[0 0],'k');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Z-scored reconstruction accuracy');
legend(mdzr_plts,[stimtype {'rock'}]);

% Compute the difference in reconstruction accuracy between the individual
% instrument reconstruction and the rock reconstruction (> 0 if indiv
% instrument is better reconstructed)
% (This is done pairwise for each trial)
diff_zr = NaN(maxtr,length(sbjs),length(wnd),length(stimtype));
for jj = 1:length(stimtype)
    diff_zr(:,:,:,jj) = rshp_zr(:,:,:,jj) - rshp_rockzr;
end

% Plot difference in reconstruction accuracy (speech relative to other
% stimuli)
figure
set(gcf,'Position',[665,300,550,400]);
hold on
dz_plts = NaN(length(stimtype),1);
for jj = 1:(length(stimtype))
    diff_zr_mn = squeeze(mean(zr(:,:,jj,:),1,'omitnan'))-squeeze(mean(zr_rock,1,'omitnan'));
    pl = plot(1:length(wnd),diff_zr_mn,'Color',clr{jj},'LineWidth',1.5);
    dz_plts(jj) = pl(1);
end
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Z-r speech rel to other stimuli');
legend(dz_plts,stimtype(1:length(stimtype)));

% Plot the median and bootstrapped 95% range of median differences
instrdiff_plts = plot_medians_boot(diff_zr,1000,clr);
set(gcf,'Position',[665,300,550,400]);
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Z-r instrument rel to full rock env');
legend(instrdiff_plts,stimtype(1:length(stimtype)));

% Test if z-scored reconstruction accuracy is significantly larger than
% 0 for each stimtype/wnd pairing (identifies range of frequencies that
% contain information)
ZR = reshape(permute(zr,[1 4 2 3]),[maxtr*length(sbjs) length(wnd) length(stimtype)]);
p_zr = NaN(length(wnd),length(stimtype));
st_zr = cell(length(wnd),length(stimtype));
for jj = 1:length(stimtype)
    for w = 1:length(wnd)
        [p_zr(w,jj),~,st_zr{w,jj}] = signrank(ZR(:,w,jj),0,'tail','right');
    end
end

% Test if reconstruction accuracy for the instrument is significantly larger than
% for the full rock envelope, based on pairwise trial-by-trial differences
DIFFZR = reshape(diff_zr,[maxtr*length(sbjs) length(wnd) length(stimtype)]);
p_diffzr = NaN(length(wnd),length(stimtype));
st_diffzr = cell(length(wnd),length(stimtype));
for jj = 1:length(stimtype)
    for w = 1:length(wnd)
        [p_diffzr(w,jj),~,st_diffzr{w,jj}] = signrank(DIFFZR(:,w,jj),0,'tail','both');
    end
end