% Compute z-scored reconstruction accuracies, and plot z-scored
% reconstruction accuracies vs dB envelope power within the same frequency
% range (proportional to signal power)

addpath('~/Documents/MATLAB/shadedErrorBar/');
addpath('..');

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
stimtype = {'rock','classical','vocals','speech'};
% clr = {'r','g','c','b'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000 16000];
tottr = 10;
niter = 50;
Fs = 512; % sampling rate of stimulus envelope
tlims = 16; % amount of time to skip from the start and end of the stimulus,
    % identical to range of time used for reconstruction
respth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';
envpth = '/Volumes/ZStore/NeuroMusic/';
eegpth = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/'; % needed to get the original stimulus trigger values
nperm = 1000*length(wnd)*(length(stimtype)-1); 
    % number of times to permute the data to get a null distribution of median differences
    
% Load the names of all EEG data files
% fls = what(eegpth);
% eeg_fls = fls.mat;
% flag_presented_stim = false(tottr,length(stimtype),length(sbjs));
% for s = 1:length(sbjs)
%     % identify files for this subject
%     sbj_mats = cellfun(@(x) strcmp(x(1:length(sbjs{s})+1),[sbjs{s} '_']),eeg_fls);
%     sbj_mats = find(sbj_mats); % get their indexes in the eeg_fls array
%     for m = 1:length(sbj_mats)
%         % Load the mat file
%         b = load([eegpth eeg_fls{sbj_mats(m)}]);
%         stimtrig = (b.stim(b.stim<80)); % get stimulus trigs less than 80 (85 is a tone-evoked trial)
%         stim_val = floor(stimtrig/10); % stimulus type (10-19=rock; 20-29=classical; 30-39=vocals; 40-49=speech)
%         tr_val = mod(stimtrig,10)+1;
%         % indicate which stimuli were presented
%         for n = 1:length(stimtrig)
%             flag_presented_stim(tr_val(n),stim_val(n),s) = true;
%         end
%         % show that this data file was loaded
%         disp(eeg_fls{sbj_mats(m)});
%     end
% end
% 
% save('Presented_stim_by_subject','flag_presented_stim','tottr','sbjs','stimtype');

load('Presented_stim_by_subject');

% Compute z-scored reconstruction accuracies
zr = NaN(tottr,length(wnd),length(stimtype),length(sbjs));
for s = 1:length(sbjs)
    for ii = 1:length(wnd),
        for jj = 1:length(stimtype),
            fl = sprintf('%s_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d = load([respth fl]);
            % Get the stimulus indexes that were presented for this
            % subject
            idx = flag_presented_stim(:,jj,s);
            zr(idx,ii,jj,s) = (d.r-mean(d.nullr))/std(d.nullr);
            disp(fl)
        end
    end
end

% Compute average power of dB envelope within the same corresponding frequency ranges
dbenv_pw = NaN(tottr,length(wnd),length(stimtype));
for jj = 1:length(stimtype)
    env_fl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',Fs,stimtype{jj});
    S = load([envpth env_fl]); % load the file of stimulus envelopes
    for n = 1:tottr
        stim = S.stimset{n}; % get the stimulus envelope for that trial
        % remove time from start and end (like in reconstructions)
        tidx = usetinds(tlims,Fs,length(stim));
        % zero-center the stimulus envelope, we only care about variation
        % in dB
        stim = detrend(stim(tidx),0);
        % compute the power spectrum (Welch's)
        [Pw,frq] = pwelch(stim,max(wnd)/1000*Fs,max(wnd)/1000/2*Fs,max(wnd)/1000*Fs,Fs);
        for ii = 1:length(wnd)
            frq_range = [1000/wnd(ii) 1000/wnd(ii)*8]; % frequency range of analysis
            frq_idx = frq>=frq_range(1) & frq<=frq_range(2);
            dbenv_pw(n,ii,jj) = mean(Pw(frq_idx));
        end
    end
    disp(env_fl);
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
% xtick_lbl = cell(length(wnd),1);
% for n = 1:length(wnd)
%     xtick_lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*8);
% end

% Rearrange zr so trials x subjects are along a column
ZR = reshape(permute(zr,[1 4 2 3]),[tottr*length(sbjs) length(wnd) length(stimtype)]);
% Plot ZR vs dB envelope power for each frequency range, and compute
% correlations
corr_env = NaN(length(wnd),1);
pcorr_env = NaN(length(wnd),1);
plcorr = NaN(length(stimtype),1);
figure
for ii = 1:length(wnd)
    subplot(2,length(wnd)/2,ii);
    hold on
    for jj = 1:length(stimtype)
        zr_tmp = ZR(:,ii,jj);
        % repeat db envelope power array so there's a value for each subject
        envpw_tmp = repmat(dbenv_pw(:,ii,jj),[length(sbjs) 1]);
        plcorr(jj) = plot(envpw_tmp,zr_tmp,'.','Color',clr{jj},'MarkerSize',14);
    end
    set(gca,'FontSize',12,'XScale','log');
    xlabel('Average envelope power');
    ylabel('Z-scored r');
    title(sprintf('%.3g - %.3g Hz',1000/wnd(ii),1000/wnd(ii)*8));
    % Compute correlation between envelope power and zr across stimulus
    % types
    zr_cmp = squeeze(ZR(:,ii,:)); 
    zr_cmp = reshape(zr_cmp,[tottr*length(sbjs)*length(stimtype) 1]);
    envpw_cmp = repmat(squeeze(dbenv_pw(:,ii,:)),[length(sbjs) 1]);
    envpw_cmp = reshape(envpw_cmp,[tottr*length(sbjs)*length(stimtype) 1]);
    [corr_env(ii),pcorr_env(ii)] = corr(envpw_cmp,zr_cmp,'type','Spearman','rows','complete');
end
legend(plcorr,stimtype);