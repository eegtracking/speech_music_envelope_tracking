% Average the reconstructions across subjects for each trial and compute
% the power spectrum.  Normalize this by the average after randomizing the
% phases in each reconstruction.  Check where peaks occur relative to the
% BPM of the music.
% Nate Zuk (2020)

addpath('..');

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB',...
    'MT','SL','SOS'};
wnd = [125 250 500];
tottr = 10; % total number of trials (6-7 were presented per subject)
drumpth = '/Volumes/ZStore/NeuroMusic/new_analyses/drum_rcnstrs/';
Fs = 512;
nshuff = 100; % number of times to iteratively generate "null" averaged reconstructions

% % Load the reconstructions
% rcnstrs = cell(tottr,length(wnd)); % to store all reconstructions
% sbjs_by_trials = cell(tottr,1); % stores which subject was recorded for each stimulus
% for n = 1:tottr % set up arrays in each cell
%     for w = 1:length(wnd) % set up for each window size
%         rcnstrs{n,w} = [];
%     end
%     sbjs_by_trials{n} = {};
% end
% for s = 1:length(sbjs)
%     for w = 1:length(wnd)
%         % drum reconstruction
%         drumfl = sprintf('DrumRcnstr_%s_%dms',sbjs{s},wnd(w));
%         S_drum = load([drumpth drumfl]);
%         % get the stimulus indexes for the stimuli that were presented to
%         % this subject
%         stimidx = S_drum.stim_used;
%         for n = 1:length(stimidx)
%             % z-score each prediction (ensure the overall power is the same
%             % when comparing across models)
%             rcnstrs{stimidx(n),w} = [rcnstrs{stimidx(n),w} S_drum.pred{n}];
%         end
%         disp(drumfl)
%     end
%     % save the subject ID for all stimuli that were presented to that
%     % subject
%     for n = 1:length(stimidx)
%         sbjs_by_trials{stimidx(n)} = [sbjs_by_trials{stimidx(n)} sbjs(s)];
%     end
% end
% 
% % For each stimulus, average the reconstructions across subjects, then
% % average the phase-randomized reconstructions across subjects 100 times
% pwspec = NaN(5*Fs+1,length(wnd),tottr);
% nullspec = NaN(5*Fs+1,length(wnd),tottr);
% for n = 1:tottr
%     for w = 1:length(wnd)
%         spectm = tic;
%         R = rcnstrs{n,w};
%         % average across subjects
%         avgR = zscore(mean(R,2));
%         % compute the power spectrum
%         [pwspec(:,w,n),frq] = pwelch(avgR,10*Fs,5*Fs,10*Fs,Fs);
%         % Compute the null spectrum by randomizing the phases of the
%         % reconstructions for each subjects and averaging them together
%         nl = NaN(5*Fs+1,nshuff);
%         for ii = 1:nshuff
%             % randomize phases
%             nullR = NaN(size(R));
%             for jj = 1:size(R,2)
%                 nullR(:,jj) = randomize_phases(R(:,jj));
%             end
%             % compute average and spectrum
%             nullavgR = zscore(mean(nullR,2));
%             nl(:,ii) = pwelch(nullavgR,10*Fs,5*Fs,10*Fs,Fs);
%         end
%         nullspec(:,w,n) = mean(nl,2);
%         % show that this combo was completed
%         fprintf('Wnd %d ms, stim %d: completed @ %.3f s\n',wnd(w),n,toc(spectm));
%     end
% end
% 
% save('DrumPwSpec','pwspec','nullspec','frq','sbjs_by_trials','Fs');

load('DrumPwSpec');

% Make the legend for the spectrum plots, different window sizes
spec_leg = cell(length(wnd),1);
for w = 1:length(wnd)
    spec_leg{w} = sprintf('%d ms',wnd(w));
end

% Compute the adjusted PSD
diffspec = pwspec-nullspec;

% Plot the adjusted spectrum
% (note that I'm not working with dB values, I found that peaks were
% clearer when working with power spectral amplitude, V^2/freq)
figure
for n = 1:tottr
    subplot(5,2,n);
    plot(frq,diffspec(:,:,n),'LineWidth',1.5);
    xlabel('Frequency (Hz)');
    set(gca,'FontSize',12,'XLim',[1 20]);
    legend(spec_leg)
    title(['Stim ' num2str(n)]);
end

% Compute the average tempo (in Hz), based on the beat timings computed by Ellis's
% model (2007)
bttms_pth = '/Volumes/ZStore/NeuroMusic/All Stimuli/Rock_Music_Beats/';
fls = dir(bttms_pth); % get the list of files in this directory
tempo = NaN(tottr,1);
for f = 3:length(fls) % skip the first two files which are . and ..
    fid = fopen([bttms_pth fls(f).name]);
    beats = fscanf(fid,'%f');
    tempo(f-2) = 1/median(diff(beats)); % compute the tempo based on the median beat difference
    fclose(fid);
    disp(fls(f).name);
end

% Compute thespectrum differences relative to 1x, 2x, 3x, and
% 4x the tempo of the music
peak_spec_diffs = NaN(4,tottr,3); % stims x scaling x window
peak_freqs = NaN(4,tottr,3);
for n = 1:tottr
    for ii = 1:4 % for each scaling factor
        % look for maximum +/-8% surrounding peak (see McKinney et al)
        peak_range = frq>=0.92*tempo(n)*ii & frq<=1.08*tempo(n)*ii;
        peak_spec_diffs(ii,n,:) = max(diffspec(peak_range,:,n));
        % get the frequency at the peak value for each model
        frq_vals = frq(peak_range);
        pkf = NaN(3,1);
        for w = 1:length(wnd)
%             pkf = frq_vals(diffspec(peak_range,w,n)==max(peak_spec_diffs(ii,n,w)));
            peak_freqs(ii,n,w) = frq_vals(diffspec(peak_range,w,n)==max(peak_spec_diffs(ii,n,w)));
        end
%         peak_freqs(ii,n) = mean(pkf);
    end
end
% plot the max differences across windows (mean might bias towards a
% particular frequency range, since 1-2 Hz is not represented by 2 of the
% windows)
cmap = colormap('jet');
figure
hold on
for n = 1:tottr
    clridx = round(((n-1)/tottr)*63)+1;
    plot(1:4,max(peak_spec_diffs(:,n,:),[],3),'.-','Color',cmap(clridx,:),'LineWidth',1.5,'MarkerSize',18);
end
set(gca,'FontSize',16,'XTick',1:4);
xlabel('Scaling relative to tempo');
ylabel('Max spectrum across models');

% plot the max differences as a function of frequency (see note above)
figure
hold on
for n = 1:tottr
    clridx = round(((n-1)/tottr)*63)+1;
    pks = squeeze(peak_spec_diffs(:,n,:));
    pkf = NaN(4,1);
    for ii = 1:4
        maxidx = find(pks(ii,:)==max(pks(ii,:)));
        pkf(ii) = peak_freqs(ii,n,maxidx);
    end
    plot(pkf,max(peak_spec_diffs(:,n,:),[],3),'.-','Color',cmap(clridx,:),'LineWidth',1.5,'MarkerSize',18);
end
set(gca,'FontSize',16,'XLim',[1 12]);
xlabel('Frequency (Hz)');
ylabel('Max spectrum across models');

% Plot both the null and power spectral density for trial one, and show the
% adjust PSD for that trial
figure
set(gcf,'Position',[335 80 450 620]);
clrs = [0 0 1; 1 0 0; 0 0.7 0];
subplot(2,1,1);
hold on
for w = 1:length(wnd)
    plot(frq,nullspec(:,w,1),'--','Color',clrs(w,:),'LineWidth',1.5);
end
for w = 1:length(wnd)   
    plot(frq,pwspec(:,w,1),'Color',clrs(w,:),'LineWidth',1.5);
end
set(gca,'FontSize',14,'XLim',[1 12]);
xlabel('Frequency (Hz)');
ylabel('PSD (V^2/Hz)');

subplot(2,1,2);
hold on
spec_pl = NaN(3,1);
for w = 1:length(wnd)
    spec_pl(w) = plot(frq,diffspec(:,w,1),'Color',clrs(w,:),'LineWidth',1.5);
    % plot dots at the peak values
    for ii = 1:4 % check each peak
        pk = squeeze(peak_spec_diffs(ii,1,:));
        if pk(w) == max(pk)
            plot(peak_freqs(ii,1,w),pk(w),'.','Color',clrs(w,:),'MarkerSize',18);
        end
    end
end
set(gca,'FontSize',14,'XLim',[1 12]);
xlabel('Frequency (Hz)');
ylabel('Adjusted PSD');
legend(spec_pl,spec_leg);