% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
stimtype = {'rock','classical','vocals','speech'};
% clr = {'r','g','c','b'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000 16000];
maxtr = 7;
niter = 50;
nstd_zr = 2; % number of standard deviations above chance distribution to signify
    % significantly better reconstruction
respth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';
nperm = 1000*length(wnd)*(length(stimtype)-1); 
    % number of times to permute the data to get a null distribution of median differences

% Load the no-averaging results
r = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
nr = NaN(niter,length(wnd),length(stimtype),length(sbjs));
zr = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
for s = 1:length(sbjs)
    for ii = 1:length(wnd),
        for jj = 1:length(stimtype),
            fl = sprintf('%s_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d = load([respth fl]);
            ntr = size(d.r,1);
            r(1:ntr,ii,jj,s) = reshape(d.r,[ntr 1]);
            nr(:,ii,jj,s) = d.nullr;
            zr(1:ntr,ii,jj,s) = (d.r-mean(d.nullr))/std(d.nullr);
            disp(fl)
        end
    end
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*8);
end

% Plot zr
figure
set(gcf,'Position',[300,300,550,400]);
hold on
z_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype)
    pl = plot(1:length(wnd),squeeze(mean(zr(:,:,jj,:),1,'omitnan')),'Color',clr{jj},'LineWidth',1.5);
    z_plts(jj) = pl(1);
end
plot([1 length(wnd)],[nstd_zr nstd_zr],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Z-transformed reconstruction accuracy');
legend(z_plts,stimtype);

% Reshape zr so that we can compute 
rshp_zr = permute(zr,[1 4 2 3]);
rshp_r = permute(r,[1 4 2 3]); 
% this will be used to compute the difference in reconstruction accuracies
% (not z-scored accuracies)

mdzr_plts = plot_medians_boot(rshp_zr,1000,clr);
set(gcf,'Position',[300,300,550,400]);
plot([1 length(wnd)],[0 0],'k');
plot([1 length(wnd)],[nstd_zr nstd_zr],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Z-scored reconstruction accuracy');
legend(mdzr_plts,stimtype);

mdr_plts = plot_medians_boot(rshp_r,1000,clr);
set(gcf,'Position',[300,300,550,400]);
plot([1 length(wnd)],[0 0],'k');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Reconstruction accuracy');
legend(mdr_plts,stimtype);

% ZR = reshape(rshp_zr,[maxtr*length(sbjs) length(wnd) length(stimtype)]);
% 
% % Plot median and bootstrapped standard error of the median
% mdzr = squeeze(median(ZR,'omitnan'));
% nboot = 1000;
% bootmd = NaN(nboot,length(wnd),length(stimtype));
% for n = 1:nboot
%     % randomly sample values with replacement, so that there are the same
%     % number of samples for each subject
%     boot_zr = NaN(size(rshp_zr));
%     for s = 1:length(sbjs)
%         for jj = 1:length(wnd)
%             for kk = 1:length(stimtype)
%                 tmpzr = rshp_zr(:,s,jj,kk);
%                 shfidx = randi(sum(~isnan(tmpzr)),sum(~isnan(tmpzr)),1);
%                 tmpzr_idx = find(~isnan(tmpzr));
%                 boot_zr(1:sum(~isnan(tmpzr)),s,jj,kk) = tmpzr(tmpzr_idx(shfidx));
%             end
%         end
%     end
%     BZR = reshape(boot_zr,[maxtr*length(sbjs) length(wnd) length(stimtype)]);
%     bootmd(n,:,:) = squeeze(median(BZR,'omitnan'));
% %     idx = randi(maxtr*length(sbjs),maxtr*length(sbjs),1);
% %     bootmd(n,:,:) = squeeze(median(ZR(idx,:,:),'omitnan'));
% end
% uqzr = squeeze(quantile(bootmd,0.75));
% lqzr = squeeze(quantile(bootmd,0.25));
% figure
% hold on
% mdzr_plts = NaN(length(stimtype),1);
% for jj = 1:length(stimtype)
%     pl = shadedErrorBar(1:length(wnd),mdzr(:,jj),[mdzr(:,jj)-lqzr(:,jj) uqzr(:,jj)-mdzr(:,jj)],...
%         'lineProps',{'Color',clr{jj}});
%     mdzr_plts(jj) = pl.mainLine;
% end
% set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
%     'XDir','reverse','XTickLabelRotation',45);
% xlabel('Frequency (Hz)');
% ylabel('Z-r speech rel to other stimuli');
% legend(mdzr_plts,stimtype);

% Compute the difference in z-scored reconstruction accuracy, averaged across trials
% of the stimulus type
diff_zr = NaN(length(sbjs),length(wnd),length(stimtype)-1);
for jj = 1:length(stimtype)-1
    diff_zr = squeeze(mean(rshp_zr(:,:,:,4)))-squeeze(mean(rshp_zr(:,:,:,jj)));
end

% To get a sense of how differen the r-values themselves are:
% Compute the difference in reconstruction accuracy, averaged across trials
% of the stimulus type
diff_r = NaN(length(sbjs),length(wnd),length(stimtype)-1);
for jj = 1:length(stimtype)-1
    diff_r = squeeze(mean(rshp_r(:,:,:,4)))-squeeze(mean(rshp_r(:,:,:,jj)));
end

% Plot difference in reconstruction accuracy (speech relative to other
% stimuli)
figure
set(gcf,'Position',[665,300,550,400]);
hold on
dz_plts = NaN(length(stimtype)-1,1);
for jj = 1:(length(stimtype)-1)
    diff_zr = squeeze(mean(zr(:,:,4,:),1,'omitnan'))-squeeze(mean(zr(:,:,jj,:),1,'omitnan'));
    pl = plot(1:length(wnd),diff_zr,'Color',clr{jj},'LineWidth',1.5);
    dz_plts(jj) = pl(1);
end
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Z-r speech rel to other stimuli');
legend(dz_plts,stimtype(1:length(stimtype)-1));

% Plot the median and bootstrapped 95% range of median differences
mddiff_plts = plot_mddiff_boot(rshp_zr(:,:,:,1:3),rshp_zr(:,:,:,4),1000,clr);
set(gcf,'Position',[665,300,550,400]);
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Z-r speech rel to other stimuli');
legend(mddiff_plts,stimtype(1:length(stimtype)-1));

% Plot the median and bootstrapped 95% range of median differences i
% r-values
mddiffr_plts = plot_mddiff_boot(rshp_r(:,:,:,1:3),rshp_r(:,:,:,4),1000,clr);
set(gcf,'Position',[665,300,550,400]);
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('R-value speech rel to other stimuli');
legend(mddiffr_plts,stimtype(1:length(stimtype)-1));

% Test if z-scored reconstruction accuracy is significantly larger than
% 0 for each stimtype/wnd pairing (identifies range of frequencies that
% contain information)
ZR = reshape(permute(zr,[1 4 2 3]),[maxtr*length(sbjs) length(wnd) length(stimtype)]);
p_zr = NaN(length(wnd),length(stimtype));
st_zr = cell(length(wnd),length(stimtype));
for jj = 1:length(stimtype)
    for w = 1:length(wnd)
        [p_zr(w,jj),~,st_zr{w,jj}] = signrank(ZR(:,w,jj),0,'tail','right');
    end
end

% Test for "significant" reconstruction, aka. > 2 std above null
% distribution (identifies frequencies that perform above chance for
% reconstruction)
p_sign_zr = NaN(length(wnd),length(stimtype));
st_sign_zr = cell(length(wnd),length(stimtype));
for jj = 1:length(stimtype)
    for w = 1:length(wnd)
        [p_sign_zr(w,jj),~,st_sign_zr{w,jj}] = signrank(ZR(:,w,jj),2,'tail','right');
    end
end

% Test if reconstruction accuracy for speech is significantly larger than
% for the other stimuli
nulldiffmdzrs = NaN(nperm,length(wnd),length(stimtype)-1);
pspdiff = NaN(length(wnd),length(stimtype)-1);
for jj = 1:length(stimtype)-1
    for w = 1:length(wnd)
        % get a null distribution of median values for permuted zrs by
        % randomly reassigning zrs to either speech or one of the other
        % stimuli, and do the shuffling within subjects
        ZR_otherstim = squeeze(zr(:,w,jj,:));
        ZR_speech = squeeze(zr(:,w,4,:));
        [pspdiff(w,jj),nulldiffmdzrs(:,w,jj)] = permutation_test_subjects(ZR_otherstim,ZR_speech,...
            'nperm',nperm,'tail','both');
    end
end

% Save the reconstruction accuracies (ZR)
save('NeuroMusic_rcnstr','zr','sbjs','stimtype','wnd');