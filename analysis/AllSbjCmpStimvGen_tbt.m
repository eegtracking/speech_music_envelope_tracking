% Get the reconstruction accuracies for stimulus-specific and
% stimulus-general models, and compute the difference between them as a
% function of window size (frequency)
%%% Plot the TRF models of each type of stimulus

addpath('~/Documents/MATLAB/shadedErrorBar/');
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
% sbjs = {'AB','AF','CB','EB','EJ','HC','JH','JS','KR','MB','SL','SOS'}; 
    % without subjects with strong eyeblink topography @ 200-500 ms
stimtype = {'rock','classical','vocals','speech'};
% clr = {'r','g','c','b'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
% wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000];
wnd = [250 2000];
maxtr = 7;
nchan = 128;
plot_chans = 1:128;
% plot_chans = setxor(1:128,[72:74 78:82 91:96]);
Fs = 512;
respth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';

% Load the reconstruction accuracies
r_sp = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
r_gen = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
mdl_sp = cell(length(wnd),length(sbjs));
mdl_gen = cell(length(wnd),length(sbjs));
all_mdl_sp = cell(length(wnd),length(sbjs));
all_mdl_gen = cell(length(wnd),length(sbjs));
DLY = cell(length(wnd),1);
for s = 1:length(sbjs)
    for ii = 1:length(wnd),
        % setup storing info for model
        DLY{ii} = -(-ceil(wnd(ii)/1000*Fs):0); % see lagGen, delays are ordered
        % from -max to -min in the backwards model
        M_sp = NaN(length(DLY{ii}),nchan,maxtr,length(stimtype));
        M_gen = NaN(length(DLY{ii}),nchan,maxtr,length(stimtype));
        for jj = 1:length(stimtype),
            % load the stimulus-general results
            fl_gen = sprintf('%s_ALL_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d_gen = load([respth fl_gen]);
            % stimulus-specific
            fl = sprintf('%s_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d = load([respth fl]);
            ntr = size(d.r,1);
            r_sp(1:ntr,ii,jj,s) = reshape(d.r,[ntr 1]);
            % stimulus-general
            r_gen(1:ntr,ii,jj,s) = reshape(d_gen.r,[ntr 1]);
            % tranform model to EEG
            for n = 1:ntr
                % stimulus specific model
                mdl = reshape(d.model_t{n},[length(DLY{ii}),d.npcs]);
                % normalize each model weight based on the ratio of PC variance
                % to total variance
                rM = mdl.*(ones(length(DLY{ii}),1)*d.var_pc(:,n)')/sum(d.var_pc(:,n));
                M_sp(:,:,n,jj) = rM*d.cf{n}(:,1:d.npcs)';
                % stimulus general model
                mdl = reshape(d_gen.model_t{n},[length(DLY{ii}),d_gen.npcs]);
                % normalize each model weight based on the ratio of PC variance
                % to total variance
                rM = mdl.*(ones(length(DLY{ii}),1)*d_gen.var_pc(:,n)')/sum(d_gen.var_pc(:,n));
                M_gen(:,:,n,jj) = rM*d_gen.cf{n}(:,1:d_gen.npcs)';
            end
            % average models across trials
%             mdl_sp{ii,s} = squeeze(mean(M_sp,3,'omitnan'));
%             mdl_gen{ii,s} = squeeze(mean(M_gen,3,'omitnan'));
            % save models without averaging across trials
            all_mdl_sp{ii,s} = M_sp;
            all_mdl_gen{ii,s} = M_gen;
            disp(fl)
            disp(fl_gen)
        end
    end
end
clear M_sp M_gen d_gen d

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*8);
end

% Plot the difference in reconstruction accuracies
figure
hold on
df_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype)
    diffr = squeeze(r_sp(:,:,jj,:)-r_gen(:,:,jj,:));
    pl = plot(1:length(wnd),squeeze(mean(diffr,1,'omitnan')),'Color',clr{jj},'LineWidth',1.5);
    df_plts(jj) = pl(1);
end
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('Difference in reconstruction accuracy');
legend(df_plts,stimtype);

% Test if any of these time bins are significantly different than 0,
% cumulatively test across subjects
% Use wilcoxon signed-rank
DIFFR = r_sp-r_gen;
% % rearrange so trials x subjects are along the same column
DIFFR = reshape(permute(DIFFR,[1 4 2 3]),[maxtr*length(sbjs) length(wnd) length(stimtype)]);
% iteratively test for significance
p_diffr = NaN(length(wnd),length(stimtype));
st_diffr = cell(length(wnd),length(stimtype));
for ii = 1:length(stimtype)
    for w = 1:length(wnd)
        [p_diffr(w,ii),~,st_diffr{w,ii}] = signrank(DIFFR(:,w,ii),0,'tail','both');
    end
end

% Plot the median difference in reconstruction accuracies abd the standard
% error of the median
DIFFR = r_sp-r_gen;
DIFFR = permute(DIFFR,[1 4 2 3]);
mddiff_plt = plot_medians_boot(DIFFR,1000,clr);
plot([1 length(wnd)],[0 0],'k--');
set(gcf,'Position',[360 275 500 400]);
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Difference in reconstruction accuracy');
legend(mddiff_plt,stimtype);

%% Plot the models
% plot the models
% figure
% for ii = 1:length(stimtype)
%     subplot(length(stimtype),1,ii); % different plot for each stimulus
%     hold on
%     for w = 1:length(wnd)
%         MDL_GEN = NaN(length(DLY{w}),length(sbjs));
%         MDL_SP = NaN(length(DLY{w}),length(sbjs));
%         for s = 1:length(sbjs)
%             MDL_GEN(:,s) = mean(mdl_gen{w,s}(:,plot_chans,ii),2);
%             MDL_SP(:,s) = mean(mdl_sp{w,s}(:,plot_chans,ii),2);
%         end
%         plot(DLY{w}/Fs*1000,mean(MDL_GEN,2),'k','LineWidth',1);
%         plot(DLY{w}/Fs*1000,mean(MDL_SP,2),clr{ii},'LineWidth',1)
%     end
%     set(gca,'FontSize',16,'XScale','log');
%     ylabel('w');
%     title(sprintf('%s',stimtype{ii}));
% end
% xlabel('Time (ms)');
% clear MDL_GEN MDL_SP

% plot the difference between the models
% figure
% for ii = 1:length(stimtype)
%     subplot(length(stimtype),1,ii); % different plot for each stimulus
%     hold on
%     for w = 1:length(wnd)
%         DIFF_MDL = NaN(length(DLY{w}),length(sbjs));
%         for s = 1:length(sbjs)
%             DIFF_MDL(:,s) = squeeze(mean(mdl_sp{w,s}(:,plot_chans,ii)-mdl_gen{w,s}(:,plot_chans,ii),2));
%         end
%         plot(DLY{w}/Fs*1000,mean(DIFF_MDL,2),clr{ii},'LineWidth',1)
%     end
%     set(gca,'FontSize',16,'XScale','log','YLim',[-0.0012 0.0012]);
%     ylabel('w_{sp}-w_{gen}');
%     title(sprintf('%s',stimtype{ii}));
% end
% xlabel('Time (ms)');
% clear DIFF_MDL

% Plot the stimulus-specific and stimulus-general models for specific time
% windows
wnd_plot = [250 2000];
figure
set(gcf,'Position',[360,50,475,650]);
for w = 1:length(wnd_plot)
    subplot(length(wnd_plot),1,w);
    wnd_idx = wnd==wnd_plot(w);
    mdl_leg = NaN(length(stimtype)+1,1);
    hold on
    M_SP = NaN([size(all_mdl_sp{wnd_idx,1}) length(sbjs)]);
    for s = 1:length(sbjs)
        M_SP(:,:,:,:,s) = all_mdl_sp{wnd_idx,s};
    end
    for ii = 1:length(stimtype)
%         plot(DLY{wnd_idx}/Fs*1000,mean(md_sbj_mdls{wnd_idx}(:,plot_chans,ii),2),clr{ii},'LineWidth',2);
        % get the appropriate model, concatenate subjects along 4th dim
        mdl = squeeze(mean(mean(M_SP(:,plot_chans,:,ii,:),3,'omitnan'),2));
        plt = shadedErrorBar(DLY{wnd_idx}/Fs*1000,mean(mdl,2),std(mdl,[],2)/sqrt(length(sbjs)),...
            'lineProps',{'Color',clr{ii},'LineWidth',2});
        mdl_leg(ii) = plt.mainLine;
    end
    M_GEN = NaN([length(DLY{wnd_idx}) nchan maxtr length(sbjs)]);
    for s = 1:length(sbjs)
        M_GEN(:,:,:,s) = squeeze(mean(all_mdl_gen{wnd_idx,s},4)); % average across stimulus types
    end
    mdlgen = squeeze(mean(mean(M_GEN(:,plot_chans,:,:),3,'omitnan'),2));
    plt = shadedErrorBar(DLY{wnd_idx}/Fs*1000,mean(mdlgen,2),std(mdlgen,[],2)/sqrt(length(sbjs)),...
        'lineProps',{'Color','k','LineWidth',2});
    mdl_leg(ii+1) = plt.mainLine;
    set(gca,'FontSize',16);
    xlabel('Delay (ms)');
    ylabel('w');
    title(sprintf('%d ms',wnd_plot(w)));
    legend(mdl_leg,[stimtype {'general'}]);
end

% Plot stimulus-general models for each individual subject
figure
set(gcf,'Position',[350 20 550 150]);
for w = 1:length(wnd_plot)
    subplot(1,length(wnd_plot),w);
    wnd_idx = wnd==wnd_plot(w);
    M_GEN = NaN([length(DLY{wnd_idx}) nchan maxtr length(sbjs)]);
    for s = 1:length(sbjs)
        M_GEN(:,:,:,s) = squeeze(mean(all_mdl_gen{wnd_idx,s},4)); % average across stimulus types
    end
    mdlgen = squeeze(mean(mean(M_GEN(:,plot_chans,:,:),3,'omitnan'),2));
    plt = plot(DLY{wnd_idx}/Fs*1000,mdlgen,'Color','k','LineWidth',1);
    set(gca,'FontSize',14,'XLim',[0 wnd_plot(w)]);
    xlabel('Delay (ms)');
    title(sprintf('%d ms',wnd_plot(w)));
end

clear M_GEN M_SP

use_wnd = 2000; % window to plot the topography
dly_to_plot = [200 500];
wnd_idx = find(wnd==use_wnd);
dly_idx = DLY{wnd_idx}>=dly_to_plot(1)/1000*Fs & DLY{wnd_idx}<=dly_to_plot(2)/1000*Fs;
% get the gen model for this specific window
M_GEN = NaN([length(DLY{wnd_idx}) nchan maxtr length(sbjs)]);
for s = 1:length(sbjs)
    M_GEN(:,:,:,s) = squeeze(mean(all_mdl_gen{wnd_idx,s},4)); % average across stimulus types
end
MD = median(M_GEN,4);
clear M_GEN
figure
set(gcf,'Position',[75 400 300 250]);
topoplot(mean(MD(dly_idx,:,ii)),'~/Projects/EEGanly/chanlocs.xyz');
colorbar
title(sprintf('General, %d - %d ms',dly_to_plot(1),dly_to_plot(2)));

% Compute the difference in model weights for the 1000 and 2000 ms windows,
% between 400 and 600 ms where the effect seems strongest for speech, and
% compute the correlation between the difference in model weights and the
% difference in reconstruction accuracy
% use_dlys = [350 500]; % range of delays to use
% use_wnd = [250 2000]; % windows to examine for correlation
% mdl_wght_diff = NaN(maxtr,length(use_wnd),length(stimtype),length(sbjs));
% for s = 1:length(sbjs)
%     for ii = 1:length(stimtype)
%         for w = 1:length(use_wnd)
%             wnd_idx = find(wnd==use_wnd(w));
%             % identify the correct delays
%             dly_idx = DLY{wnd_idx}>=use_dlys(1)/1000*Fs & DLY{wnd_idx}<=use_dlys(2)/1000*Fs;
%             % get the average weights across channels within this delay
%             % range, relative to average weight
%             avg_sp = squeeze(mean(mean(all_mdl_sp{wnd_idx,s}(:,plot_chans,:,ii),2),1));
%             w_sp = squeeze(mean(mean(all_mdl_sp{wnd_idx,s}(dly_idx,plot_chans,:,ii),2),1))-avg_sp;
%             avg_gen = squeeze(mean(mean(all_mdl_gen{wnd_idx,s}(:,plot_chans,:,ii),2),1));
%             w_gen = squeeze(mean(mean(all_mdl_gen{wnd_idx,s}(dly_idx,plot_chans,:,ii),2),1))-avg_gen;
%             % compute the difference in weights
%             mdl_wght_diff(:,w,ii,s) = w_sp-w_gen;
%         end
%     end
% end
% reorder the weights into column arrays for trials x subjects
% WDIFF = reshape(permute(mdl_wght_diff,[1 4 2 3]),[maxtr*length(sbjs) length(use_wnd) length(stimtype)]);
% corr_diff = NaN(length(stimtype),length(use_wnd));
% pcorr_diff = NaN(length(stimtype),length(use_wnd));
% for ii = 1:length(stimtype)
%     for w = 1:length(use_wnd)
%         wnd_idx = find(wnd==use_wnd(w));
%         [corr_diff(ii,w),pcorr_diff(ii,w)] = corr(DIFFR(:,wnd_idx,ii),WDIFF(:,w,ii),...
%             'type','Spearman','rows','complete');
%     end
% end
% Plot the correlation between these
% figure
% for w = 1:length(use_wnd)
%     subplot(1,length(use_wnd),w)
%     wnd_idx = find(wnd==use_wnd(w));
%     hold on
%     for ii = 1:length(stimtype)
%         plot(WDIFF(:,w,ii),DIFFR(:,wnd_idx,ii),'.','Color',clr{ii},'MarkerSize',14);
%     end
%     set(gca,'FontSize',14);
%     xlabel(sprintf('w_{sp} - w_{gen}, %d - %d ms',use_dlys(1),use_dlys(2)));
%     ylabel('r_{sp} - r_{gen}');
%     title([xtick_lbl{wnd_idx} ' Hz']);
% end
% legend(stimtype)
