% Compute a PCA/spline reconstruction for a single trial using the PCA/spline model

addpath('..');
addpath('~/Projects/mTRF-Toolbox');

sbj = 'HC';
stimcode = 45; % specify the particular trial using the stimulus code
tmin = 0;
tmax = 500;
map = -1;
desFs = 512;
mdlFs = 1000/(tmax-tmin)*16; % 3x highest frequency (32 Hz splines was optimal for 
    % 500 ms window to reconstruct dB envelope for Natural Speech)
tlims = 16;
npcs = 64; % 64 principal components was optimized on the Natural Speech dataset
    % using a 500 ms window to reconstruct dB envelope
do_mov_avg = true;
% do_mov_avg = false;

% determine the stimulus type
if floor(stimcode/10)==1, stimtype = 'rock';
elseif floor(stimcode/10)==2, stimtype = 'classical';
elseif floor(stimcode/10)==3, stimtype = 'vocals';
elseif floor(stimcode/10)==4, stimtype = 'speech';
else
    error('stimcode must be between 10 and 49');
end

% load the subject results for this stimulus type
res_pth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';
res_fl = sprintf('%s_%s_%dms',sbj,stimtype,tmax);
res = load([res_pth res_fl]);

% Get the list of stimcodes associated with this stimulus type (because we
% need to check which index is the correct EEG)
stimcodes = (0:9)+10*floor(stimcode/10);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);
if isempty(eeg)
    error('Stimulus code does not correspond to one of the trials for this subject');
end

% Load the stimuli
disp('Loading stimuli...');
stimpth = '/Volumes/ZStore/NeuroMusic/';
stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtype);
    %%% I will need to change this to be 512 Hz envelopes, no downsampling
s = load([stimpth stimfl]);
stims = s.stimset;
stim_idx = mod(stimcode,10)+1;
% stims = stims(stim_idx);
% identify the indexes within the set of all 10 stimuli that correspond to
% EEG trials
stim_used = find(cellfun(@(x) ~isempty(x),eeg));
% get the index for the model results that corresponds to this stimulus
% code
model_idx = find(stim_used==stim_idx);
% isolate the eeg for this stimulus code
% eeg = eeg(stim_idx);
% remove empty trials
eeg = eeg(stim_used);
stims = stims(stim_used);
% flip each of the stimulus arrays into columns
for ii = 1:length(stims), stims{ii} = stims{ii}'; end

% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
    if eFs~=desFs
        eeg{ii} = resample(eeg{ii},desFs,eFs);
    end
end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims)
    %%% NZ (19-1-2020), don't skip silences, could have had strange effects
    %%% on null distribution, overenflated tracking at low frequencies
    tidx{ii} = ones(length(stims{ii}),1);
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(st));
    eeg{ii} = eeg{ii}-avg;
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        % NZ (15-1-2020), don't remove silences when doing moving average
        s_on = stims{ii};
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii} = s_on;
    end
end
clear avg s_on

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,eeg);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(eeg{n},1)); end
    end
end

% Compute the downsampling rate, for the spline transform
ds = desFs/mdlFs;

disp('Model training...');
traintrs = setxor(1:length(stims),model_idx);
pc = cell(length(traintrs),1);
for ii = 1:length(traintrs)
%     eeg{ii} = detrend(eeg{ii},0); % center each channel
    pc{ii} = eeg{traintrs(ii)}*res.cf{model_idx}(:,1:npcs); % transform to pcs
    pc{ii} = zscore(pc{ii}); % normalize to variance of 1
end

disp('(Using spline interpolation...)');
model = mTRFtrain_spline(stims(traintrs),pc,desFs,ds,map,tmin,tmax,0,tidx(traintrs));

disp('Computing the envelope reconstruction...');
    % transform the testing trial into pc
testpc = zscore(eeg{model_idx}*res.cf{model_idx}(:,1:npcs));
[pred,r] = mTRFpredict_spline(stims{model_idx},testpc,model,desFs,ds,map,tmin,tmax,tidx(model_idx));
% mdl = mTRFreversetransform_spline(stims{1}',testpc,res.model_t{model_idx},desFs,ds,map,tmin,tmax,tidx(1));
% [pred,r] = mTRFpredict_spline(stims{1}',testpc,mdl,desFs,ds,map,tmin,tmax,tidx(1));
% pred = mTRFpredict_spline(stims{1}',testpc,res.model_t{model_idx},desFs,ds,map,tmin,tmax,tidx(1));

% Plot the prediction and the stimulus
% for stimcode 45, sbj HC, I plotted 107.5 < t < 109.5 s
figure
hold on
t = (0:length(stims{model_idx})-1)/desFs;
plot(t(tidx{model_idx}),zscore(stims{model_idx}(tidx{model_idx})),'k');
plot(t(tidx{model_idx}),pred,'r','LineWidth',1.5);
set(gca,'FontSize',14,'XLim',[107.5 109.5]);
xlabel('Time (s)');
ylabel('Z-scored dB Envelope');
legend('Original','Reconstruction');