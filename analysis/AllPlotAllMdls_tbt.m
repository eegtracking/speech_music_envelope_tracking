% Plot the topography of the model weights for a subject and particular
% window size
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));
addpath('~/Documents/MATLAB/shadedErrorBar/');

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
% sbjs = {'AB','AF','CB','EB','EJ','HC','JH','JS','KR','MB','SL','SOS'};
    % subjects without eyeblinks (200-500 ms)
stimtypes = {'rock','classical','vocals','speech'};
% clr = {'r','g','c','b'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
maxtr = 7;
% wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000 16000];
% wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000];
wnd = [250 2000];
nchan = 128;
npc = 64;
Fs = 512;
% plot_chans = 65:109;
plot_chans = 1:128;
% plot_chans = 85;
% dly = -(-ceil(wnd/1000*Fs):0);

res_fld = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';

all_mdls = cell(length(wnd),length(sbjs));
all_mdls_trs = cell(length(wnd),length(sbjs));
DLY = cell(length(wnd),1); % to store the delay vectors for the models
for s = 1:length(sbjs)
    for w = 1:length(wnd) % for each model window size
        % compute the delay vector, in indexes
        DLY{w} = -(-ceil(wnd(w)/1000*Fs):0); % see lagGen, delays are ordered
            % from -max to -min in the backwards model
        M = NaN(length(DLY{w}),nchan,maxtr,length(stimtypes));

        for ii = 1:length(stimtypes)
            fl = sprintf('%s_%s_%dms',sbjs{s},stimtypes{ii},round(wnd(w)));

            d = load([res_fld fl]);

            % Reshape the model so it's delay x time
            nreps = length(d.model_t);
            for n = 1:nreps
                mdl = reshape(d.model_t{n},[length(DLY{w}),npc]);
                % normalize each model weight based on the ratio of PC variance
                % to total variance
                rM = mdl.*(ones(length(DLY{w}),1)*d.var_pc(:,n)')/sum(d.var_pc(:,n));
                M(:,:,n,ii) = rM*d.cf{n}(:,1:npc)';
            end
            disp(fl);
        end
        % Average across reps
        all_mdls{w,s} = squeeze(mean(M,3,'omitnan'));
        all_mdls_trs{w,s} = M;
%         all_mdl
    end
end

% Compute the median model across subjects
md_sbj_mdls = cell(length(wnd),1);
for w = 1:length(wnd)
    M = NaN([size(all_mdls{w,1}) length(sbjs)]);
    for s = 1:length(sbjs)
        M(:,:,:,s) = all_mdls{w,s};
    end
%     md_sbj_mdls{w} = mean(M,4);
    md_sbj_mdls{w} = median(M,4);
end

% Plot images for each of the model weights
% figure
% for w = 1:length(wnd)
%     subplot(2,5,w);
%     imagesc(DLY{w}/Fs*1000,1:128,md_sbj_mdls{w}(:,:,4)');
%     colorbar;
%     xlabel('Delay (ms)');
%     ylabel('Channel');
% end

cmap = colormap('jet'); % plot each window size using a different color

figure
set(gcf,'Position',[80 20 460 675]);
for ii = 1:length(stimtypes)
    subplot(length(stimtypes),1,ii); % different plot for each stimulus
    hold on
    for w = 1:length(wnd)
%         plot(DLY{w}/Fs*1000,mean(md_sbj_mdls{w}(:,plot_chans,ii),2),clr{ii})
        clridx = round((w-1)/length(wnd)*63)+1;
        plot(DLY{w}/Fs*1000,mean(md_sbj_mdls{w}(:,plot_chans,ii),2),'Color',cmap(clridx,:)*0.8);
    end
    set(gca,'FontSize',16,'XScale','log','XLim',[1 wnd(end)],'XTick',10.^(0:4));
    ylabel('w');
    title(sprintf('%s',stimtypes{ii}));
end
xlabel('Time (ms)');

% Plot the weights for specific window sizes for all stimuli
% (average and standard error across subjects)
wnd_plot = [250 2000];
figure
set(gcf,'Position',[360,50,475,650]);
for w = 1:length(wnd_plot)
    subplot(length(wnd_plot),1,w);
    wnd_idx = wnd==wnd_plot(w);
    mdl_leg = NaN(length(stimtypes),1);
    hold on
    for ii = 1:length(stimtypes)
%         plot(DLY{wnd_idx}/Fs*1000,mean(md_sbj_mdls{wnd_idx}(:,plot_chans,ii),2),clr{ii},'LineWidth',2);
        % get the appropriate model, concatenate subjects along 4th dim
        M = NaN([size(all_mdls{wnd_idx,1}) length(sbjs)]);
        for s = 1:length(sbjs)
            M(:,:,:,s) = all_mdls{wnd_idx,s};
        end
%         for ii = 1:length(stimtypes)
        mdl = squeeze(mean(M(:,plot_chans,ii,:),2));
        plt = shadedErrorBar(DLY{wnd_idx}/Fs*1000,mean(mdl,2),std(mdl,[],2)/sqrt(length(sbjs)),...
            'lineProps',{'Color',clr{ii},'LineWidth',2});
        mdl_leg(ii) = plt.mainLine;
%         end
    end
    set(gca,'FontSize',16);
    xlabel('Delay (ms)');
    ylabel('w');
    title(sprintf('%d ms',wnd_plot(w)));
    legend(mdl_leg,stimtypes);
end

% Plot the models for each subject individually
figure
set(gcf,'Position',[350 20 550 670]);
for w = 1:length(wnd_plot)
    wnd_idx = wnd==wnd_plot(w);
    for ii = 1:length(stimtypes)
        plt_idx = length(wnd_plot)*(ii-1)+mod(w-1,length(wnd_plot))+1;
        subplot(length(stimtypes),length(wnd_plot),plt_idx);
        if ii == 1, title(sprintf('%d ms',wnd_plot(w))); end
        % get the correct model
        M = NaN([size(all_mdls{wnd_idx,1}) length(sbjs)]);
        for s = 1:length(sbjs)
            M(:,:,:,s) = all_mdls{wnd_idx,s};
        end
        % average across channels
        mdl = squeeze(mean(M(:,plot_chans,ii,:),2));
        plt = plot(DLY{wnd_idx}/Fs*1000,mdl,'Color',clr{ii},'LineWidth',1);
        set(gca,'FontSize',14,'XLim',[0 wnd_plot(w)]);
        xlabel('Delay (ms)');
    end
end

clear M

% Plot the median weights across all subjects and trials
wnd_plot = [250 2000];
for w = 1:length(wnd_plot)
    wnd_idx = wnd==wnd_plot(w);
    M = NaN([size(all_mdls_trs{wnd_idx,s}) length(sbjs)]);
    for s = 1:length(sbjs)
        M(:,:,:,:,s) = all_mdls_trs{wnd_idx,s};
    end
    mdl = squeeze(mean(M(:,plot_chans,:,:,:),2)); % average across channels
    mdl = permute(mdl,[2 4 1 3]); % [trials x subjects x delay x stimtype]
    mdl_leg = plot_medians_boot(mdl,100,clr,DLY{wnd_idx}/Fs*1000);
    set(gca,'FontSize',16);
    xlabel('Delay (ms)');
    ylabel('w');
    title(sprintf('%d ms',wnd_plot(w)));
    legend(mdl_leg,stimtypes);
end

clear M

% Plot the time course of the weights, averaged across channels, with the
% standard error across subjects
% figure
% for ii = 1:length(stimtypes)
%     for w = 1:length(wnd)
%         plt_idx = (w-1)*length(stimtypes)+ii;
%         subplot(length(wnd),length(stimtypes),plt_idx);
%         if w==1, title(stimtypes{ii}); end % add the title if it's the top plot
%         hold on
%         % get the appropriate model, concatenate subjects along 4th dim
%         M = NaN([size(all_mdls{w,1}) length(sbjs)]);
%         for s = 1:length(sbjs)
%             M(:,:,:,s) = all_mdls{w,s};
%         end
%         mdl = squeeze(mean(M(:,plot_chans,ii,:),2));
%         % plot the individual curves across subjects and the mean
% %         plot(DLY{w}/Fs*1000,mdl,clr{ii},'LineWidth',0.5);
% %         plot(DLY{w}/Fs*1000,mean(mdl,2),clr{ii},'LineWidth',3);
%         shadedErrorBar(DLY{w}/Fs*1000,mean(mdl,2),std(mdl,[],2),...
%             'lineProps',{'Color',clr{ii},'LineWidth',2});
%         set(gca,'FontSize',14,'XScale','log','XLim',[1 wnd(end)],...
%             'YTick',[]);
%         if w~=length(wnd), set(gca,'XTickLabel',''); end
%         if ii==1, ylabel(sprintf('%d ms',wnd(w))); end % add the window size 
%             % on the y axis if it's the left-most plot
%     end
% end

% Plot a topography of the response at a particular delay
use_wnd = 2000; % window to plot the topography
dly_to_plot = [200 500];
wnd_idx = find(wnd==use_wnd);
dly_idx = DLY{wnd_idx}>=dly_to_plot(1)/1000*Fs & DLY{wnd_idx}<=dly_to_plot(2)/1000*Fs;
figure
set(gcf,'Position',[75 400 1125 250]);
for ii = 1:length(stimtypes)
    subplot(1,length(stimtypes),ii);
    topoplot(mean(md_sbj_mdls{wnd_idx}(dly_idx,:,ii)),'~/Projects/EEGanly/chanlocs.xyz');
    colorbar
    title(sprintf('%s, %d - %d ms',stimtypes{ii},dly_to_plot(1),dly_to_plot(2)));
end