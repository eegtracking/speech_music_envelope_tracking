% Plot some example EEG from the NeuroMusic dataset

addpath('~/Projects/EEGanly/');

sbj = 'HC';
block = 3; % number of the block to plot
tr = 4; % trial to plot
start_time = 10; % start time of the plot (in s)
plot_dur = 2; % duration of EEG to plot (in s)
mvwnd_size = 500; % size of moving average window (in ms)

% load the EEG
eegpth = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/';
eegfl = sprintf('%s_block_%d',sbj,block);
load([eegpth eegfl]);

% get the appropriate indexes for the plot
t = (0:size(eeg{tr},1)-1)/eFs;
t_idx = t>=start_time & t<=(start_time+plot_dur);

% plot
EEGplot(detrend(eeg{tr}(t_idx,1:25:end),0)*2,eFs,1);
set(gcf,'Position',[89,448,371,229]);

% Convert to principal components
% remove moving average @ 2 Hz
mveeg{tr} = eeg{tr}-movmean(detrend(eeg{tr}),mvwnd_size/1000*eFs);
[~,pcs] = pca(mveeg{tr});
% plot the principal components
EEGplot(detrend(pcs(t_idx,1:4),0)*2,eFs,1);
set(gcf,'Position',[289,448,371,229]);