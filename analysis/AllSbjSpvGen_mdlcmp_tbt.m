% Get the reconstruction accuracies for stimulus-specific and
% stimulus-general models, and compute the difference between them as a
% function of window size (frequency)
%%% Plot the TRF models of each type of stimulus

addpath('~/Documents/MATLAB/shadedErrorBar/');
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
% sbjs = {'AF','CB','CQ','EB','EJ','HC','JH','JS','KR','MB','SL','SOS'}; 
    % without subjects that have a strong eyeblink component 350-550 ms
stimtype = {'rock','classical','vocals','speech'};
clr = {'r','g','c','b'};
% wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000];
wnd = 500;
maxtr = 7;
nchan = 128;
plot_chans = 1:128;
% plot_chans = setxor(1:128,[72:74 78:82 91:96]);
Fs = 512;
respth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';

% Load the reconstruction accuracies
r_sp = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
r_gen = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
mdl_sp = cell(length(wnd),length(sbjs));
mdl_gen = cell(length(wnd),length(sbjs));
all_mdl_sp = cell(length(wnd),length(sbjs));
all_mdl_gen = cell(length(wnd),length(sbjs));
DLY = cell(length(wnd),1);
for s = 1:length(sbjs)
    for ii = 1:length(wnd),
        % setup storing info for model
        DLY{ii} = -(-ceil(wnd(ii)/1000*Fs):0); % see lagGen, delays are ordered
        % from -max to -min in the backwards model
        M_sp = NaN(length(DLY{ii}),nchan,maxtr,length(stimtype));
        M_gen = NaN(length(DLY{ii}),nchan,maxtr,length(stimtype));
        for jj = 1:length(stimtype),
            % load the stimulus-general results
            fl_gen = sprintf('%s_ALL_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d_gen = load([respth fl_gen]);
            % stimulus-specific
            fl = sprintf('%s_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d = load([respth fl]);
            ntr = size(d.r,1);
            r_sp(1:ntr,ii,jj,s) = reshape(d.r,[ntr 1]);
            % stimulus-general
            r_gen(1:ntr,ii,jj,s) = reshape(d_gen.r,[ntr 1]);
            % tranform model to EEG
            for n = 1:ntr
                % stimulus specific model
                mdl = reshape(d.model_t{n},[length(DLY{ii}),d.npcs]);
                % normalize each model weight based on the ratio of PC variance
                % to total variance
                rM = mdl.*(ones(length(DLY{ii}),1)*d.var_pc(:,n)')/sum(d.var_pc(:,n));
                M_sp(:,:,n,jj) = rM*d.cf{n}(:,1:d.npcs)';
                % stimulus general model
                mdl = reshape(d_gen.model_t{n},[length(DLY{ii}),d_gen.npcs]);
                % normalize each model weight based on the ratio of PC variance
                % to total variance
                rM = mdl.*(ones(length(DLY{ii}),1)*d_gen.var_pc(:,n)')/sum(d_gen.var_pc(:,n));
                M_gen(:,:,n,jj) = rM*d_gen.cf{n}(:,1:d_gen.npcs)';
            end
            % save models without averaging across trials
            all_mdl_sp{ii,s} = M_sp;
            all_mdl_gen{ii,s} = M_gen;
            disp(fl)
            disp(fl_gen)
        end
    end
end
clear M_sp M_gen d_gen d

% Plot the median difference in reconstruction accuracies abd the standard
% error of the median
DIFFR = r_sp-r_gen;
DIFFR = permute(DIFFR,[1 4 2 3]);

% Plot the topograhy of the difference in model weights
topo_wnd = 500; % windows to examine for correlation
use_wnd_idx = find(wnd==topo_wnd);

% Compute the correlation between the difference in weights on each channel
% and the difference in reconstruction accuracy
all_mdl_diff = NaN(length(DLY{use_wnd_idx}),nchan,maxtr,length(stimtype),length(sbjs));
for s = 1:length(sbjs)
    for ii = 1:length(stimtype) 
%         all_w_sp = all_mdl_sp{use_wnd_idx,s}(:,:,:,ii);
        all_w_sp = detrend(all_mdl_sp{use_wnd_idx,s}(:,:,:,ii),0);
%         all_w_gen = all_mdl_gen{use_wnd_idx,s}(:,:,:,ii);
        all_w_gen = detrend(all_mdl_gen{use_wnd_idx,s}(:,:,:,ii),0);
        all_mdl_diff(:,:,:,ii,s) = all_w_sp-all_w_gen;
    end
end
% reshape the model differences so that all trials and subjects are
% aligned in same dimension
ALL_MDL_DIFF = permute(all_mdl_diff,[3 5 1 2 4]);
clear all_w_sp all_w_gen all_mdl_diff
% ALL_MDL_DIFF = reshape(ALL_MDL_DIFF,[maxtr*length(sbjs) length(DLY{use_wnd_idx}) nchan length(stimtype)]);
% mdl_plt = plot_medians_boot(squeeze(mean(ALL_MDL_DIFF,3)),1000,clr,DLY{use_wnd_idx}/Fs*1000);
% plot([1 length(DLY{use_wnd_idx})],[0 0],'k--');
% set(gca,'FontSize',14);
% xlabel('Delay (ms)');
% ylabel('Difference in model weight');
% legend(mdl_plt,stimtypes);
% % Identify channels where the squared magnitude of the overall difference
% % across delays for speech is correlated with the difference in 
% % reconstruction accuracy
% corrdiff_stim_mdl_r = NaN(nchan,length(stimtype));
% for ii = 1:length(stimtype)
%     STIM_MDL_DIFF = reshape(ALL_MDL_DIFF(:,:,:,:,4),[maxtr*length(sbjs) length(DLY{use_wnd_idx}) nchan]);
%     MAG_STIM_MDL_DIFF = squeeze(mean(STIM_MDL_DIFF.^2,2));
%     SPCH_DIFFR = reshape(DIFFR(:,:,use_wnd_idx,4),[maxtr*length(sbjs) 1]);
%     corrdiff_spch_mdl_r = corr(SPCH_DIFFR,MAG_SPCH_MDL_DIFF,'type','Spearman','rows','complete');
% end
% figure
% for ii = 1:length(stimtype)
% topoplot(corrdiff_spch_mdl_r,'~/Projects/EEGanly/chanlocs.xyz');

% Compute the time course of the running mean of the squared difference in
% weights, summed across channels, for all stimuli. Use a gaussian window
% with a size of 0.25 s, equal to the lowpass cutoff frequency of the
% splines. At what point is speech uniquely significantly larger than the
% other stimuli?
mag_plt = plot_medians_boot(squeeze(sum(ALL_MDL_DIFF.^2,4)),100,clr,DLY{use_wnd_idx}/Fs*1000);
set(gca,'FontSize',14);
xlabel('Delay (ms)');
ylabel('Squared difference in model weight');
legend(mag_plt,stimtype);

% Identify time points where the squared model difference is significantly
% larger for speech than the other stimuli
pmagdiff = NaN(length(DLY{use_wnd_idx}),1);
zmagdiff = NaN(length(DLY{use_wnd_idx}),1);
MAG_DIFF = squeeze(sum(ALL_MDL_DIFF.^2,4));
for t = 1:length(DLY{use_wnd_idx})
    spchmag = reshape(MAG_DIFF(:,:,t,4),[maxtr*length(sbjs) 1]);
%     for ii = 1:length(stimtype)-1
%         stimmag = reshape(MAG_DIFF(:,:,t,ii),[maxtr*length(sbjs) 1]);
%         [pmagdiff(t,ii),~,st] = ranksum(spchmag,stimmag,'tail','right');
%         zmagdiff(t,ii) = st.zval;
%     end
    stimmag = reshape(MAG_DIFF(:,:,t,1:3),[maxtr*length(sbjs)*3 1]);
    [pmagdiff(t),~,st] = ranksum(spchmag,stimmag,'tail','right');
    zmagdiff(t) = st.zval;
end
figure
subplot(2,1,1)
plot(DLY{use_wnd_idx}/Fs*1000,zmagdiff,'k')
xlabel('Delay (ms)')
ylabel('Rank sum statistic');
subplot(2,1,2)
hold on
plot(DLY{use_wnd_idx}/Fs*1000,pmagdiff)
plot([0 2000],[0.001 0.001]/length(DLY{use_wnd_idx}),'k--')
set(gca,'YScale','log')
xlabel('Delay (ms)');
ylabel('Rank sum p-value');

% Compute the correlation, delay-by-delay, between the squared magnitude of the
% difference in weights and the difference in reconstruction accuracy. Use
% fisher's z-transformed Spearman's correlations, summed across channels
%%% I need to plot chance correlation values as well, this is a complicated
%%% measure
% SPCH_MDL_DIFF = reshape(ALL_MDL_DIFF(:,:,:,:,4),[maxtr*length(sbjs) length(DLY{use_wnd_idx}) nchan]);
% MAG_SPCH_MDL_DIFF = SPCH_MDL_DIFF.^2;
% % MAG_SPCH_MDL_DIFF = diff(SPCH_MDL_DIFF,[],2).^2;
% SPCH_DIFFR = reshape(DIFFR(:,:,use_wnd_idx,4),[maxtr*length(sbjs) 1]); 
%     % speech difference in reconstruction accuracy
% nanidx = isnan(SPCH_DIFFR); % identify NaN values, aka missing trials (< maxtr)
% nperm = 20; % number of times to shuffle trials
% disp('Computing correlations between weight difference and reconstruction difference...');
% corrdly_spch = NaN(length(DLY{use_wnd_idx}),1);
% nullcorrdly = NaN(length(DLY{use_wnd_idx}),nperm); % null distribution of summed correlations
% for t = 1:length(DLY{use_wnd_idx})
%     % Identify NaN values and remove them
%     diffr = SPCH_DIFFR(~nanidx);
%     magmdl = squeeze(MAG_SPCH_MDL_DIFF(~nanidx,t,:));
%     c = corr(diffr,magmdl,'type','Spearman');
%     corrdly_spch(t) = mean(atan(c)); % average the Fisher's transformed correlations
%     for n = 1:nperm
%         rndidx = randperm(length(diffr));
%         nc = corr(diffr(rndidx),magmdl,'type','Spearman');
%         nullcorrdly(t,n) = mean(atan(nc));
%     end
% end
% % mdnull = median(nullcorrdly,2);
% % uqnull = quantile(nullcorrdly,0.95,2);
% % lqnull = quantile(nullcorrdly,0.05,2);
% % use distribution across all time, because the distribution of
% % correlations is constant
% allnull = reshape(nullcorrdly,[numel(nullcorrdly) 1]);
% mdnull = median(allnull);
% uqnull = quantile(allnull,0.999);
% lqnull = quantile(allnull,0.001);
% figure
% hold on
% nt = length(DLY{use_wnd_idx}); % number of time points
% shadedErrorBar(DLY{use_wnd_idx}/Fs*1000,mdnull*ones(nt,1),ones(nt,1)*[mdnull-lqnull uqnull-mdnull]);
% plot(DLY{use_wnd_idx}/Fs*1000,corrdly_spch,'k','LineWidth',2);
% set(gca,'FontSize',14);
% xlabel('Delay (ms)');
% ylabel('Z-corr between r diff and wght diff');
% title('Speech');

% Plot the topography of correlation between average model weight magnitude
% difference within this delay range and difference in reconstruction
% accuracy
% use_dlys = [400 600]; % range of delays to use
% dlyidx = DLY{use_wnd_idx}>=use_dlys(1)/1000*Fs & DLY{use_wnd_idx}<=use_dlys(2)/1000*Fs;
% MAG_DLY_MDL_DIFF = squeeze(mean(SPCH_MDL_DIFF(:,dlyidx,:).^2,2));
% % MAG_DLY_MDL_DIFF = squeeze(mean(diff(SPCH_MDL_DIFF(:,dlyidx,:),[],2).^2,2));
% corrdiff_spch_mdl_r = corr(SPCH_DIFFR,MAG_DLY_MDL_DIFF,'type','Spearman','rows','complete');
% figure
% topoplot(corrdiff_spch_mdl_r,'~/Projects/EEGanly/chanlocs.xyz');
% title(sprintf('%d - %d ms, speech',use_dlys(1),use_dlys(2)));