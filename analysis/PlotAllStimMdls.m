% Plot the topography of the model weights for a subject and particular
% window size
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbj = 'HC';
stimtypes = {'rock','classical','vocals','speech'};
clr = {'r','g','c','b'};
nreps = 5;
wnd = 2000;
npc = 128;
Fs = 512;
plot_chans = 65:109;
topo_dly = [50 100];
dly = 0:ceil(wnd/1000*Fs);

res_fld = '../rcnstr_res/';
wnd_fld = sprintf('%dms/',round(wnd));

M = NaN(length(dly),npc,nreps,length(stimtypes));
for s = 1:length(stimtypes)
    fl = sprintf('%s_%s_db',sbj,stimtypes{s});

    d = load([res_fld wnd_fld fl]);    

    % Reshape the model so it's delay x time
    nreps = length(d.model_t);
    for n = 1:nreps
        mdl = reshape(d.model_t{n},[length(dly),npc]);
%         mdl = reshape(d.model{n},[length(dly),npc]);
        rM = mdl.*(ones(length(dly),1)*mean(d.var_pc));
        M(:,:,n,s) = rM*d.cf';
    end
    disp(fl);
end
% Average across reps
M = squeeze(mean(M,3));

figure
hold on
for s = 1:length(stimtypes)
    plot(dly/Fs*1000,mean(M(:,plot_chans,s),2),clr{s},'LineWidth',2)
end
set(gca,'FontSize',16);
title(sprintf('%s, %d ms',sbj,wnd));
xlabel('Time (ms)');
ylabel('Model weight');
legend(stimtypes);

% dly_idx = dly/Fs*1000>=topo_dly(1) & dly/Fs*1000<=topo_dly(2);
% 
% figure
% topoplot(mean(M(dly_idx,:)),'~/Projects/EEGanly/chanlocs.xyz','maplimits','maxmin');
% colorbar;
% title(sprintf('%d - %d ms',dly_range(1),dly_range(2)));