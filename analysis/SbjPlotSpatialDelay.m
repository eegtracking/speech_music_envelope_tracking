% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbj = 'AB';
stimtype = 'speech';
wnd = [500 1000 2000];
wnd_clr = {'r','m','b'};
nfolds = 50;
respth = '~/Projects/NeuroMusic/rcnstr_spatial/';

% Load the no-averaging results
r = cell(length(wnd),1);
dly = cell(length(wnd),1);
model = cell(length(wnd),1);
for ii = 1:length(wnd),
    fl = sprintf('%dms/%s_%s_delay',round(wnd(ii)),sbj,stimtype);
    d = load([respth fl]);
    dly{ii} = d.dly;
    tmpr = d.r;
    tmpr = permute(tmpr,[1 3 2]); % move delay to last dimension of matrix
    r{ii} = reshape(tmpr,[nfolds length(dly{ii})]);
    M = d.model(:,1);
    for jj = 1:length(M) % squeeze the model, if necessary
        M{jj} = squeeze(M{jj});
    end
    allM = zeros(size(M{1}));
    for jj = 1:length(M)
        allM = allM+M{jj};        
    end
    allM = allM/length(M);
    % Transform the model to EEG channels
    rM = allM.*(mean(d.var_pc)'*ones(1,length(dly{ii}))); % apply variance of each PC
    model{ii} = (rM'*d.cf')';
    Fs = d.desFs;
    disp(fl)
end

% plot the reconstruction accuracies for each window size
figure
hold on
wnd_leg = cell(length(wnd),1);
pl_r = NaN(length(wnd),1);
for ii = 1:length(wnd)
    pl = plot(dly{ii}/Fs*1000,r{ii}',wnd_clr{ii}); 
    pl_r(ii) = pl(1);
    wnd_leg{ii} = sprintf('%.2g Hz',1000/wnd(ii));
end
set(gca,'FontSize',16);
xlabel('Delay (ms)');
ylabel('Reconstruction accuracy');
legend(pl_r,wnd_leg);

% compute the delay with the maximum reconstruction accuracy
peak_dly = NaN(length(wnd),1);
peak_idx = NaN(length(wnd),1);
for ii = 1:length(wnd)
    peak_idx(ii) = find(mean(r{ii},1)==max(mean(r{ii},1)),1,'first');
    peak_dly(ii) = dly{ii}(peak_idx(ii))/Fs*1000;
end
fprintf('Peak delay = %.2f ms\n',mean(peak_dly));
% get the delay index that is closest to the average
% peak_dly = 250;
dly_diff = abs(dly{1}/Fs*1000-mean(peak_dly));
use_idx = find(dly_diff==min(dly_diff),1,'first');

% plot the topographies at the max delay
figure
set(gcf,'Position',[360 480 800 215]);
for ii = 1:length(wnd)
    subplot(1,length(wnd),ii);
    topoplot(model{ii}(:,use_idx),'~/Projects/EEGanly/chanlocs.xyz','maplimits','maxmin');
    colorbar;
    title(sprintf('%d ms',wnd(ii)));
end