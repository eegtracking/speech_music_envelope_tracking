% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbj = 'AB';
stimtype = {'rock','classical','vocals','speech'};
% stimtype = {'classical','vocals','speech'};
% clr = {'g','c','b'};
clr = {'r','g','c','b'};
wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000 16000];
% wnd = [250 500 1000 2000 4000];
nfolds = 50;
niter = 50;
% respth = '~/Projects/NeuroMusic/rcnstr_res_genPCA/';
respth = '~/Projects/NeuroMusic/rcnstr_res/';

% Load the no-averaging results
r = NaN(nfolds,length(wnd),length(stimtype));
nr = NaN(niter,length(wnd),length(stimtype));
zr = NaN(nfolds,length(wnd),length(stimtype));
dp = NaN(length(wnd),length(stimtype));
for ii = 1:length(wnd),
    for jj = 1:length(stimtype),
        fl = sprintf('%dms/%s_%s_db',round(wnd(ii)),sbj,stimtype{jj});
        d = load([respth fl]);
        r(:,ii,jj) = reshape(d.r,[nfolds 1]);
        nr(:,ii,jj) = d.nullr;
        zr(:,ii,jj) = (r(:,ii,jj)-mean(nr(:,ii,jj)))/std(nr(:,ii,jj));
        dp(ii,jj) = (mean(r(:,ii,jj))-mean(nr(:,ii,jj)))/sqrt(0.5*(var(r(:,ii,jj))+var(nr(:,ii,jj))));
        disp(fl)
    end
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*4);
end

% Plot the results
% reconstruction accuracy
figure
hold on
for jj = 1:length(stimtype),
    mrnull = squeeze(mean(nr(:,:,jj)));
    strnull = squeeze(std(nr(:,:,jj)));
    shadedErrorBar(1:length(wnd),mrnull,strnull,'lineProps',{clr{jj},'LineWidth',1});
end
% moving averaged
r_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype),
    mr = squeeze(mean(r(:,:,jj)));
    str = squeeze(std(r(:,:,jj)));
    r_plts(jj) = errorbar(1:length(wnd),mr,str,clr{jj},'LineWidth',2);
end
% moving average null
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('Reconstruction accuracy');
title(sbj);
legend(r_plts,stimtype);

% d-prime
figure
hold on
% moving averaged
for jj = 1:length(stimtype),
    plot(1:length(wnd),dp(:,jj),clr{jj},'LineWidth',2);
end
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('d-prime');
title(sbj);
legend(stimtype);