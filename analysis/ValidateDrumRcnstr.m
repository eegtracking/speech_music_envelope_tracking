% Load all of the drum reconstructions as well as the reconstruction
% accuracies for the drum instruments. Check to make sure that the
% reconstruction accuracies are equal. This is to validate that the
% reconstructions are identical to the ones computed earlier, before the
% reconstructions themselves are analyzed further.
% Nate Zuk (2020)

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB',...
    'MT','SL','SOS'};
wnd = [125 250 500];
maxtr = 7; % maximum number of trials per subject
drumpth = '/Volumes/ZStore/NeuroMusic/new_analyses/drum_rcnstrs/';
instrpth = '/Volumes/ZStore/NeuroMusic/new_analyses/instr_tbt/';

r_drum = NaN(maxtr,length(wnd),length(sbjs)); % for the drum reconstruction results
r_instr = NaN(maxtr,length(wnd),length(sbjs)); % for the general instrument reconstruction results
for s = 1:length(sbjs)
    for w = 1:length(wnd)
        % drum reconstruction
        drumfl = sprintf('DrumRcnstr_%s_%dms',sbjs{s},wnd(w));
        S_drum = load([drumpth drumfl]);
        ntr = length(S_drum.r);
        r_drum(1:ntr,w,s) = S_drum.r;
        disp(drumfl)
        % instrument reconstruction
        instrfl = sprintf('%s_drums_%dms',sbjs{s},wnd(w));
        S_instr = load([instrpth instrfl]);
        r_instr(1:ntr,w,s) = S_instr.r;
        disp(instrfl)
    end
end

% Compute the difference between these values
diffr = r_drum-r_instr;
% Compute the error for each subject
maxdiffr = NaN(length(sbjs),1);
mindiffr = NaN(length(sbjs),1);
for s = 1:length(sbjs)
    DIFFR = reshape(diffr(:,:,s),[maxtr*length(wnd),1]);
    maxdiffr(s) = max(DIFFR);
    mindiffr(s) = min(DIFFR);
end