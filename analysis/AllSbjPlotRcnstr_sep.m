% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
stimtype = {'rock','classical','vocals','speech'};
% clr = {'r','g','c','b'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
wnd = [31.25 62.5 125 250 500 1000 2000 4000];
nfolds = 50;
niter = 50;
maxtr = 7;
% respth = '~/Projects/NeuroMusic/rcnstr_res_genPCA/';
respth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_sep/';
nperm = 1000*length(wnd)*(length(stimtype)-1);

% Load the no-averaging results
r = NaN(nfolds,maxtr,length(wnd),length(stimtype),length(sbjs));
nr = NaN(niter,maxtr,length(wnd),length(stimtype),length(sbjs));
dp = NaN(maxtr,length(wnd),length(stimtype),length(sbjs));
for s = 1:length(sbjs)
    for ii = 1:length(wnd),
        for jj = 1:length(stimtype),
            fl = sprintf('%s_%s_%dms',sbjs{s},stimtype{jj},round(wnd(ii)));
            d = load([respth fl]);
            ntr = size(d.r,3);
            r(:,1:ntr,ii,jj,s) = reshape(d.r,[nfolds ntr]);
            nr(:,1:ntr,ii,jj,s) = d.nullr;
            dp(:,ii,jj,s) = (mean(r(:,:,ii,jj,s),'omitnan')-mean(nr(:,:,ii,jj,s),'omitnan'))./...
                sqrt(0.5*(var(r(:,:,ii,jj,s),[],'omitnan')+var(nr(:,:,ii,jj,s),[],'omitnan')));
            disp(fl)
        end
    end
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*8);
end

% d-prime
figure
hold on
% moving averaged
dp_pl = NaN(length(stimtype),1);
for jj = 1:length(stimtype),
    mdp = squeeze(mean(dp(:,:,jj,:),'omitnan'));
    pl = plot(1:length(wnd),mdp,'Color',clr{jj},'LineWidth',1.5);
    dp_pl(jj) = pl(1);
end
plot([1 length(wnd)],[1 1],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('d-prime');
legend(dp_pl,stimtype);

% Plot the median d-prime
rshp_dp = permute(dp,[1 4 2 3]);
meddp_plts = plot_medians_boot(rshp_dp,1000,clr);
set(gcf,'Position',[300,300,550,400]);
% plot([1 length(wnd)],[1 1],'k--');
plot([1 length(wnd)],[0 0 ],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('d-prime');
legend(meddp_plts,stimtype);

% Plot difference in d-prime reconstruction accuracy (speech relative to other
% stimuli)
figure
hold on
ddp_plts = NaN(length(stimtype)-1,1);
for jj = 1:(length(stimtype)-1)
    diff_dp = squeeze(mean(dp(:,:,4,:),1,'omitnan'))-squeeze(mean(dp(:,:,jj,:),1,'omitnan'));
    pl = plot(1:length(wnd),diff_dp,'Color',clr{jj},'LineWidth',2);
    ddp_plts(jj) = pl(1);
end
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('D-prime speech rel to other stimuli');
legend(ddp_plts,stimtype(1:length(stimtype)-1));

% Plot median difference and distribution
mddiff_plts = plot_mddiff_boot(rshp_dp(:,:,:,1:3),rshp_dp(:,:,:,4),1000,clr(1:3));
set(gcf,'Position',[665,300,550,400]);
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('D-prime speech rel to other stimuli');
legend(mddiff_plts,stimtype(1:length(stimtype)-1));

% Test if d-primes for speech are significantly larger than dprimes for
% other stimuli
disp('Statistical test of difference between speech and other stimuli...');
nulldiffmddps = NaN(nperm,length(wnd),length(stimtype)-1);
pdpdiff = NaN(length(wnd),length(stimtype)-1);
for jj = 1:length(stimtype)-1
    for w = 1:length(wnd)
        % get a null distribution of median values for permuted zrs by
        % randomly reassigning zrs to either speech or one of the other
        % stimuli, and do the shuffling within subjects
        DP_otherstim = squeeze(dp(:,w,jj,:));
        DP_speech = squeeze(dp(:,w,4,:));
        [pdpdiff(w,jj),nulldiffmddps(:,w,jj)] = permutation_test_subjects(DP_otherstim,DP_speech,...
            'nperm',nperm,'tail','both');
    end
end