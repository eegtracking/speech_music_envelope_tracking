% Plot the topography of the spatial model weights at 2000 ms
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbj = 'SOS';
stimtype = 'speech';
wnd = 2000;
npc = 64;
nchan = 128;
% nchan = [1:31 33:128];
dly_range = [200 500]; % range of delays over which to average weights

res_fld = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';
% wnd_fld = sprintf('%dms/',wnd);
fl = sprintf('%s_%s_%dms',sbj,stimtype,wnd);

d = load([res_fld fl]);
Fs = d.desFs;
dly = -(floor(-d.tmax/1000*Fs):ceil(-d.tmin/1000*Fs));

% Reshape the model so it's delay x time
nreps = length(d.model_t);
M = NaN(length(dly),nchan,nreps);
for n = 1:nreps
    mdl = reshape(d.model_t{n},[length(dly),npc]);
%     mdl = reshape(d.model{n},[length(dly),npc]);
    rM = mdl.*(ones(length(dly),1)*d.var_pc(:,n)')/sum(d.var_pc(:,n));
    M(:,:,n) = rM*d.cf{n}(:,1:npc)';
end
% Average across reps
M = mean(M,3);

% remove channels? (see subject MB)
% rmv_chans = 32;
rmv_chans = [];
use_chans = setxor(1:nchan,rmv_chans);
M = M(:,use_chans);

figure
imagesc(dly/Fs*1000,use_chans,M');
set(gca,'FontSize',16);
colorbar;
xlabel('Time (ms)');
ylabel('Channel');

dly_idx = dly/Fs*1000>=dly_range(1) & dly/Fs*1000<=dly_range(2);

figure
set(gcf,'Position',[600 350 450 300]);
topoplot(mean(M(dly_idx,:)),'~/Projects/EEGanly/chanlocs.xyz','maplimits','maxmin');
colorbar;
title(sprintf('%s, %s, %d - %d ms',sbj,stimtype,dly_range(1),dly_range(2)));