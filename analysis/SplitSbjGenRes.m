% Run this to split files which contain all stimuli using the
% stimulus-general model into separate results files for each stimulus type

sbj = 'AB';
stimtype = {'rock','classical','vocals','speech'};
% stim_idxs = 10:10:40; % get the ending indexes for each stimulus type in the
    % stimulus general results files
clr = {'r','g','c','b'};
wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000];
maxtr = 7;
respth = '~/Projects/NeuroMusic/rcnstr_tbt/';

% Load the reconstruction accuracies
r_sp = NaN(maxtr,length(wnd),length(stimtype));
r_gen = NaN(maxtr,length(wnd),length(stimtype));
for ii = 1:length(wnd),
    all_gen = sprintf('%s_ALL_%dms',sbj,round(wnd(ii)));
    all_d = load([respth all_gen]);
    % get general parameters saved in the results file
    tmin = all_d.tmin;
    tmax = all_d.tmax;
    desFs = all_d.desFs;
    mdlFs = all_d.mdlFs;
    tlims = all_d.tlims;
    npcs = all_d.npcs;
    ds = all_d.ds;
    % now compute the stimulus-type-specific results variables
    tr_cnt = 0;
    for jj = 1:length(stimtype),
        tr_idx = (1:maxtr)+tr_cnt;
        tidx = all_d.r(tr_idx);
        r = all_d.r(tr_idx);
        mse = all_d.mse(tr_idx);
        model_t = all_d.model_t(tr_idx);
        cf = all_d.cf(tr_idx);
        var_pc = all_d.var_pc(:,tr_idx);
        % get the stimulus-general file name for results of a specific stimulus
        % type
        fl_gen = sprintf('%s_ALL_%s_%dms',sbj,stimtype{jj},round(wnd(ii)));
        % save the results
        save([respth fl_gen],'tmin','tmax','desFs','mdlFs','tlims','npcs','ds',...
            'tidx','r','mse','model_t','cf','var_pc');
        % increment tr_cnt
        tr_cnt = tr_cnt+maxtr;
        disp(fl_gen)
    end
end