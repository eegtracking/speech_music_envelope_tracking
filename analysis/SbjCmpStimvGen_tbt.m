% Get the reconstruction accuracies for stimulus-specific and
% stimulus-general models, and compute the difference between them as a
% function of window size (frequency)
%%% Plot the TRF models of each type of stimulus

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbj = 'CQ';
stimtype = {'rock','classical','vocals','speech'};
% stim_idxs = 10:10:40; % get the ending indexes for each stimulus type in the
    % stimulus general results files
clr = {'r','g','c','b'};
wnd = [31.25 62.5 125 250 500 1000 2000 4000 8000];
maxtr = 7;
nchan = 128;
plot_chans = 65:109;
Fs = 512;
respth = '~/Projects/NeuroMusic/rcnstr_tbt/';

% Load the reconstruction accuracies
r_sp = NaN(maxtr,length(wnd),length(stimtype));
r_gen = NaN(maxtr,length(wnd),length(stimtype));
mdl_sp = cell(length(wnd),1);
mdl_gen = cell(length(wnd),1);
DLY = cell(length(wnd),1);
for ii = 1:length(wnd),
    % setup storing info for model
    DLY{ii} = -(-ceil(wnd(ii)/1000*Fs):0); % see lagGen, delays are ordered
    % from -max to -min in the backwards model
    M_sp = NaN(length(DLY{ii}),nchan,maxtr,length(stimtype));
    M_gen = NaN(length(DLY{ii}),nchan,maxtr,length(stimtype));
    for jj = 1:length(stimtype),
        % load the stimulus-general results
        fl_gen = sprintf('%s_ALL_%s_%dms',sbj,stimtype{jj},round(wnd(ii)));
        d_gen = load([respth fl_gen]);
        % stimulus-specific
        fl = sprintf('%s_%s_%dms',sbj,stimtype{jj},round(wnd(ii)));
        d = load([respth fl]);
        ntr = size(d.r,1);
        r_sp(1:ntr,ii,jj) = reshape(d.r,[ntr 1]);
        % stimulus-general
        r_gen(1:ntr,ii,jj) = reshape(d_gen.r,[ntr 1]);
        % tranform model to EEG
        for n = 1:ntr
            % stimulus specific model
            mdl = reshape(d.model_t{n},[length(DLY{ii}),d.npcs]);
            % normalize each model weight based on the ratio of PC variance
            % to total variance
            rM = mdl.*(ones(length(DLY{ii}),1)*d.var_pc(:,n)')/sum(d.var_pc(:,n));
            M_sp(:,:,n,jj) = rM*d.cf{n}(:,1:d.npcs)';
            % stimulus general model
            mdl = reshape(d_gen.model_t{n},[length(DLY{ii}),d_gen.npcs]);
            % normalize each model weight based on the ratio of PC variance
            % to total variance
            rM = mdl.*(ones(length(DLY{ii}),1)*d_gen.var_pc(:,n)')/sum(d_gen.var_pc(:,n));
            M_gen(:,:,n,jj) = rM*d_gen.cf{n}(:,1:d_gen.npcs)';
        end
        % average models across trials
        mdl_sp{ii} = squeeze(mean(M_sp,3,'omitnan'));
        mdl_gen{ii} = squeeze(mean(M_gen,3,'omitnan'));
        disp(fl)
        disp(fl_gen)
    end
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*8);
end

% Plot the results
% reconstruction accuracy
figure
hold on
r_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype),
    pl = plot(1:length(wnd),r_sp(:,:,jj)',clr{jj},'LineWidth',1);
    plot(1:length(wnd),r_gen(:,:,jj)',[clr{jj} '--'],'LineWidth',1);
    r_plts(jj) = pl(1);
end
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('Reconstruction accuracy');
title(sbj);
legend(r_plts,stimtype);

% Plot the difference in reconstruction accuracies
figure
hold on
df_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype)
    diffr = r_sp(:,:,jj)-r_gen(:,:,jj);
    plot(1:length(wnd),diffr',clr{jj},'LineWidth',1);
    pl = plot(1:length(wnd),mean(diffr,1,'omitnan'),clr{jj},'LineWidth',3);
    df_plts(jj) = pl(1);
end
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45);
xlabel('Frequency (Hz)');
ylabel('Difference in reconstruction accuracy');
title(sbj);
legend(df_plts,stimtype);

%% Plot the models
% plot the models
figure
for s = 1:length(stimtype)
    subplot(length(stimtype),1,s); % different plot for each stimulus
    hold on
    for w = 1:length(wnd)
        plot(DLY{w}/Fs*1000,mean(mdl_gen{w}(:,plot_chans,s),2),'k','LineWidth',1);
        plot(DLY{w}/Fs*1000,mean(mdl_sp{w}(:,plot_chans,s),2),clr{s},'LineWidth',1)
    end
    set(gca,'FontSize',16,'XScale','log');
    ylabel('w');
    title(sprintf('%s, %s',sbj,stimtype{s}));
end
xlabel('Time (ms)');

% plot the difference between the models
figure
for s = 1:length(stimtype)
    subplot(length(stimtype),1,s); % different plot for each stimulus
    hold on
    for w = 1:length(wnd)
        plot(DLY{w}/Fs*1000,mean(mdl_sp{w}(:,plot_chans,s)-mdl_gen{w}(:,plot_chans,s),2),clr{s},'LineWidth',1)
    end
    set(gca,'FontSize',16,'XScale','log');
    ylabel('w_{sp}-w_{gen}');
    title(sprintf('%s, %s',sbj,stimtype{s}));
end
xlabel('Time (ms)');