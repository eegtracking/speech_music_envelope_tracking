% Plot the topography of the spatial model weights at 2000 ms
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbj = 'AB';
stimtype = 'speech';
wnd = 500;
npc = 128;

res_fld = '../rcnstr_res/';
wnd_fld = sprintf('%dms/',wnd);
fl = sprintf('%s_%s_db',sbj,stimtype);

d = load([res_fld wnd_fld fl]);
Fs = d.desFs;
dly = floor(d.tmin/1000*Fs):ceil(d.tmax/1000*Fs);

% Reshape the model so it's delay x time
nreps = length(d.model_t);
M = NaN(length(dly),npc,nreps);
for n = 1:nreps
    mdl = reshape(d.model_t{n},[length(dly),npc]);
%     mdl = reshape(d.model{n},[length(dly),npc]);
    rM = mdl.*(ones(length(dly),1)*mean(d.var_pc));
    M(:,:,n) = rM*d.cf';
end
% Average across reps
M = mean(M,3);

figure
imagesc(dly/Fs*1000,1:npc,M');
set(gca,'FontSize',16);
colorbar;
xlabel('Time (ms)');
ylabel('Channel');

dly_range = [200 300]; % range of delays over which to average weights
dly_idx = dly/Fs*1000>=dly_range(1) & dly/Fs*1000<=dly_range(2);

figure
topoplot(mean(M(dly_idx,:)),'~/Projects/EEGanly/chanlocs.xyz','maplimits','maxmin');
colorbar;
title(sprintf('%d - %d ms',dly_range(1),dly_range(2)));