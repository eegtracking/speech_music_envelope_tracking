% Demonstrate how the spectrum of some signal (random noise, for example)
% changes after the moving average is removed and spline interpolation
addpath('~/Projects/mTRF-toolbox/');
% dur = 180; % duration of the signal (s)
dur = 32; % duration of the signal (s);
eFs = 512;
desFs = 512;
s = randn(dur*eFs,1); % take a speech example
% s = [1; zeros(dur*eFs-1,1)];
nscale = 16; % # of splines in the TRF window
% nscale = 8; % NZ (11-1-2020), optimal is 8x 1/window_size

% Compute the power spectrum of each step
[P,frq_e] = pwelch(s,5*desFs,[],[],desFs);

win_sizes = 500;
fprintf('Window = %.2f ms\n',win_sizes);
% size of mTRF window
% tmin = 0;
% tmax = win_sizes;
% When using tmin = 0, the spline that best matches the original signal is
% the first one, which is discontinuous at t=0. This introduces an edge
% effect in the spectrum, which does not appropriately capture the
% filtering properties of the spline transformation. A centered spline is a
% better representation.
tmin = -win_sizes/2;
tmax = win_sizes/2;
% remove moving average
t = -ceil(tmax/1000*desFs):-floor(tmin/1000*desFs);
if desFs<eFs,
    s = resample(s,desFs,eFs);
end
smv = s - movmean(s,length(t));

% get the lagGen matrix of noise
SMV = lagGen(smv,t);

% spline interpolation
mdlFs = 1000/(tmax-tmin)*nscale;
ds = desFs/mdlFs;
if ds>1
    [spline_matrix,SSPL] = spline_transform(SMV,ds);
else
    spline_matrix = eye(length(t));
end
% transform a delta function (zero lag) using the spline matrix
% then compute dot product with the spline-transformed design matrix
%     dlt = [zeros(1,length(t)-1) 1];
% dlt = zeros(1,length(t)); dlt(ceil(length(t)/2)) = 1;
% spdlt = dlt*spline_matrix*(spline_matrix'*spline_matrix)^(-1);
% %     sflt = SSPL*spdlt';
% sflt = SMV*(spdlt*spline_matrix')';
% compute the best fit to the original signal (find the best-fitted spline)
mdl = SSPL \ smv;
sflt = SSPL*mdl;

% clear SSPL

[Pflt,~] = pwelch(sflt,5*desFs,[],[],desFs);

figure
hold on
plot(frq_e,10*log10(P),'k');
plot(frq_e,10*log10(Pflt),'Color','b','LineWidth',2);
plot([1000/win_sizes 1000/win_sizes],[-100 0],'k--');
plot([mdlFs mdlFs]/2,[-100 0],'k--');
% set(gca,'XScale','log','YScale','log','FontSize',16);
set(gca,'XScale','log','FontSize',16);
xlabel('Frequency (Hz)');
ylabel('Power spectral density (dB)');
% legend('Original','Removed movmean','Spline filtered');

figure
hold on
plot([frq_e(2) frq_e(end)],[0 0],'k--');
plot(frq_e,10*log10(Pflt)-10*log10(P),'Color','b','LineWidth',2);
plot([1000/win_sizes 1000/win_sizes],[-60 20],'k--');
plot([mdlFs mdlFs]/2,[-60 20],'k--');
set(gca,'XScale','log','FontSize',16);
xlabel('Frequency (Hz)');
ylabel('Power spectral density of filter');

% fprintf('Geometric mean: %.3f\n',geomean([mdlFs/2 1000/(tmax-tmin)]));