#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH -c 4
#SBATCH -n 1
#SBATCH --mem=60GB

sbj=$1
stim=$2
wnd=$3

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "sbj='${sbj}'; stimtype='${stim}'; wnd=${wnd}; OrigEnvSeptrials(sbj,stimtype,wnd);" -singleCompThread
rm -rf /scratch/$USER/$SLURM_JOB_ID
