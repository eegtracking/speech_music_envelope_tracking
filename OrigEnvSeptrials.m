function OrigEnvSeptrials(sbj,stimtype,window_size,varargin)

tmin = 0;
tmax = window_size+tmin;
map = -1;
desFs = 128;
tlims = 2;
nfolds = 10;
niter = 10;
voltlimdb = -55; % NZ (22-3-2019) changed to -55 dB, specified where silences occur
    % see also HistLevel_NeuroMusic.m
sil_tol = 0.001; % the sound is identified as "on" when the silence vector equals 1,
    % this is the tolerance around 1 at which to define the sound as "on"
do_mov_avg = true;
lambdas = 10.^(0:8);

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
stimpth = '~/NeuroMusic/stims/';
stimfl = sprintf('NeuroMusicdBEnvelopes_128Hz_%s',stimtype);
s = load([stimpth stimfl]);
stims = s.stimset;
% Transpose all stimuli to column vectors
for ii = 1:length(stims), stims{ii} = stims{ii}'; end
% clear s

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

disp('Convert stimuli to dB');
silence = cell(length(stims),1); % to store silent periods
for ii = 1:length(stims),
    % store 0 anytime there's silence
    silence{ii} = ones(length(stims{ii}),1);
    silence{ii}(stims{ii}<voltlimdb) = 0;
    stims{ii}(stims{ii}<voltlimdb) = voltlimdb;
end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% if do_mov_avg, disp('...and removing silences...'); end
disp('...and removing silences...');
tidx = cell(length(silence),1);
for ii = 1:length(silence),
    tidx{ii} = zeros(length(silence{ii}),1);
    tidx{ii}(abs(silence{ii}-1)<sil_tol) = 1; % set to 1 when stimulus is on, and no ringing
    l = usetinds(tlims,desFs,length(silence{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
et = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(et));
    eeg{ii} = eeg{ii}-avg;
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        s_on = stims{ii};
        s_on(~tidx{ii}) = mean(s_on(tidx{ii})); % set silent periods to the average of
            % the non-silent envelope, to avoid edge effects as speech
            % turns on and off
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii}(tidx{ii}) = s_on(tidx{ii});
%         savg = movmean(stims{ii},length(t));
%         stims{ii} = stims{ii}-savg;
    end
end
clear avg s_on

% Downsample to EEG and stimulus by 4x, to make it more managable for the
% computer
for ii = 1:length(eeg),
    eeg{ii} = resample(eeg{ii},desFs,eFs);
    eeg{ii} = zscore(eeg{ii}); % zscore after resampling, because this can change the variance
end

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,eeg);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(eeg{n},1)); end
    end
end

disp('Computing the envelope reconstruction...');
[r,~,mse,model] = mTRFcrossval(stims,eeg,desFs,map,tmin,tmax,lambdas,tidx,...
    'nfolds',nfolds,'randomize_idx',false);

disp('Computing null distribution...');
optlmb = lambdas(mean(r)==max(mean(r)));
[nullr,nullmse] = mTRFnull_standard(stims,eeg,desFs,map,tmin,tmax,optlmb,tidx,...
     'niter',niter);

svpth = sprintf('~/NeuroMusic/orig_septrials_res/%dms/',round(tmax-tmin));
svfl = sprintf('%s_%s',sbj,stimtype);
save([svpth svfl],'r','mse','model','desFs','tmin','tmax','tlims','voltlimdb','tidx',...
    'nfolds','lambdas','nullr','nullmse');
