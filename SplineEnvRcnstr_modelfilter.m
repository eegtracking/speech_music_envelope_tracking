% Demonstrate how the spectrum of some signal (random noise, for example)
% changes after the applying the reconstruction model
addpath('~/Projects/EEGanly/PCA_spline_modeling/');
addpath('~/Projects/mTRF_Zukedits/');
dur = 100; % duration of the signal (s)
eFs = 512;
desFs = 128;
npcs = 16;
s = randn(dur*eFs,npcs+1); % take a speech example

% Load the model
sbj = 'HC';
stimtype = 'speech';
wnd = 2000; % TRF duration
respth = sprintf('rcnstr_res/%dms/',round(wnd));
resfl = sprintf('%s_%s_db',sbj,stimtype);
m = load([respth resfl]);
model = m.model;
clear m

% size of mTRF window
tmin = 0;
tmax = wnd+tmin;
% remove moving average
t = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
smv = s - movmean(s,length(t));
smv = resample(smv,desFs,eFs);

% create the "prediction" using the loaded model
mdlFs = 1000/(tmax-tmin)*16;
ds = desFs/mdlFs;
pred = mTRFpredict_spline(ones(size(smv,1),1),smv,model,desFs,ds,-1,tmin,tmax,[]);

% Compute the power spectrum of each step
[P,frq] = pwelch(pred,10*desFs,5*desFs,[],desFs);
[S,~] = pwelch(smv,10*desFs,5*desFs,[],desFs);

figure
hold on
plot(frq,(P*ones(1,npcs+1))./S,'b','LineWidth',2);
plot(1000/(tmax-tmin)*ones(1,2),[10^-10 10^-2],'k--'); % highpass cutoff
plot(1000/(tmax-tmin)*ones(1,2)*8,[10^-10 10^-2],'k--'); % lowpass cutoff
set(gca,'XScale','log','YScale','log','FontSize',16);
xlabel('Frequency (Hz)');
ylabel('Power spectral density');
title(sprintf('%s, %s',sbj,stimtype));
legend('Original','Removed movmean','Spline filtered');

% Compute the difference in angle between the prediction and the original
% stimulus
allP = fft(pred);
allS = fft(smv);
fullfrq = (0:length(pred)-1)/length(pred)*desFs;
figure
% plot(fullfrq,unwrap((angle(allP)*ones(1,npcs+1))-angle(allS)),'k','LineWidth',2);
plot(fullfrq,unwrap(angle(allP))-unwrap(angle(mean(allS,2))),'k','LineWidth',2);
% D = diff(unwrap(angle(allP)-angle(mean(allS,2))));
% plot(fullfrq(1:end-1),D/diff(fullfrq(2:3)),'k','LineWidth',2);
set(gca,'XScale','log','XLim',[0.01 50],'FontSize',16);
xlabel('Frequency (Hz)');
ylabel('Phase (radians)');
% ylabel('Derivative of phase (radians/Hz)');
title(sprintf('%s, %s',sbj,stimtype));