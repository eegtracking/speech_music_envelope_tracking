% Compute the number and duration of silences, using a cutoff point of -55
% dB V
stimtag = 'rock';
desFs = 512; % sampling rate of the envelope
thres = -55;
cutoff = 0.5; % number of s to remove from start and end of stimulus before analysis

stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtag);
stimpth = '/Volumes/ZStore/NeuroMusic/';
s = load([stimpth stimfl]);

sil = cell(length(s.stimset),1);
nsil = NaN(length(s.stimset),1);
sil_dur = NaN(length(s.stimset),1);
for ii = 1:length(s.stimset)
    use_idx = (desFs*cutoff+1):(length(s.stimset{ii})-desFs*cutoff);
    sil{ii} = s.stimset{ii}(use_idx)<thres;
    % skip the first 0.5 seconds and count the number of silences and their
    % durations
    sil_start = diff(sil{ii})==1;
    sil_end = diff(sil{ii})==-1;
    % compute the number of silences (we expect the last silence start to
    % occur without an end at the end of the stimulus)
    st_idx = find(sil_start);
    ed_idx = find(sil_end);
    nsil(ii) = length(st_idx)-1;
    % compute the median duration of the silences (skip the first "end"
    % silence because it corresponds to the start of the stimulus)
    sil_dur(ii) = mean(ed_idx(2:end)-st_idx(1:end-1))/desFs;
end