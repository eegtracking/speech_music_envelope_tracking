function stims = getstimlist(stimlst,stimpth)
% Load a list of stimuli from a text file

disp('Loading list of stimuli...');
lstf = fopen([stimpth stimlst]);
tline = fgetl(lstf);
stims = {};
while ischar(tline)
    stims = [stims {strtrim(tline)}]; % get the stimulus name on each line
    tline = fgetl(lstf);
end
fclose(lstf);