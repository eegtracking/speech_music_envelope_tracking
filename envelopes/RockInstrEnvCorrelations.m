% Compute the correlation between each instrument (vocals, guitar, bass,
% drums) and the full rock envelope
addpath('..');
instrtag = {'vocals','guitar','bass','drums'};
desFs = 512; % sampling rate of the envelope
tlims = 16; % range of time to remove from the start and end of the envelope

rockfl = sprintf('NeuroMusicdBEnvelopes_%dHz_rock',desFs);
% stimfl = sprintf('NeuroMusicdBInstruments_%dHz_%s',desFs,instrtag);
stimpth = '/Volumes/ZStore/NeuroMusic/';
rockstim = load([stimpth rockfl]); % load the full rock envelopes
rockstim = rockstim.stimset;

c = NaN(10,4); % stores correlation values

for ii = 1:length(instrtag)
    % load the instrument envelopes
    instrfl = sprintf('NeuroMusicdBInstruments_%dHz_%s',desFs,instrtag{ii});
    instrstim = load([stimpth instrfl]);
    instrstim = instrstim.stimset;
    % for each trial, compute correlation
    for n = 1:10
        tidx = usetinds(tlims,desFs,length(rockstim{n}));
        c(n,ii) = corr(instrstim{n}(tidx)',rockstim{n}(tidx)');
    end
end

% rearrange elements into a column for dot_median_plot
C = reshape(c,[numel(c) 1]);
lbls = reshape(repmat(1:4,10,1),[40 1]);

dot_median_plot(lbls,C);
set(gca,'FontSize',14,'XTickLabel',instrtag);
ylabel('Correlation between instrument and full rock envelope');

% compute significance of difference between drums and other instrument
% correlations
p_cmp = NaN(3,1);
st_cmp = cell(3,1);
for ii = 1:length(instrtag)-1
    [p_cmp(ii),~,st_cmp{ii}] = signrank(c(:,4),c(:,ii),'tail','right');
end