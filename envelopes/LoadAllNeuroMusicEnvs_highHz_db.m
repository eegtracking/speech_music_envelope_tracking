% Load all NeuroMusic stimuli of one type

addpath(genpath('~/Projects/EnvelopeExtract/'));
addpath('~/Projects/NeuroMusic/');

eFs = 512;
stimtag = 'vocals';

stimpth = '/Volumes/ZStore/NeuroMusic/All Stimuli/Stimuli_Presentation/';
fslst = 'stimlist.txt';

stimcodes = getstimcodes(stimtag);

% Load the eeg and stimuli
stimset = loadneuromusicstims_db(stimpth,stimcodes,fslst,eFs);
ntr = length(stimset);

% Save the envelopes
svpth = '/Volumes/ZStore/NeuroMusic/';
svnm = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',eFs,stimtag);
save([svpth svnm],'stimset');