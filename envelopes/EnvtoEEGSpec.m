% Plot the variance in the power spectrum for a set of NeuroMusic stimuli
addpath('~/Projects/NeuroMusic/');
addpath('~/Documents/Matlab/shadedErrorBar/');
stimtypes = {'rock','classical','vocals','speech'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
Fs = 512;
wnd = 16; % window size (in s);
overlap = 8; % overlap between windows (in s);
nstim = 10; % number of stimuli
tlims = 16;

% First, get the average dB variance across all stimuli
var_env = NaN(nstim,length(stimtypes));
for jj = 1:length(stimtypes)
    s = load(['/Volumes/ZStore/NeuroMusic/NeuroMusicdBEnvelopes_512Hz_' stimtypes{jj}]);
    for ii = 1:length(s.stimset)
        stim = s.stimset{ii};
        tidx = usetinds(tlims,Fs,length(stim));
        var_env(ii,jj) = var(stim(tidx));
    end
end
avg_var = mean(mean(var_env));

PW = NaN(wnd*Fs/2,nstim,length(stimtypes));
disp('Computing power spectra of dB envelopes...');
for jj = 1:length(stimtypes),
    s = load(['/Volumes/ZStore/NeuroMusic/NeuroMusicdBEnvelopes_512Hz_' stimtypes{jj}]);
    for ii = 1:length(s.stimset),
        stim = s.stimset{ii};
        % Remove moving average associated with largest window
        stim = stim-movmean(stim,Fs*wnd);
        tidx = usetinds(tlims,Fs,length(stim));
%         stim = zscore(stim(tidx));
%         stim = detrend(stim(tidx));
        %%% NZ (7-12-2020) Changed to detrend(~,0), was linearly detrending
        %%% before
        stim = detrend(stim(tidx),0)/sqrt(avg_var); % normalize by the average st.dev. across all stimuli
        [pw,frq] = pwelch(stim,wnd*Fs,overlap*Fs,wnd*Fs,Fs,'power');
        for kk = 1:length(frq)-1
            % average power spectrum within 1/3 octave around center
            % frequency
            frq_range_idx = frq>=(frq(kk+1)*2^(-1/6)) & frq<=(frq(kk+1)*2^(1/6));
            PW(kk,ii,jj) = sqrt(mean(pw(frq_range_idx)));
        end
    end
end

disp('Computing power spectra of EEG...');
% Iterate through each EEG mat file for NeuroMusic, get the EEG for each
% trial, average across channels, z-score, and then compute power spectrum.
% Then add across all trials and subjects, and divide by the total number
% of iterations
eeg_fld = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/';
eeg_fls = what(eeg_fld);
mats = eeg_fls.mat;
neegs = 0;
EEG_AVG_PW = zeros(length(frq)-1,1);
for m = 1:length(mats)
    % load the eeg
    d = load([eeg_fld mats{m}]);
    for n = 1:length(d.eeg)
        % remove moving average (detrend first to avoid discontinuities)
        eeg = detrend(d.eeg{n});
        eeg = eeg-movmean(eeg,Fs*wnd);
        % average across channels
        eeg = mean(eeg,2);
        % z-score
        eeg = zscore(eeg);
        % compute power spectrum
        eeg_pw = pwelch(eeg,wnd*Fs,overlap*Fs,wnd*Fs,Fs,'power');
        EEG_PW = NaN(length(frq)-1,1);
        for kk = 1:length(frq)-1
            % average power spectrum within 1/3 octave around center
            % frequency
            frq_range_idx = frq>=(frq(kk+1)*2^(-1/6)) & frq<=(frq(kk+1)*2^(1/6));
            EEG_PW(kk) = sqrt(mean(eeg_pw(frq_range_idx)));
        end
        % add to total power spectrum (summed across all trials / subjects)
        EEG_AVG_PW = EEG_AVG_PW+EEG_PW;
        % add to neegs
        neegs = neegs+1;
    end
    % display that the data for this block has been loaded
    disp(mats{m});
end

% Average across power spectra
EEG_AVG_PW = EEG_AVG_PW/neegs;

% Compute a bootstrapped distribution of median spectra
nboot = 1000;
boot_mdspec = NaN(length(frq)-1,nboot,length(stimtypes));
for jj = 1:length(stimtypes)
    for n = 1:nboot
        rand_smp = randi(nstim,nstim,1);
        boot_mdspec(:,n,jj) = median(PW(:,rand_smp,jj)./EEG_AVG_PW,2);
    end
end
uq_bootspec = squeeze(quantile(boot_mdspec,0.95,2)); % 95% quantile
lq_bootspec = squeeze(quantile(boot_mdspec,0.05,2)); % 5% quantile
md_spec = squeeze(median(PW./repmat(EEG_AVG_PW,1,nstim,length(stimtypes)),2));
    
% Median
figure
set(gcf,'Position',[360,10,500,350]);
hold on
avg_plt = NaN(length(stimtypes),1);
for jj = 1:length(stimtypes),
    plt = shadedErrorBar(frq(2:end),md_spec(:,jj),...
        [uq_bootspec(:,jj)-md_spec(:,jj) md_spec(:,jj)-lq_bootspec(:,jj)]',...
        'lineProps',{'Color',clr{jj},'LineWidth',2});3
%     plt = shadedErrorBar(log2(frq(2:end)),md_spec(:,jj),...
%         [uq_bootspec(:,jj)-md_spec(:,jj) md_spec(:,jj)-lq_bootspec(:,jj)]',...
%         'lineProps',{'Color',clr{jj},'LineWidth',2});
    avg_plt(jj) = plt.mainLine;
end
set(gca,'FontSize',16,'XScale','log','XLim',[0.1 64],'XTick',10.^(-1:2));
% set(gca,'FontSize',16,'XLim',log2([0.1 64]),'XTick',-3:6,'XTickLabel',2.^(-3:6));
xlabel('Frequency (Hz)');
ylabel('Envelope power / EEG power');
legend(avg_plt,stimtypes);