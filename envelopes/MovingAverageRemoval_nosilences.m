% Plot a stimulus envelope before and after moving averaging
% NZ (4-1-2020) -- because I'm skipping silences in envelope, I will also
% mark which parts of the envelope are reconstructed

addpath('..');

stimtype = 'speech';
stimidx = 5;
wnd = 8000; % trf window size, in ms
Fs = 512;
voltlimdb = -55; % below this db voltage, identify as a silent period
tlims = 16; % skip the first and last 2 seconds

stimpth = '/Volumes/ZStore/NeuroMusic/';
stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',Fs,stimtype);
s = load([stimpth stimfl]);

% get and store the original envelope in a separate variable
orig = s.stimset{stimidx};

% store 0 anytime there's silence
limited_env = orig; % store a new envelope where the voltage is limited to voltlimdb
% silence = ones(length(orig),1);
% silence(orig<voltlimdb) = 0;
% limited_env(orig<voltlimdb) = voltlimdb;

% skip the first and last 2 seconds of the envelope as well
% tidx = silence; 
tidx = ones(length(orig),1);
l = usetinds(tlims,Fs,length(orig)); % apply tlims
tidx(~l) = 0;
tidx = logical(tidx); % turn into logical array

dly = 0:ceil(wnd/1000*Fs); % delay vector for window
mov_rmv = limited_env;
mov_rmv(~tidx) = mean(mov_rmv(tidx)); % set silent periods to the average of
%     the non-silent envelope, to avoid edge effects as speech
%     turns on and off
mov_rmv = mov_rmv-mean(mov_rmv); % shift to 0 mean (so it starts and ends near zero, 
    % avoid edge effects of moving average)
mov_rmv = mov_rmv-movmean(mov_rmv,length(dly));

% orig = s.stimset{stimidx};
% dly = 0:ceil(wnd/1000*Fs);
% mov_rmv = orig - movmean(orig,length(dly));

t = (0:length(orig)-1)/Fs;
figure
subplot(2,1,1);
plot(t,orig,'k');
set(gca,'FontSize',16,'XLim',[0 max(t)]);
% set(gca,'FontSize',16,'XLim',[80 100]); %%% use this when plotting speech,
    %%% makes silences skipped clearer
xlabel('Time (s)');
ylabel('dB V');
title('Original');

subplot(2,1,2);
hold on
plot(t,mov_rmv,'k');
% set silences to NaN to skip them when plotting
tskip = t; tskip(~tidx) = NaN;
mov_rmv_skip = mov_rmv; mov_rmv_skip(~tidx) = NaN;
plot(tskip,mov_rmv_skip,'r','LineWidth',1.5);
set(gca,'FontSize',16,'XLim',[0 max(t)]);
% set(gca,'FontSize',16,'XLim',[80 100]);
xlabel('Time (s)');
ylabel('dB V');
mv_tle = sprintf('Moving average removed: %d ms',wnd);
title(mv_tle);
legend('Without moving average','Regions reconstructed');

[Porig,F] = pwelch(orig(~isnan(mov_rmv_skip)),Fs*30,[],[],Fs);
[Prmv,~] = pwelch(mov_rmv_skip(~isnan(mov_rmv_skip)),Fs*30,[],[],Fs);
% [Prmv,~] = pwelch(mov_rmv,Fs*30,[],[],Fs);
figure
hold on
plot(F,Porig,'k','LineWidth',2);
plot(F,Prmv,'b','LineWidth',2);
set(gca,'FontSize',16,'XScale','log','YScale','log');
xlabel('Frequency (Hz)');
ylabel('Power spectral density');
legend('Original','Without low frequencies');