function stimset = loadneuromusicstims_db(stimpth,stimcodes,fslst,eFs,varargin)
% Load the stimuli from the neuromusic dataset for a specific set of
% stimulus codes
% Inputs:
%   - stimpth = path containing stimuli
%   - stimcodes = array of numbers containing stimulus codes to examine
%   - fslst = file in stimpth containing a list of all stimuli, in order of stimulus code
%   - stimtype = type of stimuli ('env'=envelope, 'spec'=spectrogram)
%   - eFs = sampling rate of the stimulus (Hz)
% Outputs:
%   - stimset = structure containing stimuli
% Nate Zuk (3-2019): This converts into dB, for the spline-based
% reconstruction method

totalstimcodes = 10:49;
voltlimdb = -100;

if ~isempty(varargin),
    for n = 2:2:length(varargin)
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Load the list of stimuli
stimlist = getstimlist(fslst,stimpth);

% Calculate voltlim
voltlim = 10^(voltlimdb/20);

stimset = cell(length(stimcodes),1);
for ii = 1:length(stimcodes) % For each stimulus code...
    stimtm = tic;
    % Load the sound file
    stimcd = find(stimcodes(ii)==totalstimcodes);
    disp(['--> ' stimlist{stimcd}]);
    disp('Loading the stimulus...');
    stimfl = strtrim([stimpth stimlist{stimcd}]);
    [y,Fs] = audioread(stimfl);
    if size(y,2)>1, y = mean(y,2); end % average channels if there are multiple

    % Get the envelope based on the gammatonegram
    disp('Computing the gammatonegram...');
    y = resample(y,Fs/2,Fs); % downsample y to make gammatonegram computation easier
%                 GM = gammatonegram(y,Fs/2,0.025,1/eFs,16,125,8000,0,0);
%                     % 25 ms summation windows
%                     % 1/eFs spacing between windows
%                     % 16 channels, logarithmically spaced between 125 and 8000 Hz
%                 if strcmp(stimtype,'env'), env = mean(GM)'; end
    [env,tmpFs] = gammatoneenv(y,Fs/2);
%     env = resample(fenv,eFs,tmpFs);

    % Set fenv to a max value of +/- 1 V
    env = env/max(abs(env));
    
    % Set all V below voltlim to voltlim
    env(env<voltlim) = voltlim;
    
    % Convert to dB
    env = 20*log10(env);
    
    % Downsample
    env = resample(env,eFs,tmpFs);

    disp('Processing the stimulus...');
    stimset{ii} = env;

    disp(['Completed @ ' num2str(toc(stimtm)) ' s!']);
end
