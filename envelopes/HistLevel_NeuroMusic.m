% Plot the level distribution of all NeuroMusic stimuli
%%% Based on looking at the histograms, I decided to flag "silences" as <
%%% -55 dB V (one rock song had min dB ~-60, so I thought that was a bad
%%% cutoff)
%%% (11-5-2020) -- No longer removing silences
stimtag = 'classical';
% instrtag = 'drums';
desFs = 512; % sampling rate of the envelope

stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtag);
% stimfl = sprintf('NeuroMusicdBInstruments_%dHz_%s',desFs,instrtag);
stimpth = '/Volumes/ZStore/NeuroMusic/';
s = load([stimpth stimfl]);
% for ii = 1:length(s.stimset), s.stimset{ii} = s.stimset{ii}'; end
% S = cell2mat(s.stimset);

edges = -100:1:20;
h = NaN(length(edges)-1,10);
md = NaN(10,1);
for ii = 1:length(s.stimset),
    md(ii) = median(s.stimset{ii});
    h(:,ii) = histcounts(s.stimset{ii},edges);
end

figure
cnts = edges(1:end-1)+diff(edges)/2;
bar(cnts,h,1);
set(gca,'FontSize',16,'YScale','log');
xlabel('dB V');
ylabel('# time samples');
title(stimtag);
% title(instrtag);