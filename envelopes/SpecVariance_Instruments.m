% Plot the variance in the power spectrum for a set of NeuroMusic stimuli
addpath('~/Projects/NeuroMusic/');
stimtypes = {'vocals','guitar','bass','drums'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
Fs = 512;
wnd = 20; % window size (in s);
overlap = 5; % overlap between windows (in s);
nstim = 10; % number of stimuli
tlims = 16;

PW = NaN(wnd*Fs/2+1,nstim,length(stimtypes));
stim_examples = cell(length(stimtypes),1);

for jj = 1:length(stimtypes),
    s = load(['/Volumes/ZStore/NeuroMusic/NeuroMusicdBInstruments_512Hz_' stimtypes{jj}]);
    for ii = 1:length(s.stimset),
        stim = s.stimset{ii};
        tidx = usetinds(tlims,Fs,length(stim));
        stim = zscore(stim(tidx));
        stim_examples{jj} = stim; % save the stimulus
        [pw,frq] = pwelch(stim,wnd*Fs,overlap*Fs,wnd*Fs,Fs,'power');
        PW(:,ii,jj) = 10.*log(pw);
    end
end

% Example envelopes
figure
set(gcf,'Position',[360,15,400,680]);
for jj = 1:length(stimtypes),
    subplot(length(stimtypes),1,jj);
    t = tlims+(0:length(stim_examples{jj})-1)/Fs; % time array
    plot(t,stim_examples{jj},'Color',clr{jj},'LineWidth',2);
    set(gca,'FontSize',14,'XLim',[0 200]);
    xlabel('Time (s)');
    ylabel(stimtypes{jj});
end
    
% Mean
figure
set(gcf,'Position',[360,10,500,685]);
subplot(2,1,1);
hold on
for jj = 1:length(stimtypes),
    % remove log-linear trend
    X = [ones(length(frq)-1,1) log10(frq(2:end))];
    b = X \ mean(PW(2:end,:,jj),2);
    detrend_PW = mean(PW(2:end,:,jj),2)-X*b;
    plot(frq,mean(PW(:,:,jj),2),'Color',clr{jj},'LineWidth',2);
    %%% Detrended power spectrum shows peaks more clearly but seems harder
    %%% to interpret -- makes low-frequency peaks not clear
%     plot(frq(2:end),detrend_PW,'Color',clr{jj},'LineWidth',2);
end
set(gca,'FontSize',16,'XScale','log');
xlabel('Frequency (Hz)');
ylabel('Average power (dB)');
legend(stimtypes);

% Variance
subplot(2,1,2);
hold on
for jj = 1:length(stimtypes),
    plot(frq,var(PW(:,:,jj),[],2),'Color',clr{jj},'LineWidth',2);
end
set(gca,'FontSize',16,'XScale','log');
xlabel('Frequency (Hz)');
ylabel('Variance in power (dB^2)');
legend(stimtypes);