% Compute the ratio of envelope power within a frequency range  (signal) relative to
% envelope power above that range (noise) when the 1) envelope moving
% average is removed above the lower cutoff of the frequency range, 2)
% z-score the envelope
% Nate Zuk (2020)
addpath('~/Projects/NeuroMusic/');
addpath('~/Documents/Matlab/shadedErrorBar/');
stimtypes = {'rock','classical','vocals','speech'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
Fs = 512;
spec_wnd = 16; % window size (in s);
frange = [0 3]; % lower and upper cutoff of "signal" range relative to
    % center frequency, in octaves
center_freqs = 2.^(-3:0.5:4.5);
nstim = 10; % number of stimuli
tlims = 16;

snr = NaN(length(center_freqs),nstim,length(stimtypes));
for jj = 1:length(stimtypes),
    disp(stimtypes{jj});
    s = load(['/Volumes/ZStore/NeuroMusic/NeuroMusicdBEnvelopes_512Hz_' stimtypes{jj}]);
    for ii = 1:length(s.stimset),
        stim = s.stimset{ii};
        for n = 1:length(center_freqs)
            % Remove moving average
            hp_cutoff = center_freqs(n)*2.^(frange(1));
            zstim = stim-movmean(stim,Fs/hp_cutoff);
            tidx = usetinds(tlims,Fs,length(zstim));
            zstim = zscore(zstim(tidx));
            [pw,frq] = pwelch(zstim,spec_wnd*Fs,spec_wnd/2*Fs,spec_wnd*Fs,Fs,'power');
            % Compute the signal power (average power within 3-octave range)
            sig_range_idx = frq>=(center_freqs(n)*2^(frange(1))) & frq<=(center_freqs(n)*2^(frange(2)));
            ns_range_idx = frq>(center_freqs(n)*2^(frange(2)));
            snr(n,ii,jj) = mean(pw(sig_range_idx))/mean(pw(ns_range_idx));
%             snr(n,ii,jj) = mean(pw(sig_range_idx))/mean(pw); % relative
%             to average power
        end
    end
end
  
% Compute a bootstrapped distribution of median snr
nboot = 1000;
boot_mdspec = NaN(length(center_freqs),nboot,length(stimtypes));
for jj = 1:length(stimtypes)
    for n = 1:nboot
        rand_smp = randi(nstim,nstim,1);
        boot_mdspec(:,n,jj) = median(snr(:,rand_smp,jj),2);
    end
end
uq_bootsnr = squeeze(quantile(boot_mdspec,0.95,2)); % 95% quantile
lq_bootsnr = squeeze(quantile(boot_mdspec,0.05,2)); % 5% quantile
md_snr = squeeze(median(snr,2));
    
% Median
figure
set(gcf,'Position',[360,10,500,400]);
hold on
avg_plt = NaN(length(stimtypes),1);
for jj = 1:length(stimtypes),
    plt = shadedErrorBar(center_freqs,md_snr(:,jj),...
        [uq_bootsnr(:,jj)-md_snr(:,jj) md_snr(:,jj)-lq_bootsnr(:,jj)]',...
        'lineProps',{'Color',clr{jj},'LineWidth',2});
    avg_plt(jj) = plt.mainLine;
end
set(gca,'FontSize',16,'XScale','log','XLim',[center_freqs(1) center_freqs(end)],'XTick',10.^(-1:2),'YScale','log');
xlabel('Highpass frequency (Hz)');
ylabel('Median SNR (3-oct sig bandwidth)');
legend(avg_plt,stimtypes);