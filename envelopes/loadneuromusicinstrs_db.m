function stimset = loadneuromusicinstrs_db(instrpth,stimlistpth,instrtype,fslst,eFs,varargin)
% Load the individual instruments for the neuromusic rock songs and compute
% their envelopes
% Inputs:
%   - instrpth = path containing wav files with individual instruments
%   - stimlistpth = path containing the stimulus list
%   - instrtype = name of the instrument to analyze
%   - fslst = file in stimpth containing a list of all stimuli, in order of stimulus code
%   - eFs = sampling rate of the stimulus (Hz)
% Outputs:
%   - stimset = structure containing stimuli
% Nate Zuk (3-2019): This converts into dB, for the spline-based
% reconstruction method

rockstimcodes = 10:19;
totstimcodes = 10:49;
voltlimdb = -100;
sil_time = 0.5; % number of seconds of silence preceding the stimulus

if ~isempty(varargin),
    for n = 2:2:length(varargin)
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Load the list of stimuli
stimlist = getstimlist(fslst,stimlistpth);
[~,rockidx] = intersect(totstimcodes,rockstimcodes);
stimlist = stimlist(rockidx); % remove everything except the rock songs

% Calculate voltlim
voltlim = 10^(voltlimdb/20);

stimset = cell(length(rockstimcodes),1);
for ii = 1:length(rockstimcodes) % For each stimulus code...
    stimtm = tic;
    % Load the sound file
%     stimcd = find(stimcodes(ii)==totalstimcodes);
    stimfl = strtrim([instrpth stimlist{ii}]);
    % remove "_present" from filename
    stimfl = stimfl(1:strfind(stimfl,'_present')-1);
    instrfl = [stimfl '-' instrtype '.wav'];
    disp(['--> ' instrfl]);
    disp('Loading the stimulus...');
    [y,Fs] = audioread(instrfl);
    if size(y,2)>1, y = mean(y,2); end % average channels if there are multiple
    y = [zeros(Fs*sil_time,1); y; zeros(Fs*sil_time,1)]; % add silence preceding and after, to match
        % the timing of the rock music envelopes

    % Get the envelope based on the gammatonegram
    disp('Computing the gammatonegram...');
    y = resample(y,Fs/2,Fs); % downsample y to make gammatonegram computation easier
    [env,tmpFs] = gammatoneenv(y,Fs/2); % removed lowpass filteirng from gammatoneenv

    % Set fenv to a max value of +/- 1 V
    env = env/max(abs(env));
    
    % Set all V below voltlim to voltlim
    env(env<voltlim) = voltlim;
    
    % Convert to dB
    env = 20*log10(env);
    
    % Downsample
    env = resample(env,eFs,tmpFs);

    disp('Processing the stimulus...');
    stimset{ii} = env;

    disp(['Completed @ ' num2str(toc(stimtm)) ' s!']);
end
