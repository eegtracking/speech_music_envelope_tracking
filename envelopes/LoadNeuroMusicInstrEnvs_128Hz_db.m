% Load the individual instrument envelopes for the NeuroMusic rock songs

addpath(genpath('~/Projects/EnvelopeExtract/'));
addpath('~/Projects/NeuroMusic/');

eFs = 512;
instrtag = 'bass';

instrpth = '/Volumes/ZStore/NeuroMusic/All Stimuli/SeparateInstruments/';
stimlistpth = '/Volumes/ZStore/NeuroMusic/All Stimuli/Stimuli_Presentation/';
fslst = 'stimlist.txt';

% Load the eeg and stimuli
stimset = loadneuromusicinstrs_db(instrpth,stimlistpth,instrtag,fslst,eFs);
ntr = length(stimset);

% Save the envelopes
svpth = '/Volumes/ZStore/NeuroMusic/';
svnm = sprintf('NeuroMusicdBInstruments_128Hz_%s',instrtag);
save([svpth svnm],'stimset');