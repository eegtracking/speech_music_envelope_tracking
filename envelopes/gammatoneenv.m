function [envelope,eFs] = gammatoneenv(y,Fs)
% Compute the envelope summed across gammatone filters
% Code original written by Adam Bednar

%% Filter setup
% Lowpass filter
Fpass = 8e3;
Fstop = 10e3;
wav_fs=Fs;
Apass = 1;
Astop = 60;
h = fdesign.lowpass(Fpass,Fstop,Apass,Astop,Fs);
lpf1 = design(h,'cheby2','MatchExactly','stopband');
clear Fpass Fstop h;

% GammacHirp filterbank
downsmp = 1; % downsampling factor
GCparam.fs = wav_fs/downsmp;
GCparam.NumCh = 32; % NZ edit (4/26/17), was 121
GCparam.FRange = [100,8e3];
GCparam.OutMidCrct = 'ELC';
% GCparam.OutMidCrct = 'No';
% GCparam.Ctrl = 'dyn';

% Envelope filter
Fpass = 30;
Fstop = 32;
eFs = GCparam.fs;
h = fdesign.lowpass(Fpass,Fstop,Apass,Astop,eFs);
lpf2 = design(h,'cheby2','MatchExactly','stopband');
clear Fpass Fstop h;

%% Envelope extraction 
% Filter below Nyquist frequency
y = filtfilthd(lpf1,y);

% Downsample
y= nt_dsample(y,downsmp);
y=y';

% Bandpass filter
spectrogram = GCFBv210(y,GCparam);

% Calculate narrowband and broadband envelopes
disp('Compute hilbert transform...');
for chn=1:size(spectrogram,1)
    disp(['Channel ' num2str(chn)]);
    spectrogram(chn,:)=abs(hilbert(spectrogram(chn,:)));
end

envelope = mean(spectrogram,1);

% % Filter and downsample envelope
% envelope = filtfilthd(lpf2,envelope');
