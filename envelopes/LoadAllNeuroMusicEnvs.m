% Load all NeuroMusic stimuli of one type

addpath(genpath('~/EnvelopeExtract/'));
addpath('~/SpeechMusicGen');

desFs = 64; % desired sampling rate for reconstruction (Hz)
eFs = 512;
voltlimdb = -80;

stimpth = '/scratch/nzuk/stims/';
fslst = 'stimlist.txt';

% Load the eeg and stimuli
stimset = loadneuromusicstims(stimpth,10:49,fslst,'env',eFs);
ntr = length(stimset);

% Filter the stimuli
% disp('Filtering stimuli...');
% Fpass2 = 30;                 % Upper passband frequency (Hz)
% Fstop2 = 32;                % Upper stopband frequency (Hz)
% Astop  = 60;                % Stopband attenuation (dB)
% Apass  = 1;                 % Passband attenuation (dB)
% l = fdesign.lowpass(Fpass2,Fstop2,Apass,Astop,eFs);
% lpf = design(l,'cheby2','MatchExactly','stopband');
% for ii = 1:ntr,
%    stimset{ii} = filtfilthd(lpf,stimset{ii});
% end
% Note: the envelopes from gammatoneenv, in loadneuromusicstims, are
% already lowpass filtered to 30 Hz

disp(['Thresholding stimulus envelope to ' num2str(voltlimdb) ' dB...']);
voltlim = 10^(voltlimdb/20); % convert limit from dB to V
for ii = 1:ntr,
    if sum(stimset{ii}<voltlim)>0,
        stimset{ii}(stimset{ii}<voltlim) = voltlim; % set to minimum dB V
    end
end

% Convert stimuli to dB
disp('Converting to dB...');
for ii = 1:ntr,
	stimset{ii} = 20*log10(stimset{ii});
end

disp('Downsampling envelope...');
for ii = 1:ntr,
    stimset{ii} = resample(stimset{ii},desFs,eFs);
end

% Reshape the stimulus cell array so each column is a different stimulus
% type (rock,classical,vocals,speech)
stimset = reshape(stimset,[10 4]);

% Save the envelopes
svpth = '~/FltRcnstr_stims/';
svnm = 'NeuroMusicdBEnvelopes';
save([svpth svnm],'stimset','desFs','voltlimdb');