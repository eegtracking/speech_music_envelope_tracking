% Load all NeuroMusic stimuli of one type, and compute the rms of the
% original stimulus, skipping  (in V)

addpath('~/Projects/NeuroMusic/');

tlims = 16; % amount of time to skip from the beginning and end

stimpth = '/Volumes/ZStore/NeuroMusic/All Stimuli/Stimuli_Presentation/';
fslst = 'stimlist.txt';
stimlist = getstimlist(fslst,stimpth);
stimcodes = 10:49;

% Load each stimulus
rms_stim = NaN(length(stimcodes),1);
for s = 1:length(stimcodes)
    [y,Fs] = audioread([stimpth stimlist{stimcodes(s)-9}]);
    y = mean(y,2); % average L and R
    % remove the first and last tlims seconds
    tidx = usetinds(tlims,Fs,size(y,1));
    y = y(tidx);
    % compute rms
    rms_stim(s) = rms(y);
    
    disp(stimlist{stimcodes(s)-9});
end

% Rearrange rms_stim so columns correspond to stimulus types
stimtypes = {'rock','classical','vocals','speech'};
rms_stim = reshape(rms_stim,[10,length(stimtypes)]);