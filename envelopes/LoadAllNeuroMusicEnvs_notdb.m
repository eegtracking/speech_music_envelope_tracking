% Load all NeuroMusic stimuli of one type

addpath(genpath('~/EnvelopeExtract/'));
addpath('~/SpeechMusicGen');

desFs = 64; % desired sampling rate for reconstruction (Hz)
eFs = 512;
voltlimdb = -80;

stimpth = '/scratch/nzuk/stims/';
fslst = 'stimlist.txt';

% Load the eeg and stimuli
stimset = loadneuromusicstims(stimpth,10:49,fslst,'env',eFs);
ntr = length(stimset);

disp('Downsampling envelope...');
for ii = 1:ntr,
    stimset{ii} = resample(stimset{ii},desFs,eFs);
end

% Reshape the stimulus cell array so each column is a different stimulus
% type (rock,classical,vocals,speech)
stimset = reshape(stimset,[10 4]);

% Save the envelopes
svpth = '~/FltRcnstr_stims/';
svnm = 'NeuroMusicEnvelopes';
save([svpth svnm],'stimset','desFs');