function stimset = loadneuromusicstims(stimpth,stimcodes,fslst,eFs,varargin)
% Load the stimuli from the neuromusic dataset for a specific set of
% stimulus codes
% Inputs:
%   - stimpth = path containing stimuli
%   - stimcodes = array of numbers containing stimulus codes to examine
%   - fslst = file in stimpth containing a list of all stimuli, in order of stimulus code
%   - stimtype = type of stimuli ('env'=envelope, 'spec'=spectrogram)
%   - eFs = sampling rate of the stimulus (Hz)
% Outputs:
%   - stimset = structure containing stimuli
% Nate Zuk (3-2019): This converts into dB, for the spline-based
% reconstruction method

totalstimcodes = 10:49;
voltlimdb = -100;

if ~isempty(varargin),
    for n = 2:2:length(varargin)
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Load the list of stimuli
stimlist = getstimlist(fslst,stimpth);

stimset = cell(length(stimcodes),1);
for ii = 1:length(stimcodes) % For each stimulus code...
    stimtm = tic;
    % Load the sound file
    stimcd = find(stimcodes(ii)==totalstimcodes);
    disp(['--> ' stimlist{stimcd}]);
    disp('Loading the stimulus...');
    stimfl = strtrim([stimpth stimlist{stimcd}]);
    [y,Fs] = audioread(stimfl);
    if size(y,2)>1, y = mean(y,2); end % average channels if there are multiple

    % Get the envelope based on the gammatonegram
    disp('Computing the gammatonegram...');
    y = resample(y,Fs/2,Fs); % downsample y to make gammatonegram computation easier
    [env,tmpFs] = gammatoneenv(y,Fs/2);
        % Note: the envelope from gammatoneenv is already lowpass filtered
        % to 30 Hz
%     env = resample(fenv,eFs,tmpFs);

    % Set fenv to a max value of +/- 1 V
    env = env/max(abs(env));
    
    % Downsample
    env = resample(env,eFs,tmpFs);

    disp('Processing the stimulus...');
    stimset{ii} = env;

    disp(['Completed @ ' num2str(toc(stimtm)) ' s!']);
end
