% Compute the durations of all of the stimuli used in NeuroMusic based on
% the envelope (excluding click and 0.5 s)
stimtag = {'rock','classical','vocals','speech'};
desFs = 512; % sampling rate of the envelope

stimpth = '/Volumes/ZStore/NeuroMusic/';

durs = NaN(10,4);
for jj = 1:length(stimtag)
    % load the stimulus envelopes
    stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtag{jj});
    s = load([stimpth stimfl]);
    for ii = 1:length(s.stimset)
        durs(ii,jj) = length(s.stimset{ii})/desFs - 0.5;
    end
end