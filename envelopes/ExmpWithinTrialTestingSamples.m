% Plot a stimulus envelope before and after moving averaging
stimtype = 'speech';
stimidx = 1;
wnd = 500; % trf window size, in ms
Fs = 512;
tlims = 16;

stimpth = '/Volumes/ZStore/NeuroMusic/';
stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',Fs,stimtype);
s = load([stimpth stimfl]);

orig = s.stimset{stimidx};
dly = 0:ceil(wnd/1000*Fs);
mov_rmv = orig - movmean(orig,length(dly));

% randomly sample 1/10th of the datapoints
test_samples = randperm(length(mov_rmv),round(length(mov_rmv)/10));

figure
hold on
t = (0:length(mov_rmv)-1)/Fs;
plot(t,mov_rmv,'k');
plot(t(test_samples),mov_rmv(test_samples),'r.','MarkerSize',14);
% set(gca,'FontSize',16,'XLim',[tlims max(t)-tlims]);
set(gca,'FontSize',16,'XLim',[20 25]);
xlabel('Time (s)');
ylabel('dB V');
title(sprintf('Moving average removed: %s, trial %d, %d ms',stimtype,stimidx,wnd));