% Go through all of the EEG data and tag which stimuli were presented for
% each subject

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR_','KRK','MB',...
    'MT','SL','SOS'};
stimcodes = 10:49; % all stimulus codes

eegpth = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/';
% load the names of all EEG data files in this directory
eegfls = what(eegpth);

stim_presented = zeros(length(stimcodes),length(sbjs));
for s = 1:length(sbjs)
    % find all files containing a prefix with this subject ID
    sbjidx = cellfun(@(x) strcmp(x(1:length(sbjs{s})),sbjs{s}),eegfls.mat);
    sbjfls = eegfls.mat(sbjidx);
    sbj_stimcodes = []; % to collect stimulus codes for this subject
    for ii = 1:length(sbjfls)
        % load the data
        d = load([eegpth sbjfls{ii}]);
        % get the stimulus codes
        sbj_stimcodes = [sbj_stimcodes; d.stim];
        % show that this data has been loaded
        disp(sbjfls{ii});
    end
    % get the unique set of stimulus codes
    sc = unique(sbj_stimcodes);
    % align them with the full set of stimulus codes
    [~,ia] = intersect(stimcodes,sc);
    % tag that these stimuli were presented for this subject
    stim_presented(ia,s) = 1;
end