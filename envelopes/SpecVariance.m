% Plot the variance in the power spectrum for a set of NeuroMusic stimuli
addpath('~/Projects/NeuroMusic/');
addpath('~/Documents/Matlab/shadedErrorBar/');
stimtypes = {'rock','classical','vocals','speech'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
Fs = 512;
wnd = 16; % window size (in s);
overlap = 8; % overlap between windows (in s);
nstim = 10; % number of stimuli
% tlims = 16;
tlims = 0;
% env_xlims = [0 200];
env_xlims = [40 60]; % range of times to plot of the envelopes
env_exmp_idx = [10 10 10 7];

% PW = NaN(wnd*Fs/2+1,nstim,length(stimtypes));
% var_env = NaN(nstim,length(stimtypes));
rms_env = NaN(nstim,length(stimtypes));
PW = NaN(wnd*Fs/2,nstim,length(stimtypes));
stim_examples = cell(length(stimtypes),1);

for jj = 1:length(stimtypes),
    s = load(['/Volumes/ZStore/NeuroMusic/NeuroMusicdBEnvelopes_512Hz_' stimtypes{jj}]);
    for ii = 1:length(s.stimset),
        stim = s.stimset{ii};
%         var_env(ii,jj) = var(s.stimset{ii});
        rms_env(ii,jj) = rms(10.^(s.stimset{ii}/20));
        % Remove moving average associated with largest window
%         stim = stim-movmean(stim,Fs*wnd);
        tidx = usetinds(tlims,Fs,length(stim));
%         stim = zscore(stim(tidx));
        stim = detrend(stim(tidx),0);
%         stim = stim(tidx);
%         stim = 10.^(stim(tidx)/20);
        if ii == env_exmp_idx(jj), stim_examples{jj} = stim; end % save the stimulus envelope for plotting
        [pw,frq] = pwelch(stim,wnd*Fs,overlap*Fs,wnd*Fs,Fs,'power');
%         [pw,frq] = periodogram(stim,[],wnd*Fs,Fs,'power');
%         PW(:,ii,jj) = 10.*log10(pw);
%         PW(:,ii,jj) = 10.*log10(pw(2:end));
        PW(:,ii,jj) = log10(pw(2:end));
%         for n = 1:length(frq)-1
% %             % for each center modulation frequency, get the frequency
% %             % values 1/3 octave around the center, and average their power,
% %             % normalized by the DC power value
%             frq_range_idx = frq>=(frq(n+1)*2^(-1/6)) & frq<=(frq(n+1)*2^(1/6));
% %             frq_range_idx = frq==frq(n+1);
% % %             PW(n,ii,jj) = mean(10.*log10(pw(frq_range_idx)/pw(1)));
%             PW(n,ii,jj) = sqrt(mean(pw(frq_range_idx)/pw(1)));
% % %             PW(n,ii,jj) = sqrt(mean((pw(frq_range_idx)/pw(1))))*frq(n+1)^(1/2);
%         end
%         PW(:,ii,jj) = 10.*log(pw/pw(1)); % modulation depth (PW(f)/PW(0));
%         PW(:,ii,jj) = pw(2:end);
    end
end

% Example envelopes
figure
set(gcf,'Position',[360,15,400,680]);
for jj = 1:length(stimtypes),
    subplot(length(stimtypes),1,jj);
    t = 2+(0:length(stim_examples{jj})-1)/Fs; % time array
    plot(t,stim_examples{jj},'Color',clr{jj},'LineWidth',2);
    set(gca,'FontSize',14,'XLim',env_xlims);
    xlabel('Time (s)');
    ylabel(stimtypes{jj});
end
  
% Compute a bootstrapped distribution of median spectra
nboot = 1000;
boot_mdspec = NaN(length(frq)-1,nboot,length(stimtypes));
for jj = 1:length(stimtypes)
    for n = 1:nboot
        rand_smp = randi(nstim,nstim,1);
        boot_mdspec(:,n,jj) = median(PW(:,rand_smp,jj),2);
    end
end
uq_bootspec = squeeze(quantile(boot_mdspec,0.95,2)); % 95% quantile
lq_bootspec = squeeze(quantile(boot_mdspec,0.05,2)); % 5% quantile
md_spec = squeeze(median(PW,2));
    
% Median
figure
set(gcf,'Position',[360,10,500,685]);
subplot(2,1,1);
hold on
avg_plt = NaN(length(stimtypes),1);
for jj = 1:length(stimtypes),
%     plot(frq(2:end),mean(PW(:,:,jj),2),'Color',clr{jj},'LineWidth',2);
%     plt = shadedErrorBar(frq(2:end),mean(PW(:,:,jj),2),std(PW(:,:,jj),[],2)/sqrt(nstim),...
%         'lineProps',{'Color',clr{jj},'LineWidth',2});
    plt = shadedErrorBar(frq(2:end),md_spec(:,jj),...
        [uq_bootspec(:,jj)-md_spec(:,jj) md_spec(:,jj)-lq_bootspec(:,jj)]',...
        'lineProps',{'Color',clr{jj},'LineWidth',2});
    avg_plt(jj) = plt.mainLine;
    % remove log-linear trend
%     X = [ones(length(frq)-1,1) log10(frq(2:end))];
%     b = X \ mean(PW(2:end,:,jj),2);
%     detrend_PW = mean(PW(2:end,:,jj),2)-X*b;
%     plot(frq,mean(PW(:,:,jj),2),'Color',clr{jj},'LineWidth',2);
    %%% Detrended power spectrum shows peaks more clearly but seems harder
    %%% to interpret -- makes low-frequency peaks not clear
%     plot(frq(2:end),detrend_PW,'Color',clr{jj},'LineWidth',2);
end
set(gca,'FontSize',16,'XScale','log','XLim',[0.1 Fs/2],'XTick',10.^(-1:2),'XTickLabel',10.^(-1:2));
% ytick = -4:2; ytick_exp = cell(length(ytick),1);
% for n = 1:6, ytick_exp{n} = sprintf('10^{%d}',ytick(n)); end
set(gca,'YTick',-4:2,'YTickLabel',10.^(-4:2),'YLim',[-4 2]);
% set(gca,'FontSize',16);
xlabel('Frequency (Hz)');
ylabel('Median power');
% ylabel('Median envelope spectrum');
legend(avg_plt,stimtypes);

% Variance
subplot(2,1,2);
hold on
for jj = 1:length(stimtypes),
%     plot(frq,var(PW(:,:,jj),[],2),'Color',clr{jj},'LineWidth',2);
    plot(frq(2:end),var(PW(:,:,jj),[],2),'Color',clr{jj},'LineWidth',2);
end
set(gca,'FontSize',16,'XScale','log','XLim',[0.1 Fs/2],'XTick',10.^(-1:2));
xlabel('Frequency (Hz)');
ylabel('Variance in power');
% ylabel('Variance in envelope spectrum');
legend(stimtypes);