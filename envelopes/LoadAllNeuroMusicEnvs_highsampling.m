% Load all NeuroMusic stimuli of one type

addpath(genpath('~/Projects/EnvelopeExtract/'));

eFs = 512;

stimpth = '/Volumes/ZStore/NeuroMusic/All Stimuli/Stimuli_Presentation/';
fslst = 'stimlist.txt';

% Load the eeg and stimuli
stimset = loadneuromusicstims(stimpth,10:49,fslst,'env',eFs);
ntr = length(stimset);

% Reshape the stimulus cell array so each column is a different stimulus
% type (rock,classical,vocals,speech)
stimset = reshape(stimset,[10 4]);

% Save the envelopes
svpth = '~/FltRcnstr_stims/';
svnm = 'NeuroMusicdBEnvelopes_raw';
save([svpth svnm],'stimset');