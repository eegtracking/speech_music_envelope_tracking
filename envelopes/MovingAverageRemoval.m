% Plot a stimulus envelope before and after moving averaging
stimtype = 'speech';
stimidx = 1;
wnd = 1000; % trf window size, in ms
Fs = 128;

stimpth = '/Volumes/ZStore/NeuroMusic/';
stimfl = sprintf('NeuroMusicdBEnvelopes_128Hz_%s',stimtype);
s = load([stimpth stimfl]);

orig = s.stimset{stimidx};
dly = 0:ceil(wnd/1000*Fs);
mov_rmv = orig - movmean(orig,length(dly));

t = (0:length(orig)-1)/Fs;
figure
subplot(2,1,1);
plot(t,orig,'k');
set(gca,'FontSize',16,'XLim',[2 max(t)-2]);
xlabel('Time (s)');
ylabel('dB V');
title('Original');

subplot(2,1,2);
plot(t,mov_rmv,'k');
set(gca,'FontSize',16,'XLim',[2 max(t)-2]);
xlabel('Time (s)');
ylabel('dB V');
mv_tle = sprintf('Moving average removed: %d ms',wnd);
title(mv_tle);