% Plot the level distribution of all Natural Speech envelopes
%%% Based on looking at the histograms, I decided to flag "silences" as <
%%% -55 dB V (one rock song had min dB ~-60, so I thought that was a bad
%%% cutoff)

voltlimdb = -100;
voltlim = 10^(voltlimdb/20);

ntr = 20; % number of separate "runs" or trials
stimpth = '/Volumes/Untitled/Natural Speech/Stimuli/Envelopes/';
stimset = cell(ntr,1);
for n = 1:ntr
    stimfl = sprintf('audio%d_128Hz',n);
    s = load([stimpth stimfl]);
    % remove negative values, based on voltlim
    env = s.env;
%     env(env<voltlim) = voltlim;
    % convert to dB, using the real value to account for negative voltages
    stimset{n} = 20*real(log10(env));
end

edges = -100:1:20;
h = NaN(length(edges)-1,10);
md = NaN(10,1);
for ii = 1:length(stimset),
    md(ii) = median(stimset{ii});
    h(:,ii) = histcounts(stimset{ii},edges);
end

figure
cnts = edges(1:end-1)+diff(edges)/2;
bar(cnts,h,1);
set(gca,'FontSize',16,'YScale','log');
xlabel('dB V');
ylabel('# time samples');
title('Natural Speech envelopes');