function [model,t] = mTRFtrain(stim,resp,fs,map,tmin,tmax,lambda,tlims)
%mTRFtrain mTRF Toolbox training function.
%   MODEL = MTRFTRAIN(STIM,RESP,FS,MAP,TMIN,TMAX,LAMBDA) performs ridge
%   regression on the stimulus property STIM and the neural response data
%   RESP to solve for their linear mapping function MODEL. Pass in MAP==1
%   to map in the forward direction or MAP==-1 to map backwards. The
%   sampling frequency FS should be defined in Hertz and the time lags
%   should be set in milliseconds between TMIN and TMAX. Regularisation is
%   controlled by the ridge parameter LAMBDA.
%
%   [...,T] = MTRFTRAIN(...) also returns the vector of time lags T for
%   plotting MODEL and the regression constant C for absorbing any bias
%   when testing MODEL.
%
%   NZ edits (25-1-2019): 
%   -- The design matrix is z-scored before training the
%      model
%   -- Only ridge regression is used, quadratic (or Tikhonov) regression
%      has been removed
%
%   Inputs:
%   stim   - stimulus property (time by features)
%   resp   - neural response data (time by channels)
%   fs     - sampling frequency (Hz)
%   map    - mapping direction (forward==1, backward==-1)
%   tmin   - minimum time lag (ms)
%   tmax   - maximum time lag (ms)
%   lambda - ridge parameter
%
%   Outputs:
%   model  - linear mapping function (MAP==1: feats by lags by chans,
%            MAP==-1: chans by lags by feats)
%   t      - vector of time lags used (ms)
%
%   See README for examples of use.
%
%   See also LAGGEN MTRFTRANSFORM MTRFPREDICT MTRFCROSSVAL
%   MTRFMULTICROSSVAL.

%   References:
%      [1] Lalor EC, Pearlmutter BA, Reilly RB, McDarby G, Foxe JJ (2006)
%          The VESPA: a method for the rapid estimation of a visual evoked
%          potential. NeuroImage 32:1549-1561.
%      [1] Crosse MC, Di Liberto GM, Bednar A, Lalor EC (2015) The
%          multivariate temporal response function (mTRF) toolbox: a MATLAB
%          toolbox for relating neural signals to continuous stimuli. Front
%          Hum Neurosci 10:604.

%   Author: Edmund Lalor, Michael Crosse, Giovanni Di Liberto
%   Lalor Lab, Trinity College Dublin, IRELAND
%   Email: edmundlalor@gmail.com
%   Website: www.lalorlab.net
%   April 2014; Last revision: Jan 8, 2016

% convert stim and resp into cell arrays, if they aren't cells
if ~iscell(stim), stim = {stim}; end
if ~iscell(resp), resp = {resp}; end

% Define x and y
if tmin > tmax
    error('Value of TMIN must be < TMAX')
end
if map == 1
    x = stim;
    y = resp;
elseif map == -1
    x = resp;
    y = stim;
    [tmin,tmax] = deal(tmax,tmin);
else
    error('Value of MAP must be 1 (forward) or -1 (backward)')
end
clear stim resp

% Convert time lags to samples
tmin = floor(tmin/1e3*fs*map);
tmax = ceil(tmax/1e3*fs*map);

dim1 = size(x{1},2)*length(tmin:tmax);
M = eye(dim1,dim1); % just does ridge regression

% Generate lag matrix
% X = lagGen(x,tmin:tmax); % remove standardization step
% X = zscore(X); % zscore the columns of X
disp('Creating the design matrices and standardizing...');
% Create the design matrices
std_tm = tic;
for i = 1:numel(x)
    % Generate lag matrix
    x{i} = lagGen(x{i},tmin:tmax); %%% skip constant term (NZ)
    % Set X and y to the same length
    minlen = min([size(x{i},1) size(y{i},1)]);
    x{i} = x{i}(1:minlen,:);
    y{i} = y{i}(1:minlen,:);
    % Remove time indexes, if specified
    if iscell(tlims), % if tlims is a cell array, it means that specific indexes were supplied
        tinds = tlims{i};
    else
        tinds = usetinds(tlims,fs,minlen);
    end
    x{i} = zscore(x{i}(tinds,:)); % z-score the design matrix X and output matrix Y
    y{i} = zscore(y{i}(tinds,:));
end
fprintf('** Completed z-scoring in %.3f s\n',toc(std_tm));

% Set up regularisation
% dim = size(x,2);
% M = eye(dim,dim);

% Compute the correlation matrices X'X and X'y
[xtx,xty] = compute_linreg_matrices(x,y);

% Calculate model
model = (xtx+lambda*M)^(-1)*xty;

% Format outputs
model = reshape(model,size(x{1},2),length(tmin:tmax),size(y{1},2));
t = (tmin:tmax)/fs*1e3;

end
