function [stims,preds] = matchstims(fullstims,fullpreds,tlims,Fs)
% For cell arrays contain stimuli and predictions, remove empty cells,
% truncate the stimuli to tlims, and standardize the stimuli. This makes it
% easier to compute the fvu and correlation later.
% NZ (2018)

if length(fullstims)~=length(fullpreds),
    error('Input stimulus and prediction cell arrays must be the same length');
end

% Identify non-empty cells
usecells = cellfun(@(x) ~isempty(x),fullstims)&cellfun(@(x) ~isempty(x),fullpreds);
% Remove empty cells
fullstims = fullstims(usecells);
fullpreds = fullpreds(usecells);

stims = cell(sum(usecells),1);
preds = cell(sum(usecells),1);
for tr = 1:sum(usecells),
    % Truncate the stimulus based on tlims
    tidx = usetinds(tlims,Fs,size(fullstims{tr},1));
    if sum(tidx)~=size(fullpreds{tr},1),
        warning('Prediction is not the same length as truncated stimulus, tlims may be incorrect.');
    end
    stims{tr} = zscore(fullstims{tr}(tidx,:)); % standardize the stimulus
    if size(fullpreds{tr},1)==size(fullstims{tr},1), % if the predictions need to be truncated too to match
        preds{tr} = fullpreds{tr}(tidx,:);
    else % otherwise, assume they are already the appropriate length
        preds{tr} = fullpreds{tr};
    end
end