function SplineEnvRcnstr_testing(sbj,stimtype,window_size,varargin)
% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes
% NZ (3-1-2020) -- do PCA on individual data for window size / stimulus
% type, instead of using the general PCs
% NZ (9-1-2020) -- changed desFs to 128 to make it comparable to
% OrigEnvRcnstr for testing
% NZ (15-1-2020) -- stopped removing silences, produces issues when
% creating the null distribution (introduces discontinuities between EEG
% and stim), also perhaps not necessary
% NZ (28-1-2020) -- Andy mentioned that PCA should be applied only to the
% training set.  The testing set can then be transformed using the
% eigenvectors calculated from the training set.

tmin = 0;
tmax = window_size+tmin;
map = -1;
npcs = 128;
desFs = 128;
mdlFs = 1000/(tmax-tmin)*16; % 2x highest frequency of interest in the model
tlims = 16; %%% NZ (15-1-2020) changed to 16 because max window is 16 s
nfolds = 10;
voltlimdb = -55; % NZ (22-3-2019) changed to -55 dB, specified where silences occur
    % see also HistLevel_NeuroMusic.m
do_mov_avg = true;
lambdas = [0 10.^(0:8)];

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
stimpth = '~/NeuroMusic/stims/';
stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtype);
s = load([stimpth stimfl]);
stims = s.stimset;
% Transpose all stimuli to column vectors
for ii = 1:length(stims), stims{ii} = stims{ii}'; end
% clear s

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
    eeg{ii} = resample(eeg{ii},desFs,eFs);
end

% disp('Identify silences in the stimuli...');
% silence = cell(length(stims),1); % to store silent periods
% for ii = 1:length(stims),
%     % store 0 anytime there's silence
%     silence{ii} = ones(length(stims{ii}),1);
%     silence{ii}(stims{ii}<voltlimdb) = 0;
%     stims{ii}(stims{ii}<voltlimdb) = voltlimdb;
% end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims),
%     tidx{ii} = ones(length(silence{ii}),1);
%     if do_mov_avg,
% %         tidx{ii} = silence{ii}; %%% NZ (3-1-2020), silences weren't being removed here before
%     else
    %%% NZ (15-1-2020) -- not removing silences when doing moving average
    tidx{ii} = ones(length(stims{ii}),1); % only skip silences if removing the envelope moving average
%     end
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
% et = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(st));
    eeg{ii} = eeg{ii}-avg;
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        % NZ (15-1-2020), don't remove silences when doing moving average
        s_on = stims{ii};
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii} = s_on;
    end
end
clear avg s_on

% Transform EEG into PCs
% disp('Transform to PCs...');
% allEEG = cell2mat(eeg);
% cf = pca(allEEG); % get the principal components of the EEG
% pc = cell(length(eeg),1);
% for ii = 1:length(eeg)
% %     eeg{ii} = detrend(eeg{ii},0); % center each channel
%     pc{ii} = eeg{ii}*cf; % transform to pcs
%     pc{ii} = zscore(pc{ii}); % normalize to variance of 1
% end
% clear allEEG eeg

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
% NZ (28-1-2020) -- changed to eeg, doing pc later
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,eeg);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(eeg{n},1)); end
    end
end

% Compute the downsampling rate, for the spline transform
ds = desFs/mdlFs;

% Preallocate variables to store model and prediction accuracies
r_cv = cell(length(stims),1);
mse_cv = cell(length(stims),1);
model = cell(length(stims),1);
r_test = NaN(length(stims),1);
mse_test = NaN(length(stims),1);
r_test_noreg = NaN(length(stims),1);
mse_test_noreg = NaN(length(stims),1);
cf = cell(length(stims),1);
optidx = NaN(length(stims),1);
for n = 1:length(stims)
    fprintf('** Testing with trial %d **\n',n);
    tst_tm = tic;
    traintrs = setxor(1:length(stims),n);
    
    disp('Transform to PCs...');
    allEEG = cell2mat(eeg(traintrs));
    cf{n} = pca(allEEG); % get the principal components of the EEG
    pc = cell(length(traintrs),1);
    for ii = 1:length(traintrs)
        pc{ii} = eeg{traintrs(ii)}*cf{n}(:,1:npcs); % transform to pcs
        pc{ii} = zscore(pc{ii}); % normalize to variance of 1
    end
    clear allEEG
    
    %%% NZ (31-1-2020) -- using "tmp" crossval and predict functions, in
    %%% order to test how including model weights at the edge lags (tmin
    %%% and tmax) affects performance
    disp('Computing the envelope reconstruction...');
    disp('(Using spline interpolation...)');
    if mdlFs<desFs,
        [r_cv{n},~,mse_cv{n},model{n}] = mTRFcrossval_spline(stims(traintrs),pc,desFs,...
            ds,map,tmin,tmax,lambdas,tidx(traintrs),'nfolds',nfolds);
%         [r_cv{n},~,mse_cv{n},model{n}] = mTRFcrossval_spline_tmp(stims(traintrs),pc,desFs,...
%             ds,map,tmin,tmax,lambdas,tidx(traintrs),'nfolds',nfolds);
    else
        [r_cv{n},~,mse_cv{n},model{n}] = mTRFcrossval(stims(traintrs),pc,desFs,...
            map,tmin,tmax,lambdas,tidx(traintrs),'nfolds',nfolds);
    end
    disp('-- Testing --');
    % Use the model corresponding to lambda=0
    %%% NZ (31-1-2020) added option of testing using the optimal lambda
    %%% (especially for mdlFs==desFs)
    if mdlFs<desFs,
        optidx(n) = 1;
    else
        optidx(n) = find(mean(r_cv{n})==max(mean(r_cv{n})),1,'first');
    end
    use_model = model{n}(:,optidx(n));
    % transform the testing eeg
    testpc = zscore(eeg{n}*cf{n}(:,1:npcs));
    % test the model
    if mdlFs<desFs,
        [~,r_test(n),~,mse_test(n)] = mTRFpredict_spline(stims{n},testpc,use_model,desFs,ds,map,tmin,tmax,tidx(n));
%         [~,r_test(n),~,mse_test(n)] = mTRFpredict_spline_tmp(stims{n},testpc,use_model,desFs,ds,map,tmin,tmax,tidx(n));
    else
        [~,r_test(n),~,mse_test(n)] = mTRFpredict(stims{n},testpc,use_model,desFs,map,tmin,tmax,tidx(n));
        [~,r_test_noreg(n),~,mse_test_noreg(n)] = mTRFpredict(stims{n},testpc,model{n}(:,1),desFs,map,tmin,tmax,tidx(n));
    end
    fprintf('-- Completed training and testing for trial %d @ %.3f s\n',n,toc(tst_tm));
end

if do_mov_avg, 
    svpth = sprintf('~/NeuroMusic/reg_effects_indivpca/%dms/',round(tmax-tmin));
else
    svpth = sprintf('~/NeuroMusic/reg_effects_indivpca/%dms_noavg/',round(tmax-tmin));
end
svfl = sprintf('%s_%s_%dHz_%dpcs_reg',sbj,stimtype,mdlFs,npcs);
save([svpth svfl],'r_cv','mse_cv','model','desFs','ds','tmin','tmax','tlims',...
     'nfolds','voltlimdb','tidx','lambdas','cf','r_test','mse_test','optidx',...
     'r_test_noreg','mse_test_noreg');
