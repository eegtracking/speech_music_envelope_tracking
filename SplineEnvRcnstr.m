% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes

addpath('~/Projects/EEGanly/PCA_spline_modeling/');
addpath('~/Documents/MATLAB/ellipsoid_fit/');
addpath('~/Projects/mTRF_Zukedits/');

sbj = 'HC';
stimtype = 'classical';
tmin = -200;
tmax = 800;
map = -1;
npcs = 16;
tlims = [1 Inf];
nfolds = 10;

% Load the general PCA transformation
D = electrode_distance_matrix('chanlocs.xyz','~/Projects/EEGanly/');
cf = pca(D);
cf = [ones(size(D,1),1) cf];

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
if strcmp(stimtype,'rock'), stimcolumn = 1;
elseif strcmp(stimtype,'classical'), stimcolumn = 2;
elseif strcmp(stimtype,'vocals'), stimcolumn = 3;
elseif strcmp(stimtype,'speech'), stimcolumn = 4;
else
    error('Unknown stimulus tag');
end
stimpth = '/Volumes/Untitled/NeuroMusic/';
stimfl = 'NeuroMusicEnvelopes_raw';
load([stimpth stimfl]);
stims = stimset(:,stimcolumn);

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
t = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(t));
    eeg{ii} = eeg{ii}-avg;
    savg = movmean(stims{ii},length(t));
    stims{ii} = stims{ii}-savg;
end
clear avg savg

% Transform EEG into PCs
disp('Transform to PCs...');
pc = cell(length(eeg),1);
for ii = 1:length(eeg),
    pc{ii} = eeg{ii}*cf(:,1:npcs+1);
end
clear eeg

% Downsample to EEG and stimulus by 4x, to make it more managable for the
% computer
desFs = 128;
for ii = 1:length(pc),
    pc{ii} = resample(pc{ii},desFs,eFs);
    stims{ii} = resample(stims{ii},desFs,eFs);
end
dest = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);

mdlFs = 32; % 2x highest frequency of interest in the model
ds = desFs/mdlFs;

disp('Computing the envelope reconstruction...');
[r,~,mse,model] = mTRFcrossval_spline(stims,pc,desFs,ds,map,tmin,tmax,0,tlims,'nfolds',nfolds);

disp('Compute a null distribution of reconstruction accuracies...');
niter = 50;
[nullr,nullmse] = mTRFnull(stims,pc,desFs,ds,map,tmin,tmax,0,tlims,'niter',niter,...
    'randomize_idx',true,'size_for_testing',nfolds);

svpth = '~/Projects/NeuroMusic/rcnstr_res/';
svfl = sprintf('%s_%s_cv',sbj,stimtype);
save([svpth svfl],'r','mse','model','desFs','ds','tmin','tmax','npcs','tlims','nullr','nullmse');