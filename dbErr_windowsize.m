% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbj = 'AB';
stimtype = {'rock','classical','vocals','speech'};
clr = {'r','g','c','b'};
wnd = [250 2000];
nfrq = 100; % number of frequency components to include
frqrange = [0.1 50];
tlims = 2;
Fs = 128;
respth = '~/Projects/NeuroMusic/pred_res/';

% Load the stimuli
stimpth = '/Volumes/ZStore/NeuroMusic/';

% Load the moving-average-removed results
dberror = NaN(nfrq,length(wnd),length(stimtype));
for ii = 1:length(wnd),
    for jj = 1:length(stimtype),
        fl = sprintf('%dms/%s_%s',round(wnd(ii)),sbj,stimtype{jj});
        d = load([respth fl]);
        % load the stimuli for this stimulus type
        stimfl = sprintf('NeuroMusicdBEnvelopes_128Hz_%s',stimtype{jj});
        s = load([stimpth stimfl]);
        % transpose the stimuli to column vectors
        for n = 1:length(s.stimset), s.stimset{n} = s.stimset{n}'; end
        % match the stimuli
        [stims,preds] = matchstims(s.stimset(d.useidx),d.pred,tlims,Fs);
        % z-score the stimuli and predictions (in order to make it more
        % comparable to reconstruction accuracy?)
%         for n = 1:length(stims), 
%             stims{n} = zscore(stims{n}); 
%             preds{n} = zscore(preds{n});
%         end
        % compute the residuals
        rsdl = cell(length(stims),1);
        for n = 1:length(stims), rsdl{n} = stims{n}-preds{n}; end
        % compute dB error
        [dberr,frq] = compute_err_db(rsdl,stims,Fs,'nfrqbins',nfrq,...
            'frqrange',frqrange,'plotting',false,'septrials',true);
        dberror(:,ii,jj) = squeeze(mean(dberr,2));
        disp(fl);
    end
end

figure
for ii = 1:length(wnd),
    subplot(length(wnd),1,ii);
    hold on
    for jj = 1:length(stimtype),
        plot(frq,dberror(:,ii,jj),clr{jj},'LineWidth',2);
    end
    plot(frqrange,[0 0],'k--');
    set(gca,'FontSize',16,'XScale','log');
    xlabel('Frequency (Hz)');
    ylabel('Error reduction ratio');
    title(sprintf('%d ms',wnd(ii)));
    legend(stimtype)
end