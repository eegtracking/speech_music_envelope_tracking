% Plot the reconstruction accuracies for, across stimulus types, using
% d-prime (Pearson's r relative to null distribution)

sbj = 'AB';
stimtypes = {'rock','classical','vocals','speech'};
restype = 'db'; % suffix on the end of the file name to stimulus preprocessing used
respth = '~/Projects/NeuroMusic/rcnstr_res_genPCA/';
nfold = 10; % number of CV folds used to get reconstruction accuracy
niter = 50; % number of iterations used to get the null distribution

r = NaN(nfold,length(stimtypes));
nullr = NaN(niter,length(stimtypes));
dprime = NaN(nfold,length(stimtypes));
for ii = 1:length(stimtypes),
    fl = sprintf('%s_%s_%s',sbj,stimtypes{ii},restype);
    d = load([respth fl]);
    r(:,ii) = d.r;
    nullr(:,ii) = d.nullr;
    dprime(:,ii) = (r(:,ii)-mean(nullr(:,ii)))/std(nullr(:,ii));
    disp(fl);
end

% Plot the d-primes
R = reshape(r,[nfold*length(stimtypes) 1]);
lbl = ones(nfold,1)*(1:length(stimtypes));
lbl = reshape(lbl,[nfold*length(stimtypes) 1]);
dot_median_plot(lbl,R);
set(gca,'FontSize',16,'XTickLabel',stimtypes);
ylabel('Reconstruction accuracy (Pearsons r)');

% Plot the d-primes
DP = reshape(dprime,[nfold*length(stimtypes) 1]);
dot_median_plot(lbl,DP);
set(gca,'FontSize',16,'XTickLabel',stimtypes);
ylabel('d-prime of reconstruction accuracy');