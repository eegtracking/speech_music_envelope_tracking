function [max_val,opt_row,opt_col] = get_matrix_max(X)
% Get the maximum value in matrix X as well as the row and column where
% that value is contained

nrows = size(X,1);
ncols = size(X,2);

all_rows_idx = repmat((1:nrows)',[1,ncols]);
all_cols_idx = repmat((1:ncols),[nrows,1]);

% reshape matrices into vectors
RSHP_X = reshape(X,[numel(X) 1]);
ALL_ROWS = reshape(all_rows_idx,[numel(X) 1]);
ALL_COLS = reshape(all_cols_idx,[numel(X) 1]);

% find the maximum value in the matrix x
max_val = max(RSHP_X);
% get the corresponding row and column
opt_idx = find(RSHP_X==max_val);
opt_row = ALL_ROWS(opt_idx);
opt_col = ALL_COLS(opt_idx);