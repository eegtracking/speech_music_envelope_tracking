% Plot z-scored reconstruction accuracies for speech in addition to the new
% z-scored accuracies with music-optimized model hyperparameters (see
% musicstim_opt) and the original accuracies for music using the
% hyperparameters optimized to Natural Speech
% Nate Zuk (2020)

addpath('~/Projects/NeuroMusic/'); % for dot_median_plot.m

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
stimtype = {'rock','classical','vocals','speech'};
clr = {[1 0.5 0],[0 0.8 0],[0.9 0 0.9],[0 0 1]};
wnd = 500;
maxtr = 7;
niter = 50;
respth = '/Volumes/ZStore/NeuroMusic/new_analyses/rcnstr_tbt/';
musoptpth = '../musicstim_tbt_woptparams/';
nperm = 1000*length(wnd)*(length(stimtype)-1); 
    % number of times to permute the data to get a null distribution of median differences

% Load the results using the hyperparameters optimized to Natural Speech
r = NaN(maxtr,length(stimtype),length(sbjs));
nr = NaN(niter,length(stimtype),length(sbjs));
zr = NaN(maxtr,length(stimtype),length(sbjs));
for s = 1:length(sbjs)
    for jj = 1:length(stimtype),
        fl = sprintf('%s_%s_%dms',sbjs{s},stimtype{jj},round(wnd));
        d = load([respth fl]);
        ntr = size(d.r,1);
        r(1:ntr,jj,s) = reshape(d.r,[ntr 1]);
        nr(:,jj,s) = d.nullr;
        zr(1:ntr,jj,s) = (d.r-mean(d.nullr))/std(d.nullr);
        disp(fl)
    end
end

% Load the results using the hyperparameters optimized to each music
% stimulus
r_mus = NaN(maxtr,length(stimtype)-1,length(sbjs));
nr_mus = NaN(niter,length(stimtype)-1,length(sbjs));
zr_mus = NaN(maxtr,length(stimtype)-1,length(sbjs));
for s = 1:length(sbjs)
    for jj = 1:length(stimtype)-1,
        fl = sprintf('%s_%s_%dms',sbjs{s},stimtype{jj},round(wnd));
        d = load([musoptpth fl]);
        ntr = size(d.r,1);
        r_mus(1:ntr,jj,s) = reshape(d.r,[ntr 1]);
        nr_mus(:,jj,s) = d.nullr;
        zr_mus(1:ntr,jj,s) = (d.r-mean(d.nullr))/std(d.nullr);
        disp(fl)
    end
end

% Plot dots for zr for each subject and trial. On the left, plot speech,
% optimized to Natural Speech. On the right, plot the music zrs at
% individual x ticks, with Natural Speech optimized zr on the left of the
% tick, and music optimized zr on the right of the tick.
stim_lbl = repmat(1:4,[maxtr*length(sbjs) 1]);
LBL = reshape(stim_lbl,[maxtr*length(sbjs)*4 1]);
perm_zr = permute(zr,[1 3 2]);
ZR = reshape(perm_zr,[maxtr*length(sbjs)*4 1]);
perm_zrmus = permute(zr_mus,[1 3 2]);
ZR_MUS = reshape(perm_zrmus,[maxtr*length(sbjs)*3 1]);
[zr_plt,x_tick] = dot_median_plot(LBL(1:maxtr*length(sbjs)*3),[ZR(1:maxtr*length(sbjs)*3) ZR_MUS]);
hold on
jittered_x_vals = (rand(maxtr*length(sbjs),1)-0.5)*0.2+4;
plot(jittered_x_vals,ZR(LBL==4),'.','Color',clr{4},'MarkerSize',14);
plot([-0.2 0.2]+4,ones(1,2)*median(ZR(LBL==4),'omitnan'),...
    'Color',clr{4},'LineWidth',4);
set(gca,'XTick',1:4,'XTickLabels',stimtype);
ylabel('Z-scored reconstruction accuracy');

% Test if zr is signficantly different between speech (Natural Speech
% optimized) and the other music stimuli (music optimized)
p_mopt = NaN(length(stimtype)-1,1);
st_mopt = cell(length(stimtype)-1,1);
for ii = 1:length(stimtype)-1
    [p_mopt(ii),~,st_mopt{ii}] = ranksum(ZR_MUS(LBL==ii),ZR(LBL==4));
end