% Plot the difference in PCA/spline reconstruction accuracy vs
% regularization reconstruction accuracy as a function of the model Fs
% Run on Natural Speech dataset

wnd = 500; % TRF window size (in ms)
stimtype = 'classical';
mdlfs = [8 16 32 64 128]; % model sampling frequencies (in Hz)
npcs = [8 16 32 64 128];
npcs_colors = {'b','c','m','r','g'};
sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR_','KRK','MB',...
    'MT','SL','SOS'};
Fs = 512;
nchan = 128;
max_trs = 7; % maximum number of trials

idx_dly = 0:wnd/1000*Fs;
nparams = length(idx_dly)*nchan;

respth = '../res/';

% load the reconstruction accuracies for each spline model
r_spl = NaN(max_trs,length(mdlfs),length(npcs),length(sbjs));
for s = 1:length(sbjs)
    % reconstruction accuracy for PCA spline
    for ii = 1:length(mdlfs)
        for jj = 1:length(npcs)
            splfl = sprintf('%s_%s_%dHz_%dpcs',sbjs{s},stimtype,mdlfs(ii),npcs(jj));
            spl = load([respth splfl]);
            ntr = length(spl.r_test);
            r_spl(1:ntr,ii,jj,s) = spl.r_test;
        end
    end
    disp(['Subject' num2str(sbjs{s})]);
end

% Compute the percent change in reconstruction accuracy
RSPL = reshape(permute(r_spl,[1 4 2 3]),[max_trs*length(sbjs) length(mdlfs) length(npcs)]);

% Plot the original PCA/spline reconstruction accuracies
figure
set(gcf,'Position',[380,220,450,350])
hold on
rspl_pl = NaN(length(npcs),1);
npcs_leg = cell(length(npcs),1);
for jj = 1:length(npcs)
% plot(1:length(mdlfs),RSPL,'r');
    mspl = median(RSPL(:,:,jj),'omitnan');
    uqspl = quantile(RSPL(:,:,jj),0.75);
    lqspl = quantile(RSPL(:,:,jj),0.25);
    rspl_pl(jj) = errorbar(1:length(mdlfs),mspl,uqspl-mspl,mspl-lqspl,'LineWidth',2,'Color',npcs_colors{jj});
    npcs_leg{jj} = sprintf('%d PCs',npcs(jj));
end
set(gca,'FontSize',16,'XTick',1:length(mdlfs),'XTickLabel',mdlfs);
xlabel('Sampling frequency of splines (Hz)');
ylabel('PCA/spline reconstruction accuracy');
legend(rspl_pl,npcs_leg);

% Plot the median values of RSPL as an image
mRSPL = squeeze(median(RSPL,1,'omitnan'));
figure
imagesc(1:length(mdlfs),1:length(npcs),mRSPL');
colorbar
set(gca,'FontSize',14,'XTick',1:length(mdlfs),'XTickLabel',mdlfs,...
    'YTick',1:length(npcs),'YTickLabel',npcs);
xlabel('Sampling frequency of splines (Hz)');
ylabel('# principal components');
[max_rspl,opt_mdlfs_idx,opt_npcs_idx] = get_matrix_max(mRSPL);
fprintf('Optimum spline sampling = %d Hz\n', mdlfs(opt_mdlfs_idx));
fprintf('Optimum # PCs = %d\n', npcs(opt_npcs_idx));

% [p_kw,~,st_kw] = kruskalwallis(RSPL);
% Run kruskal wallis test on all npcs-nspline pairs, but adjust values
% relative to 128 PCs and 128 Hz sampling
% adjRSPL = reshape(RSPL,[max_trs*length(sbjs) length(mdlfs)*length(npcs)]);
% adjRSPL = adjRSPL-adjRSPL(:,end)*ones(1,length(mdlfs)*length(npcs));
% % get a label for each column of adjRSPL
% adjRSPL_cols = cell(length(mdlfs)*length(npcs),1);
% for jj = 1:length(npcs)
%     for ii = 1:length(mdlfs)
%         idx = (jj-1)*length(mdlfs)+ii;
%         adjRSPL_cols{idx} = sprintf('%d Hz, %d pcs',mdlfs(ii),npcs(jj));
%     end
% end
% [p_kw,~,st_kw] = kruskalwallis(adjRSPL);
% cmp = multcompare(st_kw,'ctype','dunn-sidak','display','off');