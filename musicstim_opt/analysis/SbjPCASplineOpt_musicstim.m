% Plot the difference in PCA/spline reconstruction accuracy vs
% regularization reconstruction accuracy as a function of the model Fs
% using the Natural Speech dataset

stimtype = 'vocals';
wnd = 500;
mdlfs = [8 16 32 64 128]; % model sampling frequencies (in Hz)
npcs = [8 16 32 64 128];
mdlfs_colors = {'b','c','m','r','g'};
npcs_colors = {'b','c','m','r','g'};
sbj = 'AB';
% Fs = 512;
Fs = 128;
nchan = 128;
lambdas = 10.^(0:8);
ntr = 7;

idx_dly = -(-wnd/1000*Fs:0);
nparams = length(idx_dly)*nchan;

respth = '../res/';

% load the reconstruction accuracies for each spline model
r_spl = NaN(ntr,length(mdlfs),length(npcs));
for ii = 1:length(mdlfs)
    for jj = 1:length(npcs)
        splfl = sprintf('%s_%s_%dHz_%dpcs',sbj,stimtype,mdlfs(ii),npcs(jj));
        spl = load([respth splfl]);
        r_spl(:,ii,jj) = spl.r_test;
    %     r_spl(:,ii) = spl.mse_test;
        disp(splfl);
    end
end

% Plot reconstruction accuracies
figure
hold on
r_leg = NaN(length(npcs),1);
r_tag = cell(length(npcs),1);
for jj = 1:length(npcs)
    pl = plot(1:length(mdlfs),r_spl(:,:,jj),'Color',npcs_colors{jj});
    r_leg(jj) = pl(1);
    r_tag{jj} = sprintf('%d PCs',npcs(jj));
end
set(gca,'FontSize',16,'XTick',1:length(mdlfs),'XTickLabel',mdlfs);
xlabel('Sampling frequency of splines (Hz)');
ylabel('Reconstruction accuracy (Pearson''s r)');
title(sbj);
legend(r_leg,r_tag);

% Plot average reconstruction accuracies in an image
figure
imagesc(1:length(mdlfs),1:length(npcs),squeeze(mean(r_spl,1)));
colorbar;
set(gca,'FontSize',14,'XTick',1:length(mdlfs),'XTickLabel',mdlfs,...
    'YTick',1:length(npcs),'YTickLabel',npcs);
xlabel('Sampling frequency of splines (Hz)');
ylabel('# principal components');
title(sbj);