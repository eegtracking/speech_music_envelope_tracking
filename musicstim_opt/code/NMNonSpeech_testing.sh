#!/bin/bash
#SBATCH --time=08:00:00
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --mem=64GB

sbj=$1
stim=$2
mdlfs_idx=$SLURM_ARRAY_TASK_ID
npcs=$3

all_mdlfs=(8 16 32 64 128)
mdlfs=${all_mdlfs[$mdlfs_idx]}

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(8); sbj='${sbj}'; stimtype='${stim}'; SplineRcnstr_nonspeech_testing(sbj,stimtype,${npcs},${mdlfs});"
rm -rf /scratch/$USER/$SLURM_JOB_ID
