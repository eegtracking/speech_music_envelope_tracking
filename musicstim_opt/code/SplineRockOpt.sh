#!/bin/bash
#SBATCH --time=24:00:00
#SBATCH -c 4
#SBATCH -n 1
#SBATCH --mem=16GB

sbj=$1

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(8); sbj='${sbj}'; SplineEnvRcnstr_rockopt(sbj);"
rm -rf /scratch/$USER/$SLURM_JOB_ID
