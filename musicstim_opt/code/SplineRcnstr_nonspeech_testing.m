function SplineRcnstr_nonspeech_testing(sbj,stimtype,npcs,mdlFs,varargin)
% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes
% NZ (3-1-2020) -- do PCA on individual data for window size / stimulus
% type, instead of using the general PCs
% NZ (9-1-2020) -- changed desFs to 128 to make it comparable to
% OrigEnvRcnstr for testing
% NZ (15-1-2020) -- stopped removing silences, produces issues when
% creating the null distribution (introduces discontinuities between EEG
% and stim), also perhaps not necessary
% NZ (28-1-2020) -- Andy mentioned that PCA should be applied only to the
% training set.  The testing set can then be transformed using the
% eigenvectors calculated from the training set.
% Nz (6-10-2020) -- This function was edited to test if a different optimal
% # PCs and splines improves reconstruction accuracy for music stimuli
% using a 500 ms window. The model is not saved.
% Stick with the original 512 Hz sampling rate, so for mdlFs = 128 Hz, no
% regularization is applied (correlation values use same number of samples
% as those from rest of analyses on this dataset

window_size = 500;
tmin = 0;
tmax = window_size+tmin;
map = -1;
desFs = 512; % (6-10-2020) changed to 512 Hz
tlims = 16; %%% NZ (15-1-2020) changed to 16 because max window is 16 s
nfolds = 10;
voltlimdb = -55; % NZ (22-3-2019) changed to -55 dB, specified where silences occur
    % see also HistLevel_NeuroMusic.m
do_mov_avg = true;

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
stimpth = '~/NeuroMusic/stims/';
stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtype);
s = load([stimpth stimfl]);
stims = s.stimset;
% Transpose all stimuli to column vectors
for ii = 1:length(stims), stims{ii} = stims{ii}'; end
% clear s

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
    eeg{ii} = resample(eeg{ii},desFs,eFs);
end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims),
    tidx{ii} = ones(length(stims{ii}),1); 
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
% et = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(st));
    eeg{ii} = eeg{ii}-avg;
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        % NZ (15-1-2020), don't remove silences when doing moving average
        s_on = stims{ii};
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii} = s_on;
    end
end
clear avg s_on

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
% NZ (28-1-2020) -- changed to eeg, doing pc later
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,eeg);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(eeg{n},1)); end
    end
end

% Compute the downsampling rate, for the spline transform
ds = desFs/mdlFs;

% Preallocate variables to store model and prediction accuracies
r_test = NaN(length(stims),1);
mse_test = NaN(length(stims),1);
for n = 1:length(stims)
    fprintf('** Testing with trial %d **\n',n);
    tst_tm = tic;
    traintrs = setxor(1:length(stims),n);
    
    disp('Transform to PCs...');
    allEEG = cell2mat(eeg(traintrs));
    cf = pca(allEEG); % get the principal components of the EEG
    pc = cell(length(traintrs),1);
    for ii = 1:length(traintrs)
        pc{ii} = eeg{traintrs(ii)}*cf(:,1:npcs); % transform to pcs
        pc{ii} = zscore(pc{ii}); % normalize to variance of 1
    end
    clear allEEG
    
    disp('Computing the envelope reconstruction...');
    disp('(Using spline interpolation...)');
    model = mTRFtrain_spline(stims(traintrs),pc,desFs,ds,map,tmin,tmax,0,tidx(traintrs));
%     [r_cv{n},~,mse_cv{n},model] = mTRFcrossval_spline(stims(traintrs),pc,desFs,...
%             ds,map,tmin,tmax,0,tidx(traintrs),'nfolds',nfolds);
    disp('-- Testing --');
    % Use the model corresponding to lambda=0
    % transform the testing eeg
    testpc = zscore(eeg{n}*cf(:,1:npcs));
    % test the model
    [~,r_test(n),~,mse_test(n)] = mTRFpredict_spline(stims{n},testpc,model,desFs,ds,map,tmin,tmax,tidx(n));
    fprintf('-- Completed training and testing for trial %d @ %.3f s\n',n,toc(tst_tm));
end

svpth = sprintf('~/NeuroMusic/musicstim_opt/%dms/',round(tmax-tmin));
svfl = sprintf('%s_%s_%dHz_%dpcs',sbj,stimtype,mdlFs,npcs);
save([svpth svfl],'desFs','ds','tmin','tmax','tlims',...
     'voltlimdb','r_test','mse_test');
