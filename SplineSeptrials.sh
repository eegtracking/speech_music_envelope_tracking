#!/bin/bash
#SBATCH --time=01:00:00
#SBATCH -c 4
#SBATCH -n 1
#SBATCH --mem=16GB

sbj=$1
stim=$2
wnd=$3
maxpc=$4

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "sbj='${sbj}'; stimtype='${stim}'; wnd=${wnd}; npcs=${maxpc}; SplineEnvRcnstr_septrials(sbj,stimtype,wnd,'npcs',npcs);" -singleCompThread
rm -rf /scratch/$USER/$SLURM_JOB_ID
