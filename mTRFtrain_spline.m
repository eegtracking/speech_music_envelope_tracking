function [model,t] = mTRFtrain_spline(stim,resp,fs,ds,map,tmin,tmax,lambda,tlims,varargin)
%mTRFtrain mTRF Toolbox training function.
%   MODEL = MTRFTRAIN(STIM,RESP,FS,MAP,TMIN,TMAX,LAMBDA) performs ridge
%   regression on the stimulus property STIM and the neural response data
%   RESP to solve for their linear mapping function MODEL. Pass in MAP==1
%   to map in the forward direction or MAP==-1 to map backwards. The
%   sampling frequency FS should be defined in Hertz and the time lags
%   should be set in milliseconds between TMIN and TMAX. Regularisation is
%   controlled by the ridge parameter LAMBDA.
%   NZ (15-3-2019) -- If there are multiple trials, they are concatenated
%   together
%
%   [...,T] = MTRFTRAIN(...) also returns the vector of time lags T for
%   plotting MODEL and the regression constant C for absorbing any bias
%   when testing MODEL.
%
%   NZ edits (25-1-2019): 
%   -- The design matrix is z-scored before training the
%      model
%   -- Only ridge regression is used, quadratic (or Tikhonov) regression
%      has been removed
%
%   Inputs:
%   stim   - stimulus property (time by features)
%   resp   - neural response data (time by channels)
%   fs     - sampling frequency (Hz)
%   ds     - downsampling ratio, for the spline transformation
%   map    - mapping direction (forward==1, backward==-1)
%   tmin   - minimum time lag (ms)
%   tmax   - maximum time lag (ms)
%   lambda - ridge parameter
%
%   Outputs:
%   model  - linear mapping function (MAP==1: feats by lags by chans,
%            MAP==-1: chans by lags by feats)
%   t      - vector of time lags used (ms)
%
%   See README for examples of use.
%
%   See also LAGGEN MTRFTRANSFORM MTRFPREDICT MTRFCROSSVAL
%   MTRFMULTICROSSVAL.

%   References:
%      [1] Lalor EC, Pearlmutter BA, Reilly RB, McDarby G, Foxe JJ (2006)
%          The VESPA: a method for the rapid estimation of a visual evoked
%          potential. NeuroImage 32:1549-1561.
%      [1] Crosse MC, Di Liberto GM, Bednar A, Lalor EC (2015) The
%          multivariate temporal response function (mTRF) toolbox: a MATLAB
%          toolbox for relating neural signals to continuous stimuli. Front
%          Hum Neurosci 10:604.

%   Author: Edmund Lalor, Michael Crosse, Giovanni Di Liberto
%   Lalor Lab, Trinity College Dublin, IRELAND
%   Email: edmundlalor@gmail.com
%   Website: www.lalorlab.net
%   April 2014; Last revision: Jan 8, 2016

verbose = true;

if ~isempty(varargin)
    for n = 2:2:length(varargin)
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Define x and y
if tmin > tmax
    error('Value of TMIN must be < TMAX')
end
if map == 1
    x = stim;
    y = resp;
elseif map == -1
    x = resp;
    y = stim;
    [tmin,tmax] = deal(tmax,tmin);
else
    error('Value of MAP must be 1 (forward) or -1 (backward)')
end
clear stim resp

% Convert time lags to samples
tmin = floor(tmin/1e3*fs*map);
tmax = ceil(tmax/1e3*fs*map);

% Generate lag matrix
if verbose, fprintf('Creating the lag matrices...'); end
mattm = tic;
if ds>1 % if downsampling is necessary (using splines) (NZ, 11-2-2020)
    spline_matrix = spline_transform(tmin:tmax,ds);
else
    spline_matrix = eye(length(tmin:tmax)); % use the identity matrix (no transformation)
end
w_spl = (spline_matrix'*spline_matrix)^(-1)*spline_matrix'; % whitened spline transform

if iscell(x)|iscell(y),
    Xc = cell(length(x),1);
    yc = cell(length(y),1);
    dimx = size(x{1},2); % get the original dimension of x
    for ii = 1:length(x),
        yc{ii} = y{ii};
%         Xc{ii} = [];
        catx = [];
%         tmpx = lagGen(x{ii},tmin:tmax);
        % Convert the lag matrix to spline
        for jj = 1:dimx,
%             idx = jj:dimx:size(tmpx,2); % get the indexes of the correct rows
            % compute the spline transform
            tmpx = lagGen(x{ii}(:,jj),tmin:tmax); %%% skip constant term (NZ)
            splx = w_spl*tmpx';
%             Xc{ii} = [Xc{ii} splx'];
            catx = [catx splx'];
        end
        Xc{ii} = catx;
        % Set X and y to the same length
        minlen = min([size(Xc{ii},1) size(yc{ii},1)]);
        Xc{ii} = Xc{ii}(1:minlen,:);
        yc{ii} = yc{ii}(1:minlen,:);
        % Remove time indexes, if specified
        if iscell(tlims),
            tinds = tlims{ii};
        else
            tinds = usetinds(tlims,fs,minlen);
        end
        Xc{ii} = zscore(Xc{ii}(tinds,:)); % zscore the columns of X
        yc{ii} = zscore(yc{ii}(tinds,:)); % zscore the output matrix Y
    end
%     X = cell2mat(Xc); % concatenate the arrays together
%     y = cell2mat(yc);
    X = Xc; y = yc;
    clear Xc yc
else
%     tmpx = lagGen(x,tmin:tmax); % remove standardization step
    dimx = size(x,2);
    % Convert the lag matrix to spline
    X = [];
    for jj = 1:dimx,
%         idx = jj:dimx:size(tmpx,2); % get the indexes of the correct rows
        % compute the spline transform
        tmpx = lagGen(x(:,jj),tmin:tmax); %%% skip constant term (NZ)
        splx = w_spl*tmpx';
        X = [X splx];
    end
    % Set X and y to the same length
    minlen = min([size(X,1) size(y,1)]);
    X = X(1:minlen,:);
    y = y(1:minlen,:);
    % Remove time indexes, if specified
    tinds = usetinds(tlims,fs,minlen);
    X = zscore(X(tinds,:)); % zscore the columns of X
    y = zscore(y(tinds,:)); % zscore the output matrix Y
end
if verbose, fprintf('Completed @ %.3f s\n',toc(mattm)); end
clear x tmpx

% Set up regularisation
dim = size(X,2);
M = eye(dim,dim);

% Compute the correlation matrices X'X and X'y
[xtx,xty] = compute_linreg_matrices(X,y,[],[],'verbose',verbose);

% Calculate model
if verbose, disp('Computing the model and transforming back to delay...'); end
dim2 = size(y,2);
nspl = size(spline_matrix,2); % # of splines used
ntm = length(tmin:tmax); % # of delays originally
model = NaN(dimx*ntm,dim2);
spl_model = (xtx+lambda*M)^(-1)*xty;
% Transform the model back to time
for jj = 1:dimx,
    idx = (jj-1)*nspl+1:jj*nspl; % get the indexes of the correct rows
    tidx = (jj-1)*ntm+1:jj*ntm;
    model(tidx,:) = spline_matrix*spl_model(idx,:);
end

% Format outputs
t = (tmin:tmax)/fs*1e3;

end
