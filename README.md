# Speech_Music_Envelope_Tracking

Using frequency-band-limited EEG-based envelope reconstruction, compare neural tracking of speech and music across a wide range of frequencies. Envelope reconstruction is done by transforming the EEG into principal components, retaining a set number of components (64 were determined to be optimal) and transforming the time-delayed EEG design matrix into a set of cubic basis splines.  In order to restrict envelope reconstruction to frequencies within the range of the model size, the moving average of both the stimulus envelope and EEG using a window size equal to the maximum delay in the model were removed prior to modeling.

To cite this code, please reference the following manuscript (this also has more information on the project):

> Zuk, NJ, Murphy, JW, Reilly, RB, Lalor, EC (preprint). "Envelope reconstruction of speech and music highlights unique tracking of speech at low frequencies", bioRxiv.

I have provided the code from this project as is at the submission of the manuscript. I went through *many* variants of the modeling over the course of the project, and each new variant had its own program. All of that code is contained here. However, for the project, these were the programs I used for modeling the envelope in the main results:

`SplineEnvRcnstr_trialbytrial.m` -- doing a leave-one-trial-out procedure to fit the model and compute reconstruction accuracy on each trial, either for one stimulus type or all stimuli
`OrigEnvRcnstr_testing.m` -- Runs the "original" method of reconstruction (aka. linear modeling with regularization) on data from the current dataset; this was used to generate Figure 7 in the manuscript.

`SplineEnvRcnstr_withinstim.m` -- does a "within trial" variant of the modeling procedure by iteratively leaving out 1/10th of the data randomly selected within a trial and training on the rest of the trial. This was used to create the plot Figure S5.

The programs in the `analysis` directory were used to load the envelope reconstruction results and models and produce the plots shown in the manuscript.

The programs in the `OldManAnalysis` were used to optimize the model on the Natural Speech dataset ([Broderick et al, 2018][1], which used "Old Man and the Sea" as the audiobook :P) and to compute reconstruction accuracy across frequency for Figure S2.

The programs in `rock_instruments` analyze the reconstructions for individual rock instruments and examine the frequency content of the drums reconstructions (Figure 6). This directory also contains code where I examined the frequency content of the classical envelope reconstructions, but the musical beat was too irregular in the classical music and the tempo too variable to get meaningful results.

The programs in `musicstim_opt` were used to identify the optimal model hyperparameters for the music stimuli (Figure S3).

In `model_analysis_NeuroMusic`, the program of note is `SpeechPCASplineFilter.m` which estimates the frequency effects of the modeling procedure using a broadband noise input (see Figure 2d). The other programs contained in this directory are similar to those in `OldManAnalysis`; in an earlier version I had identified optimal hyperparameters based on the speech data in the current dataset, but I abandoned that procedure and optimized the hyperparameters based on Natural Speech instead.

`envelopes` contains code to examine the envelopes in the current dataset, the effects of removing the moving average, and quantify histograms of the dB voltage values for the stimuli across all datasets.
