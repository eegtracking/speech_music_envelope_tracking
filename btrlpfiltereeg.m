function flteeg = btrhpfiltereeg(eeg,Fs,varargin)
% Filter the eeg using an IIR butterworth filter with
% zero-phase filtering.  

F3dB = 0.1; % lower 3dB cutoff frequency
N = 4; % 4 order filter

if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% % Generate bandpass filter
b = fdesign.highpass('N,F3dB',N,F3dB1,F3dB2,Fs);
flt = design(b,'butter');

flteeg = filtfilthd(flt,eeg);