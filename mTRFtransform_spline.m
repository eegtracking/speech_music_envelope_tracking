function [model_t,t] = mTRFtransform_spline(stim,resp,model,fs,ds,map,tmin,tmax,tlims)
%mTRFtransform mTRF Toolbox mapping transformation function.
%   MODEL_T = MTRFTRANSFORM(B_MODEL,RESP,STIM) tansforms the coefficients
%   of the model weights MODEL into transformed model coefficients MODEL_T.
%   NZ (3-2019): Use spline interpolation
%
%   Inputs:
%   stim   - stimulus property (time by features)
%   resp   - neural response data (time by channels)
%   model  - linear mapping function (MAP==1: feats by lags by chans,
%            MAP==-1: chans by lags by feats)
%   fs     - sampling frequency (Hz)
%   map    - transformation direction (forward -> backward==1, backward ->
%            forward==-1)
%   tmin   - minimum time lag (ms)
%   tmax   - maximum time lag (ms)
%   c      - regression constant
%
%   Outputs:
%   model_t - transformed model weights (lags by chans)
%   t       - vector of time lags used (ms)
%   c_t     - transformed model constant
%
%   See README for examples of use.
%
%   See also LAGGEN MTRFTRAIN MTRFPREDICT MTRFCROSSVAL MTRFMULTICROSSVAL.

%   References:
%      [1] Haufe S, Meinecke F, Gorgen K, Dahne S, Haynes JD, Blankertz B,
%          Bie�mann F (2014) On the interpretation of weight vectors of
%          linear models in multivariate neuroimaging. NeuroImage 87:96-110.

%   Authors: Adam Bednar, Emily Teoh, Giovanni Di Liberto, Michael Crosse
%   Lalor Lab, Trinity College Dublin, IRELAND
%   Email: edmundlalor@gmail.com
%   Website: www.lalorlab.net
%   April 2016; Last revision: 15 July 2016

if nargin<9, tlims = []; end

% Define x and y
if tmin > tmax
    error('Value of TMIN must be < TMAX')
end
if map == 1
    x = stim;
    y = resp;
elseif map == -1
    x = resp;
    y = stim;
    [tmin,tmax] = deal(tmax,tmin);
else
    error('Value of MAP must be 1 (forward) or -1 (backward)')
end

if ~iscell(x), x = {x}; end
if ~iscell(y), y = {y}; end

% Convert time lags to samples
tmin = floor(tmin/1e3*fs*map);
tmax = ceil(tmax/1e3*fs*map);

% Generate lag matrix
fprintf('Creating lag matrices...');
lagtm = tic;
dimx = size(x{1},2);
Xc = cell(length(x),1);
yc = cell(length(y),1);
% spline_matrix = spline_transform(tmin:tmax,ds);
% w_spl = (spline_matrix'*spline_matrix)^(-1)*spline_matrix'; % whitened spline transform
if ds>1 % if downsampling is necessary (using splines) (NZ, 11-2-2020)
    spline_matrix = spline_transform(tmin:tmax,ds);
else
    spline_matrix = eye(length(tmin:tmax)); % use the identity matrix (no transformation)
end
w_spl = (spline_matrix'*spline_matrix)^(-1)*spline_matrix'; % whitened spline transform
for ii = 1:length(x),
    % Generate lag matrix
    %%% NZ (3-1-2020), for reducing RAM used, make a lagGen matrix for each
    %%% dimx, spline transform, and then concatenate together
    catx = [];
    for jj = 1:dimx,
        tmpx = lagGen(x{ii}(:,jj),tmin:tmax); %%% skip constant term (NZ)
%         idx = jj:dimx:size(tmpx,2); % get the indexes of the correct rows
        % compute the spline transform
%         [splx,spline_matrix] = spline_transform(tmpx,ds);
        splx = w_spl*tmpx';
        catx = [catx splx'];
    end
    Xc{ii} = catx; % replace with spline-transformed lagGen matrix
    yc{ii} = y{ii};
    % Set X and y to the same length
    minlen = min([size(Xc{ii},1) size(yc{ii},1)]);
    Xc{ii} = Xc{ii}(1:minlen,:);
    yc{ii} = yc{ii}(1:minlen,:);
    % Remove time indexes, if specified
    if iscell(tlims), % if tlims is a cell array, it means that specific indexes were supplied
        tinds = tlims{ii};
    else
        tinds = usetinds(tlims,fs,minlen);
    end
    Xc{ii} = zscore(Xc{ii}(tinds,:)); % zscore the columns of X
    yc{ii} = zscore(yc{ii}(tinds,:)); % zscore the output matrix Y
end
% X = cell2mat(Xc);
% y = cell2mat(yc);
fprintf('Completed @ %.3f s\n',toc(lagtm));

clear x y splx

% Transform the model (which is in delay) into spline
if ds>1,
    disp('Transforming model into splines...');
    ntm = length(tmin:tmax);
    % model = reshape(model,size(model,1)*size(model,2),size(model,3));
    spline_model = [];
    for jj = 1:dimx,
        idx = (jj-1)*ntm+1:jj*ntm; % get the indexes of the correct rows
        % compute the spline transform
%         [splm,spline_matrix] = spline_transform(model(idx,:)',ds);
        splm = (spline_matrix'*spline_matrix)^(-1)*spline_matrix'*model(idx,:);
        spline_model = [spline_model; splm];
    end
    clear splm
else
    spline_model = model;
end

% Transform model weights
fprintf('Transforming model...\n');
transtm = tic;
xtx = compute_linreg_matrices(Xc);
yty = compute_linreg_matrices(yc);
% spline_model_t = (X'*X)*spline_model*inv(y'*y);
% spline_model_t = xtx*spline_model*inv(yty);
fprintf('Completed @ %.3f s\n',toc(transtm));

clear Xc yc

% Transform spline model back to delay
dim2 = size(yty,2);
ntm = length(tmin:tmax); % # of delays originally
if ds>1,
    disp('Transforming back to delay...');
    nspl = size(spline_matrix,2); % # of splines used
    model_t = NaN(dimx*ntm,dim2);
    % Transform the model back to time
    for jj = 1:dimx,
        idx = (jj-1)*nspl+1:jj*nspl; % get the indexes of the correct rows
        tidx = (jj-1)*ntm+1:jj*ntm;
        model_t(tidx,:) = spline_matrix*spline_model_t(idx,:);
    end
else
    model_t = spline_model_t;
end
t = (tmin:tmax)/fs*1e3;

end
