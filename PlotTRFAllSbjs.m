% Plot the topography for a reconstruction model
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));
addpath('~/Documents/MATLAB/shadedErrorBar/');
% addpath('~/Projects/EEGanly/PCA_spline_modeling/');

sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','EQ','HC','JH','JS','KR','KRK','MB','MT','SL','SOS'};
stimtypes = {'rock','classical','vocals','speech'};
% clr = {'r','g','c','b'};
clr = {[1 0 0],[0 0.7 0],[0.7 0 0.7],[0 0 1]};
% wnd = 1150;
desFs = 128;
nchan = 128;
% topo_ranges = [100 800 1500]; % 2000 ms window
% pth_suffix = '';
pth = 'trfs/';
% get the trf delays
tmin = -150; 
tmax = 1000;
dly = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);

model = NaN(length(dly),nchan,length(sbjs),length(stimtypes));
for ii = 1:length(stimtypes),
    for s = 1:length(sbjs),
        fl = sprintf('TRF_%s_%s_%dms',sbjs{s},stimtypes{ii},tmax-tmin);
        d = load([pth fl]);
        model(:,:,s,ii) = d.model;
        disp(fl);
    end
end

% Plot the images of all TRFs, delay x channel
figure
set(gcf,'Position',[720 280 560 420]);
allM = reshape(model,[numel(model),1]);
crange = [quantile(allM,0.01) quantile(allM,0.99)];
% crange = [-1 1];
for ii = 1:length(stimtypes),
    subplot(2,2,ii);
    imagesc(dly/desFs*1000,1:128,mean(model(:,:,:,ii),3)');
    caxis(crange);
    xlabel('Delay (ms)');
    ylabel('Channel');
    title(stimtypes{ii});
    colorbar;
end

figure
set(gcf,'Position',[40 300 550 400]);
stimplts = NaN(length(stimtypes),1);
for ii = 1:length(stimtypes),
    M = mean(model(:,:,:,ii),3);
    plt = shadedErrorBar(dly/desFs*1000,mean(M,2),std(M,[],2),'lineProps',{'Color',clr{ii},'LineWidth',2});
    stimplts(ii) = plt.mainLine;
end
set(gca,'FontSize',16,'XLim',[0 1000]);
xlabel('Delay (ms)');
ylabel('Average TRF weight');
legend(stimplts,stimtypes);

% topo_ranges = (tmax-tmin)*[0 1/4 1/2 3/4];
topo_ranges = [75 125 200 400];
figure
set(gcf,'Position',[600 10 300 700]);
for ii = 1:length(stimtypes),
    for jj = 1:length(topo_ranges)-1;
        subplot(length(stimtypes),length(topo_ranges)-1,...
            (ii-1)*(length(topo_ranges)-1)+jj);
        tidx = dly/desFs*1000>topo_ranges(jj) & dly/desFs*1000<topo_ranges(jj+1);
        topoplot(mean(mean(model(tidx,:,:,ii),3),1),'~/Projects/EEGanly/chanlocs.xyz');
        caxis(crange);
%         title(sprintf('%d - %d ms, %s',round(topo_ranges(jj)),round(topo_ranges(jj+1)),stimtypes{ii}));
    end
end

save('RegularizedTRFModels','sbjs','stimtypes','model','dly','desFs');