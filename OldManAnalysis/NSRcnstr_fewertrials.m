function NSRcnstr_fewertrials(sbj,window_size,varargin)
% Load Natural Speech data and use PCA spline-transform linear regression to
% reconstruct the envelopes (note: Natural Speech was measured at 128 Hz,
% so the 2 smallest windows are not possible to analyze)
% Use only the first 7 trials, to match the amount of data in NeuroMusic
% Nate Zuk (2020)

% addpath('~/mTRF_Zukedits/');

tmin = 0;
tmax = window_size+tmin;
map = -1;
desFs = 128;
mdlFs = 1000/(tmax-tmin)*16; % 3x highest frequency (32 Hz splines was optimal for 
    % 500 ms window to reconstruct dB envelope for Natural Speech)
tlims = 16;
niter = 50;
npcs = 64; % 64 principal components was optimized on the Natural Speech dataset
    % using a 500 ms window to reconstruct dB envelope
ntr = 7;
do_mov_avg = true;

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/scratch/nzuk/Natural Speech/EEG/';
sbjpth = sprintf('%s/',sbj);
eeg = cell(ntr,1);
for n = 1:ntr
    fl = sprintf('%s_Run%d',sbj,n);
    d = load([eegpth sbjpth fl]);
    eeg{n} = d.eegData;
    % remove the average of the mastoids
    eeg{n} = eeg{n}-mean(d.mastoids,2)*ones(1,128);
end

% Load the stimuli
disp('Loading stimuli...');
stimpth = '/scratch/nzuk/Natural Speech/Stimuli/Envelopes/';
stims = cell(ntr,1);
for n = 1:ntr
    sfl = sprintf('audio%d_128Hz',n);
    s = load([stimpth sfl]);
    stims{n} = 20*real(log10(s.env));
        % see note at top (3-2-2020)
end

% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims)
    %%% NZ (19-1-2020), don't skip silences, could have had strange effects
    %%% on null distribution, overenflated tracking at low frequencies
    tidx{ii} = ones(length(stims{ii}),1);
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(st));
    eeg{ii} = eeg{ii}-avg;
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        % NZ (15-1-2020), don't remove silences when doing moving average
        s_on = stims{ii};
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii} = s_on;
    end
end
clear avg s_on

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,eeg);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(eeg{n},1)); end
    end
end

% Compute the downsampling rate, for the spline transform
ds = desFs/mdlFs;

disp('Computing the envelope reconstruction from random samplings of data across trials...');
r = NaN(length(stims),1);
mse = NaN(length(stims),1);
cf = cell(length(stims),1);
for n = 1:length(stims)
    fprintf('** Trial %d/%d **\n',n,length(stims));
    trtm = tic;
    traintrs = setxor(1:length(stims),n); % get the testing trials
    
    disp('Transform to PCs...');
    allEEG = cell2mat(eeg(traintrs));
    cf{n} = pca(allEEG); % get the principal components of the EEG
    pc = cell(length(traintrs),1);
%     npc = size(cf{n},2);
    v = NaN(npcs,length(traintrs)); % to store the original PC variances,
        % for appopriately reweighting the PCs in the model
    for ii = 1:length(traintrs)
    %     eeg{ii} = detrend(eeg{ii},0); % center each channel
        pc{ii} = eeg{traintrs(ii)}*cf{n}(:,1:npcs); % transform to pcs
        v(:,ii) = var(pc{ii}); % get the variance for all components
        pc{ii} = zscore(pc{ii}); % normalize to variance of 1
    end
    %%% don't bother saving the cf and PC variances, not planning on
    %%% analyzing model
    clear allEEG
    
    disp('(Using spline interpolation...)');
    model = mTRFtrain_spline(stims(traintrs),pc,desFs,ds,map,tmin,tmax,0,tidx(traintrs));
    
    % transform the testing trial into pc
    testpc = zscore(eeg{n}*cf{n}(:,1:npcs));
    [~,r(n),~,mse(n)] = mTRFpredict_spline(stims{n},testpc,model,desFs,ds,map,tmin,tmax,tidx(n));

    fprintf('** Completed trial %d/%d @ %.3f s\n',n,length(stims),toc(trtm));
end

[nullr,nullmse] = mTRFnull_pca(stims,eeg,cf,desFs,ds,npcs,map,tmin,tmax,0,tidx,'niter',niter,...
     'randomize_idx',false);

svpth = '~/OldMan/rcnstr_few/';
svfl = sprintf('%s_%dms',sbj,round(tmax-tmin));
save([svpth svfl],'r','mse','mdlFs','desFs','ds','tmin','tmax','tlims',...
    'nullr','nullmse','tidx','npcs');
