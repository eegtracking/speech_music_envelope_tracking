#!/bin/bash
#SBATCH --time=1-12:00:00
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --mem=32GB

sbj=$1
wnd=$2
mdlfs=(8 16 32 64 128)
npcs=$3

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(8); sbj='${sbj}'; wnd=${wnd}; mdlfs=${mdlfs[$SLURM_ARRAY_TASK_ID]}; OldManSplineRcnstr_testing(sbj,wnd,'mdlFs',mdlfs,'npcs',${npcs});"
rm -rf /scratch/$USER/$SLURM_JOB_ID
