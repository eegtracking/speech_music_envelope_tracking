% Plot the difference in PCA/spline reconstruction accuracy vs
% regularization reconstruction accuracy as a function of the model Fs
% Run on Natural Speech dataset

wnd = 500; % TRF window size (in ms)
mdlfs = [8 16 32 64 128]; % model sampling frequencies (in Hz)
npcs = [8 16 32 64 128];
npcs_colors = {'b','c','m','r','g'};
sbjs = 1:19;
% sbjs = [1:9 11:19];
Fs = 128;
nchan = 128;
max_trs = 20; % maximum number of trials

idx_dly = 0:wnd/1000*Fs;
nparams = length(idx_dly)*nchan;

anlypth = '/Volumes/ZStore/Natural_Speech_analyses/';
respth = 'reg_effects_indivpca/';
wndpth = sprintf('%dms/',wnd);

% load the reconstruction accuracies for the regularization-based model
origpth = 'orig_rcnstr_res/';
origwnd = sprintf('%dms/',wnd);

% load the reconstruction accuracies for each spline model
r_spl = NaN(max_trs,length(mdlfs),length(npcs),length(sbjs));
r_orig = NaN(max_trs,length(sbjs));
for s = 1:length(sbjs)
    % get the reconstruction accuracy for regularized model
    origfl = sprintf('Subject%d_db',sbjs(s));
    orig = load([anlypth origpth origwnd origfl]);
    ntr = length(orig.r_test);
%     r_orig(1:ntr,s) = orig.r_test;
    r_orig(1:ntr,s) = orig.mse_test;
    % reconstruction accuracy for PCA spline
    for ii = 1:length(mdlfs)
        for jj = 1:length(npcs)
            splfl = sprintf('Subject%d_%dHz_%dpcs_reg',sbjs(s),mdlfs(ii),npcs(jj));
            spl = load([anlypth respth wndpth splfl]);
            if mdlfs(ii)==128 % use the no regularization r values
%                 r_spl(1:ntr,ii,jj) = spl.r_test_noreg; % no regularization is definitely worse
                r_spl(1:ntr,ii,jj,s) = spl.r_test;
%                 r_spl(1:ntr,ii,jj,s) = spl.mse_test;
            else
                r_spl(1:ntr,ii,jj,s) = spl.r_test;
%                 r_spl(1:ntr,ii,jj,s) = spl.mse_test;
            end
        end
    end
    disp(['Subject' num2str(sbjs(s))]);
end

% Compute the percent change in reconstruction accuracy
RSPL = reshape(permute(r_spl,[1 4 2 3]),[max_trs*length(sbjs) length(mdlfs) length(npcs)]);
RORIG = repmat(reshape(r_orig,[max_trs*length(sbjs) 1]),[1 length(mdlfs) length(npcs)]);
DIFFR = RSPL-RORIG;

% plotting
figure
set(gcf,'Position',[380,220,450,350])
hold on
npcs_leg = cell(length(npcs),1);
diffr_pl = NaN(length(npcs),1);
for jj = 1:length(npcs)
%     plot(1:length(mdlfs),DIFFR(:,:,jj),'k','LineWidth',1);
    mdiff = median(DIFFR(:,:,jj),'omitnan');
    uqdiff = quantile(DIFFR(:,:,jj),0.75);
    lqdiff = quantile(DIFFR(:,:,jj),0.25);
    diffr_pl(jj) = errorbar(1:length(mdlfs),mdiff,uqdiff-mdiff,mdiff-lqdiff,npcs_colors{jj},'LineWidth',2);
    npcs_leg{jj} = sprintf('%d pcs',npcs(jj));
end
plot([1 length(mdlfs)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(mdlfs),'XTickLabel',mdlfs);
xlabel('Sampling frequency of splines (Hz)');
ylabel('Difference in reconstruction accuracy');
legend(diffr_pl,npcs_leg);

% Plot reconstruction accuracy difference as a function of # PCs
figure
set(gcf,'Position',[550,220,450,350])
hold on
mdlfs_leg = cell(length(mdlfs),1);
diffr_pl = NaN(length(npcs),1);
for jj = 1:length(mdlfs)
%     plot(1:length(mdlfs),DIFFR(:,:,jj),'k','LineWidth',1);
    mdiff = squeeze(median(DIFFR(:,jj,:),'omitnan'));
    uqdiff = squeeze(quantile(DIFFR(:,jj,:),0.75));
    lqdiff = squeeze(quantile(DIFFR(:,jj,:),0.25));
    diffr_pl(jj) = errorbar(1:length(mdlfs),mdiff,uqdiff-mdiff,mdiff-lqdiff,npcs_colors{jj},'LineWidth',2);
    mdlfs_leg{jj} = sprintf('%d Hz',mdlfs(jj));
end
plot([1 length(npcs)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(npcs),'XTickLabel',npcs);
xlabel('# principal components');
ylabel('Difference in reconstruction accuracy');
legend(diffr_pl,mdlfs_leg);

p_sr = NaN(length(mdlfs),length(npcs));
st_sr = cell(length(mdlfs),length(npcs));
for ii = 1:length(mdlfs)
    for jj = 1:length(npcs)
        [p_sr(ii,jj),~,st_sr{ii,jj}] = signrank(DIFFR(:,ii,jj));
    end
end

% Plot the original PCA/spline reconstruction accuracies
figure
set(gcf,'Position',[380,220,450,350])
hold on
rspl_pl = NaN(length(npcs),1);
for jj = 1:length(npcs)
% plot(1:length(mdlfs),RSPL,'r');
    mspl = median(RSPL(:,:,jj),'omitnan');
    uqspl = quantile(RSPL(:,:,jj),0.75);
    lqspl = quantile(RSPL(:,:,jj),0.25);
    rspl_pl(jj) = errorbar(1:length(mdlfs),mspl,uqspl-mspl,mspl-lqspl,'LineWidth',2,'Color',npcs_colors{jj});
end
set(gca,'FontSize',16,'XTick',1:length(mdlfs),'XTickLabel',mdlfs);
xlabel('Sampling frequency of splines (Hz)');
ylabel('PCA/spline reconstruction accuracy');
legend(rspl_pl,npcs_leg);

% [p_kw,~,st_kw] = kruskalwallis(RSPL);
% Run kruskal wallis test on all npcs-nspline pairs, but adjust values
% relative to 128 PCs and 128 Hz sampling
adjRSPL = reshape(RSPL,[max_trs*length(sbjs) length(mdlfs)*length(npcs)]);
adjRSPL = adjRSPL-adjRSPL(:,end)*ones(1,length(mdlfs)*length(npcs));
% get a label for each column of adjRSPL
adjRSPL_cols = cell(length(mdlfs)*length(npcs),1);
for jj = 1:length(npcs)
    for ii = 1:length(mdlfs)
        idx = (jj-1)*length(mdlfs)+ii;
        adjRSPL_cols{idx} = sprintf('%d Hz, %d pcs',mdlfs(ii),npcs(jj));
    end
end
[p_kw,~,st_kw] = kruskalwallis(adjRSPL);
cmp = multcompare(st_kw,'ctype','dunn-sidak','display','off');