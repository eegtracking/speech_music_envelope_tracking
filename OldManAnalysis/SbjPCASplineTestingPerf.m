% Plot the difference in PCA/spline reconstruction accuracy vs
% regularization reconstruction accuracy as a function of the model Fs
% using the Natural Speech dataset

wnd = 500; % TRF window size (in ms)
mdlfs = [8 16 32 64 128]; % model sampling frequencies (in Hz)
npcs = [8 16 32 64 128];
mdlfs_colors = {'b','c','m','r','g'};
npcs_colors = {'b','c','m','r','g'};
sbj = 'Subject4';
% Fs = 512;
Fs = 128;
nchan = 128;
lambdas = 10.^(0:8);

idx_dly = -(-wnd/1000*Fs:0);
nparams = length(idx_dly)*nchan;

anlypth = '/Volumes/ZStore/Natural_Speech_analyses/';
respth = 'reg_effects_indivpca/';
wndpth = sprintf('%dms/',wnd);
splwndpth = sprintf('%dms/',wnd);

% load the reconstruction accuracies for the regularization-based model
origpth = 'orig_rcnstr_res/';
origfl = sprintf('%s_db',sbj);
orig = load([anlypth origpth wndpth origfl]);
r_orig = orig.r_test;
% r_orig = orig.mse_test;
r_orig_cv = orig.r_cv;
% r_orig_cv = orig.mse_cv;
MORIG = NaN(size(orig.model{1},1),length(orig.r_test));
for n = 1:length(orig.r_test)
    MORIG(:,n) = orig.model{n}(:,1);
end
mdl_orig = mean(MORIG,2);

% load the reconstruction accuracies for each spline model
r_spl = NaN(size(r_orig,1),length(mdlfs),length(npcs));
r_cv = cell(size(r_orig,1),length(mdlfs),length(npcs));
mdl = NaN(nparams,length(mdlfs),length(npcs));
% cf = NaN(nchan,nchan,length(mdlfs));
for ii = 1:length(mdlfs)
    for jj = 1:length(npcs)
        splfl = sprintf('%s_%dHz_%dpcs_reg',sbj,mdlfs(ii),npcs(jj));
        spl = load([anlypth respth splwndpth splfl]);
        if mdlfs(ii)==128 % use the no regularization r values (the regularized ones are worse?)
%             r_spl(:,ii,jj) = spl.r_test_noreg;
            r_spl(:,ii,jj) = spl.r_test;
%             r_spl(:,ii,jj) = spl.mse_test;
        else
            r_spl(:,ii,jj) = spl.r_test;
%             r_spl(:,ii,jj) = spl.mse_test;
        end
    %     r_spl(:,ii) = spl.mse_test;
        M = NaN(nparams,length(spl.r_test));
        for n = 1:length(spl.r_test)
    %         M(:,n) = spl.model{n}(:,1);
    %         M(:,n) = spl.model{n}(:,1);
            red_mdl = spl.model{n}(:,1);
            M_rshp = reshape(red_mdl,[length(idx_dly) npcs(jj)]);
    %         rM = M_rshp.*(ones(length(idx_dly),1)*spl.var_pc(:,n)');
    %         cM = rM*d.cf{n}';
            cM = M_rshp*spl.cf{n}(:,1:npcs(jj))';
            M(:,n) = reshape(cM,[length(idx_dly)*nchan 1]);
        end
        mdl(:,ii,jj) = mean(M,2);
    %     cf(:,:,ii) = spl.cf;
        r_cv(:,ii,jj) = spl.r_cv;
%         r_cv(:,ii,jj) = spl.mse_cv;
        disp(splfl);
    end
end

% plotting
diffr = r_spl-repmat(r_orig,1,length(mdlfs),length(npcs));
figure
hold on
r_leg = NaN(length(npcs),1);
r_tag = cell(length(npcs),1);
for jj = 1:length(npcs)
%     plot(1:length(mdlfs),diffr(:,:,jj),npcs_colors{jj},'LineWidth',0.5);
%     pl = plot(1:length(mdlfs),mean(diffr(:,:,jj),1),npcs_colors{jj},'LineWidth',3);
    pl = errorbar(1:length(mdlfs),mean(diffr(:,:,jj),1),std(diffr(:,:,jj),[],1),...
       npcs_colors{jj},'LineWidth',3);
    r_leg(jj) = pl(1);
    r_tag{jj} = sprintf('%d pcs',npcs(jj));
end
set(gca,'FontSize',16,'XTick',1:length(mdlfs),'XTickLabel',mdlfs);
xlabel('Sampling frequency of splines (Hz)');
ylabel('Change in reconstruction accuracy using PCA/spline');
title(sbj);
legend(r_leg,r_tag);

% Plot each model
figure
for jj = 1:length(npcs)
    for ii = 1:length(mdlfs)
        plt_idx = (jj-1)*length(mdlfs)+ii;
        subplot(length(npcs),length(mdlfs),plt_idx);
        MDL = reshape(mdl(:,ii,jj),[nparams/nchan nchan]);
    %     imagesc(idx_dly/Fs*1000,1:128,(MDL*cf(:,:,ii)')');
        imagesc(idx_dly/Fs*1000,1:128,MDL');
        colorbar
        tle = sprintf('%d Hz, %d pcs',mdlfs(ii),npcs(jj));
        xlabel('Delay (ms)');
        ylabel('Channel');
        title(tle);
    end
end

figure
origFs = 128;
% orig_dly = 0:wnd/1000*origFs;
orig_dly = -(-wnd/1000*origFs:0);
MDLORIG = reshape(mdl_orig,[nchan length(orig_dly)]);
imagesc(orig_dly/origFs*1000,1:nchan,MDLORIG);
colorbar
title('Regularized, original model');
xlabel('Delay (ms)');
ylabel('Channel');

% Plot reconstruction accuracy for each CV iteration
figure
% set(gcf,'Position',[380,220,450,350])
%hold on
% plot(lambdas,r_orig_cv{n},'k');
for jj = 1:length(npcs)
    pl_cv = NaN(length(mdlfs)+1,1);
    cv_leg = cell(length(mdlfs)+1,1);
    subplot(ceil(length(npcs)/2),2,jj);
    hold on
    for ii = 1:length(mdlfs)
        RSPL = cell2mat(r_cv(:,ii,jj));
        m_cv_spl = median(RSPL(:,2:end));
        uq_cv_spl = quantile(RSPL(:,2:end),0.75);
        lq_cv_spl = quantile(RSPL(:,2:end),0.25);
        pl_cv(ii+1) = errorbar(lambdas,m_cv_spl,uq_cv_spl-m_cv_spl,m_cv_spl-lq_cv_spl,...
            'Color',mdlfs_colors{ii},'LineWidth',2);
        cv_leg{ii+1} = sprintf('%d Hz',mdlfs(ii));
    end
    set(gca,'XScale','log');
    title(sprintf('%d npcs',npcs(jj)));
end
% plot the original in the last plot
RORIG = cell2mat(r_orig_cv); % concatenate all CV rs for original
m_cv_orig = median(RORIG(:,2:end));
uq_cv_orig = quantile(RORIG(:,2:end),0.75);
lq_cv_orig = quantile(RORIG(:,2:end),0.25);
pl_cv(1) = errorbar(lambdas,m_cv_orig,uq_cv_orig-m_cv_orig,m_cv_orig-lq_cv_orig,...
    'LineWidth',2,'Color','k');
cv_leg{1} = 'Regularized';
set(gca,'FontSize',14,'XScale','log');
xlabel('\lambda');
ylabel('CV r');
legend(pl_cv,cv_leg);
% title(sprintf('%d Hz',mdlfs(ii)));