#!/bin/bash
#SBATCH --time=2-00:00:00
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --mem=32GB

sbj=$1
wnd=$2

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(8); sbj='${sbj}'; wnd=${wnd}; OldManOrigRcnstr_testing(sbj,wnd);"
rm -rf /scratch/$USER/$SLURM_JOB_ID
