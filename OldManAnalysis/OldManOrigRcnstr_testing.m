function OldManOrigRcnstr_testing(sbj,window_size,varargin)
% Load Natural Speech data and use typical ridge regression to
% reconstruct the envelopes.  This is done testing on each trial
% separately. This will be compared to the PCA/spline method

tmin = 0;
tmax = window_size+tmin;
map = -1;
desFs = 128; % this should be the sampling rate of the EEG and the stimulus
    % the Natural Speech data is set to 128 Hz
tlims = 16; %%% NZ (15-1-2020) changed to 16 because max window is 16 s
nfolds = 10;
% voltlimdb = -100; % set to -100 dB because this is the value I use to remove
    % negative envelope values in NeuroMusic
    % NZ (3-2-2020) -- envelope artifacts with values < -80 dB V are less
    % pronounced using real(log10(env)) for Natural Speech. This is not as
    % much of an issue for NeuroMusic, since there are no artifacts < -80
    % dB in the envelope. Here I will use real(log10).
do_mov_avg = true;
lambdas = [0 10.^(0:8)]; % note, added "no regularization" here
ntr = 20; % number of trials (look at the number of "runs" for each subject)

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Load the eeg for each run
disp('Loading eeg...');
eegpth = '/scratch/nzuk/Natural Speech/EEG/';
sbjpth = sprintf('%s/',sbj);
eeg = cell(ntr,1);
for n = 1:ntr
    fl = sprintf('%s_Run%d',sbj,n);
    d = load([eegpth sbjpth fl]);
    eeg{n} = d.eegData;
    % remove the average of the mastoids
    eeg{n} = eeg{n}-mean(d.mastoids,2)*ones(1,128);
end

% Load the stimuli
disp('Loading stimuli...');
stimpth = '/scratch/nzuk/Natural Speech/Stimuli/Envelopes/';
stims = cell(ntr,1);
for n = 1:ntr
    sfl = sprintf('audio%d_128Hz',n);
    s = load([stimpth sfl]);
    stims{n} = 20*real(log10(s.env));
        % see note at top (3-2-2020)
end

% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
end

% disp('Convert stimuli to dB');
% silence = cell(length(stims),1); % to store silent periods
% for ii = 1:length(stims),
%     % store 0 anytime there's silence
%     silence{ii} = ones(length(stims{ii}),1);
%     silence{ii}(stims{ii}<voltlimdb) = 0;
%     stims{ii}(stims{ii}<voltlimdb) = voltlimdb;
% end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
%if do_mov_avg, disp('...and removing silences...'); end
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims),
    tidx{ii} = ones(length(stims{ii}),1);
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
% et = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    if do_mov_avg,
        avg = movmean(eeg{ii},length(st));
        eeg{ii} = eeg{ii}-avg;
    else % use moving average of 10 s window (corresponding to highpass filtering at 0.1 Hz)
        avg = movmean(eeg{ii},desFs*10);
        eeg{ii} = eeg{ii}-avg;
    end
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        s_on = stims{ii};
%         s_on(~tidx{ii}) = mean(s_on(tidx{ii})); % set silent periods to the average of
            % the non-silent envelope, to avoid edge effects as speech
            % turns on and off
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
%         stims{ii}(tidx{ii}) = s_on(tidx{ii});
        stims{ii} = s_on;
%         savg = movmean(stims{ii},length(t));
%         stims{ii} = stims{ii}-savg;
    end
end
clear avg s_on

% Z-score each EEG channel
for ii = 1:length(eeg),
%     eeg{ii} = resample(eeg{ii},desFs,eFs);
%     eeg{ii} = detrend(eeg{ii}); % remove linear trends in the EEG
    eeg{ii} = zscore(eeg{ii}); % zscore after resampling, because this can change the variance
end

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,eeg);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(eeg{n},1)); end
    end
end

r_cv = cell(length(stims),1);
mse_cv = cell(length(stims),1);
model = cell(length(stims),1);
r_test = NaN(length(stims),1);
mse_test = NaN(length(stims),1);
optidx = NaN(length(stims),1);
for n = 1:length(stims)
    fprintf('** Testing with trial %d **\n',n);
    tst_tm = tic; % keep track of total training and testing time for this trial
    traintrs = setxor(1:length(stims),n);
    disp('Computing the envelope reconstruction...');
    [r_cv{n},~,mse_cv{n},model{n}] = mTRFcrossval(stims(traintrs),eeg(traintrs),desFs,...
        map,tmin,tmax,lambdas,tidx(traintrs),'nfolds',nfolds);
    disp('-- Testing --');
    % Use the model with the best CV accuracy
    optidx(n) = find(mean(r_cv{n})==max(mean(r_cv{n})),1,'first');  
    use_model = model{n}(:,optidx(n));
    % test the model
    [~,r_test(n),~,mse_test(n)] = mTRFpredict(stims{n},eeg{n},use_model,desFs,map,tmin,tmax,tidx(n));
    fprintf('-- Completed training and testing for trial %d @ %.3f s\n',n,toc(tst_tm));
end

if do_mov_avg, 
    svpth = sprintf('~/OldMan/orig_rcnstr_res/%dms/',round(tmax-tmin));
else
    svpth = sprintf('~/OldMan/orig_rcnstr_res/%dms_noavg/',round(tmax-tmin));
end
svfl = sprintf('%s_db',sbj);
save([svpth svfl],'model','desFs','tmin','tmax','tlims','tidx',...
    'nfolds','lambdas','r_cv','mse_cv','r_test','mse_test','optidx');
