% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject in the Natural Speech dataset

addpath('~/Documents/MATLAB/shadedErrorBar/');
addpath('../analysis/');

sbjs = 1:19; % index for each subject: "Subject1"..."Subject19"
wnd = [125 250 500 1000 2000 4000 8000 16000];
maxtr = 7; % number of trials per subject
niter = 50;
respth = '/Volumes/ZStore/Natural_Speech_analyses/NS_rcnstr_few/';
nperm = 1000*length(wnd); 
    % number of times to permute the data to get a null distribution of median differences

% Load the no-averaging results
r = NaN(maxtr,length(wnd),length(sbjs));
nr = NaN(niter,length(wnd),length(sbjs));
zr = NaN(maxtr,length(wnd),length(sbjs));
for s = 1:length(sbjs)
    for ii = 1:length(wnd),
        fl = sprintf('Subject%d_%dms',sbjs(s),round(wnd(ii)));
        d = load([respth fl]);
        ntr = size(d.r,1);
        r(1:ntr,ii,s) = reshape(d.r,[ntr 1]);
        nr(:,ii,s) = d.nullr;
        zr(1:ntr,ii,s) = (d.r-mean(d.nullr))/std(d.nullr);
        disp(fl)
    end
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*8);
end

% Load the NeuroMusic data, for plotting and stats
NM = load('NeuroMusic_rcnstr');
% reshape zr from the neuromusic data so it's
% trials x subjects x window x stimtype
nm_zr = permute(NM.zr,[1 4 2 3]);
NMZR = reshape(nm_zr,[7*length(NM.sbjs) length(NM.wnd) 4]); % trials x subjects on 1st dim

% Plot zr
% Offset by 2 in x-axis to overlap with plotting from NeuroMusic
figure
set(gcf,'Position',[300,300,550,400]);
hold on
pl = plot((1:length(wnd))+2,squeeze(mean(zr,1,'omitnan')),'Color','b','LineWidth',1);
z_plts(1) = pl(1);
z_plts(2) = plot((1:length(wnd))+2,median(NMZR(:,3:end,4),1,'omitnan'),'Color','b','LineWidth',5);
plot([1 length(wnd)]+2,[0 0],'k--');
set(gca,'FontSize',16,'XTick',(1:length(wnd))+2,'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]+2);
xlabel('Frequency (Hz)');
ylabel('Z-transformed reconstruction accuracy');
legend(z_plts,'Natural Speech','NeuroMusic speech');

% Reshape zr so that we can compute 
rshp_zr = permute(zr,[1 3 2]);

mdzr_plts = plot_medians_boot(rshp_zr,1000,{'b'},(1:length(wnd))+2);
mdzr_plts(2) = plot((1:length(wnd))+2,median(NMZR(:,3:end,4),1,'omitnan'),'Color','b','LineWidth',5);
set(gcf,'Position',[300,300,550,400]);
plot([1 length(wnd)]+2,[0 0],'k');
set(gca,'FontSize',16,'XTick',(1:length(wnd))+2,'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]+2);
xlabel('Frequency (Hz)');
ylabel('Z-r speech rel to other stimuli');
legend(mdzr_plts,'Natural Speech','NeuroMusic speech');

% Test if z-scored reconstruction accuracy is significantly larger than
% 0 for each stimtype/wnd pairing (identifies range of frequencies that
% contain information)
ZR = reshape(permute(zr,[1 3 2]),[maxtr*length(sbjs) length(wnd)]);
p_zr = NaN(length(wnd),1);
st_zr = cell(length(wnd),1);
for w = 1:length(wnd)
    [p_zr(w),~,st_zr{w}] = signrank(ZR(:,w),0,'tail','right');
end

% Test if there is a significant difference between Natural Speech
% reconstruction accuracies and NeuroMusic speech reconstruction accuracies
% at each window
% ns_zr = permute(zr,[1 3 2]); % trials x subjects x window
p_nmcmp = NaN(length(wnd),1);
st_nmcmp = cell(length(wnd),1);
for w = 1:length(wnd)
    % identify the correct window in NeuroMusic
    nm_wnd_idx = NM.wnd==wnd(w);
    [p_nmcmp(w),~,st_nmcmp{w}] = ranksum(ZR(:,w),NMZR(:,nm_wnd_idx,4));
end