#!/bin/bash
#SBATCH --time=02:00:00
#SBATCH -c 8
#SBATCH -n 1
#SBATCH --mem=64GB

sbj=$1
wnd=(125 250 500 1000 2000 4000 8000 16000)

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(8); sbj='${sbj}'; wnd=${wnd[$SLURM_ARRAY_TASK_ID]}; NSRcnstr_fewertrials(sbj,wnd);"
rm -rf /scratch/$USER/$SLURM_JOB_ID
