% Plot z-scored reconstruction accuracy for a specific subject from the
% Natural Speech dataset
% Nate Zuk (2020)

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbj = 'Subject13';
wnd = [125 250 500 1000 2000 4000 8000 16000];
maxtr = 20;
niter = 50;
respth = '/Volumes/ZStore/Natural_Speech_analyses/NS_rcnstr_tbt/';

% Load the no-averaging results
r = NaN(maxtr,length(wnd));
nr = NaN(niter,length(wnd));
zr = NaN(maxtr,length(wnd));
for ii = 1:length(wnd),
    fl = sprintf('%s_%dms',sbj,round(wnd(ii)));
    d = load([respth fl]);
    ntr = size(d.r,1);
    r(:,ii) = reshape(d.r,[ntr 1]);
    nr(:,ii) = d.nullr;
    zr(:,ii) = (d.r-mean(d.nullr))/std(d.nullr);
    disp(fl)
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);

% Label ticks on the x axis with bandwidths
xtick_lbl = cell(length(wnd),1);
for n = 1:length(wnd)
    xtick_lbl{n} = sprintf('%.3g - %.3g',1000/wnd(n),1000/wnd(n)*8);
end

% Plot the results
% reconstruction accuracy
figure
set(gcf,'Position',[145 300 500 400]);
hold on
mrnull = squeeze(mean(nr));
strnull = squeeze(std(nr));
shadedErrorBar(1:length(wnd),mrnull,strnull,'lineProps',{'Color','b','LineWidth',1});
% moving averaged
pl = plot(1:length(wnd),r,'Color','b','LineWidth',1.5);
r_plts = pl(1);
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Reconstruction accuracy');
title(sbj);
legend(r_plts,'speech');

% Plot zr
figure
set(gcf,'Position',[445 300 500 400]);
hold on
plot(1:length(wnd),zr,'Color','b','LineWidth',1);
pl = plot(1:length(wnd),mean(zr,1,'omitnan'),'Color','b','LineWidth',3);
z_plts = pl(1);
plot([1 length(wnd)],[0 0],'k--');
set(gca,'FontSize',16,'XTick',1:length(wnd),'XTickLabel',xtick_lbl,...
    'XDir','reverse','XTickLabelRotation',45,'XLim',[1 length(wnd)]);
xlabel('Frequency (Hz)');
ylabel('Z-transformed reconstruction accuracy');
title(sbj);
legend(z_plts,'speech');