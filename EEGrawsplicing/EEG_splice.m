% Load the EEG file and filter

% Paths to add
addpath('~/eeglab14_0_0b/plugins/Biosig3.1.0/biosig/t250_ArtifactPreProcessingQualityControl/');
addpath('~/eeglab14_0_0b/plugins/Biosig3.1.0/biosig/t200_FileAccess/');
addpath('~/gammatonegram/');

nchan = 128;
mastoidchans = [135 136];

% Load a bdf file and filster it
eegpth = '/scratch/nzuk/bdfs/';
eegfnms = {'EJ_block_1_2_3','EJ_block_4','EJ_block_5','EJ_block_6',...
    'EJ_block_7'};
svpth = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';

ALLEEG = {};
allstimval = [];

for n = 1:length(eegfnms),
    disp(['** ' eegfnms{n} ' **']);
    disp('Loading EEG...');
    % Using sopen now because readbdf is outdated
    hdr = sopen([eegpth eegfnms{n} '.bdf']);
    [fulleeg,hdr] = sread(hdr);
    % Make fulltrig
    fulltrigs = zeros(size(fulleeg,1),1);
    POS = hdr.BDF.Trigger.POS;
    TYP = hdr.BDF.Trigger.TYP;
    for ii = 1:length(TYP)-1,
        if TYP(ii)>0, fulltrigs(POS(ii):POS(ii+1)-1)=TYP(ii); end
    end
    eFs = hdr.SPR; % sampling rate
    sclose(hdr);

    % Remove average of mastoid references
    disp('Removing the mastoid reference...');
    mastref = mean(fulleeg(:,mastoidchans),2);
    fulleeg = fulleeg(:,1:128)-(mastref*ones(1,128));

    % Splice the signal
    disp('Splicing the EEG signal...');
    stimcodes = [10:49 85];
    clicktrig = 128;
    [eeg,trig,stim] = splicebdf(fulleeg,fulltrigs,stimcodes,clicktrig,eFs);
    clear fulleeg
    
    disp('Saving result...');    
    dashind = strfind(eegfnms{1},'_'); % get the subject tag
    sbj = eegfnms{1}(1:dashind-1);
    halftr = round(length(allstimval)/2);
    svfl = sprintf('%s',eegfnms{n});
    save([svpth svfl],'eeg','stim','eFs');
end