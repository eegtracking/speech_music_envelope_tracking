% Plot reconstruction accuracy and d-prime as a function of window size for
% a subject 

addpath('~/Documents/MATLAB/shadedErrorBar/');

sbj = 'AB';
stimtype = {'rock','classical','vocals','speech'};
clr = {'r','g','c','b'};
wnd = [62.5 125 250 500 1000 2000 4000];
wnd_noavg = [250 500 1000 2000 4000 16000];
nfolds = 10;
niter = 50;
respth = '~/Projects/NeuroMusic/rcnstr_res_genPCA/';

% Load the no-averaging results
r = NaN(nfolds,length(wnd),length(stimtype));
nr = NaN(niter,length(wnd),length(stimtype));
zr = NaN(nfolds,length(wnd),length(stimtype));
dp = NaN(length(wnd),length(stimtype));
for ii = 1:length(wnd),
    for jj = 1:length(stimtype),
        fl = sprintf('%dms/%s_%s_db',round(wnd(ii)),sbj,stimtype{jj});
        d = load([respth fl]);
        r(:,ii,jj) = d.r;
        nr(:,ii,jj) = d.nullr;
        zr(:,ii,jj) = (d.r-mean(d.nullr))/std(d.nullr);
        dp(ii,jj) = (mean(d.r)-mean(d.nullr))/sqrt(0.5*(var(d.r)+var(d.nullr)));
        disp(fl)
    end
end

% Load the no-averaging results
r_noavg = NaN(nfolds,length(wnd_noavg),length(stimtype));
nr_noavg = NaN(niter,length(wnd_noavg),length(stimtype));
zr_noavg = NaN(nfolds,length(wnd_noavg),length(stimtype));
dp_noavg = NaN(length(wnd_noavg),length(stimtype));
for ii = 1:length(wnd_noavg),
    for jj = 1:length(stimtype),
        fl = sprintf('%dms_noavg/%s_%s_db',round(wnd_noavg(ii)),sbj,stimtype{jj});
        d = load([respth fl]);
        r_noavg(:,ii,jj) = d.r;
        nr_noavg(:,ii,jj) = d.nullr;
        zr_noavg(:,ii,jj) = (d.r-mean(d.nullr))/std(d.nullr);
        dp_noavg(ii,jj) = (mean(d.r)-mean(d.nullr))/sqrt(0.5*(var(d.r)+var(d.nullr)));
        disp(fl)
    end
end

% Get the center frequency of the bandpass filter implemented by the TRF
cntr = (1000./wnd)*sqrt(8);
cntr_noavg = (1000./wnd_noavg)*sqrt(8);

% Plot the results
% reconstruction accuracy
figure
hold on
for jj = 1:length(stimtype),
    mrnull = squeeze(mean(nr(:,:,jj)));
    strnull = squeeze(std(nr(:,:,jj)));
%     errorbar(cntr,mrnull,strnull,[clr{jj} '--'],'LineWidth',1);
    shadedErrorBar(wnd,mrnull,strnull,'lineProps',{clr{jj},'LineWidth',1});
end
% moving averaged
r_plts = NaN(length(stimtype),1);
for jj = 1:length(stimtype),
    mr = squeeze(mean(r(:,:,jj)));
    str = squeeze(std(r(:,:,jj)));
    r_plts(jj) = errorbar(wnd,mr,str,clr{jj},'LineWidth',2);
end
% % not moving averaged
% for jj = 1:length(stimtype),
%     mr = squeeze(mean(r_noavg(:,:,jj)));
%     str = squeeze(std(r_noavg(:,:,jj)));
%     errorbar(cntr_noavg,mr,str,[clr{jj} '--'],'LineWidth',2);
% end
% moving average null
set(gca,'XScale','log','FontSize',16,'XTick',wnd,'XLim',[min(wnd) max(wnd)]);
xlabel('Window Size (ms)');
ylabel('Reconstruction accuracy');
title(sbj);
legend(r_plts,stimtype);

% d-prime
figure
hold on
% moving averaged
for jj = 1:length(stimtype),
%     mdp = squeeze(mean(dp(:,:,jj)));
%     stdp = squeeze(std(dp(:,:,jj)));
%     errorbar(wnd,mdp,stdp,clr{jj},'LineWidth',2);
    plot(wnd,dp(:,jj),clr{jj},'LineWidth',2);
end
% not moving averaged
for jj = 1:length(stimtype),
%     mdp = squeeze(mean(dp_noavg(:,:,jj)));
%     stdp = squeeze(std(dp_noavg(:,:,jj)));
%     errorbar(wnd_noavg,mdp,stdp,[clr{jj} '--'],'LineWidth',2);
    plot(wnd_noavg,dp_noavg(:,jj),[clr{jj} '--'],'LineWidth',2);
end
set(gca,'XScale','log','FontSize',16,'XTick',unique([wnd wnd_noavg]));
xlabel('Window size (ms)');
ylabel('d-prime');
title(sbj);
legend('rock','classical','vocals','speech');