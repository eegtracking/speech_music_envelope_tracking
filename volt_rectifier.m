function newV = volt_rectifier(oldV,voltlim)
% Use a piecewise rectification funct ion to rectify the
% oldV signal to newV.
% The rectification is:
%   if oldV>10*voltlim: newV = oldV 
%   if oldV<voltlim/10: newV = voltlim if oldV<voltlim/10
%   if voltlim/10<=oldV<=10*voltlim:  
%       (This enforces the constraints that df=0 @ voltlim/2, f=voltlim @
%       voltlim/10, and df=1 @ 10*voltlim)

upper_lim = 10*voltlim;
lower_lim = voltlim/10;

% First, solve the system of equations that gets the cubic function linking
% the two points, voltlim/2 and 2*voltlim
y = [0; 1; voltlim; upper_lim];
X = [1/2*lower_lim^2  lower_lim         1               0;...
    1/2*upper_lim^2   upper_lim         1               0;...
    1/6*lower_lim^3   1/2*lower_lim^2   lower_lim       1;...
    1/6*upper_lim^3   1/2*upper_lim^2   upper_lim       1];
b = X \ y;

newV = oldV;
newV(oldV<lower_lim) = voltlim;
mid_range_idx = oldV>=lower_lim & oldV<=upper_lim;
newV(mid_range_idx) = ...
    (b(1)/6)*oldV(mid_range_idx).^3 + ...
    (b(2)/2)*oldV(mid_range_idx).^2 + ...
    b(3)*oldV(mid_range_idx) + ...
    b(4);