function SplineEnvRcnstr_pcabystim(sbj,stimtype,window_size,varargin)
% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes
% This cannot be used for window_size = 125 ms (because it cant be spline
% transformed) or 16000 ms (because the moving average shouldn't be
% removed)
%%% NZ - PCA-transform each dataset individually, aka. for each particular
%%% window size and stimulus type.  This would be optimal.

% addpath('~/mTRF_Zukedits/');

tmin = 0;
tmax = window_size+tmin;
map = -1;
desFs = 512;
mdlFs = 1000/(tmax-tmin)*8; % 2x highest frequency of interest in the model
    % Optimal model for speech for 500 ms window uses a two octave
    % bandwidth, but it is very slight, and there's not significant
    % difference across bandwidths (NZ, 11-1-2020)
tlims = 16;
nfolds = 10;
nreps = 5; % number of times to repeat mTRFcrossval (use a new resampling of the data)
voltlimdb = -55; % NZ (22-3-2019) changed to -55 dB, specified where silences occur
    % see also HistLevel_NeuroMusic.m
do_mov_avg = true;

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
stimpth = '~/NeuroMusic/stims/';
stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtype);
    %%% I will need to change this to be 512 Hz envelopes, no downsampling
s = load([stimpth stimfl]);
stims = s.stimset;
% Transpose all stimuli to column vectors
for ii = 1:length(stims), stims{ii} = stims{ii}'; end
% clear s

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
    if eFs~=desFs
        eeg{ii} = resample(eeg{ii},desFs,eFs);
    end
end

% disp('Convert stimuli to dB');
% % voltlim = 10^(voltlimdb/20);
% silence = cell(length(stims),1); % to store silent periods
% for ii = 1:length(stims),
%     % store 0 anytime there's silence
%     silence{ii} = ones(length(stims{ii}),1);
%     silence{ii}(stims{ii}<voltlimdb) = 0;
%     stims{ii}(stims{ii}<voltlimdb) = voltlimdb;
% end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims),
    %%% NZ (19-1-2020), don't skip silences, could have had strange effects
    %%% on null distribution, overenflated tracking at low frequencies
    tidx{ii} = ones(length(stims{ii}),1);
%     if do_mov_avg,
%         tidx{ii} = silence{ii}; %%% NZ (3-1-2020), silences weren't being removed here before
%     else
%         tidx{ii} = ones(length(silence{ii}),1); % only skip silences if removing the envelope moving average
%     end
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(st));
    eeg{ii} = eeg{ii}-avg;
%     if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
%         s_on = stims{ii};
%         s_on(~tidx{ii}) = mean(s_on(tidx{ii})); % set silent periods to the average of
%             % the non-silent envelope, to avoid edge effects as speech
%             % turns on and off
%         s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
%             % avoid edge effects of moving average)
%         s_on = s_on-movmean(s_on,length(st));
%         stims{ii}(tidx{ii}) = s_on(tidx{ii});
% %         savg = movmean(stims{ii},length(t));
% %         stims{ii} = stims{ii}-savg;
%     end
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        % NZ (15-1-2020), don't remove silences when doing moving average
        s_on = stims{ii};
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii} = s_on;
    end
end
clear avg s_on

% Transform EEG into PCs
disp('Transform to PCs...');
allEEG = cell2mat(eeg);
cf = pca(allEEG); % get the principal components of the EEG
pc = cell(length(eeg),1);
var_pc = NaN(length(eeg),size(cf,2)); % to store the original PC variances,
    % for appopriately reweighting the PCs in the model
for ii = 1:length(eeg)
%     eeg{ii} = detrend(eeg{ii},0); % center each channel
    pc{ii} = eeg{ii}*cf; % transform to pcs
    var_pc(ii,:) = var(pc{ii});
    pc{ii} = zscore(pc{ii}); % normalize to variance of 1
end
clear allEEG eeg

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,pc);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(pc{n},1)); end
    end
end

% Compute the downsampling rate, for the spline transform
ds = desFs/mdlFs;

disp('Computing the envelope reconstruction from random samplings of data across trials...');
r = NaN(nfolds,nreps);
mse = NaN(nfolds,nreps);
model = cell(nreps,1);
model_t = cell(nreps,1);
for n = 1:nreps
    fprintf('** Rep %d/%d **\n',n,nreps);
    reptm = tic;
    if mdlFs<desFs,
        disp('(Using spline interpolation...)');
        [r(:,n),~,mse(:,n),model{n}] = mTRFcrossval_spline(stims,pc,desFs,ds,map,tmin,tmax,0,tidx,...
            'nfolds',nfolds);
    elseif mdlFs==desFs,
        [r(:,n),~,mse(:,n),model{n}] = mTRFcrossval(stims,pc,desFs,map,tmin,tmax,0,tidx,...
            'nfolds',nfolds);
    else
        error('Model sampling frequency must be equal to or less than initial sampling frequency');
    end

    disp('Compute transformation to forward model...');
    model_t{n} = mTRFtransform_spline(stims,pc,model{n},desFs,ds,map,tmin,tmax,tidx);
    
    fprintf('** Completed rep %d/%d @ %.3f s\n',n,nreps,toc(reptm));
end

% disp('Compute a null distribution of reconstruction accuracies...');
niter = nfolds*nreps;
if mdlFs<desFs,
    [nullr,nullmse] = mTRFnull(stims,pc,desFs,ds,map,tmin,tmax,0,tidx,'niter',niter,...
        'randomize_idx',true,'size_for_testing',nfolds);
else
    [nullr,nullmse] = mTRFnull_standard(stims,pc,desFs,map,tmin,tmax,0,tidx,'niter',niter,...
        'randomize_idx',true,'size_for_testing',nfolds);
end

if do_mov_avg, 
    svpth = sprintf('~/NeuroMusic/rcnstr_res/%dms/',round(tmax-tmin));
else
    svpth = sprintf('~/NeuroMusic/rcnstr_res/%dms_noavg/',round(tmax-tmin));
end
svfl = sprintf('%s_%s_db',sbj,stimtype);
save([svpth svfl],'r','mse','model','mdlFs','desFs','ds','tmin','tmax','tlims',...
    'nullr','nullmse','voltlimdb','tidx','var_pc','model_t','cf');
