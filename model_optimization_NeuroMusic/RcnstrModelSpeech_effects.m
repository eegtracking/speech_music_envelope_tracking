% Load the regularized reconstruction models for speech, and plot the
% average of the models, as well as spectrum after filtering white noise
% Used to make Fig. 1c and d in the paper
% Nate Zuk (2020)

addpath('~/Projects/mTRF-Toolbox/');

%%% Load the reconstruction models for speech, and average across subjects
sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK','MB',...
    'MT','SL','SOS'};
stimtype = 'speech';
orig_pth = '/Volumes/ZStore/NeuroMusic/analyses/orig_rcnstr_res/500ms_noavg/';
chn_to_use = 1:128; % look at channel Fz
Fs = 128; % sampling rate (in Hz)
nchan = 128; % number of channels
tmin = 0; % min and max delays for the model (in ms)
tmax = 500;
plot_dur = 2; % duration of signal to plot (in s)
plot_start = 2; % starting time to plot (in s)

% compute the set of delays
dly = floor(-tmax/1000*Fs):ceil(-tmin/1000*Fs);

MDL = NaN(length(dly),length(sbjs));
for s = 1:length(sbjs)
    % load the results
    fl = sprintf('%s_%s_db',sbjs{s},stimtype);
    d = load([orig_pth fl]);
    % get the optimum model for each trial, and average across trials
    mdl = NaN(size(d.model{1},1),length(d.model));
    for n = 1:length(d.model)
        mdl(:,n) = d.model{n}(:,d.optidx(n)); % use the optimum model based on cross-validation
    end
    mdl = mean(mdl,2); % average across trials
    M = reshape(mdl,[nchan length(dly)]);
    % get a single channel, and save to all-subject array
    MDL(:,s) = mean(M(chn_to_use,:),1)';
    % show the data has been loaded
    disp(fl);
end

% Create a white noise signal
trf = mean(MDL,2)/abs(sum(mean(MDL,2)));
dur = 100; % duration of the signal (s)
s = randn(dur*Fs,1); % take a speech example
% filter with the TRF
X = lagGen(s,dly);
y = X*trf;
clear X;

% normalize to have a sum of 1
figure
set(gcf,'Position',[360 155 325 550]);
subplot(3,1,1); % plot the original signal
t = (0:length(s)-1)/Fs;
idx_to_plot = plot_start*Fs + (1:plot_dur*Fs);
plot(t(idx_to_plot),s(idx_to_plot),'k');
set(gca,'FontSize',14,'XLim',[plot_start plot_start+plot_dur]);
xlabel('Time (s)');
ylabel('Original signal');
% trf
subplot(3,1,2);
plot(-dly/Fs*1000,trf,'k','LineWidth',2);
set(gca,'FontSize',14,'XLim',[tmin tmax]);
xlabel('Delay (ms)');
ylabel('Average weight across EEG channels');
% output signal
subplot(3,1,3);
plot(t(idx_to_plot),y(idx_to_plot),'b');
set(gca,'FontSize',14,'XLim',[plot_start plot_start+plot_dur]);
xlabel('Time (s)');
ylabel('Output signal');

% Plot the pwelch power spectrum
[P,frq_e] = pwelch(s,20*Fs,10*Fs,[],Fs);
[Py,frq_y] = pwelch(y,20*Fs,10*Fs,[],Fs);
figure
hold on
plot(frq_e,10*log10(P),'k','LineWidth',1);
plot(frq_y,10*log10(Py),'b','LineWidth',1.5);
wnd = 1000/(tmax-tmin);
plot([wnd wnd],[-60 0],'k--');
set(gca,'FontSize',14,'XScale','log');
xlabel('Frequency (Hz)');
ylabel('Power (dB)');
legend('Original','TRF filtered','Width of TRF');