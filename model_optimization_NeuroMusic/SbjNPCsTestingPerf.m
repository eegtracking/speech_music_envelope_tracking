% Plot the difference in PCA/spline reconstruction accuracy vs
% regularization reconstruction accuracy as a function of the model Fs

wnd = 500; % TRF window size (in ms)
mdlfs = 16; % model sampling frequencies (in Hz)
npcs = [8 16 32 64 128];
npcs_colors = {'r','m','g','b','c'};
sbj = 'AB';
% Fs = 512;
Fs = 128;
nchan = 128;
lambdas = 10.^(0:8);

idx_dly = -(-wnd/1000*Fs:0);
nparams = length(idx_dly)*nchan;

respth = '../reg_effects_indivpca/';
% respth = '../reg_effects_testing_highsr/';
wndpth = sprintf('%dms/',wnd);
splwndpth = sprintf('%dms_justsplines/',wnd);

% load the reconstruction accuracies for the regularization-based model
origpth = '../orig_rcnstr_res/';
origfl = sprintf('%s_speech_db',sbj);
orig = load([origpth wndpth origfl]);
r_orig = orig.r_test;
% r_orig = orig.mse_test;
r_orig_cv = orig.r_cv;
% r_orig_cv = orig.mse_cv;
MORIG = NaN(size(orig.model{1},1),length(orig.r_test));
for n = 1:length(orig.r_test)
    MORIG(:,n) = orig.model{n}(:,1);
end
mdl_orig = mean(MORIG,2);

% load the reconstruction accuracies for each spline model
r_spl = NaN(size(r_orig,1),length(npcs));
r_cv = cell(size(r_orig,1),length(npcs));
mdl = NaN(nparams,length(mdlfs));
% cf = NaN(nchan,nchan,length(mdlfs));
for ii = 1:length(npcs)
    if npcs(ii)==128
        splfl = sprintf('%s_speech_%dHz_reg',sbj,mdlfs);
    else
        splfl = sprintf('%s_speech_%dHz_%dpcs_reg',sbj,mdlfs,npcs(ii));
    end
%     spl = load([respth wndpth splfl]);
    spl = load([respth splwndpth splfl]);
    r_spl(:,ii) = spl.r_test;
%     r_spl(:,ii) = spl.mse_test;
    M = NaN(nparams,length(spl.r_test));
    for n = 1:length(spl.r_test)
        red_mdl = spl.model{n}(:,1);
        M_rshp = reshape(red_mdl,[length(idx_dly) npcs(ii)]);
%         rM = M_rshp.*(ones(length(idx_dly),1)*spl.var_pc(:,n)');
%         cM = rM*d.cf{n}';
        cM = M_rshp*spl.cf{n}(:,1:npcs(ii))';
        M(:,n) = reshape(cM,[length(idx_dly)*nchan 1]);
    end
    mdl(:,ii) = mean(M,2);
    r_cv(:,ii) = spl.r_cv;
%     r_cv(:,ii) = spl.mse_cv;
end

% plotting
% diffr = (r_spl-r_orig*ones(1,length(mdlfs)))./(r_orig*ones(1,length(mdlfs)));
diffr = r_spl-r_orig*ones(1,length(npcs));
figure
plot(1:length(npcs),diffr,'r','LineWidth',1.5);
set(gca,'FontSize',16,'XTick',1:length(npcs),'XTickLabel',npcs);
xlabel('# of principal components');
ylabel('Percent change in reconstruction accuracy using PCA/spline');
title(sbj);
% 
% p_sr = NaN(length(npcs),1);
% st_sr = cell(length(npcs),1);
% for ii = 1:length(mdlfs)
%     [p_sr(ii),~,st_sr{ii}] = signrank(diffr(:,ii));
% end

% Plot each model
figure
for ii = 1:length(npcs)
    subplot(3,2,ii);
    MDL = reshape(mdl(:,ii),[nparams/nchan nchan]);
%     imagesc(idx_dly/Fs*1000,1:128,(MDL*cf(:,:,ii)')');
    imagesc(idx_dly/Fs*1000,1:128,MDL');
    colorbar
    tle = sprintf('%d pcs',npcs(ii));
    xlabel('Delay (ms)');
    ylabel('Channel');
    title(tle);
end

figure
origFs = 128;
% orig_dly = 0:wnd/1000*origFs;
orig_dly = -(-wnd/1000*origFs:0);
MDLORIG = reshape(mdl_orig,[nchan length(orig_dly)]);
imagesc(orig_dly/origFs*1000,1:nchan,MDLORIG);
colorbar
title('Regularized, original model');
xlabel('Delay (ms)');
ylabel('Channel');

% Plot reconstruction accuracy for each CV iteration
figure
set(gcf,'Position',[380,220,450,350])
hold on
pl_cv = NaN(length(npcs)+1,1);
cv_leg = cell(length(npcs)+1,1);
RORIG = cell2mat(r_orig_cv); % concatenate all CV rs for original
m_cv_orig = median(RORIG);
uq_cv_orig = quantile(RORIG,0.75);
lq_cv_orig = quantile(RORIG,0.25);
pl_cv(1) = errorbar(lambdas,m_cv_orig,uq_cv_orig-m_cv_orig,m_cv_orig-lq_cv_orig,...
    'LineWidth',2,'Color','k');
cv_leg{1} = 'Regularized';
for ii = 1:length(npcs)
    RSPL = cell2mat(r_cv(:,ii));
    m_cv_spl = median(RSPL(:,2:end));
    uq_cv_spl = quantile(RSPL(:,2:end),0.75);
    lq_cv_spl = quantile(RSPL(:,2:end),0.25);
    pl_cv(ii+1) = errorbar(lambdas,m_cv_spl,uq_cv_spl-m_cv_spl,m_cv_spl-lq_cv_spl,...
        'Color',npcs_colors{ii},'LineWidth',2);
    cv_leg{ii+1} = sprintf('%d pcs',npcs(ii));
end
set(gca,'FontSize',14,'XScale','log');
xlabel('\lambda');
ylabel('CV r');
legend(pl_cv,cv_leg);