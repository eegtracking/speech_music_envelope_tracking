% Plot the difference in PCA/spline reconstruction accuracy vs
% regularization reconstruction accuracy as a function of the model Fs

wnd = 500; % TRF window size (in ms)
mdlfs = 16; % model sampling frequencies (in Hz)
npcs = [8 16 32 64 128];
sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK',...
    'MB','MT','SL','SOS'};
Fs = 128;
nchan = 128;
max_trs = 7; % maximum number of trials

idx_dly = 0:wnd/1000*Fs;
nparams = length(idx_dly)*nchan;

% respth = '../reg_effects_indivpca/shorter_tlims/';
respth = '../reg_effects_indivpca/';
wndpth = sprintf('%dms_justsplines/',wnd);
% wndpth = sprintf('%dms_skipsil/',wnd);

% load the reconstruction accuracies for the regularization-based model
% origpth = '../orig_rcnstr_res/no_tlims_testing/';
origpth = '../orig_rcnstr_res/';
origwnd = sprintf('%dms/',wnd);

% load the reconstruction accuracies for each spline model
r_spl = NaN(max_trs,length(mdlfs),length(sbjs));
r_orig = NaN(max_trs,length(sbjs));
for s = 1:length(sbjs)
    % get the reconstruction accuracy for regularized model
    origfl = sprintf('%s_speech_db',sbjs{s});
    orig = load([origpth origwnd origfl]);
    ntr = length(orig.r_test);
    r_orig(1:ntr,s) = orig.r_test;
%     r_orig(1:ntr,s) = orig.mse_test;
    % reconstruction accuracy for PCA spline
    for ii = 1:length(npcs)
        if npcs(ii)==128,
            splfl = sprintf('%s_speech_%dHz_reg',sbjs{s},mdlfs);
        else
            splfl = sprintf('%s_speech_%dHz_%dpcs_reg',sbjs{s},mdlfs,npcs(ii));
        end
        spl = load([respth wndpth splfl]);
%         r_spl(1:ntr,ii,s) = spl.r_test;
%         r_spl(1:ntr,ii,s) = spl.mse_test;
        r_spl(1:ntr,ii,s) = spl.r_test;
    end
    disp(sbjs{s});
end

% Compute the percent change in reconstruction accuracy
RSPL = reshape(permute(r_spl,[1 3 2]),[max_trs*length(sbjs) length(npcs)]);
RORIG = reshape(r_orig,[max_trs*length(sbjs) 1])*ones(1,length(npcs));
% DIFFR = (RSPL-RORIG)./RORIG;
DIFFR = RSPL-RORIG;

% plotting
figure
set(gcf,'Position',[380,220,450,350])
hold on
% plot(1:length(mdlfs),DIFFR*100,'k','LineWidth',1);
% plot(1:length(mdlfs),median(DIFFR*100,'omitnan'),'r','LineWidth',4);
plot(1:length(npcs),DIFFR,'k','LineWidth',1);
mdiff = median(DIFFR,'omitnan');
uqdiff = quantile(DIFFR,0.75);
lqdiff = quantile(DIFFR,0.25);
errorbar(1:length(npcs),mdiff,uqdiff-mdiff,mdiff-lqdiff,'r','LineWidth',4);
set(gca,'FontSize',16,'XTick',1:length(npcs),'XTickLabel',npcs);
xlabel('# principal components');
% ylabel('Percent change in reconstruction accuracy using PCA/spline');
ylabel('Difference in reconstruction accuracy');

p_sr = NaN(length(npcs),1);
st_sr = cell(length(npcs),1);
for ii = 1:length(npcs)
    [p_sr(ii),~,st_sr{ii}] = signrank(DIFFR(:,ii));
%     [~,p_sr(ii),~,st_sr{ii}] = ttest(DIFFR(:,ii));
end

figure
% subplot(2,1,1)
plot(1:length(npcs),RORIG,'k');
set(gca,'FontSize',16,'XTick',1:length(npcs),'XTickLabel',npcs);
xlabel('# principal components');
ylabel('Original reconstruction accuracy');
% subplot(2,1,2)
figure
set(gcf,'Position',[380,220,450,350])
hold on
plot(1:length(npcs),RSPL,'r');
mspl = median(RSPL,'omitnan');
uqspl = quantile(RSPL,0.75);
lqspl = quantile(RSPL,0.25);
errorbar(1:length(npcs),mspl,uqspl-mspl,mspl-lqspl,'LineWidth',4,'Color',[0.5 0 0])
set(gca,'FontSize',16,'XTick',1:length(npcs),'XTickLabel',npcs);
xlabel('Sampling frequency of splines (Hz)');
ylabel('PCA/spline reconstruction accuracy');

% [p_kw,~,st_kw] = kruskalwallis(RSPL);
adjRSPL = RSPL-RSPL(:,npcs==128)*ones(1,length(npcs));
[p_kw,~,st_kw] = kruskalwallis(adjRSPL);
cmp = multcompare(st_kw,'ctype','dunn-sidak','display','off');