% Plot the testing reconstruction accuracies for each stimulus type with 
% silences and low frequency information and after the silences have been 
% removed. In each stimulus type, draw a line between the same trial in
% each condition.
% Nate Zuk (2020)

addpath('..');

sbj = 'AB';
stimtypes = {'rock','classical','vocals','speech'};
wnd = 500; % window size of the TRF, in ms
ntr = 7;

orig_pth = '../orig_rcnstr_res/';
noavg_pth = sprintf('%dms_noavg/',wnd);
rmvlf_pth = sprintf('%dms/',wnd);

r_noavg = NaN(ntr,length(stimtypes));
r_rmvlf = NaN(ntr,length(stimtypes));

for ii = 1:length(stimtypes)
    flnm = sprintf('%s_%s_db',sbj,stimtypes{ii});
    d_rmvlf = load([orig_pth rmvlf_pth flnm]);
    maxtr = length(d_rmvlf.r_test);
    r_rmvlf(1:maxtr,ii) = d_rmvlf.r_test;
    d_noavg = load([orig_pth noavg_pth flnm]);
    r_noavg(1:maxtr,ii) = d_noavg.r_test;
end

% Plot the reconstruction accuracies
stimlbl = reshape(ones(ntr,1)*(1:length(stimtypes)),[ntr*length(stimtypes) 1]); % label each element in the r matrix
RNOAVG = reshape(r_noavg,[ntr*length(stimtypes) 1]);
RRMVLF = reshape(r_rmvlf,[ntr*length(stimtypes) 1]);
cell_midline = dot_connect_plot(stimlbl,[RNOAVG, RRMVLF]);
hold on
plot([0 length(stimtypes)+1],zeros(1,2),'k--');
set(gca,'XTickLabels',stimtypes);
ylabel('Reconstruction accuracy (Pearson''s r)');