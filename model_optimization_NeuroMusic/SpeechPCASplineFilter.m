% Demonstrate how the spectrum of some signal (random noise, for example)
% changes after the moving average is removed and spline interpolation
addpath('~/Projects/EEGanly/PCA_spline_modeling');
% dur = 180; % duration of the signal (s)
dur = 32; % duration of the signal (s);
eFs = 512;
desFs = 512;
s = randn(dur*eFs,1); % take a speech example
% s = [1; zeros(dur*eFs-1,1)];
nscale = 16; % # of splines in the TRF window
% nscale = 8; % NZ (11-1-2020), optimal is 8x 1/window_size

% Compute the power spectrum of each step
[P,frq_e] = pwelch(s,10*desFs,[],[],desFs);

% win_sizes = 16000*2.^(0:-1:-5);
% win_sizes = 32000*2.^(0:-1:-2);
win_sizes = 500;
fprintf('Window = %.2f ms\n',win_sizes);
% size of mTRF window
tmin = 0;
tmax = win_sizes;
% remove moving average
t = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
if desFs<eFs,
    s = resample(s,desFs,eFs);
end
smv = s - movmean(s,length(t));

% spline interpolation
mdlFs = 1000/(tmax-tmin)*nscale;
ds = desFs/mdlFs;
[sspl,spline_matrix] = spline_transform(smv',ds);
sflt = sspl*spline_matrix';

%%% Too smooth
spline_knots = augknt(linspace(t(1),t(end),floor(length(t)/ds)+1),4);
% make the matrix
spline_matrix = spcol(spline_knots,4,t);
% get the center spline
smoothing_spline = spline_matrix(:,round(size(spline_matrix,2)/2));
smoothing_spline = smoothing_spline/sum(smoothing_spline);
% apply smoothing
ssmooth = conv(smv,smoothing_spline,'same');

clear sspl smv

% [Pmv,frq_mv] = pwelch(smv,10*desFs,[],[],desFs);
[Pflt,~] = pwelch(sflt,10*desFs,[],[],desFs);

figure
set(gcf,'Position',[360 350 430 340]);
hold on
plot(frq_e,10*log10(P),'k');
mdl_clr = 'b';
plot(frq_e,10*log10(Pflt),'Color',mdl_clr,'LineWidth',2);
plot([1000/(tmax-tmin) 1000/(tmax-tmin)],[-100 0],'k--');
plot([mdlFs/2 mdlFs/2],[-100 0],'k--');
set(gca,'XScale','log','FontSize',16,'XLim',[1/16 100]);
xlabel('Frequency (Hz)');
ylabel('Power spectral density (dB)');
legend('Original','Removed movmean');

figure
hold on
plot(frq_e,10*log10(Pflt)-10*log10(P),'Color',mdl_clr,'LineWidth',2);
set(gca,'XScale','log','FontSize',16,'XLim',[1/16 100]);
xlabel('Frequency (Hz)');
ylabel('Power spectral density of filter');

fprintf('Geometric mean: %.3f\n',geomean([mdlFs/2 1000/(tmax-tmin)]));