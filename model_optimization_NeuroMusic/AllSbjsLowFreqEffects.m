% Plot the testing reconstruction accuracies for each stimulus type with 
% silences and low frequency information and after the silences have been 
% removed. In each stimulus type, draw a line between the same trial in
% each condition.
% Nate Zuk (2020)

addpath('..');

% Add in KR later
sbjs = {'AB','AF','CB','CQ','DH','EB','EJ','HC','JH','JS','KR','KRK',...
    'MB','MT','SL','SOS'};
stimtypes = {'rock','classical','vocals','speech'};
wnd = 500; % window size of the TRF, in ms
max_ntr = 7;

orig_pth = '/Volumes/ZStore/NeuroMusic/analyses/orig_rcnstr_res/';
noavg_pth = sprintf('%dms_noavg/',wnd);
rmvlf_pth = sprintf('%dms/',wnd);

r_noavg = NaN(max_ntr,length(stimtypes),length(sbjs));
r_rmvlf = NaN(max_ntr,length(stimtypes),length(sbjs));

for s = 1:length(sbjs)
    for ii = 1:length(stimtypes)
        flnm = sprintf('%s_%s_db',sbjs{s},stimtypes{ii});
        d_rmvlf = load([orig_pth rmvlf_pth flnm]);
        ntr = length(d_rmvlf.r_test); % get the number of trials run for this subject
        r_rmvlf(1:ntr,ii,s) = d_rmvlf.r_test;
        d_noavg = load([orig_pth noavg_pth flnm]);
        r_noavg(1:ntr,ii,s) = d_noavg.r_test;
        disp(flnm);
    end
end

% Plot the reconstruction accuracies (each subject as different color)
stimlbl = reshape(ones(ntr,1)*(1:length(stimtypes)),[ntr*length(stimtypes) 1]); % label each element in the r matrix
RNOAVG = reshape(r_noavg,[ntr*length(stimtypes) length(sbjs)]);
RRMVLF = reshape(r_rmvlf,[ntr*length(stimtypes) length(sbjs)]);
figure
cmap = colormap('jet');
for s = 1:length(sbjs)
    cidx = round((s-1)/length(sbjs)*(size(cmap,1)-1))+1;
    cell_midline = dot_connect_plot(stimlbl,[RNOAVG(:,s) RRMVLF(:,s)],...
        'dot_color',cmap(cidx,:),'new_fig',false,...
        'plot_symbol','.','dot_size',14);
end
hold on
plot([0 length(stimtypes)+1],zeros(1,2),'k--');
set(gca,'XTickLabels',stimtypes);
ylabel('Reconstruction accuracy (Pearson''s r)');

% Test if there is a significant difference in r across genres with and
% without low-frequency information
ALLNOAVG = reshape(RNOAVG,[numel(RNOAVG),1]);
ALLRMVLF = reshape(RRMVLF,[numel(RRMVLF),1]);
ALLSTIM = repmat(stimlbl,[length(sbjs) 1]);
[p_wlf,~,st_wlf] = kruskalwallis(ALLNOAVG,ALLSTIM);
cmp_wlf = multcompare(st_wlf,'ctype','dunn-sidak','display','off');
[p_nolf,~,st_nolf] = kruskalwallis(ALLRMVLF,ALLSTIM);
cmp_nolf = multcompare(st_nolf,'ctype','dunn-sidak','display','off');

% Do a pairwise comparison between with and without low freq for each
% stimulus type
p_lf_effect = NaN(length(stimtypes),1);
st_lf_effect = cell(length(stimtypes),1);
for ii = 1:length(stimtypes)
    [p_lf_effect(ii),~,st_lf_effect{ii}] = signrank(ALLRMVLF(ALLSTIM==ii),ALLNOAVG(ALLSTIM==ii));
end