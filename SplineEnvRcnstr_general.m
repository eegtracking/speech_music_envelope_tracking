function SplineEnvRcnstr_general(sbj,stimtype,window_size,varargin)
% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes
% This cannot be used for window_size = 125 ms (because it cant be spline
% transformed) or 16000 ms (because the moving average shouldn't be
% removed)
%%% NZ - PCA-transform the EEG beforehand, so that they are all oriented
%%% the same

addpath('~/Projects/EEGanly/PCA_spline_modeling/');
addpath('~/Projects/mTRF_Zukedits/');

tmin = 0;
tmax = window_size+tmin;
map = -1;
npcs = 16;
desFs = 128;
mdlFs = 1000/(tmax-tmin)*16; % 2x highest frequency of interest in the model
tlims = 2;
nfolds = 10;
voltlimdb = -55; % NZ (22-3-2019) changed to -55 dB, specified where silences occur
    % see also HistLevel_NeuroMusic.m
sil_tol = 0.001; % the sound is identified as "on" when the silence vector equals 1,
    % this is the tolerance around 1 at which to define the sound as "on"
do_mov_avg = true;

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Load the general PCA transformation
D = electrode_linear_distance_matrix('chanlocs.xyz','~/Projects/EEGanly/');
cf = pca(D);
cf = [ones(size(D,1),1)/sqrt(size(D,1)) cf];

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
% if strcmp(stimtype,'rock'), stimcolumn = 1;
% elseif strcmp(stimtype,'classical'), stimcolumn = 2;
% elseif strcmp(stimtype,'vocals'), stimcolumn = 3;
% elseif strcmp(stimtype,'speech'), stimcolumn = 4;
% else
%     error('Unknown stimulus tag');
% end
stimpth = '/Volumes/ZStore/NeuroMusic/';
stimfl = sprintf('NeuroMusicdBEnvelopes_128Hz_%s',stimtype);
s = load([stimpth stimfl]);
% stims = s.stimset(:,stimcolumn);
stims = s.stimset;
% Transpose all stimuli to column vectors
for ii = 1:length(stims), stims{ii} = stims{ii}'; end
% clear s

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

disp('Convert stimuli to dB');
% voltlim = 10^(voltlimdb/20);
silence = cell(length(stims),1); % to store silent periods
for ii = 1:length(stims),
    % store 0 anytime there's silence
    silence{ii} = ones(length(stims{ii}),1);
    silence{ii}(stims{ii}<voltlimdb) = 0;
    stims{ii}(stims{ii}<voltlimdb) = voltlimdb;
%     stims{ii}(stims{ii}<voltlim) = voltlim;
%     stims{ii} = 20*log10(stims{ii});
end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% if do_mov_avg, disp('...and removing silences...'); end
disp('...and removing silences...');
tidx = cell(length(silence),1);
for ii = 1:length(silence),
%     if do_mov_avg,
        tidx{ii} = zeros(length(silence{ii}),1);
        tidx{ii}(abs(silence{ii}-1)<sil_tol) = 1; % set to 1 when stimulus is on, and no ringing
%     else
%         tidx{ii} = ones(length(silence{ii}),1);
%     end
    l = usetinds(tlims,desFs,length(silence{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
et = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(et));
    eeg{ii} = eeg{ii}-avg;
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        s_on = stims{ii};
        s_on(~tidx{ii}) = mean(s_on(tidx{ii})); % set silent periods to the average of
            % the non-silent envelope, to avoid edge effects as speech
            % turns on and off
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii}(tidx{ii}) = s_on(tidx{ii});
%         savg = movmean(stims{ii},length(t));
%         stims{ii} = stims{ii}-savg;
    end
end
clear avg s_on

% Transform EEG into PCs
disp('Transform to PCs...');
pc = cell(length(eeg),1);
for ii = 1:length(eeg),
    pc{ii} = eeg{ii}*cf;
end
allPC = cell2mat(pc);
allEEG = cell2mat(eeg);
% sort the principal components by variance, and retain only the largest
% ones
var_pc = var(allPC);
% compute the % variance explained by the retained PCs
usepcs = 1:npcs+1;
transPC = allPC(:,usepcs)*cf(:,usepcs)';
SSE_red = sum(sum((transPC-allEEG).^2));
SST = sum(sum((allEEG).^2));
vexp_red = 1-SSE_red/SST;
fprintf('Variance explained by retained PCs: %.2f%%\n',vexp_red*100);
for ii = 1:length(pc),
    pc{ii} = pc{ii}(:,usepcs);
end
clear allPC allEEG eeg transPC

% Downsample to EEG and stimulus by 4x, to make it more managable for the
% computer
for ii = 1:length(pc),
    pc{ii} = resample(pc{ii},desFs,eFs);
    pc{ii} = zscore(pc{ii}); % zscore after resampling, because this can change the variance
%     stims{ii} = resample(stims{ii},desFs,eFs);
%     silence{ii} = resample(silence{ii},desFs,eFs); % get affects of ringing due to downsampling here
end

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,pc);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(pc{n},1)); end
    end
end

% Compute the downsampling rate, for the spline transform
ds = desFs/mdlFs;

disp('Computing the envelope reconstruction...');
if mdlFs<desFs,
    disp('(Using spline interpolation...)');
    [r,~,mse,model] = mTRFcrossval_spline(stims,pc,desFs,ds,map,tmin,tmax,0,tidx,...
        'nfolds',nfolds);
elseif mdlFs==desFs,
    [r,~,mse,model] = mTRFcrossval(stims,pc,desFs,map,tmin,tmax,0,tidx,...
        'nfolds',nfolds);
else
    error('Model sampling frequency must be equal to or less than initial sampling frequency');
end

disp('Compute transformation to forward model...');
if mdlFs<desFs,
    disp('(Using spline interpolation...)');
end
model_t = mTRFtransform_spline(stims,pc,model,desFs,ds,map,tmin,tmax,tlims);

% disp('Compute a null distribution of reconstruction accuracies...');
if npcs==16,
    niter = 50;
    if mdlFs<desFs,
        [nullr,nullmse] = mTRFnull(stims,pc,desFs,ds,map,tmin,tmax,0,tidx,'niter',niter,...
            'randomize_idx',true,'size_for_testing',nfolds);
    else
        [nullr,nullmse] = mTRFnull_standard(stims,pc,desFs,map,tmin,tmax,0,tidx,'niter',niter,...
            'randomize_idx',true,'size_for_testing',nfolds);
    end
else
    nullr = []; nullmse = [];
end

if do_mov_avg, 
    svpth = sprintf('~/Projects/NeuroMusic/rcnstr_res/%dms/',round(tmax-tmin));
else
    svpth = sprintf('~/Projects/NeuroMusic/rcnstr_res/%dms_noavg/',round(tmax-tmin));
end
if npcs~=16,
    svfl = sprintf('%s_%s_db_%dpcs',sbj,stimtype,npcs);
else
    svfl = sprintf('%s_%s_db',sbj,stimtype);
end
save([svpth svfl],'r','mse','model','mdlFs','desFs','ds','tmin','tmax','tlims',...
    'nullr','nullmse','nfolds','voltlimdb','tidx','usepcs','var_pc','model_t');