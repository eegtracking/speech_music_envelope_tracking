#!/bin/bash
#SBATCH --time=2-00:00:00
#SBATCH -c 16
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --mem=256GB

sbj=$1
stim=$2
wnd=$3
tr=$4

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(16); sbj='${sbj}'; stimtype='${stim}'; wnd=${wnd}; OrigEnvRcnstr_highfs(sbj,stimtype,wnd,${tr});"
rm -rf /scratch/$USER/$SLURM_JOB_ID
