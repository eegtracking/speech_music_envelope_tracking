#!/bin/bash
#SBATCH --time=02:00:00
#SBATCH -c 4
#SBATCH -n 1
#SBATCH --mem=16GB

sbj=$1
stim=$2
wnd=(31.25 62.5 125 250 500 1000 2000 4000 8000 16000)

module add matlab
mkdir -p /scratch/$USER/$SLURM_JOB_ID
matlab -nodesktop -nosplash -r "maxNumCompThreads(4); sbj='${sbj}'; stimtype='${stim}'; wnd=${wnd[$SLURM_ARRAY_TASK_ID]}; SpatialEnvRcnstr_trialbytrial(sbj,stimtype,wnd,0);"
rm -rf /scratch/$USER/$SLURM_JOB_ID
