% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes

addpath('~/Projects/EEGanly/PCA_spline_modeling/');
addpath('~/Documents/MATLAB/ellipsoid_fit/');
addpath('~/Projects/mTRF_Zukedits/');

sbj = 'HC';
stimtype = 'classical';
tmin = -200;
tmax = 300;
map = -1;
npcs = 16;
tlims = [1 Inf];
nfolds = 10;
voltlimdb = -80;
sil_tol = 0.001; % the sound is identified as "on" when the silence vector equals 1,
    % this is the tolerance around 1 at which to define the sound as "on"

% Load the general PCA transformation
D = electrode_distance_matrix('chanlocs.xyz','~/Projects/EEGanly/');
cf = pca(D);
cf = [ones(size(D,1),1) cf];

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
if strcmp(stimtype,'rock'), stimcolumn = 1;
elseif strcmp(stimtype,'classical'), stimcolumn = 2;
elseif strcmp(stimtype,'vocals'), stimcolumn = 3;
elseif strcmp(stimtype,'speech'), stimcolumn = 4;
else
    error('Unknown stimulus tag');
end
stimpth = '/Volumes/Untitled/NeuroMusic/';
stimfl = 'NeuroMusicEnvelopes_raw';
load([stimpth stimfl]);
stims = stimset(:,stimcolumn);

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

disp('Convert stimuli to dB');
voltlim = 10^(voltlimdb/20);
silence = cell(length(stims),1); % to store silent periods
for ii = 1:length(stims),
    % store 0 anytime there's silence
    silence{ii} = ones(length(stims{ii}),1);
    silence{ii}(stims{ii}<voltlim) = 0;
    stims{ii}(stims{ii}<voltlim) = voltlim;
    stims{ii} = 20*log10(stims{ii});
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
t = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(t));
    eeg{ii} = eeg{ii}-avg;
    savg = movmean(stims{ii},length(t));
    stims{ii} = stims{ii}-savg;
end
clear avg savg

% Transform EEG into PCs
disp('Transform to PCs...');
pc = cell(length(eeg),1);
for ii = 1:length(eeg),
    pc{ii} = eeg{ii}*cf(:,1:npcs+1);
end
clear eeg

% Downsample to EEG and stimulus by 4x, to make it more managable for the
% computer
desFs = 128;
for ii = 1:length(pc),
    pc{ii} = resample(pc{ii},desFs,eFs);
    stims{ii} = resample(stims{ii},desFs,eFs);
    silence{ii} = resample(silence{ii},desFs,eFs); % get affects of ringing due to downsampling here
end
dest = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
tidx = cell(length(silence),1);
for ii = 1:length(silence),
    tidx{ii} = zeros(length(silence{ii}),1);
    tidx{ii}(abs(silence{ii}-1)<sil_tol) = 1; % set to 1 when stimulus is on, and no ringing
    l = usetinds(tlims,desFs,length(silence{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

mdlFs = 32; % 2x highest frequency of interest in the model
ds = desFs/mdlFs;

disp('Computing the envelope reconstruction...');
[r,~,mse,model] = mTRFcrossval_square(stims,pc,desFs,ds,map,tmin,tmax,0,tidx,...
    'nfolds',nfolds);

disp('Compute a null distribution of reconstruction accuracies...');
niter = 50;
[nullr,nullmse] = mTRFnull_square(stims,pc,desFs,ds,map,tmin,tmax,0,tidx,'niter',niter,...
    'randomize_idx',true,'size_for_testing',nfolds);

svpth = '~/Projects/NeuroMusic/rcnstr_res/';
svfl = sprintf('%s_%s_square',sbj,stimtype);
save([svpth svfl],'r','mse','model','desFs','ds','tmin','tmax','npcs','tlims',...
    'nullr','nullmse','nfolds','voltlimdb','tidx');