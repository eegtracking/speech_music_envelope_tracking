function tinds = usetinds(tlims,Fs,maxind)
% tinds = usetinds(tlims,Fs,maxind)
% Identify the indexes that should be used based on the time limits
% provided, the sampling rate, and the stimulus duration. It is assumed
% that the starting index is t=0. The output is a logical array with 1 for
% each index to be used.
% Nate Zuk (2017)

t = (0:maxind-1)/Fs;
if ~isempty(tlims),
    if length(tlims)==1,
        % set limits relative to start and end of stimulus
        tinds = t>=tlims&t<=(maxind/Fs-tlims);
    elseif length(tlims)==2,
        % use the times in usets as limits
        tinds = t>=tlims(1)&t<=tlims(2);
    elseif length(tlims)>2,
        tinds = false(1,maxind);
        tidx = round(tlims*Fs);
        tidx(tidx>maxind) = []; % remove indexes that are greater than maxind
        tinds(tidx) = true;
    end
else
    tinds = true(1,maxind);
end