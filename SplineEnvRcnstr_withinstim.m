function SplineEnvRcnstr_withinstim(sbj,stimtype,window_size,varargin)
% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes
% This cannot be used for window_size = 125 ms (because it cant be spline
% transformed) or 16000 ms (because the moving average shouldn't be
% removed)
%%% NZ - PCA-transform each dataset individually, aka. for each particular
%%% window size and stimulus type.  This would be optimal.
%%% NZ (20-1-2020) - In contrast with _pcabystim.m, this function trains
%%% and tests on random samplings of the data within each trial, in order
%%% to get a within-trial reconstruction accuracy.  This negates effects of
%%% trial-by-trial variability in the envelopes, which could be
%%% overenflating the reconstruction accuracy of speech relative to the
%%% other stimuli.
%%% NZ (9-2-2020) -- I ran the PCA/spline optimization on the Natural
%%% Speech dataset, and found that the optimum r values for a 500 ms window
%%% for dB envelope reconstruction were at 32 Hz splines (a 3-octave
%%% window) and 64 principal components. I have chaged the parameters used
%%% here accordingly
%%% NZ (11-2-2020) -- I have kept the PCA transformation across all trials,
%%% PCA transforming for each trial separately and then fitting model could
%%% be problematic, whereas across trials forces the transformation to be
%%% generalized to all trials of that stimulus type

% addpath('~/mTRF_Zukedits/');

tmin = 0;
tmax = window_size+tmin;
map = -1;
desFs = 512;
mdlFs = 1000/(tmax-tmin)*16; % 2x highest frequency of interest in the model
    % Optimal model for speech for 500 ms window uses a two octave
    % bandwidth, but it is very slight, and there's not significant
    % difference across bandwidths (NZ, 11-1-2020)
    %%% NZ (11-2-2020), changed to 3 octave bandwidth, based on
    %%% optimization to Natural Speech data with 500 ms window
npcs = 64;
tlims = 16;
nfolds = 10;
nreps = 5; % number of times to repeat mTRFcrossval (use a new resampling of the data)
do_mov_avg = true;

% Parse varargin
if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/scratch/nzuk/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
stimpth = '~/NeuroMusic/stims/';
stimfl = sprintf('NeuroMusicdBEnvelopes_%dHz_%s',desFs,stimtype);
    %%% I will need to change this to be 512 Hz envelopes, no downsampling
s = load([stimpth stimfl]);
stims = s.stimset;
% Transpose all stimuli to column vectors
for ii = 1:length(stims), stims{ii} = stims{ii}'; end
% clear s

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

% Remove linear trends in the EEG, and downsample to desFs
disp('Removing linear trends in eeg...');
for ii = 1:length(eeg)
    eeg{ii} = detrend(eeg{ii});
    if eFs~=desFs
        eeg{ii} = resample(eeg{ii},desFs,eFs);
    end
end

% Index the portions when the stimulus is not silent (specifically = 1 to
% avoid ringing) and account for tlims as well
disp('Identifying time indexes to include in the modeling...');
% disp('...and removing silences...');
tidx = cell(length(stims),1);
for ii = 1:length(stims),
    %%% NZ (19-1-2020), don't skip silences, could have had strange effects
    %%% on null distribution, overenflated tracking at low frequencies
    tidx{ii} = ones(length(stims{ii}),1);
    l = usetinds(tlims,desFs,length(stims{ii})); % apply tlims
    tidx{ii}(~l) = 0;
    tidx{ii} = logical(tidx{ii}); % turn into logical array
end

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
if do_mov_avg, 
    disp('...also removing the moving average of the envelope...');
end
st = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(st));
    eeg{ii} = eeg{ii}-avg;
    if do_mov_avg, % NZ (22-3-2019) do moving averaging only on non-silent periods
        % NZ (15-1-2020), don't remove silences when doing moving average
        s_on = stims{ii};
        s_on = s_on-mean(s_on); % shift to 0 mean (so it starts and ends near zero, 
            % avoid edge effects of moving average)
        s_on = s_on-movmean(s_on,length(st));
        stims{ii} = s_on;
    end
end
clear avg s_on

% Transform EEG into PCs
disp('Transform to PCs across all trials...');
allEEG = cell2mat(eeg);
cf = pca(allEEG); % get the principal components of the EEG
pc = cell(length(eeg),1);
var_pc = NaN(length(eeg),npcs); % to store the original PC variances,
    % for appopriately reweighting the PCs in the model
for ii = 1:length(eeg)
%     eeg{ii} = detrend(eeg{ii},0); % center each channel
    pc{ii} = eeg{ii}*cf(:,1:npcs); % transform to pcs
    var_pc(ii,:) = var(pc{ii});
    pc{ii} = zscore(pc{ii}); % normalize to variance of 1
end
clear allEEG eeg

% Check if the pcs on each trial are longer than the stimulus, otherwise
% truncate the stimulus appropriately
len_check = cellfun(@(x,y) size(x,1)>size(y,1),stims,pc);
if sum(len_check)~=0,
    warning('Some spliced EEG recordings are shorter than the stimulus duration');
    for n = 1:length(len_check),
        if len_check(n), stims{n} = stims{n}(1:size(pc{n},1)); end
    end
end

% Compute the downsampling rate, for the spline transform
ds = desFs/mdlFs;

disp('Computing the envelope reconstruction from random samplings of data across trials...');
ntr = length(stims); % get the number of trials
niter = nfolds*nreps;
r = NaN(nfolds,nreps,ntr);
mse = NaN(nfolds,nreps,ntr);
model = cell(nreps,ntr);
% model_t = cell(nreps,ntr);
nullr = NaN(niter,ntr);
nullmse = NaN(niter,ntr);
for n = 1:ntr
    fprintf('** Trial %d/%d **\n',n,ntr);
    trtm = tic;
    for m = 1:nreps
        fprintf('* Rep %d/%d *\n',m,nreps);
        disp('(Using spline interpolation...)');
        [r(:,m,n),~,mse(:,m,n),model{m,n}] = mTRFcrossval_spline(stims(n),pc(n),desFs,ds,map,tmin,tmax,0,tidx(n),...
                'nfolds',nfolds);

%         disp('Compute transformation to forward model...');
%         model_t{m,n} = mTRFtransform_spline(stims(n),pc(n),model{m,n},desFs,ds,map,tmin,tmax,tidx(n));
    end
    % Computing null
    [nullr(:,n),nullmse(:,n)] = mTRFnull(stims(n),pc(n),desFs,ds,map,tmin,tmax,0,tidx(n),'niter',niter,...
        'randomize_idx',true,'size_for_testing',nfolds);
    fprintf('** Completed trial %d/%d @ %.3f s\n',n,ntr,toc(trtm));
end

% disp('Compute a null distribution of reconstruction accuracies...');
svpth = '~/NeuroMusic/rcnstr_sep/';
svfl = sprintf('%s_%s_%dms',sbj,stimtype,round(tmax-tmin));
% removed model saving because of the size of the resulting file
% NZ (20-1-2020)
save([svpth svfl],'r','mse','mdlFs','desFs','ds','tmin','tmax','tlims',...
    'nullr','nullmse','tidx','var_pc','cf','npcs');
