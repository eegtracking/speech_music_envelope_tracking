% Load neuromusic data and use PCA spline-transform linear regression to
% reconstruct the envelopes

addpath('~/Projects/EEGanly/PCA_spline_modeling/');
addpath('~/Documents/MATLAB/ellipsoid_fit/');
addpath('~/Projects/mTRF_Zukedits/');
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));

sbj = 'JS';
stimtype = 'classical';
tmin = 0;
tmax = 500;
map = 1;
tlims = 2;
voltlimdb = -80;
do_mov_avg = true;

mdlFs = 1000/(tmax-tmin)*16; % 2x highest frequency of interest in the model

% Get the stimulus codes
stimcodes = getstimcodes(stimtype);

% Load the eeg for those stimulus codes
disp('Loading eeg...');
eegpth = '/Volumes/Untitled/NeuroMusic/raw_spliced_eeg/';
[eeg,eFs] = loadneuromusiceeg(eegpth,stimcodes,sbj);

% Load the stimuli
disp('Loading stimuli...');
if strcmp(stimtype,'rock'), stimcolumn = 1;
elseif strcmp(stimtype,'classical'), stimcolumn = 2;
elseif strcmp(stimtype,'vocals'), stimcolumn = 3;
elseif strcmp(stimtype,'speech'), stimcolumn = 4;
else
    error('Unknown stimulus tag');
end
stimpth = '/Volumes/Untitled/NeuroMusic/';
stimfl = 'NeuroMusicEnvelopes_raw';
load([stimpth stimfl]);
stims = stimset(:,stimcolumn);

% Identify stimuli that were run
disp('Removing stimuli that were not run...');
useidx = cellfun(@(x) ~isempty(x),eeg);
stims = stims(useidx);
eeg = eeg(useidx);

% Identify the number of delays in the trf, and do a moving average of the
% EEG with a window size corresponding to those delays
disp('Remove a moving average of the EEG equal to the size of the TRF...');
t = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
for ii = 1:length(eeg),
    avg = movmean(eeg{ii},length(t));
    eeg{ii} = eeg{ii}-avg;
    if do_mov_avg,
        savg = movmean(stims{ii},length(t));
        stims{ii} = stims{ii}-savg;
    end
end
clear avg savg

% Downsample to EEG and stimulus by 4x, to make it more managable for the
% computer
desFs = 128;
for ii = 1:length(eeg),
    eeg{ii} = resample(eeg{ii},desFs,eFs);
    stims{ii} = resample(stims{ii},desFs,eFs);
%     eeg{ii} = zscore(eeg{ii});
end
dest = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);

ds = desFs/mdlFs;

disp('Computing the envelope reconstruction...');
[r,~,mse,model] = mTRFcrossval_spline(stims,eeg,desFs,ds,map,tmin,tmax,0,tlims);
model = squeeze(model);
% scale the model weights based on reconstruction accuracy
R = squeeze(mean(r,1));
scaleR = (R-min(R))/(max(R)-min(R)); % scale from 0 to 1
orig_model = model.*(ones(size(model,1),1)*scaleR');

figure
topoplot(scaleR,'~/Projects/EEGanly/chanlocs.xyz');
title('Prediction accuracy, r');
colorbar

figure
plot(dest/desFs*1000,model*scaleR,'k','LineWidth',2);
set(gca,'FontSize',16);
xlabel('Delay (ms)');
ylabel('Weighted average TRF');

% Plot the model
figure
imagesc(dest/desFs*1000,1:128,orig_model');
set(gca,'FontSize',16);
xlabel('Delay (ms)');
ylabel('Channel');
colorbar

% Plot the topographies of the weights between 50 to 100 ms and 100 to 200
% ms
topo_ranges = (tmax-tmin)*[1/20 1/10 1/5 1/2];
figure
subplot(1,3,1);
tidx = dest/desFs*1000>topo_ranges(1) & dest/desFs*1000<topo_ranges(2);
topoplot(mean(orig_model(tidx,:),1),'~/Projects/EEGanly/chanlocs.xyz');
title(sprintf('%d - %d ms',topo_ranges(1),topo_ranges(2)));
subplot(1,3,2);
tidx = dest/desFs*1000>topo_ranges(2) & dest/desFs*1000<topo_ranges(3);
topoplot(mean(orig_model(tidx,:),1),'~/Projects/EEGanly/chanlocs.xyz');
title(sprintf('%d - %d ms',topo_ranges(2),topo_ranges(3)));
subplot(1,3,3);
tidx = dest/desFs*1000>topo_ranges(3) & dest/desFs*1000<topo_ranges(4);
topoplot(mean(orig_model(tidx,:),1),'~/Projects/EEGanly/chanlocs.xyz');
title(sprintf('%d - %d ms',topo_ranges(3),topo_ranges(4)));