% Plot the topography for a reconstruction model
addpath(genpath('~/Documents/MATLAB/eeglab13_6_5b/functions'));
addpath('~/Documents/MATLAB/ellipsoid_fit/');

sbj = 'HC';
stimtype = 'speech';
wnd = 2000;
pth_suffix = '';

% Load the general PCA transformation
D = electrode_distance_matrix('chanlocs.xyz','~/Projects/EEGanly/');
cf = pca(D);
cf = [ones(size(D,1),1)/sqrt(size(D,1)) cf];

pth = sprintf('rcnstr_res/%dms%s/',round(wnd),pth_suffix);
fl = sprintf('%s_%s_db',sbj,stimtype);
d = load([pth fl]);

dly = floor(d.tmin/1000*d.desFs):ceil(d.tmax/1000*d.desFs);
pc_model = reshape(d.model_t,[length(dly) length(d.usepcs)]);
w_model = flipud(pc_model).*(ones(length(dly),1)*sqrt(d.var_pc(d.usepcs)));
    % since it's a forward model, don't use 
model = w_model*cf(:,d.usepcs)';

figure
imagesc(dly/d.desFs*1000,1:128,model');
xlabel('Delay (ms)');
ylabel('Channel');
colorbar;

topo_ranges = (d.tmax-d.tmin)*[1/10 1/5 1/2 3/4];
c_range = [min(min(model)) max(max(model))];
figure
subplot(1,3,1);
tidx = dly/d.desFs*1000>topo_ranges(1) & dly/d.desFs*1000<topo_ranges(2);
topoplot(mean(model(tidx,:),1),'~/Projects/EEGanly/chanlocs.xyz');
caxis(c_range);
title(sprintf('%d - %d ms',round(topo_ranges(1)),round(topo_ranges(2))));
subplot(1,3,2);
tidx = dly/d.desFs*1000>topo_ranges(2) & dly/d.desFs*1000<topo_ranges(3);
topoplot(mean(model(tidx,:),1),'~/Projects/EEGanly/chanlocs.xyz');
caxis(c_range);
title(sprintf('%d - %d ms',round(topo_ranges(2)),round(topo_ranges(3))));
subplot(1,3,3);
tidx = dly/d.desFs*1000>topo_ranges(3) & dly/d.desFs*1000<topo_ranges(4);
topoplot(mean(model(tidx,:),1),'~/Projects/EEGanly/chanlocs.xyz');
caxis(c_range);
title(sprintf('%d - %d ms',round(topo_ranges(3)),round(topo_ranges(4))));

% Plot the average TRF across channels
figure
plot(dly/d.desFs*1000,mean(model,2),'k','LineWidth',2);
set(gca,'FontSize',16);
xlabel('Delay (ms)');
ylabel('Average model weight');