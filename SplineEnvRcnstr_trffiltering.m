% Demonstrate how the spectrum of some signal (random noise, for example)
% changes after the moving average is removed and spline interpolation
addpath('~/Projects/EEGanly/PCA_spline_modeling');
dur = 100; % duration of the signal (s)
eFs = 512;
desFs = 128;
s = randn(dur*eFs,1); % take a speech example
% s = zeros(dur*eFs,1);
% s(1) = 1;

% size of mTRF window
tmin = 0;
tmax = 1000;
% remove moving average
t = floor(tmin/1000*desFs):ceil(tmax/1000*desFs);
s = resample(s,desFs,eFs);
smv = s - movmean(s,length(t));

% spline interpolation
mdlFs = 1000/(tmax-tmin)*16;
ds = desFs/mdlFs;
if ds>1,
    [sspl,spline_matrix] = spline_transform(smv',ds);
    sflt = sspl*spline_matrix';
else
    sflt = smv;
end
clear sspl

% Compute the power spectrum of each step
[P,frq_e] = pwelch(s,10*desFs,5*desFs,[],desFs);
[Pmv,frq_mv] = pwelch(smv,10*desFs,5*desFs,[],desFs);
[Pflt,frq_flt] = pwelch(sflt,10*desFs,5*desFs,[],desFs);

figure
hold on
plot(frq_e,P,'k');
plot(frq_mv,Pmv,'b');
plot(frq_flt,Pflt,'r','LineWidth',2);
plot(1000/(tmax-tmin)*ones(1,2),[10^-10 10^-2],'k--'); % highpass cutoff
plot(1000/(tmax-tmin)*ones(1,2)*8,[10^-10 10^-2],'k--'); % lowpass cutoff
set(gca,'XScale','log','YScale','log','FontSize',16);
xlabel('Frequency (Hz)');
ylabel('Power spectral density');
legend('Original','Removed movmean','Spline filtered');

fprintf('Geometric mean: %.3f\n',geomean([mdlFs/2 1000/(tmax-tmin)]));