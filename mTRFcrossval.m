function [r,p,mse,model,v] = mTRFcrossval(stim,resp,fs,map,tmin,tmax,lambda,tlims,varargin)
%mTRFcrossval mTRF Toolbox cross-validation function.
%   [R,P,MSE] = MTRFCROSSVAL(STIM,RESP,FS,MAP,TMIN,TMAX,LAMBDA) performs
%   leave-one-out cross-validation on the set of stimuli STIM and the
%   neural responses RESP for the range of ridge parameter values LAMBDA.
%   As a measure of performance, it returns the correlation coefficients R
%   between the predicted and original signals, the corresponding p-values
%   P and the mean squared errors MSE. Pass in MAP==1 to map in the forward
%   direction or MAP==-1 to map backwards. The sampling frequency FS should
%   be defined in Hertz and the time lags should be set in milliseconds
%   between TMIN and TMAX.
%
%   [...,MODEL] = MTRFCROSSVAL(...) also returns the linear mapping functions MODEL.
%
%   NZ edits (25-1-2019):
%     -- removed "pred" variable in output since it should be recomputed on 
%        new data anyway
%     -- removed quadratic regularization (also called "Tikhonov regularization"),
%        so it just runs ridge regression. This is to ensure that the same type
%        of regularization is used, not matter how the data is sized
%     -- both the design matrix X and the output matrix y are z-scored. This
%        function should not be used if either X or y are a set of indicator
%        functions (aka. 1 for event, 0 otherwise)
%     -- instead of trial-by-trial cross-validation, 10-fold cross validation
%        is used, where each fold contains a random sampling of the data. This
%        produces more consistent tuning curves across folds, and the optimal
%        lambda computed in this way is likely more reliable
%
%   Inputs:
%   stim   - set of stimuli [cell{1,trials}(time by features)]
%   resp   - set of neural responses [cell{1,trials}(time by channels)]
%   fs     - sampling frequency (Hz)
%   map    - mapping direction (forward==1, backward==-1)
%   tmin   - minimum time lag (ms)
%   tmax   - maximum time lag (ms)
%   lambda - ridge parameter values
%   
%   Optional inputs:
%   'nfolds' - # of folds used for cross-validation
%
%   Outputs:
%   r      - correlation coefficients
%   p      - p-values of the correlations
%   mse    - mean squared errors
%   pred   - prediction [MAP==1: cell{1,trials}(lambdas by time by chans),
%            MAP==-1: cell{1,trials}(lambdas by time by feats)] (**Removed,
%            NZ)
%   model  - linear mapping function (MAP==1: trials by lambdas by feats by
%            lags by chans, MAP==-1: trials by lambdas by chans by lags by
%            feats)
%
%   See README for examples of use.
%
%   See also LAGGEN MTRFTRAIN MTRFPREDICT MTRFMULTICROSSVAL.

%   References:
%      [1] Crosse MC, Di Liberto GM, Bednar A, Lalor EC (2015) The
%          multivariate temporal response function (mTRF) toolbox: a MATLAB
%          toolbox for relating neural signals to continuous stimuli. Front
%          Hum Neurosci 10:604.

%   Author: Michael Crosse
%   Lalor Lab, Trinity College Dublin, IRELAND
%   Email: edmundlalor@gmail.com
%   Website: http://lalorlab.net/
%   April 2014; Last revision: 31 May 2016
%   Edits (January 2019): Nate Zuk

% Initial parameters
nfolds = 10;
randomize_idx = true;

if nargin<8, tlims = []; end

if ~isempty(varargin),
    for n = 2:2:length(varargin),
        eval([varargin{n-1} '=varargin{n};']);
    end
end

% Define x and y
if tmin > tmax
    error('Value of TMIN must be < TMAX')
end
if map == 1
    x = stim;
    y = resp;
elseif map == -1
    x = resp;
    y = stim;
    [tmin,tmax] = deal(tmax,tmin);
else
    error('Value of MAP must be 1 (forward) or -1 (backward)')
end
clear stim resp

% Convert x and y to cells if they aren't cells already
if ~iscell(x), x = {x}; end
if ~iscell(y), y = {y}; end

% Convert time lags to samples
tmin = floor(tmin/1e3*fs*map);
tmax = ceil(tmax/1e3*fs*map);

% Set up regularisation
dim1 = size(x{1},2)*length(tmin:tmax);
dim2 = size(y{1},2);
M = eye(dim1,dim1); % just does ridge regression

disp('Creating the design matrices and standardizing...');
% Create the design matrices
std_tm = tic;
for i = 1:numel(x)
    % Generate lag matrix
    x{i} = lagGen(x{i},tmin:tmax); %%% skip constant term (NZ)
    % Set X and y to the same length
    minlen = min([size(x{i},1) size(y{i},1)]);
    x{i} = x{i}(1:minlen,:);
    y{i} = y{i}(1:minlen,:);
    % Remove time indexes, if specified
    if iscell(tlims), % if tlims is a cell array, it means that specific indexes were supplied
        tinds = tlims{i};
    else
        tinds = usetinds(tlims,fs,minlen);
    end
    x{i} = zscore(x{i}(tinds,:)); % z-score the design matrix X and output matrix Y
    y{i} = zscore(y{i}(tinds,:));
end
fprintf('** Completed z-scoring in %.3f s\n',toc(std_tm));

if randomize_idx,
    % Randomly resample time points, in order to determine the time indexes for
    % each fold
    tottmidx = sum(cellfun(@(n) size(n,1),x));
    rndtmsmp = randperm(tottmidx);
    % Determine the starting index for each fold
    foldidx = (0:nfolds-1)*floor(tottmidx/nfolds);
        % the last fold has at most nfolds-1 samples more than the others

    disp('Starting model fitting...');
    % Make the variables that will store the error and correlation values for each lambda and each fold
    mse = NaN(nfolds,length(lambda),dim2); % mean squared error
    v = NaN(nfolds,length(lambda),dim2); % variance of the prediction
    r = NaN(nfolds,length(lambda),dim2); % Pearson's correlation
    p = NaN(nfolds,length(lambda),dim2); % significance of Pearson's correlation
    % For each fold...
    for n = 1:nfolds,
        mdlfitting_tm = tic;
        fprintf('Fold %d/%d\n',n,nfolds); % display which fold is being run
        % Evenly divide all time points into 'nfolds' randomly sampled segments, 
        % and we will iterate through each fold for cross-validation
        if n==nfolds,
            tstinds = rndtmsmp(foldidx(n)+1:end); % grab the rest of the samples if it's the last fold
        else
            tstinds = rndtmsmp(foldidx(n)+1:foldidx(n+1));
        end
        trninds = setxor(rndtmsmp,tstinds); % use the rest of the samples for training

        % Get the testing data
        xtst = cell_to_time_samples(x,tstinds);
        ytst = cell_to_time_samples(y,tstinds);
        [xtx,xty] = compute_linreg_matrices(x,y,trninds);

        % Calculate model for each lambda value
        fprintf('Cross-validating to each lambda (%d iterations)',length(lambda));
        for j = 1:length(lambda)
            model_fold = (xtx+lambda(j)*M)^(-1)*xty; % compute model
                % using ^(-1) runs a bit faster than \
            pred_fold = xtst*model_fold; % compute prediction
            for k = 1:dim2, % for each output in y
    %             mse(n,j,k) = mean((ytst(:,k)-pred_fold(:,k)).^2);
                mse(n,j,k) = var((ytst(:,k)-pred_fold(:,k))); % use variance of the errors instead
                v(n,j,k) = var(pred_fold(:,k));
                [r(n,j,k),p(n,j,k)] = corr(ytst(:,k),pred_fold(:,k));
            end
            fprintf('.');
        end 
        fprintf('\n');
        clear xtx xty xtst ytst
        fprintf('Completed %.3f s\n',toc(mdlfitting_tm));
    end
else % do trial-by-trial testing
    mse = NaN(length(x),length(lambda),dim2); % mean squared error
    v = NaN(length(x),length(lambda),dim2); % variance of the prediction
    r = NaN(length(x),length(lambda),dim2); % Pearson's correlation
    p = NaN(length(x),length(lambda),dim2); % significance of Pearson's correlation
    for n = 1:length(x),
        mdlfitting_tm = tic;
        fprintf('Trial %d/%d\n',n,length(x)); % display which fold is being run
        % Evenly divide all time points into 'nfolds' randomly sampled segments, 
        % and we will iterate through each fold for cross-validation
        trn_trs = setxor(n,1:length(x)); % use the rest of the samples for training

        % Get the testing data
        xtst = cell_to_time_samples(x(n));
        ytst = cell_to_time_samples(y(n));
        [xtx,xty] = compute_linreg_matrices(x(trn_trs),y(trn_trs));

        % Calculate model for each lambda value
        fprintf('Cross-validating to each lambda (%d iterations)',length(lambda));
        for j = 1:length(lambda)
            model_fold = (xtx+lambda(j)*M)^(-1)*xty; % compute model
                % using ^(-1) runs a bit faster than \
            pred_fold = xtst*model_fold; % compute prediction
            for k = 1:dim2, % for each output in y
                mse(n,j,k) = var((ytst(:,k)-pred_fold(:,k))); % use variance of the errors instead
                v(n,j,k) = var(pred_fold(:,k));
                [r(n,j,k),p(n,j,k)] = corr(ytst(:,k),pred_fold(:,k));
            end
            fprintf('.');
        end 
        fprintf('\n');
        clear xtx xty xtst ytst
        fprintf('Completed %.3f s\n',toc(mdlfitting_tm));
    end
end

if nargout>3, % if the model is one of the outputs
    % Compute the final models for all training data using each lambda parameter
    disp('Computing models on all training data...');
    [xtx,xty] = compute_linreg_matrices(x,y);
    model = NaN(size(x{1},2),length(lambda),dim2);
    for j = 1:length(lambda)
        model(:,j,:) = (xtx+lambda(j)*M)^(-1)*xty;
    end
end

end
