% Demonstrate how the spectrum of some signal (random noise, for example)
% changes after the applying a TRF
% addpath('~/Projects/EEGanly/PCA_spline_modeling');
addpath('~/Projects/mTRF-Toolbox/');
dur = 100; % duration of the signal (s)
eFs = 512;
s = randn(dur*eFs,1); % take a speech example

% size of mTRF window
% tmin = 0;
% tmax = 500;
% % remove moving average
% t_idx = floor(tmin/1000*eFs):ceil(tmax/1000*eFs);
% t = t_idx/eFs*1000;
% % simulate a TRF using a sinc function
% trf_freq = 10; % frequency of oscillation in the TRF (Hz)
% trf_ramp_constant = 50; % amount of time (ms) until the inverse exponential ramp reaches 63% max
% ramp = 1-exp(-t/trf_ramp_constant);
% trf = ramp.*sinc(t/1000*10);
% trf = zscore(trf); % normalize to RMS of 1

% Load TRF example
d = load('~/Documents/Matlab/NeuralOscSim/TRF_example');
trf = d.TRF;
tmin = -150;
tmax = 350;
t_idx = ceil(tmin/1000*eFs):floor(tmax/1000*eFs);
t = t_idx/eFs*1000;

% Use the TRF to filter the input s
X = lagGen(s,t_idx);
y = X*trf';
clear X;

% Plot the TRF
figure
plot(t,trf,'k','LineWidth',2);
xlabel('Time (ms)');
ylabel('TRF');

% Compute the power spectrum of each step
% [P,frq_e] = pwelch(s,10*eFs,5*eFs,[],eFs);
% [Py,frq_y] = pwelch(smv,10*eFs,5*eFs,[],eFs);
S = fft(s)/length(s);
Y = fft(y)/length(y);
frq = (0:length(S)-1)/length(S)*eFs;

figure
set(gcf,'Position',[360 5 550 700]);
subplot(2,1,1);
hold on
plot(frq,abs(S).^2,'k');
plot(frq,abs(Y).^2,'b','LineWidth',2);
set(gca,'FontSize',16,'XScale','log','YScale','log','XLim',[0.01 100]);
xlabel('Frequency (Hz)');
ylabel('Power (squared magnitude)');
legend('Original','TRF filtered');

subplot(2,1,2);
hold on
% plot(frq,angle(S),'k');
ph_diff = angle(Y)-angle(S);
% detects when a jump is >pi
% ph_change = diff(ph_diff);
% jumps = abs(ph_change)>pi;
% ph_diff([false; jumps]) = ph_diff([false; jumps])+2*pi*sign([0; ph_change(jumps)]); % adjust so that all 2*pi jumps don't occur
ph_diff(ph_diff<-pi) = ph_diff(ph_diff<-pi)+2*pi;
ph_diff(ph_diff>pi) = ph_diff(ph_diff>pi)-2*pi;
plot(frq,unwrap(ph_diff),'b','LineWidth',2);
set(gca,'FontSize',16,'XScale','log','XLim',[0.1 25]);
xlabel('Frequency (Hz)');
ylabel('Phase difference');